﻿CREATE TABLE [dbo].[WorkingWeekEnds]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [WeekEnd] DATETIME NOT NULL, 
    [CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    CONSTRAINT [FK_WorkingWeekEnds_Users] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id])
)
