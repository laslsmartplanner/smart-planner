﻿CREATE TABLE [dbo].[UserLevels]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [LevelName] NVARCHAR(50) NOT NULL, 
    [CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_UserLevels_Users_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_UserLevels_Users_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
