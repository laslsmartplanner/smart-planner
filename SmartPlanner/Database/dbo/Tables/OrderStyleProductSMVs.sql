﻿CREATE TABLE [dbo].[OrderStyleProductSMVs]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[OrderInquiryId] INT NOT NULL,
	[SplitKey] INT NOT NULL,
    [OrderId] INT NOT NULL, 
    [StyleId] INT NOT NULL, 
    [ProductId] INT NOT NULL, 
    [ProductionSMV] FLOAT NOT NULL
)
