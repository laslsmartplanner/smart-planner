﻿CREATE TABLE [dbo].[OrderCombinations]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [OrderDetailsId] INT NOT NULL, 
    [SizeName] NVARCHAR(50) NOT NULL, 
    [Qty] INT NOT NULL, 
	[CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL,
	[StyleProductId] INT NULL,
	[ColorName] NVARCHAR(50) NULL,
	[PlanDeliveryQty] DECIMAL(18, 2) NULL,
	[CutQty] DECIMAL(18, 2) NULL,
	[ShipQty] DECIMAL(18, 2) NULL,
    CONSTRAINT [FK_OrderCombinations_Splits] FOREIGN KEY ([OrderDetailsId]) REFERENCES [OrderDetails]([Id])
)
