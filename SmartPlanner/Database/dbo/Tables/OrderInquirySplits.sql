﻿CREATE TABLE [dbo].[OrderInquirySplits]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [OrderInquiryId] INT NOT NULL, 
    [SplitId] INT NOT NULL, 
    [Qty] INT NOT NULL, 
    [ShipmentDate] DATETIME NOT NULL, 
    [CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_OrderInquirySplits_OrderInquiry] FOREIGN KEY ([OrderInquiryId]) REFERENCES [OrderInquiries]([Id]),
	CONSTRAINT [FK_split_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_split_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
