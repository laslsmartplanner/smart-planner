﻿CREATE TABLE [dbo].[POTypeTnADurations]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PoTypeId] INT NOT NULL, 
    [TnADefaultId] INT NOT NULL, 
    [Duration] INT NOT NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    CONSTRAINT [FK_POTypeTnADurations_PoType] FOREIGN KEY ([PoTypeId]) REFERENCES [ConfigurationTableValues]([Id]), 
    CONSTRAINT [FK_POTypeTnADurations_TnA] FOREIGN KEY ([TnADefaultId]) REFERENCES [TnADefaults]([Id]), 
    CONSTRAINT [FK_POTypeTnADurations_Users] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]) 
)
