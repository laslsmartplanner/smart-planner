﻿CREATE TABLE [dbo].[CompanyDetails]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [ReportLogoName] NVARCHAR(50) NOT NULL DEFAULT '', 
    [OrderPrefix] NVARCHAR(50) NOT NULL DEFAULT '', 
    [Address] NVARCHAR(50) NOT NULL DEFAULT '', 
    [Phone] NVARCHAR(50) NULL DEFAULT '', 
    [StartTime] FLOAT NULL DEFAULT 8, 
    [TotalMinutes] FLOAT NULL DEFAULT 0, 
	[DefaultFactoryId] INT NULL DEFAULT 0,
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL
)
