﻿CREATE TABLE [dbo].[Operations]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [OperationName] NVARCHAR(50) NOT NULL DEFAULT '', 
    [SMV] DECIMAL(18, 2) NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL,
	CONSTRAINT [FK_Operations_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_Operations_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)