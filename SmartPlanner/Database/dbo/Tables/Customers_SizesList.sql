﻿CREATE TABLE [dbo].[Customers_SizesList]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CustomerId] INT NOT NULL, 
    [SizeName] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_Customers_SizesList_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [Customers]([Id])
)