﻿CREATE TABLE [dbo].[StyleProducts_AdditionalInformations]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StyleProductId] INT NOT NULL, 
    [FileName] NVARCHAR(50) NOT NULL,
	[FileTypeId] INT NOT NULL, 
	[CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL,
    CONSTRAINT [FK_StyleProducts_AdditionalInformations_StyleProducts] FOREIGN KEY ([StyleProductId]) REFERENCES [StyleProducts]([Id]),
	CONSTRAINT [FK_StyleProducts_AdditionalInformations_FileTypeId] FOREIGN KEY ([FileTypeId]) REFERENCES [AdditionalFileType]([Id]),
	CONSTRAINT [FK_StyleProducts_AdditionalInformations_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
	CONSTRAINT [FK_StyleProducts_AdditionalInformations_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
