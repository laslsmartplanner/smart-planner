﻿CREATE TABLE [dbo].[AdditionalFileType]
(
	[Id] INT Identity(1,1) NOT NULL PRIMARY KEY, 
    [TypeName] NVARCHAR(30) NOT NULL
)
