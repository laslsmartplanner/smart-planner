﻿CREATE TABLE [dbo].[Customers_ColorsList]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CustomerId] INT NOT NULL, 
    [ColorName] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_Customers_ColorsList_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [Customers]([Id])
)