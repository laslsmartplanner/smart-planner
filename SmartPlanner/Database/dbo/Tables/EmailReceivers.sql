﻿CREATE TABLE [dbo].[EmailReceivers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [EmailId] INT NOT NULL, 
    [ToOrCC] INT NOT NULL, 
    [ReceiverId] INT NOT NULL
)
