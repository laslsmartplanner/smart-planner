﻿CREATE TABLE [dbo].[SampleApprovalStatus]
(
	[Id] INT Identity(1,1) NOT NULL PRIMARY KEY, 
    [StatusName] NVARCHAR(20) NOT NULL
)
