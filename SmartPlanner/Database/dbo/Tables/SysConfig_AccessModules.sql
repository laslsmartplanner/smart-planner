﻿CREATE TABLE [dbo].[SysConfig_AccessModules]
(
	[Id] INT Identity(1,1) NOT NULL PRIMARY KEY, 
    [ModuleName] NVARCHAR(255) NOT NULL, 
    [ModuleNumber] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [AK_SysConfig_AccessModules_ModuleNumber] UNIQUE ([ModuleNumber])
)
