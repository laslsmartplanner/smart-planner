﻿CREATE TABLE [dbo].[Buyers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(255) NOT NULL DEFAULT '', 
    [Country] NVARCHAR(150) NOT NULL DEFAULT '', 
    [Address] NVARCHAR(500) NULL DEFAULT '', 
    [Mobile] NVARCHAR(50) NULL DEFAULT '', 
    [Email] NVARCHAR(50) NULL DEFAULT '',
	[PlannedColor] NVARCHAR(50) NULL,
	[ActualColor] NVARCHAR(50) NULL,
	[ChangedColor] NVARCHAR(50) NULL,
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_Buyers_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_Buyers_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
