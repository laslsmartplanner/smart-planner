﻿CREATE TABLE [dbo].[UserLevelAccessModules]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [UserLevelId] INT NOT NULL, 
    [AccessModuleId] INT NOT NULL, 
    [AccessTypeId] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_UserLevelAccessModules_UserLevel] FOREIGN KEY ([UserLevelId]) REFERENCES [UserLevels]([Id]), 
    CONSTRAINT [FK_UserLevelAccessModules_SysConfig_AccesModules] FOREIGN KEY ([AccessModuleId]) REFERENCES [SysConfig_AccessModules]([ModuleNumber])
)
