﻿CREATE TABLE [dbo].[Emails]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Subject] NVARCHAR(MAX) NOT NULL, 
    [Message] NVARCHAR(MAX) NOT NULL
)
