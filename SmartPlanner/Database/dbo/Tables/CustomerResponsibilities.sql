﻿CREATE TABLE [dbo].[CustomerResponsibilities]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CustomerId] INT NOT NULL, 
	[FactoryId]	INT NOT NULL,
    [CoordinatorId] INT NOT NULL, 
    [CommercialId] INT NOT NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_CustomerResponsibilities_Coordinator] FOREIGN KEY ([CoordinatorId]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_CustomerResponsibilities_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [Customers]([Id]),
	CONSTRAINT [FK_CustomerRes_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_CustomerRes_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_CustomerResponsibilities_Factories] FOREIGN KEY ([FactoryId]) REFERENCES [Factories]([Id])
)
