﻿CREATE TABLE [dbo].[SelectedStyleChangeOvers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[FactoryId] INT NOT NULL,
    [SelectedStyleProductId] INT NOT NULL, 
    [LineModuleId] INT NOT NULL, 
    [LastPlacedStyleProductId] INT NOT NULL, 
	[ChangeOver] FLOAT NOT NULL,
    [SelectedById] INT NOT NULL, 
    [SelectedOn] DATETIME NOT NULL, 
    CONSTRAINT [FK_SelectedStyleChangeOvers_Factories] FOREIGN KEY ([FactoryId]) REFERENCES [Factories]([Id]), 
    CONSTRAINT [FK_SelectedStyleChangeOvers_LineModules] FOREIGN KEY ([LineModuleId]) REFERENCES [LineModules]([Id]), 
    CONSTRAINT [FK_SelectedStyleChangeOvers_SelectedStyleProducts] FOREIGN KEY ([SelectedStyleProductId]) REFERENCES [StyleProducts]([Id]), 
    CONSTRAINT [FK_SelectedStyleChangeOvers_LastPlacedStyleProducts] FOREIGN KEY ([LastPlacedStyleProductId]) REFERENCES [StyleProducts]([Id])
)
