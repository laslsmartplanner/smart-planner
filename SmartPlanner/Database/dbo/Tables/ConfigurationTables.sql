﻿CREATE TABLE [dbo].[ConfigurationTables]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ConfigName] NVARCHAR(255) NOT NULL, 
    [ConfigId] INT NOT NULL, 
    [DisplayOrder] INT NOT NULL, 
    CONSTRAINT [AK_ConfigurationTables_ConfigId] UNIQUE ([ConfigId])
)
