﻿CREATE TABLE [dbo].[ConfigurationTableValues]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ConfigId] INT NOT NULL, 
    [ConfigValue] NVARCHAR(255) NOT NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_ConfigurationTableValues_ConfigurationTables] FOREIGN KEY ([ConfigId]) REFERENCES [ConfigurationTables]([ConfigId]), 
    CONSTRAINT [FK_ConfigurationTableValues_Users] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_ConfigurationTableValues_UsersUpdated] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
