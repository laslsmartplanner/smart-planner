﻿CREATE TABLE [dbo].[StyleRevisions]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StyleId] INT NOT NULL, 
    [RequestedById] INT NOT NULL, 
    [InquiryType] NVARCHAR(50) NOT NULL, 
    [RequestedDate] DATETIME NOT NULL, 
    [RevisedDate] DATETIME NULL, 
    [AcceptedById] INT NULL,
	[CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL
)
