﻿CREATE TABLE [dbo].[LineOutputs]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ProductivityId] INT NOT NULL, 
    [FromTime] FLOAT NOT NULL, 
    [ToTime] FLOAT NOT NULL, 
    [Qty] FLOAT NOT NULL
)
