﻿CREATE TABLE [dbo].[TnADefaults]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [KeyProcess] NVARCHAR(250) NOT NULL, 
    [Duration] INT NOT NULL DEFAULT 0, 
    [DisplayOrder] INT NOT NULL
)
