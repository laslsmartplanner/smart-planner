﻿CREATE TABLE [dbo].[LineWorkingHolidays]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [LineModuleId] INT NOT NULL, 
    [WorkingHoliday] DATETIME NOT NULL, 
    [CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_LineWorkingHolidays_LineModules] FOREIGN KEY ([LineModuleId]) REFERENCES [LineModules]([Id]),
	CONSTRAINT [FK_LineWorkingHolidays_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_LineWorkingHolidays_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
