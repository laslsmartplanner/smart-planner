﻿CREATE TABLE [dbo].[SubCategories]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CategoryId] INT NOT NULL, 
    [SubCategoryName] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_SubCategories_ConfigurationTableValues] FOREIGN KEY ([CategoryId]) REFERENCES [ConfigurationTableValues]([Id])
)
