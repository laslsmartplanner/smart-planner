﻿CREATE TABLE [dbo].[LineHolidays]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [LineModuleId] INT NOT NULL, 
    [Start] DATETIME NOT NULL, 
    [End] DATETIME NOT NULL, 
	[Title] NVARCHAR(255) NOT NULL,
    [HalfDay] BIT NOT NULL DEFAULT 0, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_LineHolidays_LineModules] FOREIGN KEY ([LineModuleId]) REFERENCES [LineModules]([Id]), 
    CONSTRAINT [FK_LineHolidays_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_LineHolidays_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
