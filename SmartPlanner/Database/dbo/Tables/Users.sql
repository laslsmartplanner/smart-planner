﻿CREATE TABLE [dbo].[Users] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [UserName] NVARCHAR (50)  NOT NULL,
    [Password] NVARCHAR (255) NOT NULL,
	[FullName] NVARCHAR (255) NOT NULL DEFAULT(''),
    [Email] NVARCHAR(50) NULL, 
    [Phone] NVARCHAR(50) NULL, 
    [UserLevelId] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [CreatedById] INT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    [LastLoginOn] DATETIME NULL, 
    [LastPasswordChangedOn] DATETIME NULL, 
    [IsTempPassword] BIT NOT NULL DEFAULT 1, 
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Users_UserLevels] FOREIGN KEY ([UserLevelId]) REFERENCES [UserLevels]([Id])
);

