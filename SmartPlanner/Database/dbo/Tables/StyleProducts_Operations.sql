﻿CREATE TABLE [dbo].[StyleProducts_Operations]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StyleProductId] INT NOT NULL, 
	[OperationId] INT NULL, 
	[CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL,
	[SMV] DECIMAL(18, 2) NULL, 
    CONSTRAINT [FK_StyleProducts_Operations_StyleProducts] FOREIGN KEY ([StyleProductId]) REFERENCES [StyleProducts]([Id]),
	CONSTRAINT [FK_StyleProducts_Operationss_Operation] FOREIGN KEY ([OperationId]) REFERENCES [Operations]([Id]),
	CONSTRAINT [FK_StyleProducts_Operations_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
	CONSTRAINT [FK_StyleProducts_Operations_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)