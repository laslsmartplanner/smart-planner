﻿CREATE TABLE [dbo].[StyleProducts_FeaturesList]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StyleProductId] INT NOT NULL, 
	[FeatureId] INT NULL, 
	[CreatedById] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL,
    CONSTRAINT [FK_StyleProducts_FeaturesList_StyleProducts] FOREIGN KEY ([StyleProductId]) REFERENCES [StyleProducts]([Id]),
	CONSTRAINT [FK_StyleProducts_FeaturesLists_Feature] FOREIGN KEY ([FeatureId]) REFERENCES [ConfigurationTableValues]([Id]),
	CONSTRAINT [FK_StyleProducts_FeaturesList_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
	CONSTRAINT [FK_StyleProducts_FeaturesList_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)