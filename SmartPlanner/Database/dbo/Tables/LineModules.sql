﻿CREATE TABLE [dbo].[LineModules]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FactoryNameId] INT NOT NULL, 
    [ModuleNumber] NVARCHAR(50) NOT NULL, 
    [PlanEmployees] INT NOT NULL, 
    [PlanEfficiency] DECIMAL NOT NULL, 
    [AvailableMinutes] DECIMAL NOT NULL, 
    [AverageProductionOut] DECIMAL NOT NULL, 
    [ProductionPerDay] INT NOT NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    [LastStyleProductId] INT NULL, 
	[LastOrderId] INT NULL,
	[LastProductId] INT NULL,
    [LastPlacedStyleProductId] INT NULL, 
    [LastPlacedIsInquiry] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_LineModules_StyleProducts] FOREIGN KEY ([LastStyleProductId]) REFERENCES [StyleProducts]([Id])
)
