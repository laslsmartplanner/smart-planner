﻿CREATE TABLE [dbo].[Notifications]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ToId] INT NOT NULL, 
    [Message] NVARCHAR(MAX) NOT NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [Watched] BIT NOT NULL DEFAULT 0, 
    [WatchedOn] DATETIME NULL, 
    CONSTRAINT [FK_Notifications_ToIdUsers] FOREIGN KEY ([ToId]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_Notifications_CreatedUser] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id])
)
