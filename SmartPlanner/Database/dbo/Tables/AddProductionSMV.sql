﻿CREATE PROCEDURE [dbo].[AddProductionSMV]
	@OrderInquiryId int,
	@SplitKey int
AS
	INSERT INTO OrderStyleProductSMVs( OrderInquiryId, SplitKey, OrderId, StyleId, ProductId, ProductionSMV)
			select @OrderInquiryId , @SplitKey,0,StyleId,ProductId,ProductionSMV
			from StyleProducts WITH(NOLOCK)
			WHERE StyleId = (
			select StyleId
			from OrderInquiries WITH(NOLOCK)
			where ID=@OrderInquiryId)