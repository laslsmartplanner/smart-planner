﻿CREATE TABLE [dbo].[Customers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(255) NOT NULL DEFAULT '', 
    [Address] NVARCHAR(255) NULL DEFAULT '', 
    [Mobile] NVARCHAR(50) NULL DEFAULT '', 
    [Email] NVARCHAR(50) NULL DEFAULT '', 
    [Country] NVARCHAR(150) NOT NULL DEFAULT '', 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL,
	CONSTRAINT [FK_Customers_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_Customers_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
