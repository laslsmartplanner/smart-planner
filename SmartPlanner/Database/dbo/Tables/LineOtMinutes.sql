﻿CREATE TABLE [dbo].[LineOtMinutes]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [LineModuleId] INT NOT NULL, 
    [WorkingDate] DATETIME NOT NULL, 
    [OtMinutes] FLOAT NOT NULL, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL
)
