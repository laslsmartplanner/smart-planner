﻿CREATE TABLE [dbo].[ProductivityDetails]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [WorkingDate] DATETIME NOT NULL, 
    [FactoryId] INT NOT NULL, 
    [LineModuleId] INT NOT NULL, 
    [OrderId] INT NOT NULL, 
    [StyleId] INT NOT NULL, 
	[ProductId] INT NOT NULL DEFAULT 0,
    [Attendance] INT NOT NULL DEFAULT 0, 
    [ProducedQty] INT NOT NULL DEFAULT 0, 
    [CreatedById] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [UpdatedById] INT NULL, 
    [UpdatedOn] DATETIME NULL, 
    CONSTRAINT [FK_ProductivityDetails_FactoryConfigTable] FOREIGN KEY ([FactoryId]) REFERENCES [ConfigurationTableValues]([Id]), 
    CONSTRAINT [FK_ProductivityDetails_OrderDetails] FOREIGN KEY ([OrderId]) REFERENCES [OrderDetails]([Id]), 
    CONSTRAINT [FK_ProductivityDetails_LineModule] FOREIGN KEY ([LineModuleId]) REFERENCES [LineModules]([Id]), 
    CONSTRAINT [FK_ProductivityDetails_StyleDetails] FOREIGN KEY ([StyleId]) REFERENCES [StyleDetails]([Id]),
	CONSTRAINT [FK_ProductivityDetails_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [Users]([Id]), 
    CONSTRAINT [FK_ProductivityDetails_UpdatedBy] FOREIGN KEY ([UpdatedById]) REFERENCES [Users]([Id])
)
