﻿CREATE FUNCTION [dbo].[GetCalculatedChangeOver]
(
	@FactoryNameId int,
	@StyleProductId1 int,
	@StyleProductId2 int
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @ChangeOver float = NULL
	SELECT @ChangeOver = case when ProductId1 <> ProductId2 OR CategoryId1 <> CategoryId2 OR SubCategoryId1 <> SubCategoryId2 then 100
			  when FeatureListCount1 =0 OR FeatureListCount2 = 0 OR OperationCount1 = 0 OR OperationCount2 = 0 then 100
			  else 
				case when FeatureListCount1 > 0 AND OperationCount1 > 0 then
					((dbo.GetCommonFeatureListCountForStyleProduct(StyleProductId1,StyleProductId2) * 50) / FeatureListCount1 ) + 
					((dbo.GetCommonOperationsCountForStyleProduct(StyleProductId1,StyleProductId2) * 50)/OperationCount1 )
				else
					0
				end
		end
	FROM
	(
	SELECT sp.Id StyleProductId1,ISNULL(fl.FeatureListCount,0) FeatureListCount1,isnull(o.Tot,0) OperationCount1,sp.CategoryId CategoryId1,sp.SubCategoryId SubCategoryId1,sp.ProductId ProductId1, p2.*
	FROM StyleProducts sp WITH(NOLOCK)
	left JOIN(
	SELECT COUNT(id) FeatureListCount,StyleProductId 
	FROM StyleProducts_FeaturesList WITH(NOLOCK)
	group by StyleProductId ) fl ON sp.Id = fl.StyleProductId
	left join
	(
	select COUNT(ID) Tot,StyleProductId
	from StyleProducts_Operations with(nolock)
	GROUP BY StyleProductId) o ON sp.Id = o.StyleProductId 

	cross JOIN(

	SELECT sp.Id StyleProductId2,ISNULL(fl.FeatureListCount,0) FeatureListCount2,ISNULL(o.Tot,0) OperationCount2,sp.CategoryId CategoryId2,sp.SubCategoryId SubCategoryId2,sp.ProductId ProductId2
	FROM StyleProducts sp WITH(NOLOCK)
	left JOIN(
	SELECT COUNT(id) FeatureListCount,StyleProductId 
	FROM StyleProducts_FeaturesList WITH(NOLOCK)
	group by StyleProductId ) fl ON sp.Id = fl.StyleProductId
	left join
	(
	select COUNT(ID) Tot,StyleProductId
	from StyleProducts_Operations with(nolock)
	GROUP BY StyleProductId) o ON sp.Id = o.StyleProductId 
	where sp.Id = @StyleProductId2) p2
	where sp.Id = @StyleProductId1
	)a

	IF @ChangeOver IS NULL
		RETURN 100
	return @ChangeOver 
END
