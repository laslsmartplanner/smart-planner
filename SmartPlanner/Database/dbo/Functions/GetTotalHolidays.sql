﻿CREATE FUNCTION [dbo].[GetTotalHolidays]
(
	@StartDate date,
	@EndDate date,
	@LineModuleId int
)
RETURNS INT
AS
BEGIN
	
declare @TotalHolidays float = 0


		WHILE @StartDate<=@EndDate
		BEGIN
			declare @LineWorkingId int = 0		
					
			IF @LineModuleId > 0
			BEGIN
				declare @LineHolidayId int=0
				declare @LineHd bit = 0
		
				SELECT @LineHolidayId = id , @LineHd = HalfDay  
				FROM LineHolidays lh WITH(NOLOCK) 
				WHERE LineModuleId = @LineModuleId AND CAST(Start as date) = @StartDate
				
				
				SELECT @LineWorkingId = Id 
				FROM LineWorkingHolidays WITH(NOLOCK)
				WHERE CAST(WorkingHoliday AS DATE) = @StartDate and LineModuleId = @LineModuleId AND
				CAST(WorkingHoliday AS DATE) NOT IN (SELECT CAST(start as date) FROM LineHolidays WITH(NOLOCK) WHERE LineModuleId = @LineModuleId AND CAST(start as date) = @StartDate )
				
			
				IF ISNULL(@LineHolidayId ,0) > 0 -- Holiday for the line
				BEGIN
					IF @LineHd > 0 
					BEGIN
						SET @TotalHolidays = @TotalHolidays + 0.5
					END
					ELSE
					BEGIN
						SET @TotalHolidays = @TotalHolidays + 1
					END
					set @StartDate = dateadd(D,1,@StartDate)
					CONTINUE
				END
			END

			declare @Id int=0
			declare @Hd bit=0
		
			SELECT @Id= c.Id, @Hd = c.HalfDay 
			FROM CalendarDays c WITH(NOLOCK) 
			WHERE CAST(c.Start AS DATE) = @StartDate 
		
			IF ISNULL(@Id,0) > 0  -- Company Holiday
			BEGIN
				IF @LineModuleId > 0
				BEGIN
					
					IF ISNULL(@LineWorkingId,0) > 0
					BEGIN
						set @StartDate = dateadd(D,1,@StartDate)
						CONTINUE
					END
				
				END
					
				IF ISNULL(@Hd,0) > 0 -- Half day for company
				BEGIN
					SET @TotalHolidays = @TotalHolidays + 0.5
					set @StartDate = dateadd(D,1,@StartDate)
					continue
				END
				set @TotalHolidays = @TotalHolidays + 1
			END
			ELSE
			BEGIN
				
				DECLARE @DatePart int = 0
				SELECT @DatePart =  DATEPART(DW, @StartDate)
			
				IF @DatePart = 1 OR @DatePart = 7
				BEGIN
					IF (not EXISTS(SELECT * FROM WorkingWeekEnds with(nolock) 
							  WHERE CAST(WeekEnd AS DATE) = @StartDate and CAST(weekEnd as date) 
								NOT IN (SELECT CAST(start as DATE) FROM CalendarDays with(nolock) where CAST(start as DATE) = @StartDate))) and @LineWorkingId=0
						BEGIN
							set @TotalHolidays = @TotalHolidays + 1
						END
				END
			END
			set @StartDate = dateadd(D,1,@StartDate)
		END


	RETURN @TotalHolidays
END
