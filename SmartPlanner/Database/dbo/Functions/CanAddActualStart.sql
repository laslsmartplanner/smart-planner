﻿CREATE FUNCTION [dbo].[CanAddActualStart]
(
	@OrderId int,
	@ProductId int
)
RETURNS BIT
AS
BEGIN
	declare @LineId int
	declare @StartDate date

	SELECT @LineId= LineModuleId,@StartDate= StartDate 
	FROM PlacedOrders WITH(NOLOCK)
	WHERE OrderDetailId = @OrderId and ProductId = @ProductId and IsInquiry=0

	declare @PrevOrderStartDate date

	SELECT @PrevOrderStartDate = MAX(startDate)
	FROM PlacedOrders WITH(NOLOCK)
	WHERE LineModuleId = @LineId and cast(StartDate as DATE) < @StartDate 

	if @PrevOrderStartDate is null
		RETURN 1

	if exists(
	select id
	from TnACalendars tna WITH(NOLOCK) 
	INNER JOIN(
	SELECT OrderDetailId,ProductId 
	FROM PlacedOrders with(nolock)
	WHERE LineModuleId = @LineId and CAST(startdate as date)=@PrevOrderStartDate and IsInquiry=0) p 
		ON tna.TnADefaultId=16 and tna.OrderDetailsId = p.OrderDetailId and tna.ProductId=p.ProductId and tna.ActualStartDate is not null 
		)
		return 1
	
	RETURN 0
END
