﻿CREATE FUNCTION [dbo].[GetCalculatedEndDates]
(
	@StartDate datetime ,
	@Duration float ,
	@LineModuleId int =0,
	@GetExactDate int =0
)
RETURNS @Dates TABLE
(
	StartDate datetime,Duration float, EndDate datetime,TotalHolidays float
)
AS
BEGIN

if CEILING(@Duration) <> @Duration and CEILING(@Duration) - @Duration <= 0.5
	SET @Duration = FLOOR(@Duration) + 0.5
else	
	SET @Duration = FLOOR(@Duration)

DECLARE @Count float = 0.5
	DECLARE @WorkedDays FLOAT =0
	DECLARE @EndDate DATETIME 
	DECLARE @TotalHolidays float =0
	SET @EndDate = DATEADD(HOUR,-12, @StartDate)

	WHILE @Count <= @Duration
	BEGIN

		WHILE @WorkedDays<@Count
		BEGIN
		
			SET @EndDate = DATEADD(HOUR,12, @EndDate)
			
			IF @LineModuleId > 0
			BEGIN
				declare @LineHolidayId int=0
				declare @LineHd bit = 0
		
				SELECT @LineHolidayId = id , @LineHd = HalfDay  
				FROM LineHolidays lh WITH(NOLOCK) 
				WHERE LineModuleId = @LineModuleId AND CAST(Start as date) = cast( @EndDate as date)
				
				declare @LineWorkingId int = 0
				
				SELECT @LineWorkingId = Id 
				FROM LineWorkingHolidays WITH(NOLOCK)
				WHERE CAST(WorkingHoliday AS DATE) = @EndDate and LineModuleId = @LineModuleId AND
				CAST(WorkingHoliday AS DATE) NOT IN (SELECT CAST(start as date) FROM LineHolidays WITH(NOLOCK) WHERE LineModuleId = @LineModuleId AND CAST(start as date) = cast(@EndDate as date) )
				
			
				IF ISNULL(@LineHolidayId ,0) > 0 -- Holiday for the line
				BEGIN
					IF @LineHd > 0 
					BEGIN
						SET @TotalHolidays = @TotalHolidays + 0.5
						SET @WorkedDays = @WorkedDays + 0.5
					END
					ELSE
					BEGIN
						SET @TotalHolidays = @TotalHolidays + 0.5
					END
					CONTINUE
				END
			END
		
		
			declare @Id int=0
			declare @Hd bit=0
		
			SELECT @Id= c.Id, @Hd = c.HalfDay 
			FROM CalendarDays c WITH(NOLOCK) 
			WHERE CAST(c.Start AS DATE) = CAST( @EndDate  as date)
		
			IF ISNULL(@Id,0) > 0  -- Company Holiday
			BEGIN
				IF @LineModuleId > 0
				BEGIN
					
					IF ISNULL(@LineWorkingId,0) > 0
					BEGIN
						SET @WorkedDays = @WorkedDays + 0.5
						CONTINUE
					END
				
				END
					
				IF ISNULL(@Hd,0) > 0 -- Half day for company
				BEGIN
					SET @WorkedDays = @WorkedDays + 0.5
					SET @TotalHolidays = @TotalHolidays + 0.5
					continue
				END
				set @TotalHolidays = @TotalHolidays + 0.5
			END
			ELSE
			BEGIN
				DECLARE @DatePart int = 0
				SELECT @DatePart =  DATEPART(DW, @EndDate)  
			
				IF @DatePart = 1 OR @DatePart = 7
				BEGIN
					IF (not EXISTS(SELECT * FROM WorkingWeekEnds with(nolock) 
							  WHERE CAST(WeekEnd AS DATE) =CAST(@EndDate as date) and CAST(weekEnd as date) 
								NOT IN (SELECT CAST(start as DATE) FROM CalendarDays with(nolock) where CAST(start as DATE) = CAST( @EndDate as DATE)))) and isnull(@LineWorkingId,0)=0
						BEGIN
						
							set @TotalHolidays = @TotalHolidays + 0.5
							CONTINUE
						END
				END
				set @WorkedDays = @WorkedDays + 0.5
			END
		END

		INSERT INTO @Dates 
		VALUES(@StartDate , @Count , @EndDate , @TotalHolidays )

		SET @Count = @Count + 0.5

	END
	
	if @GetExactDate > 0
		DELETE @Dates WHERE Duration <> @Duration 
						
	RETURN
END
