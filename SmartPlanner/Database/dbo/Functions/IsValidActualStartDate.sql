﻿CREATE FUNCTION [dbo].[IsValidActualStartDate]
(
 @OrderId int,
 @ProductId int,
 @ActualStartDate date
)
RETURNS bit
AS
BEGIN
	
	declare @LineId int
	declare @StartDate date
	declare @PlacedId int=0

	SELECT @LineId= LineModuleId,@StartDate= StartDate , @PlacedId = Id 
	FROM PlacedOrders WITH(NOLOCK)
	WHERE OrderDetailId = @OrderId and ProductId = @ProductId and IsInquiry=0

	if(@PlacedId =0 )
		return 1

	declare @IsValid bit= 1

	if @ActualStartDate is not null
	begin
		IF EXISTS(
		SELECT p.Id
		FROM PlacedOrders p WITH(NOLOCK)
		INNER JOIN TnACalendars tna WITH(NOLOCK) on p.OrderDetailId = tna.OrderDetailsId and p.ProductId = tna.ProductId and tna.TnADefaultId  = 16 and IsInquiry=0
		WHERE @ActualStartDate between CAST(p.startdate as date) and DATEADD(d,-1, CAST(COALESCE(p.ActuallyEnded,p.ActualAdditionalEndDate,tna.ActualEndDate,p.EndDate) as date)) and 
		LineModuleId = @LineId and p.id <> @PlacedId 
		)
		SET @IsValid = 0
		
		IF @IsValid = 1
		BEGIN
			if exists(
			SELECT p.Id
			FROM PlacedOrders p WITH(NOLOCK)
			INNER JOIN TnACalendars tna WITH(NOLOCK) on p.OrderDetailId = tna.OrderDetailsId and p.ProductId = tna.ProductId and tna.TnADefaultId  = 16 and IsInquiry=0
			WHERE LineModuleId = @LineId and p.id <> @PlacedId and CAST(COALESCE(p.ActuallyEnded,p.ActualAdditionalEndDate,tna.ActualEndDate,p.EndDate) as date) = @StartDate and p.EndInHalfDay = 0
			)
			set @IsValid = 0
		END
		
	end


	return @IsValid

END
