﻿CREATE FUNCTION [dbo].[FindEndDate]
(
	@StartDate datetime,
	@Duration float,
	@LineModuleId int =0
)
RETURNS DateTime
AS
BEGIN
	declare @EndDate datetime
	
	SELECT @EndDate = EndDate 
	FROM GetCalculatedEndDates(@StartDate,@Duration,@LineModuleId,0)
	WHERE Duration = @Duration

	return @EndDate
END

