﻿CREATE FUNCTION [dbo].[GetValidPreviousDate]
(
	@Date date
)
RETURNS date
AS
BEGIN
	DECLARE @DateFound bit = 0

while @DateFound = 0
BEGIN

	DECLARE @DatePart INT = 0
	SELECT @DatePart =  DATEPART(DW, @Date)
			
	IF @DatePart = 1 OR @DatePart = 7
	begin
		set @Date = DATEADD(d,-1,@date)
		continue	
	end

	if exists(
	SELECT *
			FROM CalendarDays c WITH(NOLOCK) 
			WHERE CAST(c.Start AS DATE) = @Date AND HalfDay=0)
	begin
		set @Date = DATEADD(d,-1,@date)
		continue
	end
	set @DateFound = 1
END
return @Date
END
