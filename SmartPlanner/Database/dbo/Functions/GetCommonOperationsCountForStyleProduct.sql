﻿CREATE FUNCTION [dbo].[GetCommonOperationsCountForStyleProduct]
(
	@StyleProduct1Id int,
	@StyleProduct2Id int
)
RETURNS INT
AS
BEGIN
	Declare @Tot int = 0
	 select @Tot = isnull( Count(1) ,0)
		from StyleProducts_Operations pr1 WITH(NOLOCK)
		WHERE pr1.StyleProductId = @StyleProduct1Id and pr1.OperationId IN (
								select pr2.OperationId
								from StyleProducts_Operations pr2 WITH(NOLOCK)
									where pr2.StyleProductId = @StyleProduct2Id
								)

	return @Tot
END