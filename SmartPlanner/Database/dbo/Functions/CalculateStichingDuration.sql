﻿CREATE FUNCTION [dbo].[CalculateStichingDuration]
(
	@OrderDetailId int ,
	@ProductId int,
	@LineModuleId int,
	@IsStartInHalfDay bit=0,
	@IsInquiry int=0
)
RETURNS FLOAT
AS
BEGIN
	
	--DECLARE @TotProd float=0
	declare @OrderQty float =0
	declare @Smv float=0
	DECLARE @Efficiency float =0
	
	SELECT @Efficiency = PlanEfficiency 
	FROM TnACalendars WITH(NOLOCK)
	WHERE OrderDetailsId = @OrderDetailId and ProductId = @ProductId and TnADefaultId = 16
	
	if @IsInquiry  = 1
		begin
			SELECT @OrderQty = o.OrderQty 
			, @Smv = ISNULL(sm.ProductionSMV,p.ProductionSMV)
			FROM OrderInquiries o WITH(NOLOCK) 
			INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
			INNER JOIN StyleProducts p WITH(NOLOCK) ON s.Id = p.StyleId and p.ProductId = @ProductId
			LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and p.ProductId = sm.ProductId and p.StyleId = sm.StyleId
			WHERE o.Id = @OrderDetailId
			
			SELECT @Efficiency=PlanEfficiency  
			FROM LineModules with(nolock) where id=@LineModuleId
			
		end
	else
		SELECT @OrderQty = o.OrderQty 
			, @Smv = ISNULL(sm.ProductionSMV,p.ProductionSMV)
		FROM OrderDetails o WITH(NOLOCK) 
		INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
		INNER JOIN StyleProducts p WITH(NOLOCK) ON s.Id = p.StyleId and p.ProductId = @ProductId
		LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and p.ProductId = sm.ProductId and p.StyleId = sm.StyleId
		WHERE o.Id = @OrderDetailId
		
	if @Smv * @OrderQty = 0
		return 0

	DECLARE @LineAvailable float = 0
	
	declare @LineEfficiency float=0

	SELECT @LineAvailable = AvailableMinutes * PlanEmployees
			, @LineEfficiency = PlanEfficiency 
	FROM LineModules WITH(NOLOCK)
	WHERE id=@LineModuleId

	declare @LineDet float=0

	--if @Efficiency = 0
	--	set @Efficiency = @LineEfficiency

	set @Efficiency = @Efficiency/100
	
	DECLARE @Duration float=0
	declare @RemainQty float = 0
	declare @Qty1 int = 0
	
	if @Efficiency = 0.3
	begin
		set @Qty1 = floor( (@LineAvailable * 0.3 )/ @Smv)
		if @IsStartInHalfDay =0
			set @Duration = 1
		else
			set @Duration = 0.5
		set @OrderQty = @OrderQty - @Qty1
	end
	if (@Efficiency = 0.3 or @Efficiency=0.5 ) AND @OrderQty > 0
	begin
		set @Qty1 = floor( (@LineAvailable * 0.5 )/ @Smv)
		set @Duration = @Duration + 1
		set @OrderQty = @OrderQty - @Qty1
	end
	
	if @OrderQty <= 0
		return @Duration
	
	if @LineEfficiency <> 0
		set @LineDet = (@LineEfficiency / 100)* @LineAvailable
	
	declare @TotProd float = @OrderQty * @Smv
	
	if @TotProd <> 0 and @LineDet <> 0
	BEGIN
		set @Duration  = @Duration + @TotProd/@LineDet
		--if (CEILING(@Duration) <> @Duration and CEILING(@Duration) - @Duration <= 0.5) or (@Duration > 0 and @Duration < 0.5)
		--	SET @Duration = FLOOR(@Duration) + 0.5
		--else	
		--	SET @Duration = FLOOR(@Duration)

		if ((CEILING(@Duration) = @Duration) or (CEILING(@Duration) <> @Duration and CEILING(@Duration) - @Duration <= 0.5))
			SET @Duration = CEILING(@Duration)
		else
			SET @Duration = FLOOR(@Duration) + 0.5
		
		return @Duration
	END

	return 0
END
