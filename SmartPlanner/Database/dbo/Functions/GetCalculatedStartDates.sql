﻿CREATE FUNCTION [dbo].[GetCalculatedStartDates]
(
	@EndDate date ,
	@Duration float ,
	@LineModuleId int =0,
	@GetExactDate int =0
)
RETURNS @Dates TABLE
(
	StartDate date,Duration int, EndDate date,TotalHolidays float
)
AS
BEGIN
	DECLARE @Count INT = 1
	DECLARE @WorkedDays FLOAT =0
	DECLARE @StartDate DATE 
	DECLARE @TotalHolidays float =0
	SET @StartDate = DATEADD(d,1, @EndDate)

	

	WHILE @Count <= @Duration
	BEGIN

		WHILE @WorkedDays<@Count
		BEGIN
		
			SET @StartDate = DATEADD(d,-1, @StartDate)
			
			IF @LineModuleId > 0
			BEGIN
				declare @LineHolidayId int=0
				declare @LineHd bit = 0
		
				SELECT @LineHolidayId = id , @LineHd = HalfDay  
				FROM LineHolidays lh WITH(NOLOCK) 
				WHERE LineModuleId = @LineModuleId AND Start = @StartDate
			
				IF ISNULL(@LineHolidayId ,0) > 0 -- Holiday for the line
				BEGIN
					IF @LineHd > 0 
					BEGIN
						SET @TotalHolidays = @TotalHolidays + 0.5
						SET @WorkedDays = @WorkedDays + 0.5
					END
					ELSE
					BEGIN
						SET @TotalHolidays = @TotalHolidays + 1
					END
					CONTINUE
				END
			END
		
		
			declare @Id int=0
			declare @Hd bit=0
		
			SELECT @Id= c.Id, @Hd = c.HalfDay 
			FROM CalendarDays c WITH(NOLOCK) 
			WHERE CAST(c.Start AS DATE) = @StartDate 
		
			IF ISNULL(@Id,0) > 0  -- Company Holiday
			BEGIN
				IF @LineModuleId > 0
				BEGIN
					declare @LineWorkingId int = 0
				
					SELECT @LineWorkingId = Id 
					FROM LineWorkingHolidays WITH(NOLOCK)
					WHERE WorkingHoliday = @StartDate and LineModuleId = @LineModuleId AND
					CAST(WorkingHoliday AS DATE) NOT IN (SELECT CAST(start as date) FROM LineHolidays WITH(NOLOCK) WHERE LineModuleId = @LineModuleId AND CAST(start as date) = @StartDate )
				
					IF ISNULL(@LineWorkingId,0) > 0
					BEGIN
						SET @WorkedDays = @WorkedDays + 1
						CONTINUE
					END
				
				END
					
				IF ISNULL(@Hd,0) > 0 -- Half day for company
				BEGIN
					SET @WorkedDays = @WorkedDays + 0.5
					SET @TotalHolidays = @TotalHolidays + 0.5
					continue
				END
				set @TotalHolidays = @TotalHolidays + 1
			END
			ELSE
			BEGIN
				DECLARE @DatePart int = 0
				SELECT @DatePart =  DATEPART(DW, @StartDate)
			
				IF @DatePart = 1 OR @DatePart = 7
				BEGIN
					IF not EXISTS(SELECT * FROM WorkingWeekEnds with(nolock) 
							  WHERE CAST(WeekEnd AS DATE) = @StartDate and CAST(weekEnd as date) 
								NOT IN (SELECT CAST(start as DATE) FROM CalendarDays with(nolock) where CAST(start as DATE) = @EndDate))
						BEGIN
							set @TotalHolidays = @TotalHolidays + 1
							CONTINUE
						END
				END
				set @WorkedDays = @WorkedDays + 1
			END
		END

		INSERT INTO @Dates 
		VALUES(@StartDate , @Count , @EndDate , @TotalHolidays )

		SET @Count = @Count + 1
	END
	
	if @GetExactDate > 0
		DELETE @Dates WHERE Duration <> @Duration 

	RETURN
END
