﻿CREATE FUNCTION [dbo].[GetCommonFeatureListCountForStyleProduct]
(
	@StyleProduct1Id int,
	@StyleProduct2Id int
)
RETURNS INT
AS
BEGIN
	Declare @Tot int = 0
	 select @Tot = isnull( Count(1) ,0)
		from StyleProducts_FeaturesList pr1 WITH(NOLOCK)
		WHERE pr1.StyleProductId = @StyleProduct1Id and pr1.FeatureId IN (
								select pr2.FeatureId
								from StyleProducts_FeaturesList pr2 WITH(NOLOCK)
									where pr2.StyleProductId = @StyleProduct2Id
								)

	return @Tot
END
