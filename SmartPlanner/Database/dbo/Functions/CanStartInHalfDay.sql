﻿CREATE FUNCTION [dbo].[CanStartInHalfDay]
(
	@StartDate date ,
	@OrderId int,
	@ProductId int 
)
RETURNS bit
AS
BEGIN
	if exists(
	select Id
	from PlacedOrders with(nolock)
	where CAST(COALESCE(actuallyended,actualadditionalEndDate,enddate) as date) = @StartDate and LineModuleId = (SELECT LineModuleId FROM PlacedOrders where OrderDetailId=@OrderId and ProductId=@ProductId and IsInquiry=0)
	and RIGHT('0'+CAST(DATEPART(hour, COALESCE(actuallyended,actualadditionalEndDate,enddate) ) as varchar(2)),2)='00'
	)
	return 1

	return 0
END
