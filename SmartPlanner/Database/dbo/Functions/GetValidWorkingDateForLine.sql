﻿CREATE FUNCTION [dbo].[GetValidWorkingDateForLine]
(
	@StartDate datetime,
	@LineId int ,
	@NextPrevious int=0
)
RETURNS DATETIME
AS
BEGIN
	
DECLARE @NextDate datetime = @StartDate

declare @AfterDays int = 1
	
WHILE @AfterDays > 0
BEGIN
	if @NextPrevious = 0
		SET @NextDate = DATEADD(HOUR,12,@NextDate)
	else
		SET @NextDate = DATEADD(HOUR,-12,@NextDate)
		
	DECLARE @CompanyHolidayId int = 0
	DECLARE @LineHolidayId int = 0
	DECLARE @IsWeekEnd BIT =0
	
	SELECT @CompanyHolidayId = ID FROM CalendarDays WITH(NOLOCK) where CAST(start as date) = CAST( @NextDate as DATE)
	
	SELECT @LineHolidayId = id FROM LineHolidays lh WITH(NOLOCK) WHERE LineModuleId = @LineId AND CAST(Start as date) = CAST( @NextDate as DATE)
	
	if @LineHolidayId > 0 
		CONTINUE
	
	DECLARE @DatePart int = 0
	SELECT @DatePart =  DATEPART(DW, @NextDate)

	IF @DatePart = 1 OR @DatePart = 7
		set @IsWeekEnd = 1
	
	DECLARE @LineWorkingId INT = 0
	
	SELECT @LineWorkingId = Id 
	FROM LineWorkingHolidays WITH(NOLOCK)
	WHERE CAST(WorkingHoliday AS DATE) = CAST( @NextDate as DATE) and LineModuleId = @LineId AND
	CAST(WorkingHoliday AS DATE) NOT IN (SELECT CAST(start as date) FROM LineHolidays WITH(NOLOCK) WHERE LineModuleId = @LineId AND CAST(start as date) = CAST( @NextDate as DATE) )
	
	IF @CompanyHolidayId > 0 and @LineWorkingId < 1
		continue
	
	declare @WorkingWeekEndId bit =0
	
	if @LineWorkingId = 0	
		SELECT @WorkingWeekEndId = id FROM WorkingWeekEnds with(nolock) 
		  WHERE CAST(WeekEnd AS DATE) = CAST( @NextDate as DATE) and CAST(weekEnd as date) 
			NOT IN (SELECT CAST(start as DATE) FROM CalendarDays with(nolock) where CAST(start as DATE) = CAST( @NextDate as DATE))
	
	if @IsWeekEnd=1 and @WorkingWeekEndId < 1 and @LineWorkingId < 1
		continue
	
	set @AfterDays=0
	
END
		return @NextDate
End
