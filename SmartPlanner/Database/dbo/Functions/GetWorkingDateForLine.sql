﻿CREATE FUNCTION [dbo].[GetWorkingDateForLine]
(
	@StartDate datetime,
	@LineId int ,
	@AfterDays float,
	@FactoryId int,
	@NextPrevious int=0,
	@PreviousEndDate datetime=null,
	@PlacedOrderId int
)
RETURNS DATETIME
AS
BEGIN
	
DECLARE @NextDate datetime = @StartDate

if @NextPrevious = 0
	set @PreviousEndDate = CAST(@PreviousEndDate as date)
	
WHILE @AfterDays > 0
BEGIN
	if @NextPrevious = 0
		SET @NextDate = DATEADD(HOUR,12,@NextDate)
	else
		SET @NextDate = DATEADD(HOUR,-12,@NextDate)
		
	DECLARE @CompanyHolidayId int = 0
	DECLARE @LineHolidayId int = 0
	DECLARE @IsWeekEnd BIT =0
	
	SELECT @CompanyHolidayId = ID FROM CalendarDays WITH(NOLOCK) where CAST(start as date) = CAST( @NextDate as DATE)
	
	SELECT @LineHolidayId = id FROM LineHolidays lh WITH(NOLOCK) WHERE LineModuleId = @LineId AND CAST(Start as date) = CAST( @NextDate as DATE)
	
	if @LineHolidayId > 0 
		CONTINUE
	
	DECLARE @DatePart int = 0
	SELECT @DatePart =  DATEPART(DW, @NextDate)

	IF @DatePart = 1 OR @DatePart = 7
		set @IsWeekEnd = 1
	
	DECLARE @LineWorkingId INT = 0
	
	SELECT @LineWorkingId = Id 
	FROM LineWorkingHolidays WITH(NOLOCK)
	WHERE CAST(WorkingHoliday AS DATE) = CAST( @NextDate as DATE) and LineModuleId = @LineId AND
	CAST(WorkingHoliday AS DATE) NOT IN (SELECT CAST(start as date) FROM LineHolidays WITH(NOLOCK) WHERE LineModuleId = @LineId AND CAST(start as date) = CAST( @NextDate as DATE) )
	
	IF @CompanyHolidayId > 0 and @LineWorkingId < 1
		continue
	
	declare @WorkingWeekEndId bit =0
	
	if @LineWorkingId = 0	
		SELECT @WorkingWeekEndId = id FROM WorkingWeekEnds with(nolock) 
		  WHERE CAST(WeekEnd AS DATE) = CAST( @NextDate as DATE) and CAST(weekEnd as date) 
			NOT IN (SELECT CAST(start as DATE) FROM CalendarDays with(nolock) where CAST(start as DATE) = CAST( @NextDate as DATE))
	
	if @IsWeekEnd=1 and @WorkingWeekEndId < 1 and @LineWorkingId < 1
		continue
	
	set @AfterDays = @AfterDays  - 0.5
	
	declare @IsPreviousHd bit = case when datepart(HOUR,@PreviousEndDate) > 0 then 1 else 0 end
	declare @IsNextHd bit = case when datepart(hour,@NextDate)>0 then 1 else 0 end
	
	if @NextPrevious = 0
	if @AfterDays<=0 and exists( SELECT Id FROM PlacedOrders WITH(NOLOCK) 
								where FactoryId = @FactoryId AND LineModuleId = @LineId 
								AND @PreviousEndDate is not null and  @NextDate <= @PreviousEndDate  and @PlacedOrderId<> Id )
		set @AfterDays = 0.5

END
	return @NextDate

	--if DATEPART(hour,@NextDate)=0
	--	return dateadd(hour,12,@NextDate)

 --RETURN dateadd(hour,-12, @NextDate)
END
