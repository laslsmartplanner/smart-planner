﻿CREATE FUNCTION [dbo].[GetAdditionalDays]
(
	@OrderDetailId int,
	@ProductId int,
	@LineModuleId int,
	@TotalDays float,
	@TotProducedQty float,
	@Duration float
)
RETURNS FLOAT
AS
BEGIN
	
	declare @OrderQty float =0
	declare @Smv float=0
	DECLARE @Efficiency float =0
	

	SELECT @OrderQty = o.OrderQty 
		, @Smv =  ISNULL(sm.ProductionSMV,p.ProductionSMV)
		--, @Efficiency = o.PlanEfficiency
	FROM OrderDetails o WITH(NOLOCK) 
	INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	INNER JOIN StyleProducts p WITH(NOLOCK) ON s.Id = p.StyleId and p.ProductId = @ProductId
	LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and p.ProductId = sm.ProductId and p.StyleId = sm.StyleId
	WHERE o.Id = @OrderDetailId
		
	SELECT @Efficiency = PlanEfficiency 
	FROM TnACalendars WITH(NOLOCK)
	WHERE OrderDetailsId = @OrderDetailId and ProductId = @ProductId and TnADefaultId = 16

	if @Smv * @OrderQty = 0
		return 0

	DECLARE @LineAvailable float = 0
	
	declare @LineEfficiency float=0
	

	SELECT @LineAvailable = AvailableMinutes * PlanEmployees
			, @LineEfficiency = PlanEfficiency 
	FROM LineModules WITH(NOLOCK)
	WHERE id=@LineModuleId

	declare @LineDet float=0

	--if @Efficiency = 0
	--	set @Efficiency = @LineEfficiency

	set @Efficiency = @Efficiency/100
	
	DECLARE @WorkedDays float=0
	declare @RemainQty float = 0
	declare @Qty1 int = 0
	declare @Produced1 float = 0
	declare @Produced2 float = 0
	declare @StartDate datetime
	
	select @StartDate = ActualStartDate
	from TnACalendars t with(nolock)
	where t.OrderDetailsId = @OrderDetailId and t.ProductId = @ProductId and t.TnADefaultId=16
		
	--select @Produced1 = ProducedQty 
	--from ProductivityDetails p with(nolock)
	--WHERE p.OrderId=@OrderDetailId and p.ProductId = @ProductId and CAST(workingdate as date)= CAST(@startDate as date)
	
	--select @Produced1
	
	declare @SecondDate datetime=null
	select @SecondDate = min(WorkingDate)
		from ProductivityDetails p WITH(NOLOCK)
		WHERE p.OrderId=@OrderDetailId and p.ProductId = @ProductId and CAST(workingdate as date)> CAST(@startDate as date)
	
	--select @Produced2 = ProducedQty 
	--from ProductivityDetails p with(nolock)
	--WHERE p.OrderId=@OrderDetailId and p.ProductId = @ProductId and CAST(workingdate as date)= CAST(@SecondDate as date)	
	
	--select @Produced2
	
	if @LineEfficiency <> 0
		set @LineDet = (@LineEfficiency / 100)* @LineAvailable
	
	declare @Qty2 float=0
		
	if @Efficiency = 0.3
	begin
		set @Qty1 = floor( (@LineAvailable * 0.3 )/ @Smv)
		set @WorkedDays = 1
		set @OrderQty = @OrderQty - @Qty1
	end
	if ((@Efficiency = 0.3 and @SecondDate is not null ) or @Efficiency=0.5 ) AND @OrderQty > 0
	begin
		set @Qty2 = floor( (@LineAvailable * 0.5 )/ @Smv)
		set @WorkedDays = @WorkedDays + 1
		set @OrderQty = @OrderQty - @Qty2
	end
	
	declare @Qty3 float =0
	
	if @TotalDays - @WorkedDays > 0
	begin
	
		set @Qty3 = cast((@OrderQty / (@Duration - @WorkedDays)) * (@TotalDays - @WorkedDays) as float) 	
	end
	
	
	declare @TotQty float = @Qty1 + @Qty2 + @Qty3 
	
	IF @TotQty = 0 or @LineDet=0
		return 0

	declare @AdditionalDays float = ((@TotQty - @TotProducedQty) * @Smv) / @LineDet
declare @AdditionalDaysFloor float = floor(((@TotQty - @TotProducedQty) * @Smv) / @LineDet )

if (@AdditionalDays - @AdditionalDaysFloor) >= 0.4
set @AdditionalDaysFloor = @AdditionalDaysFloor + 0.5

IF @AdditionalDaysFloor<0
	RETURN 0

return @AdditionalDaysFloor
END
