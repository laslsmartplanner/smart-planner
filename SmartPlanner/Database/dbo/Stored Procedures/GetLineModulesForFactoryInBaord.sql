﻿CREATE PROCEDURE [dbo].[GetLineModulesForFactoryInBaord]
	@Date date,
	@FactoryId int
AS
SELECT DISTINCT l.Id , l.ModuleNumber Name
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN LineModules l WITH(NOLOCK) ON p.LineModuleId = l.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 and p.ProductId=t.ProductId and IsInquiry=0
	WHERE (@Date between t.ActualStartDate and CAST(coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate( 
		case when t.IsActualStartInHalfDay=1 then dateadd(hour,12, cast(cast(t.ActualStartDate as date) as datetime)) else cast(t.ActualStartDate as date) end,p.Duration,p.LineModuleId))  AS date) ) 
	AND FactoryId = @FactoryId 
	ORDER BY l.ModuleNumber

