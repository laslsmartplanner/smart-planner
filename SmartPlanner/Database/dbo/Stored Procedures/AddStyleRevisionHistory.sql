﻿CREATE PROCEDURE [dbo].[AddStyleRevisionHistory]
	@StyleId int ,
	@RequestedBy int,
	@AcceptedBy int,
	@RevisionId int

AS

INSERT INTO StyleDetail_Revisions
                      (StyleDetailId, RequestedById, AcceptedById,RevisionId, IsMultiPiece, StyleNumber, BuyerId, AppearanceId, MaterialCode, ColorCode, OrderCode, FGCode, Sketch, FabricTypeId, OrderQty, ProductReference, 
                      Description, CreatedDate, BaseStyle, DesignReference, DivisionId, EDescription, GraphicNumber, MerchantId, NoOfSheets, OperationId, Process, CustomerId, SetValue, SgBom, TimeTableId, 
                      LaunchedDate, TotalSMV, SampleStatusId, IsConfirmed, Department, SampleStage, SpecialProcess, SpecialConcern, SpecialInstruction, RevisionSampleStage, ReasonToRevise, OptionRequest, 
                      CostingDeadline, CustomerComment, CustomerCode, ProductDesc, IEId, PlannedColor, ActualColor, ChangedColor)
(SELECT Id, @RequestedBy, @AcceptedBy,@RevisionId, IsMultiPiece, StyleNumber, BuyerId, AppearanceId, MaterialCode, ColorCode, OrderCode, FGCode, Sketch, FabricTypeId, OrderQty, ProductReference, 
                      Description, CreatedDate, BaseStyle, DesignReference, DivisionId, EDescription, GraphicNumber, MerchantId, NoOfSheets, OperationId, Process, CustomerId, SetValue, SgBom, TimeTableId, 
                      LaunchedDate, TotalSMV, SampleStatusId, IsConfirmed, Department, SampleStage, SpecialProcess, SpecialConcern, SpecialInstruction, RevisionSampleStage, ReasonToRevise, OptionRequest, 
                      CostingDeadline, CustomerComment, CustomerCode, ProductDesc, IEId, PlannedColor, ActualColor, ChangedColor
FROM StyleDetails
WHERE ID=@StyleId)
