﻿CREATE PROCEDURE [dbo].[ResetPlacedOrdersForLine]
	@OrderId int ,
	@ProductId int,
	@LineId int,
	@FactoryId int
AS


 --Return if actual end date added
IF EXISTS(
SELECT Id
FROM PlacedOrders WITH(NOLOCK)
WHERE OrderDetailId = @OrderId and ProductId = @ProductId and LineModuleId=@LineId and FactoryId = @FactoryId and ActuallyEnded IS NOT NULL and IsInquiry=0
	)
	RETURN
	

DECLARE @PlacedOrderId int =0
DECLARE @AdditionalDays float = 0
declare @AdditionalEndDate datetime
DECLARE @CurrentAdditionalDays float = 0
declare @EndDate datetime
declare @AdditionalHolidays float = 0
declare @ActualEndDate datetime
declare @ActualDuration float=0
declare @ActualHolidays float=0
declare @CurrentEndInHalfDay bit=0

SELECT 
@PlacedOrderId = d.Id
		,@AdditionalEndDate = a.EndDate 
		,@CurrentAdditionalDays = AdditionalDays 
		,@EndDate = d.EndDate 
		,@AdditionalDays=d.Additional
		,@AdditionalHolidays = a.TotalHolidays
		,@ActualEndDate = b.EndDate
		,@ActualDuration = b.Duration
		,@ActualHolidays = b.TotalHolidays
		,@CurrentEndInHalfDay = d.EndInHalfDay
FROM(

SELECT p.* ,productivity.TotDays,productivity.TotProducedQty
	,dbo.GetAdditionalDays(p.OrderDetailId,p.ProductId,@LineId,productivity.TotDays,TotProducedQty,p.Duration) Additional
FROM PlacedOrders p WITH(NOLOCK)
INNER JOIN (
			SELECT LineModuleId,SUM(producedQty) TotProducedQty, COUNT(WorkingDate) TotDays,OrderId,ProductId
			FROM ProductivityDetails p WITH(NOLOCK)
			WHERE p.OrderId = @OrderId and p.ProductId=@ProductId and p.LineModuleId = @LineId and p.FactoryId = @FactoryId
			GROUP by LineModuleId,OrderId,ProductId
			) productivity ON p.OrderDetailId = productivity.OrderId and productivity.ProductId = p.ProductId and productivity.LineModuleId = p.LineModuleId 
INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id
	)d
outer apply GetCalculatedEndDates( case when d.EndInHalfDay=1 then DATEADD(HOUR,12,CAST( CAST( d.EndDate as date) AS Datetime)) else DATEADD(d,1, CAST( d.EndDate as date)) end ,d.Additional ,d.LineModuleId,1) a
LEFT JOIN  (SELECT * FROM TnACalendars WITH(NOLOCK) WHERE TnADefaultId = 16)cal ON cal.OrderDetailsId = d.OrderDetailId and d.ProductId = cal.ProductId and IsInquiry=0
outer apply GetCalculatedEndDates(
	DATEADD(hour,12, dbo.FindEndDate(case when cal.IsActualStartInHalfDay=1 then DATEADD(hour,12,CAST( CAST(cal.ActualStartDate as date) as datetime) ) else cast(cal.ActualStartDate as DATE) end,d.Duration,d.LineModuleId))
	,d.Additional,d.LineModuleId,1
	) b

	IF EXISTS(
			SELECT * 
			FROM ProductivityDetails p WITH(NOLOCK)
			WHERE p.LineModuleId = @LineId and p.FactoryId = @FactoryId and p.WorkingDate >= cast(@EndDate as date) and p.OrderId <> @OrderId and p.ProductId <> @ProductId
			)
	RETURN


set @AdditionalDays = ABS(@additionaldays)

if (@AdditionalDays = @CurrentAdditionalDays) 
	Return

--select @AdditionalDays ,@CurrentAdditionalDays,@AdditionalEndDate, @EndDate endd,@CurrentEndInHalfDay

if @AdditionalDays = 0
begin
	set @AdditionalDays = 0
	set @AdditionalEndDate = null
	set @AdditionalHolidays =0
	set @ActualHolidays = 0
	set @ActualEndDate = null
end

--select @Shifted,@NextPrevious

declare @StartInHalfDay bit= 0


if  ( (ROUND(@AdditionalDays,0) <> @AdditionalDays and @CurrentEndInHalfDay = 0) or (ROUND(@AdditionalDays,0) = @AdditionalDays and @CurrentEndInHalfDay = 1))
	set @StartInHalfDay=1

UPDATE PlacedOrders SET AdditionalDays = ISNULL(@AdditionalDays,0) , 
		AdditionalEndDate = @AdditionalEndDate, 
		AdditionalHolidays=ISNULL( @AdditionalHolidays,0) ,
		ActualAdditionalHolidays = isnull(@ActualHolidays,0),
		ActualAdditionalEndDate = @ActualEndDate
WHERE Id=@PlacedOrderId

declare @StartDate datetime = isnull(@AdditionalEndDate,@EndDate)

EXEC ResetNextPlacedOrders @OrderId,@FactoryId,@LineId,@StartInHalfDay,@StartDate,@EndDate,@PlacedOrderId
