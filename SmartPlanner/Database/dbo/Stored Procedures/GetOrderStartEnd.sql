﻿CREATE PROCEDURE [dbo].[GetOrderStartEnd]
	@StitchingId int = 16,
	@FactoryId int=1,
	@StyleId int=0,
	@ProductId int=0,
	@ReturnStyles bit = 0,
	@ReturnOrderDetails bit = 0,
	@ReturnProducts bit=0
AS
	
	SELECT DISTINCT o.Id as OrderId
			,tnaCal.StartDate 
			,tnaCal.EndDate
			,tnaCal.Duration
			,tnaCal.Holidays
			,o.SalesOrderNumber 
			, b.Name Buyer
			, s.StyleNumber 
			,ISNULL(s.PlannedColor, b.PlannedColor) PlannedColor 
			,ISNULL(s.ActualColor, b.ActualColor) ActualColor
			, ISNULL(s.ChangedColor, b.ChangedColor) ChangedColor
			,sp.Id StyleProductId
			,sp.ProductId
			,pv.ConfigValue Product
			,o.OrderQty
			,o.CustomerPONumber
			,ISNULL(sm.ProductionSMV,sp.ProductionSMV) ProductionSMV
			,o.ShipmentDate
			,tnaCal.PlanEfficiency
			,o.StyleId 
			,o.CustomerId
			,o.Repeat
			,o.CM
			,o.MerchantId
			,0 IsInquiry
	INTO #Temp
	FROM OrderDetails o WITH(NOLOCK) 
	INNER JOIN StyleProducts sp WITH(NOLOCK) ON o.StyleId = sp.StyleId and o.FactoryNameId = @FactoryId
	LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and sp.ProductId = sm.ProductId and sp.StyleId = sm.StyleId
	LEFT JOIN (SELECT tc.OrderDetailsId,ActualEndDate ,tc.ProductId
			   FROM TnACalendars tc WITH(NOLOCK) 
				INNER JOIN (SELECT Top(1) Id 
							FROM TnADefaults WITH(NOLOCK) 
							ORDER BY DisplayOrder DESC) td ON tc.TnADefaultId = td.Id ) tc ON o.Id = tc.OrderDetailsId and tc.ProductId = sp.ProductId
	LEFT JOIN TnACalendars tnaCal WITH(NOLOCK) ON o.Id = tnaCal.OrderDetailsId and tnaCal.TnADefaultId = @StitchingId and sp.ProductId = tnaCal.ProductId
	LEFT JOIN Buyers b WITH(NOLOCK) ON o.BuyerId = b.Id 
	LEFT JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	LEFT JOIN ConfigurationTableValues pv WITH(NOLOCK) ON sp.ProductId = pv.Id
	where (tc.OrderDetailsId is null or tc.ActualEndDate is null) 
	--and (tnaCal.StartDate is null or @From is null or @To Is null or ( cast(ISNULL(tnaCal.StartDate,o.OrderConfirmationDate) as DATE) between @From and @To))
	and CAST(o.Id AS VARCHAR(50)) + CAST(sp.ProductId as varchar(50)) + '0' NOT IN (SELECT CAST(OrderDetailId as varchar(50)) + CAST(ProductId as varchar(50)) + CAST(IsInquiry as varchar(1)) from PlacedOrders WITH(NOLOCK))
	AND ( ISNULL(@StyleId,0)<1 OR o.StyleId=@StyleId)
	AND ( ISNULL(@ProductId,0)<1 or sp.ProductId=@ProductId )
	AND tnaCal.StartDate IS NOT NULL AND tnaCal.EndDate IS NOT NULL
	AND ISNULL(o.PoTypeId,0) <> 0
	
	union 
	
	select inq.Id,null StartDate,null EndDate,0 Duration,0 Holidays,SalesOrderNumber,b.Name Buyer,s.StyleNumber
	,s.PlannedColor ,s.ActualColor ,s.ChangedColor ,sp.Id StyleProductId,sp.ProductId,pr.ConfigValue Product,inq.OrderQty , CustomerPONumber,sp.ProductionSMV
	,ShipmentDate,0 PlanEfficiency,inq.StyleId,inq.CustomerId,inq.Repeat,inq.CM,inq.MerchantId,1
	from OrderInquiries inq WITH(NOLOCK)
	inner join StyleProducts sp WITH(NOLOCK) ON inq.StyleId = sp.StyleId
	left join Buyers b WITH(NOLOCK) ON inq.BuyerId = b.Id 
	inner join StyleDetails s WITH(NOLOCK) ON inq.StyleId = s.Id
	inner join ConfigurationTableValues pr WITH(nOLOCK) on sp.ProductId = pr.Id 
	where OrderInquiryStatusId=3 and ConfirmationStatusId=1
	and CAST(inq.Id AS VARCHAR(50)) + CAST(sp.ProductId as varchar(50)) + '1' NOT IN (SELECT CAST(OrderDetailId as varchar(50)) + CAST(ProductId as varchar(50)) + CAST(IsInquiry as varchar(1)) from PlacedOrders WITH(NOLOCK))
	AND ( ISNULL(@StyleId,0)<1 OR inq.StyleId=@StyleId)
	AND ( ISNULL(@ProductId,0)<1 or sp.ProductId=@ProductId )

	
	
order by SalesOrderNumber ,StyleNumber, Product

if @ReturnStyles = 1
BEGIN
	SELECT StyleId Id, StyleNumber Name
	FROM #Temp
	GROUP BY StyleId ,StyleNumber
	order by StyleNumber
	return
END

if @ReturnProducts = 1
begin
	SELECT ProductId Id, Product Name
	FROM #Temp
	GROUP BY ProductId ,Product
	order by Product
	return
end

if @ReturnOrderDetails = 1
begin
	SELECT t.Buyer,c.Name Customer,t.StyleNumber,t.CustomerPONumber,t.SalesOrderNumber,t.OrderQty,t.Repeat,t.ShipmentDate
		,t.CM,p.CurrentKeyProcess , case when LEN( p.CurrentKeyProcess )= 0 then '' else p.PossibilityToDeliver end PossibilityToDeliver,m.FullName Merchant
	FROM #Temp t
		LEFT JOIN Customers c WITH(NOLOCK) ON t.CustomerId = c.Id 
		LEFT JOIN OnGoingOrderProcesses p WITH(NOLOCK) ON t.OrderId = p.OrderDetailsId 
		LEFT JOIN Users m WITH(NOLOCK) ON t.MerchantId = m.Id 
	return
end

select * from #Temp

drop table #Temp
