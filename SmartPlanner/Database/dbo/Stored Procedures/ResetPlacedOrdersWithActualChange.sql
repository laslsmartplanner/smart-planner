﻿CREATE PROCEDURE [dbo].[ResetPlacedOrdersWithActualChange]
	@PlacedOrderId INT ,
	@OrderId int,
	@LineId int,
	@FactoryId int,
	@ShiftedOrderStartDate datetime,
	@ShiftedOrderEndDate datetime,
	@ShiftedOrderActualEndDate datetime ,-- actual Updated date from tna
	@CurrentlyEndInHalfDay bit, -- currently actual ends in h/d before update the end date(placed order)
	@ActualEndInHalfDay bit , -- ends in h/d update update the end date in tna
	@ActualStartInHalfDay bit 
	
AS
	
declare @ShiftDays float = datediff(d,@ShiftedOrderEndDate,@ShiftedOrderActualEndDate)

if @ShiftDays = 0 and (@CurrentlyEndInHalfDay <> @ActualEndInHalfDay or 
exists(select 1 from PlacedOrders with(nolock) where ID=@PlacedOrderId) ) 
	set @ShiftDays = 1

declare @ActualHolidays float = dbo.GetTotalHolidays(@ShiftedOrderStartDate,@ShiftedOrderActualEndDate,@LineId)
declare @ActualDuration float = datediff(d,@ShiftedOrderStartDate,@ShiftedOrderActualEndDate) - @ActualHolidays

if @ShiftDays = 0
	return

Update PlacedOrders Set ActuallyEnded=@ShiftedOrderActualEndDate 
    ,ActualDuration=@ActualDuration
    ,ActualHolidays=@ActualHolidays
    ,EndDate= @ShiftedOrderActualEndDate
    ,AdditionalDays=0
    ,AdditionalHolidays=0
    ,ActualAdditionalHolidays=0
    ,AdditionalEndDate=null
    ,ActualAdditionalEndDate=null
    ,StartInHalfDay=@ActualStartInHalfDay
    ,EndInHalfDay=@ActualEndInHalfDay
  WHERE Id=@PlacedOrderId

EXEC ResetNextPlacedOrders @OrderId,@FactoryId,@LineId,@ActualEndInHalfDay,@ShiftedOrderActualEndDate,@ShiftedOrderEndDate,@PlacedOrderId
