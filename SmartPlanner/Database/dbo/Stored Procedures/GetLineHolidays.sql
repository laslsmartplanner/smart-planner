﻿CREATE PROCEDURE [dbo].[GetLineHolidays]
	 @StartDate DATE ,
	 @EndDate DATE ,
	 @LineModuleId int 
AS
	SELECT 0 i,[Start],Title INTO #WeekEnds FROM CalendarDays WHERE 0=1

	DECLARE @Sat AS DATE
	DECLARE @Sun AS DATE

	DECLARE @BeginDate AS DATE = @StartDate
	DECLARE @RowNo int = -1

	WHILE @BeginDate <= @EndDate
	BEGIN
		SET @Sat = DATEADD(DAY, 7 - DATEPART(WEEKDAY, @BeginDate ), CAST(@BeginDate AS DATE)) 
		SET @Sun = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @BeginDate ), CAST(@BeginDate AS DATE)) 
		INSERT INTO #WeekEnds VALUES(@RowNo, @Sat,'Week end')
		
		set @RowNo = @RowNo -1
		
		INSERT INTO #WeekEnds VALUES(@RowNo,@Sun,'Week end')
		SET @BeginDate = @Sun
		set @RowNo = @RowNo -1
	END

	SELECT Id,Start,[End],Title,0 IsWeekEnd,HalfDay , case when HalfDay = 1 then '#fd7e14' else '#9501fc' end BackColor
	INTO #AllHolidays
	FROM CalendarDays WITH(NOLOCK) 
	WHERE  (CAST(start AS DATE) BETWEEN @StartDate AND @EndDate) and cast(start as date) not in
			(SELECT cast(start as date)
			FROM LineHolidays WITH(NOLOCK) 
			WHERE  (CAST(start AS DATE) BETWEEN @StartDate AND @EndDate) and LineModuleId = @LineModuleId)
	
	UNION
	
	SELECT id,Start,[End],Title,0,HalfDay, case when HalfDay = 1 then '#fd7e14' else '#9501fc' end BackColor
	FROM LineHolidays WITH(NOLOCK) 
	WHERE  (CAST(start AS DATE) BETWEEN @StartDate AND @EndDate) and LineModuleId = @LineModuleId

	UNION

	SELECT i,Start,DATEADD(DAY,1,start),Title,1,0 , '#f672a7' BackColor
	FROM #WeekEnds 
	WHERE (CAST(Start AS DATE) NOT IN
		(SELECT CAST(Start AS DATE) FROM CalendarDays WITH(NOLOCK) 
		 WHERE  CAST(start AS DATE) BETWEEN @StartDate AND @EndDate)) AND
		(CAST(Start AS DATE) NOT IN (SELECT CAST(WeekEnd AS DATE) from WorkingWeekEnds WITH(NOLOCK) where CAST(WeekEnd AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)  ))
	ORDER BY Start 

	SELECT * FROM #AllHolidays
	where CAST(start as date) not in 
	(SELECT CAST(WorkingHoliday AS DATE)
	FROM LineWorkingHolidays WITH(NOLOCK)
	WHERE (CAST(WorkingHoliday AS DATE) BETWEEN @StartDate AND @EndDate) and LineModuleId = @LineModuleId )

	SELECT * FROM LineWorkingHolidays

	DROP TABLE #WeekEnds 
	DROP TABLE #AllHolidays