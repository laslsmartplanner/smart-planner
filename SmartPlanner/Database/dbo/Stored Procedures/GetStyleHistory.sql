﻿CREATE PROCEDURE [dbo].[GetStyleHistory]
	@StyleId int,
	@ProductId int
AS
	SELECT s.StyleNumber,p.WorkingDate, p.ProducedQty,o.SalesOrderNumber , m.ModuleNumber 
	FROM ProductivityDetails p WITH(NOLOCK)
	INNER JOIN StyleDetails s WITH(NOLOCK) ON p.StyleId = s.Id 
	LEFT JOIN OrderDetails o WITH(NOLOCK) ON o.Id = p.OrderId 
	LEFT JOIN LineModules m WITH(NOLOCK) ON p.LineModuleId = m.Id 
	WHERE s.Id = @StyleId and p.ProductId = @ProductId
	order by WorkingDate
