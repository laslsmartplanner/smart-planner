﻿CREATE PROCEDURE [dbo].[GetStartDate]
	@EndDate date,
	@Duration float,
	@LineModuleId int =0
AS
BEGIN
	SELECT *
	FROM GetCalculatedStartDates(@EndDate,@Duration,@LineModuleId,1)

END