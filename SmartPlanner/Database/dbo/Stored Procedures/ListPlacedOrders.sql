﻿CREATE PROCEDURE [dbo].[ListPlacedOrders]
	@From date,
	@To date,
	@FactoryId int
AS
		declare @StartColumn int = -1
declare @EndColumn int =0

declare @EndColIndex int = DATEDIFF(day,@From,@To) 


SELECT p.*,o.SalesOrderNumber , o.OrderQty,o.ShipmentDate,o.CustomerPONumber,o.BuyerId,o.StyleId
INTO #OrderDet
FROM PlacedOrders p WITH(NOLOCK) 
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id and IsInquiry = 0
union
SELECT p.*,o.SalesOrderNumber , o.OrderQty,o.ShipmentDate,o.CustomerPONumber,o.BuyerId,o.StyleId 
FROM PlacedOrders p WITH(NOLOCK) 
	INNER JOIN OrderInquiries o WITH(NOLOCK) ON p.OrderDetailId = o.Id and IsInquiry = 1



select *
	--, case when p.ActEndDate > p.EndDate then p.EndDate else p.ActEndDate end ActualEndDate
	,p.ActEndDate ActualEndDate
	--,case when p.tnaActualDuration > 0 then p.tnaActualDuration + p.tnaActualHolidays
	--else 
		,(
		DATEDIFF(day,case when p.actualStartDate < @From then @From else p.ActualStartDate end
		, case when p.ActEndDate > p.EndDate then p.EndDate else p.ActEndDate end ) 
			) +1
	ActualEndColumn
	,cast(case when tnaActualEndDate IS null
	 and  DATEPART(hour, p.ActEndDate ) = 0 then 1
	 else ActEndHalf end as bit) IsActualEndInHalfDay,ActStartHalf IsActualStartInHalfDay
	 ,row_number() over (order by p.LineModuleId, p.StartDate) RowNo ,
	row_number() over (partition by p.LineModuleId order by p.LineModuleId, p.StartDate) GroupNo
	,case when p.AdditionalDays>0 and CAST( p.EndDate AS date)  < @From and DATEDIFF(DAY,CAST(p.EndDate AS date),@From)>0 then DATEDIFF(DAY,CAST(p.EndDate AS date),@From) - 1 else 0 end PlanAdjustment
	,case when p.AdditionalDays>0 and CAST(p.ActEndDate as date) < @From and DATEDIFF(day,CAST(p.ActEndDate as DATe),@From)>0 then DATEDIFF(day,CAST(p.ActEndDate as DATe),@From) - 1  else 0 end ActAdjustment
into #Placed
from(
select o.*,t.ActualEndDate tnaActualEndDate
	,t.IsActualEndInHalfDay ActEndHalf,t.IsActualStartInHalfDay ActStartHalf --, o.SalesOrderNumber
	,CASE WHEN DATEDIFF(day,@From,CAST(o.startdate as date)) < 1 THEN 1 ELSE  DATEDIFF(day,@From,CAST( o.startdate as date)) + 1 END StartColumn
	,CASE WHEN DATEDIFF(day,@From,CAST( ISNULL(o.actuallyEnded, o.EndDate) as date))>@EndColIndex THEN @EndColIndex + 2 ELSE DATEDIFF(day,@From,CAST(ISNULL(o.actuallyEnded, o.EndDate) as date))+2 END ColumnCount
	, CAST( CASE WHEN DATEDIFF(day,@From,CAST(o.startdate as date))< 0 OR DATEDIFF(day,@From,CAST(ISNULL(o.actuallyEnded , o.EndDate) as DATE))>@EndColIndex THEN 1 ELSE 0 END as bit) Locked
	,t.ActualStartDate , 
	case when t.ActualStartDate IS Not null and t.ActualEndDate IS NULL then dbo.FindEndDate(case when t.IsActualStartInHalfDay=1 then DATEADD(hour,12,CAST( CAST(t.ActualStartDate as date) as datetime) ) else cast(t.ActualStartDate as DATE) end,o.Duration,o.LineModuleId)  ELSE t.actualEndDate end ActEndDate ,
	t.ActualDuration tnaActualDuration , t.ActualHolidays tnaActualHolidays,
	case when datediff(day,@From,CAST(t.ActualStartDate as date)) + 1 < 1 then 1 else datediff(day,@From,CAST(t.ActualStartDate as date)) + 1 end ActualStartColumn
	
	,b.Name BuyerName
	, ISNULL(s.PlannedColor, b.PlannedColor) PlannedColor 
	,ISNULL(s.ActualColor, b.ActualColor) ActualColor
	, ISNULL(s.ChangedColor, b.ChangedColor) ChangedColor
	,s.StyleNumber, ISNULL(sm.ProductionSMV,sp.ProductionSMV) ProductionSMV
	,case when ISNULL(t.PlanEfficiency , 0) < 1 then l.PlanEfficiency else t.PlanEfficiency end PlanEfficiency
	,c.color CominationColor
	,cu.StartDate CuttingDate, sp.CM ProductCM,sp.Id StyleProductId
from 
/*
placedorders p with(nolock) 
inner join orderdetails o with(nolock) on p.OrderDetailId = o.Id and p.IsInquiry = 0
*/
#OrderDet o
inner join StyleProducts sp WITH(NOLOCK) ON o.StyleId = sp.StyleId and o.ProductId = sp.ProductId
LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.OrderDetailId = sm.OrderId and sp.ProductId = sm.ProductId and sp.StyleId = sm.StyleId

left join (
	select MAX(colorName) color ,OrderDetailsId,StyleProductId
	from OrderCombinations with(nolock)
	group by OrderDetailsId,StyleProductId
)c ON sp.Id = c.StyleProductId and o.OrderDetailId = c.OrderDetailsId 

left join buyers b with(NOLOCK) ON o.BuyerId = b.Id 
LEFT JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
left join TnACalendars t WITH(NOLOCK) on o.OrderDetailId = t.OrderDetailsId and t.TnADefaultId  =16 and t.ProductId=o.ProductId
left join LineModules l with(nolock) on o.LineModuleId = l.Id
left join (select StartDate,OrderDetailsId,ProductId from TnACalendars with(Nolock) where TnADefaultId = 12) cu ON o.OrderDetailId = cu.OrderDetailsId and o.ProductId = cu.ProductId 
where( (o.StartDate >= @From and o.EndDate<=@To) or (o.EndDate BETWEEN @From AND @To ) or (o.StartDate BETWEEN @From AND @To ) or (o.AdditionalEndDate is Not null and (o.AdditionalEndDate between @From and @To) ) ) and o.FactoryId = @FactoryId

)p
ORDER BY p.LineModuleId, p.StartDate

DROP TABLE #OrderDet


SELECT p.Id,
	 case when GroupNo = 1 then 'Cat0' 
		  when ChangeOver <= 25 then 'Cat1'
		  when ChangeOver <=50 then 'Cat2'
		  when ChangeOver <=75 then 'Cat3'
		  else 'Cat4'
	 end ChangeOverCategory
	 ,FactoryId
	 ,LineModuleId
	 ,OrderDetailId
	 ,ProductId
	 ,Duration
	 ,Holidays
	 ,StartDate
	 ,EndDate
	 ,StartInHalfDay
	 ,EndInHalfDay
	 ,ColumnCount
	 ,StartColumn
	 ,SalesOrderNumber
	 ,IsTemp
	 ,ActualStartDate
	 ,ActualEndDate
	 ,ActualDuration
	 ,ActualHolidays
	 ,ActualStartColumn
	 ,ActualEndColumn
	 ,StyleId
	 ,PlannedStart
	 ,PlannedEnd
	 ,Locked
	 ,CominationColor
	 ,StyleNumber
	 ,OrderQty
	 ,CustomerPONumber
	 ,ProductionSMV
	 ,ShipmentDate
	 ,HasChanged
	 ,BuyerName
	 ,PlannedColor
	 ,ActualColor
	 ,ChangedColor
	 ,AdditionalDays
	 ,AdditionalHolidays
	 ,AdditionalEndDate
	 ,ActualAdditionalHolidays
	 ,ActualAdditionalEndDate
	 ,ActuallyEnded
	 ,IsActualEndInHalfDay
	 ,IsActualStartInHalfDay
	 ,PlanEfficiency
	 ,CuttingDate
	 ,ProductCM
	 ,PlanAdjustment
	 ,ActAdjustment
	 ,IsInquiry
		
FROM(
SELECT case when p.GroupNo = 1 then 0 else dbo.GetCalculatedChangeOver(@FactoryId,p.StyleProductId,p2.StyleProductId) end ChangeOver,
	 p.*
FROM #Placed p 
	LEFT JOIN
	(
		SELECT RowNo+1 RowNo2,StyleProductId
		FROM #Placed
	) p2 ON p.RowNo = p2.RowNo2 
) p
ORDER BY p.LineModuleId, p.StartDate


; with CTE as  
    (  
     select p.ActualStartDate, CAST(p.ActualStartDate as date) WorkingDate  , 1 ColumnNum , CAST(coalesce(p.ActualAdditionalEndDate,p.ActualEndDate) as date) EndDate
			,p.FactoryId,p.LineModuleId,p.OrderDetailId,p.ProductId,p.Id
     FROM #Placed p
     WHERE p.ActualStartDate is Not null and ( ( cast( p.ActualStartDate as date) between @From and @To) OR (CAST(coalesce(p.ActualAdditionalEndDate,p.ActualEndDate) as date) between @From and @To )  )
     union all  
     select ActualStartDate, DATEADD(day,1, WorkingDate),ColumnNum+1
     ,EndDate,FactoryId,LineModuleId,OrderDetailId,ProductId,Id from CTE where WorkingDate< EndDate
    )  
  
  /*
  select *,ColumnNumber - DATEDIFF(day, CAST(ActualStartDate as date),@From),DATEDIFF(day, CAST(ActualStartDate as date),@From) from CTE
  where LineModuleId = 13 order by WorkingDate
  option (maxrecursion 0)
  */
          
    SELECT c.*, CAST( pr.ProducedQty as nvarchar(5))  ProducedQty,ColumnNum - DATEDIFF(day, CAST(ActualStartDate as date),@From) ColumnNumber
    FROM CTE c 
		LEFT JOIN ProductivityDetails pr WITH(NOLOCK) ON c.FactoryId = pr.FactoryId AND pr.LineModuleId = c.LineModuleId AND pr.OrderId = c.OrderDetailId AND pr.ProductId = c.ProductId 
		AND c.WorkingDate = CAST(pr.WorkingDate as date)
    WHERE c.WorkingDate IS NOT NULL and ColumnNum>0 and pr.ProducedQty IS NOT NULL 
    and (ColumnNum - DATEDIFF(day, CAST(ActualStartDate as date),@From)) > 0
	option (maxrecursion 0)


drop table #Placed
