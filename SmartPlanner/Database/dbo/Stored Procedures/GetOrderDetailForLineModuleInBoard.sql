﻿CREATE PROCEDURE [dbo].[GetOrderDetailForLineModuleInBoard]
	@Date date ,
	@FactoryId int ,
	@LineModuleId int,
	@OrderDetailId int = 0
AS

if @OrderDetailId = 0
begin

SELECT  top 1 o.Id , o.SalesOrderNumber Name, 0 Display
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 and p.ProductId = t.ProductId and IsInquiry=0
	WHERE (@Date >= t.ActualStartDate and @Date <= coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate(t.ActualStartDate,p.Duration,p.LineModuleId)))
	AND FactoryId = @FactoryId and LineModuleId = @LineModuleId 
end
else
begin

SELECT  top 1 o.Id , o.SalesOrderNumber Name, 0 Display
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 and p.ProductId = t.ProductId and IsInquiry=0
	WHERE (@Date >= t.ActualStartDate and @Date <= coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate(t.ActualStartDate,p.Duration,p.LineModuleId)))
	AND FactoryId = @FactoryId and LineModuleId = @LineModuleId and OrderDetailId = @OrderDetailId
end


/*

SELECT top 1 s.Id StyleId , s.StyleNumber Style , t.ProductId , cv.ConfigValue Product,P.FactoryId,p.LineModuleId,t.ActualStartDate,t.Duration,t.Holidays
--,ISNULL(t.actualEndDate,dbo.FindEndDate(t.ActualStartDate,p.Duration,p.LineModuleId)) e
	INTO #Temp
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id 
	INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 AND p.productId = t.ProductId
	INNER JOIN ConfigurationTableValues cv WITH(NOLOCK) ON t.ProductId = cv.Id 
	WHERE ((@Date >= t.ActualStartDate and @Date <= coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate(t.ActualStartDate,p.Duration,p.LineModuleId))))
	AND 
	p.FactoryId = @FactoryId and p.LineModuleId = @LineModuleId 
	
	
select * from(
	SELECT  top 1 o.Id , o.SalesOrderNumber Name, 0 Display
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 and p.ProductId = t.ProductId
	WHERE (@Date >= t.ActualStartDate and @Date <= coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate(t.ActualStartDate,p.Duration,p.LineModuleId)))
	AND FactoryId = @FactoryId and LineModuleId = @LineModuleId 

	union

/*
	SELECT s.Id , s.StyleNumber Name , 1
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id 
	INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16
	WHERE (@Date >=t.ActualStartDate or @Date<= case when t.ActualEndDate IS null then DATEADD(day,t.Duration + t.Holidays,t.actualStartDate) else t.ActualEndDate end)
	AND FactoryId = @FactoryId and LineModuleId = @LineModuleId
	*/
	
	SELECT StyleId,Style,1
	FROM #Temp
	
	union
	
	select Productid,Product,2
	from #Temp 
	
	
	) a
	
	Order by Display

Drop table #Temp
*/