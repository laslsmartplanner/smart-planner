﻿CREATE PROCEDURE [dbo].[GetLineOutputs]
	@FactoryId int=1,
	@LineId int =3,
	@WorkingDate date = '2019-07-01',
	@StartTime float = 8,
	@ProductivityId int =1
AS
	
declare @Available float=0

SELECT @Available=AvailableMinutes
FROM Factories WITH(NOLOCK)
WHERE id=@FactoryId

declare @Ot float=0

SELECT @Ot=OtMinutes
FROM LineOtMinutes WITH(NOLOCK)
WHERE LineModuleId =@LineId and CAST(workingdate as date)=@WorkingDate

declare @TotHours float = ceiling((@Available + @Ot)/60)

declare @EndTime float = @startTime + @TotHours

declare @Times table(FromTime float, ToTime float,ProducedQty float)

while @StartTime < @EndTime
begin
	INSERT @Times values(@StartTime,@StartTime + 1,null)
	set @StartTime= @StartTime+1
end

if @ProductivityId=0
	SELECT * 
	FROM @Times
ELSE
BEGIN
	select t.FromTime,t.ToTime,o.Qty  ProducedQty
	from @Times t
	LEFT JOIN LineOutputs o with(nolock) ON t.FromTime=o.FromTime AND t.ToTime=o.ToTime AND o.ProductivityId = @ProductivityId

END
