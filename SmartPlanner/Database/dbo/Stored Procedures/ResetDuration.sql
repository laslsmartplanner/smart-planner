﻿CREATE PROCEDURE [dbo].[ResetDuration]
	@OrderId int,
	@OrderProductId int =0
AS
	
declare @StyleId INT =0


SELECT  @StyleId = StyleId
FROM OrderDetails WITH(NOLOCK) WHERE id = @OrderId


DECLARE @Actual table(StartDate date,EndDate date,Holidays float,Duration float,TnADefaultId int)

if @OrderProductId < 1
BEGIN
	INSERT @Actual EXEC GetTnACalendar @OrderDetailsId=@OrderId,@GetLimited=1,@ProductId=@OrderProductId
	
	UPDATE tnaCal SET Duration=a.Duration, StartDate = a.StartDate, EndDate = a.EndDate 
	FROM @Actual a
		INNER JOIN TnACalendars tnaCal WITH(NOLOCK) ON a.TnADefaultId=tnacal.TnADefaultId and tnacal.OrderDetailsId = @OrderId and tnacal.ProductId = @OrderProductId 

	RETURN
END


declare @ProductId int

DECLARE db_cursor CURSOR FOR 
	SELECT ProductId 
	FROM StyleProducts with(nolock) WHERE StyleId = @StyleId 

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @ProductId

	WHILE @@FETCH_STATUS = 0  
	BEGIN 
		delete @Actual
		INSERT @Actual EXEC GetTnACalendar @OrderDetailsId=@OrderId,@GetLimited=1,@ProductId=@ProductId		
		
		UPDATE tnaCal SET Duration=a.Duration, StartDate = a.StartDate, EndDate = a.EndDate 
		FROM @Actual a
			INNER JOIN TnACalendars tnaCal WITH(NOLOCK) ON a.TnADefaultId=tnacal.TnADefaultId and tnacal.OrderDetailsId = @OrderId and tnacal.ProductId = @ProductId 
					
		FETCH NEXT FROM db_cursor INTO @ProductId
	END 

CLOSE db_cursor  
DEALLOCATE db_cursor
