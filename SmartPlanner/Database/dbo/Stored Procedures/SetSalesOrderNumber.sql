﻿CREATE PROCEDURE [dbo].[SetSalesOrderNumber]
	@OrderInquiryId int,
	@OrderNumberPrefix nvarchar(50)
AS
	DECLARE @Prefix nvarchar(50)

		SELECT @Prefix = OrderPrefix 
		FROM CompanyDetails WITH(NOLOCK)

	if( LEN(ISNULL(@Prefix,''))>0)
		set @OrderNumberPrefix = @Prefix

	UPDATE OrderInquiries SET SalesOrderNumber =
		(select @OrderNumberPrefix + '-' + RIGHT(CAST(YEAR(GETDATE()) as varchar(4)),2) + RIGHT('000000'+CAST(isnull(MAX(sqNo),0)+1 AS VARCHAR(6)),6) from OrderInquiries with(nolock)),
		SQNo =
		(select isnull(MAX(sqNo),0)+1 from OrderInquiries with(nolock))
	WHERE Id = @OrderInquiryId

SELECT Salesordernumber 
FROM OrderInquiries WITH(NOLOCK) where id = @OrderInquiryId
