﻿CREATE PROCEDURE [dbo].[GetProductivityForReport]
	@WorkingDate date,
	@FactoryId int=1
AS

select p.Id  , row_number() over (partition by OrderId,ProductId order by OrderId,ProductId,WorkingDate) DayNo
INTO #Days
from ProductivityDetails p WITH(NOLOCK)
order by OrderId,ProductId,WorkingDate

SELECT pd.WorkingDate
,pd.FactoryId
,LineModuleId
,pd.OrderId
,pd.StyleId
,pd.ProductId
,osp.ProductionSMV
,isnull(p.DayNo,0)  as NoOfDays
, (osp.ProductionSMV * pd.ProducedQty * 100/(pd.Attendance * l.AvailableMinutes)) as DayEfficiency
,sp.CM * pd.ProducedQty as CMProduced
,(l.PlanEmployees * l.AvailableMinutes/osp.ProductionSMV) as TargetQty
,pd.ProducedQty
,pd.Attendance
,am.AvailableMins as AvailableMin
,l.PlanEmployees
FROM ProductivityDetails pd
INNER JOIN OrderStyleProductSMVs osp on pd.OrderId = osp.OrderId and pd.ProductId = osp.ProductId
INNER JOIN LineModules l WITH(NOLOCK) ON l.Id = pd.LineModuleId
INNER JOIN StyleProducts sp WITH(NOLOCK) ON sp.ProductId = pd.ProductId and sp.StyleId = pd.StyleId
LEFT JOIN #Days p on pd.Id = p.Id
LEFT JOIN (
		select sum(AvailableMinutes + ISNULL(ot.OtMinutes,0)) AvailableMins,lm.Id
		from LineModules lm WITH(NOLOCK)
		LEFT JOIN LineOtMinutes ot WITH(NOLOCK) ON lm.Id = ot.LineModuleId
		group by lm.Id
		)am ON l.Id = am.Id
where pd.WorkingDate = @WorkingDate and pd.FactoryId = @FactoryId 
order by pd.WorkingDate, pd.LineModuleId


drop table #Days
