﻿CREATE PROCEDURE [dbo].[RemoveActuallyEnded]
	@PlacedOrderId int
AS
	
	DECLARE @OrderDetailId INT 
	DECLARE @ProductId INT
	DECLARE @LineModuleId INT
	DECLARE @IsHalfDayStart BIT
	DECLARE @StartDate datetime
	DECLARE @FactoryId int

	SELECT @OrderDetailId = OrderDetailId 
		  ,@ProductId = ProductId
		  ,@LineModuleId = LineModuleId
		  ,@IsHalfDayStart = StartInHalfDay 
		  ,@StartDate = StartDate
		  ,@FactoryId = FactoryId
	FROM PlacedOrders WITH(NOLOCK)
	WHERE Id=@PlacedOrderId

	DECLARE @Duration float = 0

	SELECT @Duration = [dbo].[CalculateStichingDuration](@OrderDetailId,@ProductId,@LineModuleId,@IsHalfDayStart,0)

	DECLARE @EndDate datetime
	DECLARE @TotalHolidays FLOAT=0

	SELECT @EndDate= EndDate,@TotalHolidays=TotalHolidays from [dbo].[GetCalculatedEndDates](@StartDate,@Duration,@LineModuleId,1)

	declare @IsHalfDayEnd bit  = 0

	 if (ROUND(@Duration,0) <> @Duration and @IsHalfDayStart =0) or DATEPART(HOUR,@EndDate)=0
		set @IsHalfDayEnd =1 

	Update PlacedOrders Set ActuallyEnded= null 
		,ActualDuration=0
		,ActualHolidays=0
		,EndDate= @EndDate
		,AdditionalDays=0
		,AdditionalHolidays=0
		,ActualAdditionalHolidays=0
		,AdditionalEndDate=null
		,ActualAdditionalEndDate=null
		,EndInHalfDay= @IsHalfDayEnd
		,Holidays = @TotalHolidays
		,Duration = @Duration
	  WHERE Id=@PlacedOrderId

	  EXEC ResetNextPlacedOrders @OrderDetailId,@FactoryId,@LineModuleId,@IsHalfDayEnd,@EndDate,@EndDate,@PlacedOrderId