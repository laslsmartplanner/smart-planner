﻿CREATE PROCEDURE [dbo].[GetOrderHistory]
	@OrderId INT,
	@ProductId INT
AS
BEGIN
	SELECT fLine.ModuleNumber FromLine,tLine.ModuleNumber ToLine,StartDate FromDate, NewStartDate ToDate,u.FullName ChangedBy,NewUpdatedOn ChangedOn,o.SalesOrderNumber
	FROM PlacedOrderHistories po WITH(NOLOCK)
	INNER JOIN LineModules fLine WITH(NOLOCK) ON po.LineModuleId = fLine.Id 
	INNER JOIN LineModules tLine WITH(NOLOCK) ON po.NewLineModuleId = tLine.Id 
	LEFT JOIN Users u WITH(NOLOCK) ON u.Id = po.NewUpdatedById
	INNER JOIN OrderDetails o WITH(NOLOCK) ON po.OrderDetailId = o.Id
	WHERE OrderDetailId=@OrderId AND po.ProductId=@ProductId
	order by NewUpdatedOn

END