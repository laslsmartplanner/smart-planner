﻿CREATE PROCEDURE [dbo].[GetPlannedQty]
	@OrderDetailId int,
	@ProductId int,
	@LineModuleId int,
	@FactoryId int,
	@Duration float
AS

	declare @OrderQty float =0
	declare @Smv float=0
	DECLARE @Efficiency float =0
	

	SELECT @OrderQty = o.OrderQty 
		, @Smv = ISNULL(sm.ProductionSMV,p.ProductionSMV)
		, @Efficiency = o.PlanEfficiency
	FROM OrderDetails o WITH(NOLOCK) 
	INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	INNER JOIN StyleProducts p WITH(NOLOCK) ON s.Id = p.StyleId and p.ProductId = @ProductId
	LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and p.ProductId = sm.ProductId and p.StyleId = sm.StyleId
	WHERE o.Id = @OrderDetailId
		
	SELECT @Efficiency = PlanEfficiency 
	FROM TnACalendars WITH(NOLOCK)
	WHERE OrderDetailsId = @OrderDetailId and ProductId = @ProductId and TnADefaultId = 16

	if @Smv * @OrderQty = 0
		begin
		select 0 Qty1 , 0 Qty2, 0 Qty3
		return
		end

	DECLARE @LineAvailable float = 0
	
	declare @LineEfficiency float=0
	

	SELECT @LineAvailable = AvailableMinutes * PlanEmployees
			, @LineEfficiency = PlanEfficiency 
	FROM LineModules WITH(NOLOCK)
	WHERE id=@LineModuleId

	declare @LineDet float=0

	set @Efficiency = @Efficiency/100
	
	DECLARE @WorkedDays float=0
	declare @RemainQty float = 0
	declare @Qty1 int = 0
	declare @Produced1 float = 0
	declare @Produced2 float = 0
	declare @StartDate datetime
	
	select @StartDate = StartDate
	from PlacedOrders t with(nolock)
	where t.OrderDetailId = @OrderDetailId and t.ProductId = @ProductId and IsInquiry=0
			
	declare @SecondDate datetime=null
	set @SecondDate = dbo.GetValidNextDate(@StartDate +1)

	--select @SecondDate = max(WorkingDate)
	--	from ProductivityDetails p WITH(NOLOCK)
	--	WHERE p.OrderId=@OrderDetailId and p.ProductId = @ProductId and CAST(workingdate as date)> CAST(@startDate as date)
		
	declare @Qty2 float=0
		
	if @Efficiency = 0.3
	begin
		set @Qty1 = floor( (@LineAvailable * 0.3 )/ @Smv)
		set @WorkedDays = 1
		set @OrderQty = @OrderQty - @Qty1
	end
	if ((@Efficiency = 0.3 and @SecondDate is not null ) or @Efficiency=0.5 ) AND @OrderQty > 0
	begin
		set @Qty2 = floor( (@LineAvailable * 0.5 )/ @Smv)
		set @WorkedDays = @WorkedDays + 1
		set @OrderQty = @OrderQty - @Qty2
	end

	declare @Qty3 float =0
	
	declare @TotalDays float=0
	set @TotalDays = @Duration
	--SELECT @TotalDays = ISNULL( COUNT(WorkingDate) ,0)
	--		FROM ProductivityDetails p WITH(NOLOCK)
	--		WHERE p.OrderId = @OrderDetailId and p.ProductId=@ProductId and p.LineModuleId = @LineModuleId and p.FactoryId = @FactoryId
	--		GROUP by LineModuleId,OrderId,ProductId
	
	
	if @TotalDays - @WorkedDays > 0
	begin
	
		set @Qty3 = cast((@OrderQty / (@Duration - @WorkedDays)) * (@TotalDays - @WorkedDays) as float) 	
	end
	
	--if @Efficiency = 0.5 
	--begin
	--	set @Qty1 = @Qty2
	--	set @Qty2 = @Qty3
	--end
	
	if @Efficiency <> 0.3 and @Efficiency <> 0.5
	begin
		set @Qty1 = @Qty3
		set @Qty2 = @Qty3
	end
	
	SELECT floor(@Qty1) Qty1 , floor(@Qty2) Qty2 , floor(@Qty3) Qty3
