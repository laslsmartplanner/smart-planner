﻿CREATE PROCEDURE [dbo].[AddOrderToLine]
	@LineModuleId int,
	@OrderDetailId int,
	@ProductId int,
	@FactoryId int,
	@Duration float,
	@UserId int,
	@StichingId int =16,
	@StartFrom date,
	@EndTo date,
	@StartDate datetime,
	@IsInquiry int=0
AS
	DECLARE @IsHalfDayStart bit=0
	DECLARE @AdditionalDays float =0
	
	DECLARE @HasChanged bit =0
		
	SELECT @IsHalfDayStart = ISNULL(EndInHalfDay,0) 
		,@AdditionalDays = ISNULL(AdditionalDays,0) + ISNULL(AdditionalHolidays ,0)
	FROM PlacedOrders WITH(NOLOCK)
	WHERE linemoduleid = @LineModuleId AND cast(coalesce(ActuallyEnded,ActualadditionalEndDate, EndDate) as date) = @StartDate
	AND cast(OrderDetailId as nvarchar(50)) + CAST(ProductId as nvarchar(50)) + CAST(IsInquiry as nvarchar(1)) <> cast(@OrderDetailId as nvarchar(50)) + CAST(@ProductId as nvarchar(50)) + CAST(@IsInquiry as nvarchar(1))
	
	
	
	if (@IsHalfDayStart =1)
	begin
		if @AdditionalDays = ROUND(@additionalDays,0)
			set @IsHalfDayStart = 1
		else
			set @IsHalfDayStart = 0
	end
	else
	begin
		if @AdditionalDays <> ROUND(@additionaldays,0)
			set @IsHalfDayStart = 1
		else
			set @IsHalfDayStart = 0
	end
		
	SELECT @Duration = [dbo].[CalculateStichingDuration](@OrderDetailId,@ProductId,@LineModuleId,@IsHalfDayStart,@IsInquiry)
	
	--DECLARE @TmpEndDate table(EndDate datetime, TotalHolidays float)
	
	DECLARE @ActualDuration float = @Duration

	if @IsHalfDayStart =1
		SET @StartDate = DATEADD(HOUR,12,@StartDate)
	--set @Duration = @Duration + 0.5
		
	--INSERT @TmpEndDate EXEC GetEndDate @StartDate,@Duration,@LineModuleId
	
	
	DECLARE @EndDate datetime
	DECLARE @TotalHolidays float=0
	
	--SELECT @EndDate= EndDate,@TotalHolidays=TotalHolidays FROM @TmpEndDate
	select @EndDate= EndDate,@TotalHolidays=TotalHolidays from [dbo].[GetCalculatedEndDates](@StartDate,@Duration,@LineModuleId,1)
	
	-- order already exists and cant overlap
	IF EXISTS( SELECT Id FROM PlacedOrders  WITH(NOLOCK)
		WHERE  ((@StartDate=cast(EndDate as date) and EndInHalfDay=0) or  (@StartDate<> cast(EndDate as date) and ((@StartDate BETWEEN StartDate AND EndDate) or (@EndDate BETWEEN StartDate and EndDate)))
		)
			  and LineModuleId = @LineModuleId and cast(OrderDetailId as nvarchar(50)) + CAST(productId as nvarchar(50))  + CAST(IsInquiry as nvarchar(1)) <> CAST( @OrderDetailId as nvarchar(50)) + cast(@ProductId as nvarchar(50))  + CAST(@IsInquiry as nvarchar(1)) )
		RETURN

	IF EXISTS(
		select iD from PlacedOrders 
		where LineModuleId = @LineModuleId and cast(OrderDetailId as nvarchar(50)) + CAST(productId as nvarchar(50))  + CAST(IsInquiry as nvarchar(1)) <> CAST( @OrderDetailId as nvarchar(50)) + cast(@ProductId as nvarchar(50))  + CAST(@IsInquiry as nvarchar(1))
		and cast(StartDate as date) > CAST(@StartDate AS DATE) and ( ( EndInHalfDay =1 and CAST(EndDate as date) <= CAST(@EndDate AS DATE)) or ( EndInHalfDay = 0 and CAST(EndDate as date) < CAST(@EndDate AS DATE)) )
	)
		RETURN


if @IsHalfDayStart =1
	SET @StartDate = DATEADD(HOUR,-12,@StartDate)

declare @LastOrderEndDate datetime
declare @LastOrderTotalDuration float=0

DECLARE @IsHalfDayEnd bit=0

declare @StartColumn int = -1
declare @IsTemp bit=0
	
DECLARE @IsUpdated bit =0
declare @PlacedOrderId int =0

declare @PlannedStartDate datetime
declare @PlannedEndDate datetime

select @PlacedOrderId = id 
from placedorders WITH(NOLOCK) WHERE OrderdetailId=@OrderDetailId and ProductId=@ProductId and IsInquiry=@IsInquiry
if(@PlacedOrderId > 0 )
begin
	
	
	INSERT INTO PlacedOrderHistories
			(FactoryId, LineModuleId, OrderDetailId,ProductId, Duration, Holidays, StartDate, EndDate, StartInHalfDay, EndInHalfDay, IsTemp, CreatedById, CreatedOn, UpdatedById, UpdatedOn,PlacedOrderId,IsInquiry)
	SELECT   FactoryId, LineModuleId, OrderDetailId,ProductId, Duration, Holidays, StartDate, EndDate, StartInHalfDay, EndInHalfDay, IsTemp, CreatedById, CreatedOn, UpdatedById, UpdatedOn,@PlacedOrderId,IsInquiry
	FROM PlacedOrders 
	where id = @PlacedOrderId
	
	DELETE placedorders WHERE id=@PlacedOrderId
	set @IsUpdated=1
	
end

	declare @Efficiency float=0

	if @IsInquiry > 0
		select @Efficiency = PlanEfficiency from LineModules with(nolock) where id=@LineModuleId
	else	
		select @PlannedStartDate = StartDate , @PlannedEndDate = EndDate , @Efficiency = PlanEfficiency
		from TnACalendars with(Nolock) where OrderDetailsId = @OrderDetailId and ProductId = @ProductId and TnADefaultId = @StichingId 

 if (ROUND(@Duration,0) <> @Duration and @IsHalfDayStart =0) or DATEPART(HOUR,@EndDate)=0
	set @IsHalfDayEnd =1 

declare @ColumnCount int = DATEDIFF(day,@StartFrom,@EndDate)
set @StartColumn = DATEDIFF(DAY,@StartFrom,@StartDate)


IF @PlannedStartDate <> @StartDate --or @PlannedEndDate <> @EndDate 
	set @HasChanged = 1

INSERT PlacedOrders (FactoryId, LineModuleId,OrderDetailId,Duration,Holidays,StartDate,EndDate,StartInHalfDay,EndInHalfDay,IsTemp,HasChanged,ProductId,PlannedStart,PlannedEnd,IsInquiry)
VALUES(@FactoryId,@LineModuleId,@OrderDetailId,@ActualDuration,@TotalHolidays,@StartDate,@EndDate,@IsHalfDayStart,@IsHalfDayEnd,@IsTemp,@HasChanged,@ProductId,@PlannedStartDate,@PlannedEndDate,@IsInquiry)

UPDATE LineModules SET LastOrderId = @PlacedOrderId , LastProductId = @ProductId ,LastPlacedIsInquiry=@IsInquiry
	, LastPlacedStyleProductId = (select s.Id
								from StyleProducts s with(nolock) 
									inner join orderdetails o with(nolock) on s.StyleId = o.StyleId 
								where o.Id = @OrderDetailId and s.ProductId = @ProductId)
WHERE Id=@LineModuleId

if @IsUpdated =1 
	begin
		UPDATE PlacedOrders SET UpdatedById = @UserId , UpdatedOn = GETDATE() where id=@PlacedOrderId
		
		UPDATE PlacedOrderHistories SET NewFactoryId=@FactoryId, NewLineModuleId=@LineModuleId, 
				NewHolidays=@TotalHolidays, NewStartDate=@StartDate, NewEndDate=@EndDate, 
				NewStartInHalfDay=@IsHalfDayStart, NewEndInHalfDay=@IsHalfDayEnd, NewIsTemp=@IsTemp, NewUpdatedById=@UserId, NewUpdatedOn=GETDATE()
		WHERE PlacedOrderId = @PlacedOrderId
	end
else
	UPDATE PlacedOrders SET CreatedById = @UserId , CreatedOn = GETDATE() where orderdetailId = @OrderDetailId and ProductId=@ProductId and IsInquiry=@IsInquiry


if @IsInquiry > 0
	SELECT p.*,@ColumnCount + 2 ColumnCount , @StartColumn + 1 StartColumn
		,b.Name BuyerName
		,o.SalesOrderNumber
		,o.OrderQty
		,o.CreatedDate  OrderCreatedDate
		,i.StatusName OrderInquiryStatus
		,s.StyleNumber
		,s.Description StyleDescription
		, ISNULL(s.PlannedColor, b.PlannedColor) PlannedColor 
		,ISNULL(s.ActualColor, b.ActualColor) ActualColor
		, ISNULL(s.ChangedColor, b.ChangedColor) ChangedColor, o.ShipmentDate, o.CustomerPONumber
		,ISNULL(sm.ProductionSMV,sp.ProductionSMV) ProductionSMV
		,case when ISNULL(@Efficiency , 0) < 1 then l.PlanEfficiency else @Efficiency end PlanEfficiency
		,IsInquiry
	FROM PlacedOrders p WITH(NOLOCK) 
	INNER JOIN OrderInquiries o WITH(NOLOCK) ON p.OrderDetailId = o.Id and IsInquiry=1
	LEFT JOIN Buyers b WITH(NOLOCK) ON o.BuyerId = b.Id 
	left join OrderInquiryStatus i WITH(NOLOCK) ON o.OrderInquiryStatusId = i.Id 
	left join StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	left join StyleProducts sp WITH(NOLOCK) ON s.Id = sp.StyleId and p.ProductId = sp.ProductId 
	left join LineModules l WITH(NOLOCK) ON p.LineModuleId = l.Id
	LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and sp.ProductId = sm.ProductId and sp.StyleId = sm.StyleId
	WHERE orderdetailid = @OrderDetailId and FactoryId = @FactoryId and linemoduleId = @LineModuleId and p.ProductId = @ProductId 

else

	SELECT p.*,@ColumnCount + 2 ColumnCount , @StartColumn + 1 StartColumn
		,b.Name BuyerName
		,o.SalesOrderNumber
		,o.OrderQty
		,o.CreatedDate  OrderCreatedDate
		,i.StatusName OrderInquiryStatus
		,s.StyleNumber
		,s.Description StyleDescription
		, ISNULL(s.PlannedColor, b.PlannedColor) PlannedColor 
		,ISNULL(s.ActualColor, b.ActualColor) ActualColor
		, ISNULL(s.ChangedColor, b.ChangedColor) ChangedColor, o.ShipmentDate, o.CustomerPONumber
		,ISNULL(sm.ProductionSMV,sp.ProductionSMV) ProductionSMV
		,case when ISNULL(@Efficiency , 0) < 1 then l.PlanEfficiency else @Efficiency end PlanEfficiency
		,IsInquiry
	FROM PlacedOrders p WITH(NOLOCK) 
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id and IsInquiry=0
	LEFT JOIN Buyers b WITH(NOLOCK) ON o.BuyerId = b.Id 
	left join OrderInquiryStatus i WITH(NOLOCK) ON o.OrderInquiryStatusId = i.Id 
	left join StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id 
	left join StyleProducts sp WITH(NOLOCK) ON s.Id = sp.StyleId and p.ProductId = sp.ProductId 
	left join LineModules l WITH(NOLOCK) ON p.LineModuleId = l.Id
	LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and sp.ProductId = sm.ProductId and sp.StyleId = sm.StyleId
	WHERE orderdetailid = @OrderDetailId and FactoryId = @FactoryId and linemoduleId = @LineModuleId and p.ProductId = @ProductId 
