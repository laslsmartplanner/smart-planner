﻿CREATE PROCEDURE [dbo].[GetTnACalendar]
	@OrderDetailsId int ,
	@TnADefaultId int=0 ,
	@GetLimited bit =0,
	@ProductId int =0,
	@ForBoard int =0,
	@LineModuleId int =0
AS

declare @TotalMinutes float=0

declare @Efficiency float =0
declare @Employees int = 0

SELECT @Efficiency=f.PlanEfficiency,@Employees = f.PlanEmployees
FROM OrderDetails o WITH(NOLOCK)
INNER JOIN Factories f WITH(NOLOCK) ON o.FactoryNameId = f.Id 

if @Efficiency > 0
	SET @Efficiency = @Efficiency/100


select @TotalMinutes=TotalMinutes from CompanyDetails

declare @LineDet float = @TotalMinutes * @Efficiency * @Employees


SELECT 
CEILING((ISNULL(sm.ProductionSMV,sp.ProductionSMV) * o.OrderQty)/@LineDet) a ,
	ISNULL(sm.ProductionSMV,sp.ProductionSMV) ProductionSMV 
	, o.OrderQty
	,o.Id OrderDetailsId
	, ISNULL(c.Id,0) Id
	, tna.Id TnADefaultId
	,tna.KeyProcess,
	o.ShipmentDate EndDate,
	CASE WHEN @ForBoard > 0 and @LineModuleId > 0 and tna.Id = 16 then
		DBO.CalculateStichingDuration(@OrderDetailsId,@ProductId,@LineModuleId,0,0)
	ELSE
		cast(--case when isnull(c.Updated,0)<1 then 
			case when tna.Id = 16 and isnull(c.Updated,0)<1 and  @TotalMinutes>0 and @LineDet>0 and (ISNULL(sm.ProductionSMV,sp.ProductionSMV) * o.OrderQty ) >0 then
				CEILING((ISNULL(sm.ProductionSMV,sp.ProductionSMV) * o.OrderQty)/@LineDet)
			when  (tna.Id = 13 and isnull(o.PrintingPlantId,0)<1)
					OR (tna.id = 14 and isnull(o.EmblishmentPlantId,0)<1) 
					OR (tna.id = 17 and isnull(o.WashingPlantId,0)<1) then
				0
			WHEN isnull(o.Repeat,0) = 1 AND (tna.Id IN(11,7,5,6,2)) THEN
				0
			when ISNULL(c.Updated,0)>0 then
				coalesce( c.Duration,p.Duration, tna.Duration)
			ELSE
				ISNULL(p.Duration,tna.Duration)
			End
		as float) 
	END Duration
	,cast(ISNULL(c.Holidays,0) as float) Holidays ,
	c.StartDate,
	c.ActualStartDate , c.ActualEndDate , c.ActualDuration, c.Remarks , ISNULL(c.responsibilityId,0) ResponsibilityId, ISNULL(c.Updated,0) Updated,c.ActualHolidays
	,ROW_NUMBER() OVER(ORDER BY tna.DisplayOrder desc) AS RowNumber,tna.DisplayOrder,@ProductId ProductId
	,cast(case when tna.Id =16 then dbo.CanAddActualStart(@OrderDetailsId,@ProductId) else 1 end as bit) CanAddActualStartDate
	,c.IsActualEndInHalfDay
	,c.IsActualStartInHalfDay,c.PlanEfficiency
INTO #Tna
FROM TnADefaults tna WITH(NOLOCK) 
	LEFT JOIN TnACalendars c WITH(NOLOCK) ON tna.Id = c.TnADefaultId and c.orderdetailsid=@OrderDetailsId and c.ProductId = @ProductId
	LEFT JOIN OrderDetails o WITH(NOLOCK) ON o.Id=@OrderDetailsId
	LEFT JOIN POTypeTnADurations p WITH(NOLOCK) ON tna.Id = p.TnADefaultId AND o.POTypeId = p.PoTypeId
	INNER JOIN StyleProducts sp WITH(NOLOCK) ON o.StyleId = sp.StyleId and sp.ProductId = @ProductId
	INNER JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id
	LEFT JOIN OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and sp.ProductId = sm.ProductId and sp.StyleId = sm.StyleId
ORDER BY tna.DisplayOrder desc


--delete #Tna where TnADefaultId<>21

DECLARE @RowNumber int 
DECLARE @EndDate date = null
declare @StartDate date
declare @Duration float
declare @TotalDuration float
declare @IsStartInHalfDay bit 
declare @IsEndInHalfDay bit
declare @Holidays float=0
declare @TnaId int

	SELECT @EndDate = EndDate FROM #Tna WHERE RowNumber = 1

	DECLARE db_cursor CURSOR FOR 
	SELECT RowNumber, startdate, (holidays + duration) TotalDuration,duration,TnADefaultId
	FROM #Tna
	--WHERE updated=0
	ORDER BY RowNumber

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @RowNumber,@StartDate,@TotalDuration,@Duration,@TnaId

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
		SELECT @EndDate = dbo.GetValidPreviousDate(@EndDate)
		
		IF @Duration > 0
			SELECT @StartDate=Startdate,@TotalDuration=TotalHolidays+Duration,@Holidays = TotalHolidays 
			FROM GetCalculatedStartDates(@EndDate,@Duration,0,1)	      
		else
		begin
			set @StartDate=@EndDate
		end
		
		UPDATE #Tna SET StartDate = @StartDate,Holidays=@Holidays WHERE RowNumber = @RowNumber
		
		IF ROUND(@TotalDuration,0) <> @TotalDuration
		BEGIN
			set @IsEndInHalfDay = 1
		END
		
		if @Duration > 0
			begin
			
			IF (@IsEndInHalfDay = 1 AND @IsStartInHalfDay = 0) or (@IsEndInHalfDay = 0 AND @IsStartInHalfDay = 1)
				set  @EndDate= @StartDate
			else
				set @EndDate = DATEADD(day,-1, @StartDate)
			end
		if @IsEndInHalfDay = 1
		begin
			set @IsStartInHalfDay = 1
			set @IsEndInHalfDay = 0
		end
		
		SELECT @EndDate = dbo.GetValidPreviousDate(@EndDate)
		UPDATE #Tna SET EndDate = @EndDate where RowNumber = @RowNumber + 1
			
		FETCH NEXT FROM db_cursor INTO @RowNumber,@StartDate,@TotalDuration,@Duration,@TnaId
	END 

	CLOSE db_cursor  
	DEALLOCATE db_cursor 

declare @OrderConfirmation datetime = null

select @OrderConfirmation = OrderConfirmationDate from OrderDetails with(nolock) where id=@OrderDetailsId

IF EXISTS(SELECT * FROM #Tna where TnADefaultId = 1 and CAST(StartDate as date) <> CAST(@orderConfirmation as date) )
BEGIN
	declare @NextStartDate datetime =@OrderConfirmation

	UPDATE #Tna set StartDate = @OrderConfirmation where TnADefaultId = 1
	declare @CurrentEndDate datetime

	DECLARE db_cursor CURSOR FOR 
	SELECT RowNumber, startdate, (holidays + duration) TotalDuration,duration,TnADefaultId
	FROM #Tna
	WHERE tnadefaultId<=9 and tnadefaultId>=1
	ORDER BY DisplayOrder

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @RowNumber,@StartDate,@TotalDuration,@Duration,@TnaId

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
		--SELECT @EndDate = dbo.GetValidNextDate(@StartDate)
		
		if @TnaId = 9
		begin
		
			declare @CurrentDuration float = 0
			
			select @CurrentEndDate = StartDate,@CurrentDuration = Duration from #Tna where RowNumber = @RowNumber - 1
					
			if @CurrentDuration > 0
				set @CurrentEndDate = DATEADD(d,-1, @CurrentEndDate)
		
			SELECT @EndDate = dbo.GetValidPreviousDate(@CurrentEndDate)
		
		--select @CurrentEndDate,@EndDate,@NextStartDate
		
			declare @NewDuration float = datediff(d,@NextStartDate,@EndDate)
			
			
			--select dbo.GetValidPreviousDate(@NextStartDate),@NextStartDate
			
			--select @CurrentEndDate,@EndDate,@NewDuration,@NextStartDate
			
			
			IF @NewDuration > 0
				select @Holidays = dbo.GetTotalHolidays(@NextStartDate,@EndDate,0)
			
				set @TotalDuration= @NewDuration - @Holidays
			
				--SELECT @EndDate=EndDate,@TotalDuration=Duration-TotalHolidays,@Holidays = TotalHolidays 
				--FROM GetCalculatedEndDates(@NextStartDate,@NewDuration-@Hol,0,1)	
			
	--select @EndDate,@TotalDuration,@Holidays,@Hol
			
				UPDATE #Tna set EndDate = @EndDate, Duration = @TotalDuration,Holidays=@Holidays where TnADefaultId = @TnaId
			break
		end
		
		IF @Duration > 0
			SELECT @EndDate=EndDate,@TotalDuration=TotalHolidays+Duration,@Holidays = TotalHolidays 
			FROM GetCalculatedEndDates(@NextStartDate,@Duration,0,1)	      
		else
		begin
			set @EndDate=@NextStartDate
		end
		
		UPDATE #Tna SET EndDate = @EndDate,Holidays=@Holidays WHERE RowNumber = @RowNumber
				
		IF ROUND(@TotalDuration,0) <> @TotalDuration
		BEGIN
			set @IsEndInHalfDay = 1
		END
		
		if @Duration > 0
			begin
			
			IF (@IsEndInHalfDay = 1 AND @IsStartInHalfDay = 0) or (@IsEndInHalfDay = 0 AND @IsStartInHalfDay = 1)
				set  @NextStartDate= @EndDate
			else
				set @NextStartDate = DATEADD(day,1, @EndDate)
			end
		if @IsEndInHalfDay = 1
		begin
			set @IsStartInHalfDay = 1
			set @IsEndInHalfDay = 0
		end
		
		SELECT @NextStartDate = dbo.GetValidNextDate(@NextStartDate)
		UPDATE #Tna SET StartDate = @NextStartDate where RowNumber = @RowNumber - 1
			
			
		FETCH NEXT FROM db_cursor INTO @RowNumber,@StartDate,@TotalDuration,@Duration,@TnaId
	END 

	CLOSE db_cursor  
	DEALLOCATE db_cursor 

END

SELECT * INTO #Filtered FROM #Tna tna
WHERE isnull(@TnADefaultId ,0) < 1 or tna.TnADefaultId = @TnADefaultId order by DisplayOrder

if @GetLimited = 1
	select StartDate,EndDate,Holidays,Duration,TnADefaultId from #Filtered order by DisplayOrder
else
	select * from #Filtered order by DisplayOrder



--SELECT @StartDate = MIN(startdate), @Duration = MAX(duration) FROM #Tna 

--SELECT * INTO #Dates FROM GetCalculatedEndDates(@StartDate,@Duration,0)

--SELECT tna.OrderDetailsId,tna.Id,tna.TnADefaultId,tna.KeyProcess,tna.StartDate,tna.Duration,tna.ActualStartDate,tna.ActualEndDate,tna.ActualDuration ,
--	   ISNULL(tna.EndDate,c.EndDate) EndDate,c.TotalHolidays Holidays,tna.ResponsibilityId ,tna.Updated,tna.ActualHolidays
--FROM #Tna tna 
--	LEFT JOIN #Dates c ON CAST(tna.StartDate as date) = CAST( c.StartDate as date) and tna.Duration = c.Duration 
--where tna.StartDate = c.StartDate and tna.Duration = c.Duration

DROP TABLE #Tna
DROP TABLE #Filtered
