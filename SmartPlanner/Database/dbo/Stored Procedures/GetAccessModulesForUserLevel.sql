﻿CREATE PROCEDURE [dbo].[GetAccessModulesForUserLevel]
	@UserLevelId int 
AS
	SELECT m.ModuleNumber AccessModuleId,ISNULL(AccessTypeId,0) AccessTypeId ,@UserLevelId UserLevelId
	FROM SysConfig_AccessModules m WITH(NOLOCK)
	LEFT JOIN UserLevelAccessModules l ON m.ModuleNumber = l.AccessModuleId AND l.UserLevelId = @UserLevelId
	ORDER BY m.ModuleNumber
