﻿CREATE PROCEDURE [dbo].[GetCompanyHolidaysCount]
	@Start date,
	@End date
AS
BEGIN
	
	DECLARE @Dates Table(id int, Start date, [End] date,Title nvarchar(255), IsWeekEnd bit,HalfDay bit,BackColor nvarchar(50))

	INSERT @Dates exec GetHolidays @Start,@End

	DECLARE @TotHolidays float =0

	SELECT @TotHolidays = ISNULL(COUNT(Id),0) FROM @Dates

	DECLARE @HalfDays float = 0

	SELECT @HalfDays = ISNULL(COUNT(Id),0) FROM @Dates where HalfDay=1

	if @HalfDays>0
		SET @HalfDays = @HalfDays / 2

	SELECT @TotHolidays - @HalfDays

END