﻿CREATE PROCEDURE [dbo].[GetEndDate]
	@StartDate date,
	@Duration float,
	@LineModuleId int =0
AS
BEGIN
	SELECT EndDate , TotalHolidays 
	FROM GetCalculatedEndDates(@StartDate,@Duration,@LineModuleId,0)
	WHERE Duration = @Duration

END