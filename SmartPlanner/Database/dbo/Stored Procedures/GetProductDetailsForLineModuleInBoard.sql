﻿CREATE PROCEDURE [dbo].[GetProductDetailsForLineModuleInBoard]
	@Date date ,
	@FactoryId int ,
	@LineModuleId int ,
	@OrderId int
AS
SELECT t.ProductId Id, cv.ConfigValue Name,p.FactoryId,p.LineModuleId,o.Id 
--,ISNULL(t.actualEndDate,dbo.FindEndDate(t.ActualStartDate,p.Duration,p.LineModuleId)) e
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id 
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 AND p.productId = t.ProductId and IsInquiry=0
	INNER JOIN ConfigurationTableValues cv WITH(NOLOCK) ON t.ProductId = cv.Id 
	WHERE (@Date between t.ActualStartDate and CAST(coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate( 
		case when t.IsActualStartInHalfDay=1 then dateadd(hour,12, cast(cast(t.ActualStartDate as date) as datetime)) else cast(t.ActualStartDate as date) end,p.Duration,p.LineModuleId))  AS date) ) 
	and p.FactoryId = @FactoryId and p.LineModuleId = @LineModuleId and o.Id = @OrderId
