﻿CREATE PROCEDURE [dbo].[GetDuration]
	@StartDate date,
	@EndDate date,
	@LineModuleId int
AS
	DECLARE @Holidays float = 0

	SELECT @Holidays = dbo.GetTotalHolidays(@StartDate,@EndDate,@LineModuleId)

	DECLARE @TotDays float = datediff(day,@EndDate,@StartDate)

	SELECT @TotDays - @Holidays Duration , @Holidays Holidays