﻿CREATE PROCEDURE [dbo].[ListLineModulesForBoard]
	@FactoryNameId int,
	@FromDate date ,
	@ToDate date
AS
	Select * 
	INTO #Lines
	From LineModules l WITH(NOLOCK)
	Where FactoryNameId = @FactoryNameId
	order by l.ModuleNumber

	SELECT * 
	FROM #Lines order by ModuleNumber

	;with cte as(
	select @FromDate WorkingDate, 1 Col
	union all
	select DATEADD(d,1,WorkingDate), Col+1 from cte where WorkingDate<@ToDate
	)
	select l.Id, c.* 
	from cte c cross join #lines l
	where dbo.GetTotalHolidays(c.WorkingDate,c.WorkingDate,l.Id) >0
	order by l.Id
	option (maxrecursion 0)

	drop table #lines
