﻿CREATE PROCEDURE [dbo].[GetFactoriesInBoard]
	@Date date
AS
		SELECT c.Id,c.FactoryName Name 
	FROM Factories c WITH(NOLOCK)
	INNER JOIN (
			select factoryId from
(
select o.factoryId,
 t.ActualStartDate,
case when t.ActualEndDate IS null then DATEADD(day,t.Duration + t.Holidays,t.actualStartDate) else t.ActualEndDate end EndDate
from PlacedOrders o WITH(NOLOCK)
INNER JOIN TnACalendars t WITH(NOLOCK) on o.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 and o.ProductId=t.ProductId and IsInquiry=0
)a
where @Date >= a.ActualStartDate or  @Date<=a.EndDate 
group by a.FactoryId ) f ON c.Id = f.FactoryId 
	order by c.FactoryName