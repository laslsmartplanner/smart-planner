﻿CREATE PROCEDURE [dbo].[GetOrdersForLineModuleInBoard]
	@Date date ,
	@FactoryId int ,
	@LineModuleId int 
AS
	SELECT distinct  o.Id , o.SalesOrderNumber Name
	FROM PlacedOrders p WITH(NOLOCK)
	INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderDetailId = o.Id  
	INNER JOIN TnACalendars t WITH(NOLOCK) on p.OrderDetailId = t.OrderDetailsId and t.TnADefaultId = 16 AND p.productId = t.ProductId and IsInquiry=0
	WHERE (@Date between t.ActualStartDate and CAST(coalesce(p.ActualAdditionalEndDate,t.actualEndDate,dbo.FindEndDate( 
		case when t.IsActualStartInHalfDay=1 then dateadd(hour,12, cast(cast(t.ActualStartDate as date) as datetime)) else cast(t.ActualStartDate as date) end,p.Duration,p.LineModuleId))  AS date) ) 
	AND 
	p.FactoryId = @FactoryId and p.LineModuleId = @LineModuleId 
	ORDER BY o.SalesOrderNumber
