﻿CREATE PROCEDURE [dbo].[ResetNextPlacedOrders]
	@OrderId int ,
	@FactoryId int,
	@LineId int,
	@StartInHalfDay int, -- next order
	@StartDate datetime, -- next order	
	@ChangedOrderEndDate datetime, -- before actual
	@PlacedOrderId int --changed
AS
	DECLARE @Duration float =0
	DECLARE @NextPlacedOrderId int = 0
	DECLARE @CurrEndDate datetime
	DECLARE @NextActualStart datetime = null
	DECLARE @NextNewActualStart datetime = null
	DECLARE @ActualEndDate datetime=null
	DECLARE @ActualHolidays float=0
	DECLARE @TnaCalId int=0


	DECLARE db_cursor CURSOR FOR 
		SELECT p.id,  p.Duration , ISNULL(p.ActuallyEnded, p.EndDate) EndDate , cal.ActualStartDate,cal.Id
		FROM PlacedOrders p WITH(NOLOCK)
			LEFT JOIN TnACalendars cal WITH(NOLOCK) ON cal.OrderDetailsId = p.OrderDetailId and p.ProductId = cal.ProductId and IsInquiry=0
		WHERE p.FactoryId = @FactoryId and p.LineModuleId = @LineId and cast(p.StartDate as date) >= cast(@ChangedOrderEndDate as date)
			and p.id <> @PlacedOrderId and (cal.TnADefaultId =16 or (cal.TnADefaultId is null and p.IsInquiry=1 ))
		ORDER BY p.StartDate
	OPEN db_cursor  
		FETCH NEXT FROM db_cursor INTO @NextPlacedOrderId,@Duration,@CurrEndDate,@NextActualStart,@TnaCalId

		WHILE @@FETCH_STATUS = 0  
		BEGIN 
			DECLARE @NextDate datetime
			
			--select @StartDate ,@NextPlacedOrderId
			
			if @StartInHalfDay=1
				set @StartDate = CAST( @StartDate as date) 
			else
				set @StartDate = DATEADD(hour,12,CAST( CAST( @StartDate as date) as datetime))
			
			select @NextDate =
			dbo.GetValidWorkingDateForLine(@StartDate,@LineId,0)
			
			declare @NewEndDate datetime
			declare @NewHolidays float
			
			select @NewEndDate = enddate , @NewHolidays = TotalHolidays
			from dbo.GetCalculatedEndDates(@NextDate,@Duration,@LineId,1)
			
				
			DECLARE @EndInHalfDay bit =0
			
			
			if (@StartInHalfDay =1 AND (@Duration + @NewHolidays)= ROUND((@Duration + @NewHolidays),0)) or
				(@StartInHalfDay =0 AND (@Duration + @NewHolidays)<> ROUND((@Duration + @NewHolidays),0))
				set @EndInHalfDay = 1
			
			select @StartDate, @StartInHalfDay starthd, @NextDate nextDate,@NewEndDate newend,@Duration dura,@NextPlacedOrderId id,@EndInHalfDay endHalf
			

			UPDATE PlacedOrders  
					 set StartDate = case when DATEPART(hour, @NextDate ) > 0 then CAST(@NextDate AS date) else DATEADD(hour,12,@NextDate) end
						,Holidays = @NewHolidays
						,EndDate = @NewEndDate -- case when DATEPART(hour, @NewEndDate ) > 0 then CAST(@NewEndDate AS date) else DATEADD(hour,12,@NewEndDate) end  
						,PrevStart = @StartDate
						,prevEnd=@CurrEndDate
						,ChangedOrderId=@OrderId
						,EndInHalfDay = @EndInHalfDay
						,StartInHalfDay= @StartInHalfDay
			where Id = @NextPlacedOrderId

			
			IF @NextActualStart IS NOT NULL
			BEGIN
				
				if @StartInHalfDay=1
					set @NextActualStart = CAST( @NextActualStart as date) 
				else
					set @NextActualStart = DATEADD(hour,12,CAST( CAST( @NextActualStart as date) as datetime))			
			
				select @NextNewActualStart =
				dbo.GetValidWorkingDateForLine(@NextActualStart,@LineId,0)
				
				select @ActualEndDate = enddate , @ActualHolidays = TotalHolidays
				from dbo.GetCalculatedEndDates(@NextNewActualStart,@Duration,@LineId,1)
				
				if @TnaCalId is not null
					UPDATE
						TnACalendars SET ActualStartDate = @NextNewActualStart 
					, ActualEndDate = @ActualEndDate 
					, ActualHolidays = @ActualHolidays 
					WHERE ID = @TnaCalId 
				
				set @NextActualStart=@ActualEndDate

			END
			
			set @StartInHalfDay = @EndInHalfDay
			set @StartDate = @NewEndDate
			
			FETCH NEXT FROM db_cursor INTO @NextPlacedOrderId,/*@StartDate,*/@Duration,@CurrEndDate,@NextActualStart,@TnaCalId
		END 

	CLOSE db_cursor  
	DEALLOCATE db_cursor
