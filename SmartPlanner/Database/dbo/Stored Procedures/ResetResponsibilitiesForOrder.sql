﻿CREATE PROCEDURE [dbo].[ResetResponsibilitiesForOrder]
	@OrderId int ,
	@UserId int 
AS
	
	SET NOCOUNT ON;

SELECT *,0 Responsibility
INTO #TnaRes
FROM TnADefaults tna WITH(NOLOCK)

DECLARE @Merchant INT =0
DECLARE @CustomerId INT =0
DECLARE @FactoryId INT =0
declare @StyleId INT =0
declare @PoTypeId INT =0

SELECT   @Merchant = MerchantId 
		,@CustomerId = CustomerId
		,@FactoryId = FactoryNameId
		,@StyleId = StyleId
		,@PoTypeId = ISNULL(PoTypeId,0)
FROM OrderDetails WITH(NOLOCK) WHERE id = @OrderId

IF @PoTypeId = 0
	RETURN

IF ISNULL(@Merchant,0)>0
	UPDATE #TnaRes
	SET Responsibility= @Merchant
	WHERE id IN(1,3,4,9,10)
	
if ISNULL(@CustomerId,0)>0 AND ISNULL(@FactoryId,0)>0
BEGIN
	DECLARE @Coordinator int
	declare @Commercial int
	
	SELECT @Coordinator = CoordinatorId , @Commercial = CommercialId 
	FROM CustomerResponsibilities c WITH(NOLOCK)
	WHERE CustomerId = @CustomerId AND FactoryId = @FactoryId 

	IF @FactoryId = 2 or @FactoryId = 3
	BEGIN
		IF ISNULL(@Coordinator,0) > 0
		BEGIN
			UPDATE #TnaRes SET Responsibility = @Coordinator
			WHERE Id IN (5,12,13,14,15,18,19,20)
		END
		
		IF ISNULL(@Merchant,0) > 0
		BEGIN
			UPDATE #TnaRes SET Responsibility = @Merchant
			WHERE Id IN (2,6)
		END
	END
	
	IF ISNULL(@Coordinator,0) > 0
	BEGIN
		UPDATE #TnaRes SET Responsibility = @Coordinator
		WHERE Id IN (7,16,17)
	END
	
	IF ISNULL( @Commercial,0) > 0
	BEGIN
		UPDATE #TnaRes SET Responsibility = @Commercial
		WHERE Id = 21
	END
	
	IF @FactoryId=1
	BEGIN
		if exists (SELECT ConsumptionCalculationResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(ConsumptionCalculationResponsibilityId,0)>0 )
		UPDATE #TnaRes SET Responsibility =	
		(SELECT ConsumptionCalculationResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(ConsumptionCalculationResponsibilityId,0)>0 )
		WHERE Id=2
		
		if exists (SELECT SizeSetsubmisionResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(SizeSetsubmisionResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT SizeSetsubmisionResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(SizeSetsubmisionResponsibilityId,0)>0)
		WHERE Id=5
		
		if exists (SELECT SizeSetCommentResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(SizeSetCommentResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT SizeSetCommentResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(SizeSetCommentResponsibilityId,0)>0)
		WHERE Id=6
		
		if exists (SELECT CuttingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(CuttingResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT CuttingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(CuttingResponsibilityId,0)>0)
		WHERE Id=12
		
		if exists (SELECT PrintingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(PrintingResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT PrintingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(PrintingResponsibilityId,0)>0)
		WHERE Id=13
		
		if exists (SELECT EmbroideryResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(EmbroideryResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT EmbroideryResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(EmbroideryResponsibilityId,0)>0)
		WHERE Id=14
		
		if exists (SELECT ReCuttingShapingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(ReCuttingShapingResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT ReCuttingShapingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(ReCuttingShapingResponsibilityId,0)>0)
		WHERE Id=15
		
		if exists (SELECT FinishingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(FinishingResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT FinishingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(FinishingResponsibilityId,0)>0)
		WHERE Id=18
		
		if exists (SELECT PackingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(PackingResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT PackingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(PackingResponsibilityId,0)>0)
		WHERE Id=19
		
		if exists (SELECT InspectionResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(InspectionResponsibilityId,0)>0)
		UPDATE #TnaRes SET Responsibility =	
		(SELECT InspectionResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(InspectionResponsibilityId,0)>0)
		WHERE Id=20
	END
	
	if exists (SELECT ProductionPalningResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(ProductionPalningResponsibilityId,0)>0)
	UPDATE #TnaRes SET Responsibility =	
	(SELECT ProductionPalningResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(ProductionPalningResponsibilityId,0)>0)
	WHERE Id=8
	
	if exists (SELECT PatternGradingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(PatternGradingResponsibilityId,0)>0)
	UPDATE #TnaRes SET Responsibility =	
	(SELECT PatternGradingResponsibilityId from Factories WHERE Id = @FactoryId and ISNULL(PatternGradingResponsibilityId,0)>0)
	WHERE Id=11
	
END


DECLARE @Actual table(StartDate date,EndDate date,Holidays float,Duration float,TnADefaultId int)

declare @ProductId int

DECLARE db_cursor CURSOR FOR 
	SELECT ProductId 
	FROM StyleProducts with(nolock) WHERE StyleId = @StyleId 

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @ProductId

	WHILE @@FETCH_STATUS = 0  
	BEGIN 
		delete @Actual
		INSERT @Actual EXEC GetTnACalendar @OrderDetailsId=@OrderId,@GetLimited=1,@ProductId=@ProductId		
		
		UPDATE tnaCal SET ResponsibilityId = t.Responsibility , Duration=t.Duration 
		FROM @Actual a
			INNER JOIN #TnaRes t ON a.TnADefaultId = t.Id 
			INNER JOIN TnACalendars tnaCal WITH(NOLOCK) ON t.Id=tnacal.TnADefaultId and tnacal.OrderDetailsId = @OrderId and tnacal.ProductId = @ProductId 
			WHERE ISNULL(Updated,0)=0
			--where ISNULL( tnacal.ResponsibilityId ,0) = 0
		
		
		INSERT INTO TnACalendars
                      (OrderDetailsId, ProductId, TnADefaultId, StartDate, Duration, Holidays, EndDate, ResponsibilityId, CreatedById, CreatedOn)
		
		SELECT @OrderId,@ProductId,t.Id,a.StartDate,a.Duration,a.Holidays,a.EndDate,t.Responsibility,@UserId,GETDATE()
		FROM @Actual a
			INNER JOIN #TnaRes t ON a.TnADefaultId = t.Id 
			LEFT JOIN TnACalendars tnaCal WITH(NOLOCK) ON t.Id=tnacal.TnADefaultId and tnacal.OrderDetailsId = @OrderId and tnacal.ProductId = @ProductId 
		WHERE tnaCal.Id IS NULL --or ISNULL(tnaCal.ResponsibilityId ,0) < 1
					
		FETCH NEXT FROM db_cursor INTO @ProductId
	END 

CLOSE db_cursor  
DEALLOCATE db_cursor 

--SELECT * FROM #TnaRes 
--select * from @Actual

DROP TABLE #TnaRes
