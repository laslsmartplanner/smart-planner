﻿CREATE PROCEDURE [dbo].[GetChangeOverForBoard]
	@FactoryNameId int ,
	@SelectedStyleProductId int,
	@UserId int
AS
	SELECT Id
		,dbo.GetCalculatedChangeOver(@FactoryNameId,@SelectedStyleProductId,LastPlacedStyleProductId) ChangeOver
	   ,LastPlacedStyleProductId 
	   ,ModuleNumber
	   ,AvailableMinutes
	INTO #Temp
	FROM LineModules WITH(NOLOCK)
	WHERE FactoryNameId = @FactoryNameId
	ORDER BY ModuleNumber

	INSERT INTO SelectedStyleChangeOvers (FactoryId, SelectedStyleProductId, LineModuleId, LastPlacedStyleProductId, ChangeOver, SelectedById, SelectedOn)
                      SELECT @FactoryNameId, @SelectedStyleProductId,Id,LastPlacedStyleProductId,ChangeOver,@Userid,GETDATE()
                      FROM #Temp
                      WHERE ChangeOver <=25
                      
	SELECT Id,
		ModuleNumber + ' : ' + CAST(AvailableMinutes AS NVARCHAR(50)) + CHAR(13) +
	   CAST(ChangeOver AS NVARCHAR(50)) Name
	FROM #Temp ORDER BY ModuleNumber

drop table #Temp