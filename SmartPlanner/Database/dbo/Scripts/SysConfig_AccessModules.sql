﻿
SET NOCOUNT ON

SET IDENTITY_INSERT [SysConfig_AccessModules] ON

MERGE INTO [SysConfig_AccessModules] AS [Target]
USING (VALUES
  (1,N'User Level Management',1)
 ,(2,N'User Management',2)
 ,(3,N'System Configuration',3)
 ,(4,N'Order Managment for Merchant',4)
 ,(5,N'Style Managment for Merchant',5)
 ,(6,N'SMV Managment for Merchant',6)
 ,(7,N'Calendar Management',7)
 ,(8,N'Daily Output Management',8)
 ,(9,N'Reports Management',9)
 ,(10,N'TnA Management',10)
 ,(11,N'Planning Board Management',11)
 ,(12,N'Style Managment for IE',12)
 ,(13,N'SMV Managment for IE',13)
 ,(19,N'Order Managment for Cutting',14)
 ,(30,N'Size/ Color Managment',15)
  ,(31,N'Feature/ Operation Managment',16)
  ,(32,N'Order Inquiry Managment',17)
) AS [Source] ([Id],[ModuleName],[ModuleNumber])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ModuleName], [Target].[ModuleName]) IS NOT NULL OR NULLIF([Target].[ModuleName], [Source].[ModuleName]) IS NOT NULL OR 
	NULLIF([Source].[ModuleNumber], [Target].[ModuleNumber]) IS NOT NULL OR NULLIF([Target].[ModuleNumber], [Source].[ModuleNumber]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ModuleName] = [Source].[ModuleName], 
  [Target].[ModuleNumber] = [Source].[ModuleNumber]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ModuleName],[ModuleNumber])
 VALUES([Source].[Id],[Source].[ModuleName],[Source].[ModuleNumber])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [SysConfig_AccessModules]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[SysConfig_AccessModules] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO



SET IDENTITY_INSERT [SysConfig_AccessModules] OFF
SET NOCOUNT OFF
GO
