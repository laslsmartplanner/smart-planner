﻿
SET NOCOUNT ON

SET IDENTITY_INSERT [SampleApprovalStatus] ON

MERGE INTO [SampleApprovalStatus] AS [Target]
USING (VALUES
  (1,N'Approved')
 ,(2,N'Not Approved')
) AS [Source] ([Id],[StatusName])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[StatusName], [Target].[StatusName]) IS NOT NULL OR NULLIF([Target].[StatusName], [Source].[StatusName]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[StatusName] = [Source].[StatusName]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[StatusName])
 VALUES([Source].[Id],[Source].[StatusName])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [SampleApprovalStatus]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[SampleApprovalStatus] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO



SET IDENTITY_INSERT [SampleApprovalStatus] OFF
SET NOCOUNT OFF
GO
