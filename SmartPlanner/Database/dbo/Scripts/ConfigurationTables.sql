﻿
SET NOCOUNT ON

SET IDENTITY_INSERT [ConfigurationTables] ON

MERGE INTO [ConfigurationTables] AS [Target]
USING (VALUES
  (1,N'Factory Names',1,1)
 ,(2,N'Order Status',2,2)
 ,(3,N'Planning Season',3,3)
 ,(4,N'Destination Media',4,4)
 ,(5,N'Destination',5,5)
 ,(6,N'Buy Group',6,6)
 ,(7,N'Vendor',7,7)
 ,(8,N'Division',8,8)
 ,(9,N'Product',9,9)
 ,(10,N'Location',10,10)
 ,(11,N'Method',11,11)
 ,(12,N'PO Type',12,12)
 ,(13,N'Appearance',13,13)
 ,(14,N'Product Type',14,14)
 ,(15,N'Product Group',15,15)
 ,(16,N'Product Category',16,16)
 ,(17,N'Fabric Type',17,17)
 ,(18,N'Time Table',18,18)
 ,(19,N'Operation',19,19)
 ,(20,N'Sample Status',20,20)
 ,(21,N'Category',21,21)
 ,(22,N'Emblishment Plant',22,22)
 ,(23,N'Feature List',23,23)
 ,(24,N'Deliver To',24,24)
 ,(25,N'Distribute From',25,25)
 ,(26,N'Washing Plant',26,26)
 ,(27,N'Printing Plant',27,27)
 ,(28,N'Size',28,28)
 ,(29,N'Price Type',29,29)
 ,(30,N'Sewing Plant',30,30)
 ,(31,N'Colors',31,31)
) AS [Source] ([Id],[ConfigName],[ConfigId],[DisplayOrder])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ConfigName], [Target].[ConfigName]) IS NOT NULL OR NULLIF([Target].[ConfigName], [Source].[ConfigName]) IS NOT NULL OR 
	NULLIF([Source].[ConfigId], [Target].[ConfigId]) IS NOT NULL OR NULLIF([Target].[ConfigId], [Source].[ConfigId]) IS NOT NULL OR 
	NULLIF([Source].[DisplayOrder], [Target].[DisplayOrder]) IS NOT NULL OR NULLIF([Target].[DisplayOrder], [Source].[DisplayOrder]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ConfigName] = [Source].[ConfigName], 
  [Target].[ConfigId] = [Source].[ConfigId], 
  [Target].[DisplayOrder] = [Source].[DisplayOrder]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ConfigName],[ConfigId],[DisplayOrder])
 VALUES([Source].[Id],[Source].[ConfigName],[Source].[ConfigId],[Source].[DisplayOrder])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [ConfigurationTables]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[ConfigurationTables] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO



SET IDENTITY_INSERT [ConfigurationTables] OFF
SET NOCOUNT OFF
GO
