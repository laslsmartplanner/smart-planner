﻿
SET NOCOUNT ON

MERGE INTO [Emails] AS [Target]
USING (VALUES
  (1,N'User Created',N'Dear User,<br/>
  Your user account has been created for GPS.<br/>
  Username: {0}<br/>
  Password: {1}')
 ,(2,N'Password Reset Request',N'User {0} reqeusted for the password reset')
 ,(3,N'Password Reset',N'New password for your user name {0} is {1}')
 ,(4,N'Fabric in house date has changed',N'Fabric in house date has changed for the following order<br/>
Order Number <OrderNumber><br/>
Buyer <Buyer><br/>
Style <Style><br/>
Style Description <StyleDescription><br/>
Odrer QTY <OrderQty><br/>
Merchant <Merchant><br/>
Received Date <ReceivedDate><br/>
Key process <KeyProcess><br/>
Plan start date <PlanStartDate><br/>
Plan end date <PlanEndDate><br/>
Status <Status>')
 ,(5,N'Trims in house date has changed',N'Trims in house date has changed for the following order<br/>
Order Number <OrderNumber><br/>
Buyer <Buyer><br/>
Style <Style><br/>
Style Description <StyleDescription><br/>
Odrer QTY <OrderQty><br/>
Merchant <Merchant><br/>
Received Date <ReceivedDate><br/>
Key process <KeyProcess><br/>
Plan start date <PlanStartDate><br/>
Plan end date <PlanEndDate><br/>
Status <Status>')
 ,(6,N'Planned cut date has been changed',N'Planned cut date has changed for the following order<br/>
Order Number <OrderNumber><br/>
Buyer <Buyer><br/>
Style <Style><br/>
Style Description <StyleDescription><br/>
Odrer QTY <OrderQty><br/>
Merchant <Merchant><br/>
Received Date <ReceivedDate><br/>
Key process <KeyProcess><br/>
Plan start date <PlanStartDate><br/>
Plan end date <PlanEndDate><br/>
Status <Status>')
 ,(7,N'Fabric Stiching date has been changed ',N'Fabric Stiching date has changed for the following order<br/>
Order Number <OrderNumber><br/>
Buyer <Buyer><br/>
Style <Style><br/>
Style Description <StyleDescription><br/>
Odrer QTY <OrderQty><br/>
Merchant <Merchant><br/>
Received Date <ReceivedDate><br/>
Key process <KeyProcess><br/>
Plan start date <PlanStartDate><br/>
Plan end date <PlanEndDate><br/>
Status <Status>')
 ,(8,N'Fabric Finishing date has been changed ',N'Fabric Finishing date has changed for the following order<br/>
Order Number <OrderNumber><br/>
Buyer <Buyer><br/>
Style <Style><br/>
Style Description <StyleDescription><br/>
Odrer QTY <OrderQty><br/>
Merchant <Merchant><br/>
Received Date <ReceivedDate><br/>
Key process <KeyProcess><br/>
Plan start date <PlanStartDate><br/>
Plan end date <PlanEndDate><br/>
Status <Status>'),
(9,'Order Inquiry Created','Order inquiry <OrderNumber> has created by <Merchant>'),
(10,'Order Inquiry has Updated','Order inquiry <OrderNumber> has updated by <Planner>'),
(11,'SMV New Inquiry has created','SMV new inquiry for <Style> has created by <Merchant>'),
(12,'SMV Revision Inquiry has created','SMV revision inquiry for <Style> has created by <Merchant>'),
(13,'SMV Inquiry has Updated','SMV inquiry for <Style> has updated by <Merchant>'),
(14,'Company Holiday(Full Day) Added','New Holiday(Full Day) added for <Holiday> by <User>'),
(15,'Company Holiday(Half Day) Added','New Holiday(Half Day) added for <Holiday> by <User>'),
(16,'Line Holiday(Full Day) Added','New Holiday(Full Day) for <LineModule> added for <Holiday> by <User>'),
(17,'Line Holiday(Half Day) Added','New Holiday(Half Day) for <LineModule> added for <Holiday> by <User>'),
(18,'Orders Not Added To the Board','Dear <User>,<br/><br/>Following orders are not yet added to the board:<br/><br/><br/><OrdersList>')
) AS [Source] ([Id],[Subject],[Message])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[Subject], [Target].[Subject]) IS NOT NULL OR NULLIF([Target].[Subject], [Source].[Subject]) IS NOT NULL OR 
	NULLIF([Source].[Message], [Target].[Message]) IS NOT NULL OR NULLIF([Target].[Message], [Source].[Message]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[Subject] = [Source].[Subject], 
  [Target].[Message] = [Source].[Message]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[Subject],[Message])
 VALUES([Source].[Id],[Source].[Subject],[Source].[Message])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [Emails]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[Emails] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO


SET NOCOUNT OFF
GO

