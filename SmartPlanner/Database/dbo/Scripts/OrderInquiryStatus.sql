﻿
SET NOCOUNT ON

SET IDENTITY_INSERT [OrderInquiryStatus] ON

MERGE INTO [OrderInquiryStatus] AS [Target]
USING (VALUES
  (1,N'Confirmed')
 ,(2,N'Not Confirmed')
 ,(3,N'Pending')
) AS [Source] ([Id],[StatusName])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[StatusName], [Target].[StatusName]) IS NOT NULL OR NULLIF([Target].[StatusName], [Source].[StatusName]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[StatusName] = [Source].[StatusName]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[StatusName])
 VALUES([Source].[Id],[Source].[StatusName])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [OrderInquiryStatus]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[OrderInquiryStatus] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO



SET IDENTITY_INSERT [OrderInquiryStatus] OFF
SET NOCOUNT OFF
GO
