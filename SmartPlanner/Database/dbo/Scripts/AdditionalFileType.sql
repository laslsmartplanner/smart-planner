﻿
SET NOCOUNT ON

SET IDENTITY_INSERT [AdditionalFileType] ON

MERGE INTO [AdditionalFileType] AS [Target]
USING (VALUES
  (1,N'Sketch File')
 ,(2,N'Thread Sheet File')
 ,(3,N'Trim Consumption File')
 ,(4,N'PPRA Report File')
 ,(5,N'Other')
) AS [Source] ([Id],[TypeName])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[TypeName], [Target].[TypeName]) IS NOT NULL OR NULLIF([Target].[TypeName], [Source].[TypeName]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[TypeName] = [Source].[TypeName]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[TypeName])
 VALUES([Source].[Id],[Source].[TypeName])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [AdditionalFileType]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[AdditionalFileType] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO



SET IDENTITY_INSERT [AdditionalFileType] OFF
SET NOCOUNT OFF
GO
