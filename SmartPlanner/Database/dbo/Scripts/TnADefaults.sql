﻿IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Order Receipt (Buyer PO)')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Order Receipt (Buyer PO)',1)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Consumption Calculation')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Consumption Calculation',2)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'BOM Generation')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('BOM Generation',3)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'PO Issue for Fabric, Trims')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('PO Issue for Fabric, Trims',4)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Size Set Submission')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Size Set Submission',5)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Size Set Comment')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Size Set Comment',6)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'PP Meeting')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('PP Meeting',7)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Production Planning Update & Circulation')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Production Planning Update & Circulation',8)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Fabric Inhouse & Allocation')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Fabric Inhouse & Allocation',9)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Trims Inhouse')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Trims Inhouse',10)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Pattern Grading')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Pattern Grading',11)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Cutting')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Cutting',12)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Printing')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Printing',13)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Embroidery')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Embroidery',14)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Re-Cutting Shaping')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Re-Cutting Shaping',15)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Fabric Stitching')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Fabric Stitching',16)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Washing')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Washing',17)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Finishing')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Finishing',18)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Packing')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Packing',19)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Inspection/ Audit')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Inspection/ Audit',20)

IF NOT EXISTS(SELECT Id FROM TnADefaults WHERE KeyProcess = 'Dispatch/ Shipping')
INSERT INTO TnADefaults(KeyProcess,DisplayOrder) VALUES('Dispatch/ Shipping',21)