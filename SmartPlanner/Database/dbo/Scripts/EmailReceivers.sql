﻿
SET NOCOUNT ON

SET IDENTITY_INSERT [EmailReceivers] ON

MERGE INTO [EmailReceivers] AS [Target]
USING (VALUES
  (1,4,0,1)
 ,(2,4,1,20)
 ,(3,4,1,18)
 ,(4,5,0,9)
 ,(6,5,1,20)
 ,(7,5,1,12)
 ,(8,6,0,9)
 ,(9,6,0,10)
 ,(10,6,1,13)
 ,(11,7,0,9)
 ,(12,7,1,10)
 ,(13,8,0,9)
 ,(14,8,1,15)
 ,(15,18,0,17)
) AS [Source] ([Id],[EmailId],[ToOrCC],[ReceiverId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[EmailId], [Target].[EmailId]) IS NOT NULL OR NULLIF([Target].[EmailId], [Source].[EmailId]) IS NOT NULL OR 
	NULLIF([Source].[ToOrCC], [Target].[ToOrCC]) IS NOT NULL OR NULLIF([Target].[ToOrCC], [Source].[ToOrCC]) IS NOT NULL OR 
	NULLIF([Source].[ReceiverId], [Target].[ReceiverId]) IS NOT NULL OR NULLIF([Target].[ReceiverId], [Source].[ReceiverId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[EmailId] = [Source].[EmailId], 
  [Target].[ToOrCC] = [Source].[ToOrCC], 
  [Target].[ReceiverId] = [Source].[ReceiverId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[EmailId],[ToOrCC],[ReceiverId])
 VALUES([Source].[Id],[Source].[EmailId],[Source].[ToOrCC],[Source].[ReceiverId])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE;

DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [EmailReceivers]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[EmailReceivers] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO



SET IDENTITY_INSERT [EmailReceivers] OFF
SET NOCOUNT OFF
GO
