﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/* Add config tables */
:r .\Scripts\ConfigurationTables.sql	


/*Creating new access modules*/
:r .\Scripts\SysConfig_AccessModules.sql

/*Adding admin User level*/
:r .\Scripts\UserLevels.sql

/* Adding email templates*/
:r .\Scripts\Emails.sql


/*Assigning access for admin user level*/
:r .\Scripts\UserLevelAccessModules.sql

/* Add TnA Defaults tables */
:r .\Scripts\TnADefaults.sql

:r .\Scripts\Roles.sql

:r .\Scripts\OrderInquiryStatus.sql

:r .\Scripts\SampleApprovalStatus.sql

:r .\Scripts\AdditionalFileType.sql

/* Adding EmailReceivers templates*/
:r .\Scripts\EmailReceivers.sql


