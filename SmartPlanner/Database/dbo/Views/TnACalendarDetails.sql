﻿CREATE VIEW [dbo].[TnACalendarDetails]
	AS 
SELECT c.Id,c.TnADefaultId,d.KeyProcess ,c.OrderDetailsId,
o.SalesOrderNumber,c.ProductId, p.ConfigValue ProductName,c.ResponsibilityId,r.FullName ResponsibilityName,r.Email ResponsibilityEmail,
c.StartDate,c.Duration,c.EndDate,c.Holidays,c.ActualStartDate,c.ActualEndDate,c.ActualDuration,c.ActualHolidays,o.FactoryNameId, f.FactoryName, 
b.Name BuyerName, cu.Name CustomerName,s.StyleNumber, o.CustomerPONumber, o.OrderQty,o.Repeat, c.Remarks 
FROM TnACalendars c WITH(NOLOCK)
INNER JOIN TnADefaults d WITH(NOLOCK) ON c.TnADefaultId = d.Id 
INNER JOIN OrderDetails o WITH(NOLOCK) ON c.OrderDetailsId = o.Id 
INNER JOIN ConfigurationTableValues p WITH(NOLOCK) ON c.ProductId = p.Id 
INNER JOIN Users r WITH(NOLOCK) ON c.ResponsibilityId = r.Id 
LEFT JOIN Factories f WITH(NOLOCK) ON o.FactoryNameId = f.Id 
LEFT JOIN Buyers b WITH(NOLOCK) ON o.BuyerId = b.Id 
LEFT JOIN Customers cu WITH(NOLOCK) ON o.CustomerId = cu.Id 
LEFT JOIN StyleDetails s WITH(NOLOCK) ON o.StyleId = s.Id  
