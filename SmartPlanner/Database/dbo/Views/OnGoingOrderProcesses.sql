﻿CREATE VIEW [dbo].[OnGoingOrderProcesses]
	AS 

	SELECT tna.OrderDetailsId,tna.TnADefaultId CurrentTnADefaultId,d.KeyProcess CurrentKeyProcess,tna.StartDate PlanStartDate,tna.EndDate PlanEndDate,
tna.ActualStartDate,cast(case when tna.ActualStartDate>tna.StartDate then 0 else 1 end as bit) PossibilityToDeliver
FROM TnACalendars tna with(nolock)
inner join(
select MAX(ActualStartDate) actualStart,OrderDetailsId
from TnACalendars with(nolock)
where ActualEndDate is null and ActualStartDate is not null
group by OrderDetailsId)l ON tna.ActualStartDate = l.actualStart and tna.OrderDetailsId = l.OrderDetailsId 
inner join TnADefaults d with(nolock) on tna.TnADefaultId = d.Id 
