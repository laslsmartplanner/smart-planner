﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.DataAccessLayer
{
    public class DataAccess
    {
        private SqlConnection sqlConnection = new SqlConnection();
        private SqlTransaction sqlTransaction = null;

        public void DBConnect()
        {
            if (!sqlConnection.State.Equals(ConnectionState.Closed))
                sqlConnection.Close();
            sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
            sqlConnection.Open();
        }
        public void BeginTransaction()
        {
            if (sqlConnection == null || !sqlConnection.State.Equals(ConnectionState.Open))
                DBConnect();
            sqlTransaction = sqlConnection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (sqlTransaction != null)
            {
                sqlTransaction.Commit();
                sqlTransaction = null;
            }
        }

        public void RollbackTransaction()
        {
            if (sqlTransaction != null)
            {
                sqlTransaction.Rollback();
                sqlTransaction = null;
            }
        }
        public void DBDisconnect()
        {
            if (sqlConnection.State.Equals(ConnectionState.Open))
                sqlConnection.Close();
            sqlConnection.Dispose();
        }

        public bool IsExists(SqlCommand sqlCommand)
        {
            SqlDataReader reader = this.ExecuteReader(sqlCommand);
            return reader.HasRows;
        }

        public SqlDataReader ExecuteReader(SqlCommand sqlCommand)
        {
            if (!sqlConnection.State.Equals(ConnectionState.Open))
                DBConnect();
            sqlCommand.Connection = sqlConnection;
            return sqlCommand.ExecuteReader();
        }

        public object ExecuteScalar(SqlCommand sqlCommand)
        {
            try
            {
                if (!sqlConnection.State.Equals(ConnectionState.Open))
                    DBConnect();
                sqlCommand.Connection = sqlConnection;
                if (sqlTransaction != null)
                    sqlCommand.Transaction = sqlTransaction;
                if (sqlCommand.CommandText.StartsWith("Insert into", StringComparison.OrdinalIgnoreCase))
                    sqlCommand.CommandText = $"{sqlCommand.CommandText} SELECT SCOPE_IDENTITY()";
                return sqlCommand.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (sqlTransaction == null)
                    DBDisconnect();
            }
        }

        public int ExecuteNonQuery(SqlCommand sqlCommand)
        {
            try
            {
                if (!sqlConnection.State.Equals(ConnectionState.Open))
                    DBConnect();
                sqlCommand.Connection = sqlConnection;
                if (sqlTransaction != null)
                    sqlCommand.Transaction = sqlTransaction;
                return sqlCommand.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (sqlTransaction == null)
                    DBDisconnect();
            }
        }
        public DataTable GetDataTable(SqlCommand sqlCommand)
        {
            try
            {
                if (!sqlConnection.State.Equals(ConnectionState.Open))
                    DBConnect();
                sqlCommand.Connection = sqlConnection;
                if (sqlTransaction != null)
                    sqlCommand.Transaction = sqlTransaction;
                SqlDataAdapter sda = new SqlDataAdapter(sqlCommand);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (sqlTransaction == null)
                    DBDisconnect();
            }
        }

        public DataSet GetDataSet(SqlCommand sqlCommand)
        {
            try
            {
                if (!sqlConnection.State.Equals(ConnectionState.Open))
                    DBConnect();
                sqlCommand.Connection = sqlConnection;
                if (sqlTransaction != null)
                    sqlCommand.Transaction = sqlTransaction;
                SqlDataAdapter sda = new SqlDataAdapter(sqlCommand);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (sqlTransaction == null)
                    DBDisconnect();
            }
        }
    }
}
