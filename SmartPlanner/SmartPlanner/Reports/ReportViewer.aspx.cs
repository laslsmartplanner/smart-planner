﻿using Microsoft.Reporting.WebForms;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPlanner.Reports
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        private ProductivityService _productivityService = new ProductivityService();
        private PlacedOrderService _placedOrderService = new PlacedOrderService();
        private StyleProductService _styleProductService = new StyleProductService();
        private StyleRevisionService _styleRevisionService = new StyleRevisionService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int reportNo = 0;
                int factoryId = -1;
                int lineId = -1;
                int styleId = -1;
                DateTime? fromDate = null;
                DateTime? toDate = null;
                int buyerId = -1;
                int customerId = -1;

                if (Request.QueryString["reportNo"] != null)
                {
                    reportNo = Convert.ToInt32(Request.QueryString["reportNo"]);
                }

                if (Request.QueryString["factoryId"] != null)
                {
                    factoryId = Convert.ToInt32(Request.QueryString["factoryId"]);
                }

                if (Request.QueryString["lineId"] != null)
                {
                    lineId = Convert.ToInt32(Request.QueryString["lineId"]);
                }

                if (Request.QueryString["styleId"] != null)
                {
                    styleId = Convert.ToInt32(Request.QueryString["styleId"]);
                }

                if (Request.QueryString["from"] != null && Request.QueryString["from"] != "")
                {
                    fromDate = Convert.ToDateTime(Request.QueryString["from"]);
                }

                if (Request.QueryString["to"] != null)
                {
                    toDate = Convert.ToDateTime(Request.QueryString["to"]);
                }

                if (Request.QueryString["buyerId"] != null)
                {
                    buyerId = Convert.ToInt32(Request.QueryString["buyerId"]);
                }

                if (Request.QueryString["customerId"] != null)
                {
                    customerId = Convert.ToInt32(Request.QueryString["customerId"]);
                }

                if (reportNo == 1)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptFactoryEfficiency.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 2)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptPlanVsActualProd.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 3)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptSewingSummaryByLineStyle.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 4)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptProductionProgress.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 5)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId, fromDate, toDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptProductionHistory.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 6)
                {
                    List<ReportDTOPlacedOrders> placedOrders = _placedOrderService.ListPlacedOrdersForReport(fromDate.Value, toDate.Value, factoryId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptOperationScheduleWeekly.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsPlanProduction", placedOrders);

                        List<ReportParameter> reportParameters = new List<ReportParameter>();
                        ReportParameter[] rptParametrList;
                        rptParametrList = new ReportParameter[2];

                        rptParametrList[0] = new ReportParameter("FactoryName", new FactoryService().GetEntityById(factoryId).FactoryName);
                        rptParametrList[1] = new ReportParameter("DateRange", fromDate.Value.ToString("yyyy-MM-dd") + " - " + toDate.Value.ToString("yyyy-MM-dd"));

                        for (int i = 0; i < rptParametrList.Length; i++)
                        {
                            reportParameters.Add(rptParametrList[i]);
                        }

                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.SetParameters(reportParameters);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 7)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptCapacityVsLoad.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 8)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, 0, 0, fromDate, toDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptDailyOutputPlan.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 9)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId, fromDate, toDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptPlannedHourSummary.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 10)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId, fromDate, toDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptSewingOutput.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 11)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId, fromDate, toDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptMonthlyFreezePlanVsCurrentPlan.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsOrderHistory", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 12)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId, fromDate, toDate);

                    double producedQty = 0;

                    foreach(Productivity productivity in productivities)
                    {
                        productivity.ProducedQty = producedQty + productivity.ProducedQty;

                        producedQty = productivity.ProducedQty;
                    }

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptCumulativeProduction.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 13)
                {
                    List<ReportDTOStyleSMV> styleProducts = _styleProductService.ListStyleSMV(buyerId, customerId, styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptStyleSMV.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsStyleSMV", styleProducts);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 14)
                {
                    List<StyleRevision> styleRevisions = _styleRevisionService.ListStyleSMV(styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptStyleRevision.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsStyleRevision", styleRevisions);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 15)
                {
                    StyleDetail styleDetail = new StyleDetailService().GetEntityById(styleId);
                    List<StyleProduct> styleProducts = new StyleProductService().ListForStyleId(styleId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptStyleDetails.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsStyleProducts", styleProducts);

                        List<ReportParameter> reportParameters = new List<ReportParameter>();
                        ReportParameter[] rptParametrList;
                        rptParametrList = new ReportParameter[7];

                        rptParametrList[0] = new ReportParameter("StyleNumber", styleDetail.StyleNumber);
                        rptParametrList[1] = new ReportParameter("BuyerName", styleDetail.Buyer.Name);
                        rptParametrList[2] = new ReportParameter("CustomerName", styleDetail.Customer.Name);
                        rptParametrList[3] = new ReportParameter("Division", styleDetail.Division.ConfigValue);
                        rptParametrList[4] = new ReportParameter("Department", styleDetail.Department);
                        rptParametrList[5] = new ReportParameter("Description", styleDetail.Description );
                        rptParametrList[6] = new ReportParameter("SampleStatus", styleDetail.SampleStatus.StatusName);


                        for (int i = 0; i < rptParametrList.Length; i++)
                        {
                            reportParameters.Add(rptParametrList[i]);
                        }

                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.SetParameters(reportParameters);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 16)
                {
                    List<Productivity> productivities = _productivityService.ListForSFactoryEfficiency(factoryId, lineId, styleId, fromDate, toDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptCumulativeProduction.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsFactoryEfficiency", productivities);
                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 17)
                {
                    List<ReportDTOPlacedOrders> placedOrders = _placedOrderService.ListPlacedOrdersForReport(fromDate.Value, toDate.Value, factoryId);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptCM.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsPlanProduction", placedOrders);

                        List<ReportParameter> reportParameters = new List<ReportParameter>();
                        ReportParameter[] rptParametrList;
                        rptParametrList = new ReportParameter[2];

                        rptParametrList[0] = new ReportParameter("FactoryName", new FactoryService().GetEntityById(factoryId).FactoryName);
                        rptParametrList[1] = new ReportParameter("DateRange", fromDate.Value.ToString("yyyy-MM-dd") + " - " + toDate.Value.ToString("yyyy-MM-dd"));

                        for (int i = 0; i < rptParametrList.Length; i++)
                        {
                            reportParameters.Add(rptParametrList[i]);
                        }

                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.SetParameters(reportParameters);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (reportNo == 18)
                {
                    List<ReportDTOProductivity> producedOrders = _productivityService.GetProductivityForReport(toDate.Value, factoryId);
                    int cumulativeProd = _productivityService.GetTotalProducedForFactoryId(toDate.Value, factoryId);

                    DateTime today = toDate.Value;
                    DateTime firstThisMonth = new DateTime(today.Year, today.Month, 1);

                    DateTime previousDate = _productivityService.GetPreviousWorkingDay(today);

                    double factoryEfficiency = _productivityService.GetPlantEffciencyForAMonth(factoryId, firstThisMonth, today);
                    double previousDateEfficiency = _productivityService.GetPlantEffciencyForAMonth(factoryId, previousDate, previousDate);

                    try
                    {
                        GPSReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RDLC/RptProductionEfficiency.rdlc");
                        GPSReportViewer.LocalReport.DataSources.Clear();
                        ReportDataSource rdc = new ReportDataSource("dsProduction", producedOrders);

                        List<ReportParameter> reportParameters = new List<ReportParameter>();
                        ReportParameter[] rptParametrList;
                        rptParametrList = new ReportParameter[5];

                        rptParametrList[0] = new ReportParameter("FactoryName", new FactoryService().GetEntityById(factoryId).FactoryName);
                        rptParametrList[1] = new ReportParameter("DateRange", toDate.Value.ToString("yyyy-MM-dd"));
                        rptParametrList[2] = new ReportParameter("CumulativeProd", cumulativeProd.ToString());
                        rptParametrList[3] = new ReportParameter("FactoryEfficiency", factoryEfficiency.ToString());
                        rptParametrList[4] = new ReportParameter("PreviousDayEfficiency", previousDateEfficiency.ToString());


                        for (int i = 0; i < rptParametrList.Length; i++)
                        {
                            reportParameters.Add(rptParametrList[i]);
                        }

                        GPSReportViewer.LocalReport.DataSources.Add(rdc);
                        GPSReportViewer.LocalReport.SetParameters(reportParameters);
                        GPSReportViewer.LocalReport.Refresh();
                        GPSReportViewer.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
        }
    }
}