﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="SmartPlanner.Reports.ReportViewer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="formCustomerReport" runat="server">  
    <div>  
        <asp:ScriptManager ID="ScriptManager1" runat="server">

        </asp:ScriptManager>
        
        <rsweb:reportviewer id="GPSReportViewer" runat="server" width="100%"></rsweb:reportviewer>  
    </div>  
</form>  
</body>
</html>
