﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class LineModulesController : Controller
    {
        private LineModuleService _lineModuleService = new LineModuleService();
        private FactoryService _facService = new FactoryService();

        // GET: LineModule
        public ActionResult Index()
        {
            return View(_lineModuleService.GetLineModules());
        }

        public ActionResult Detail(int? id)
        {
            LineModule lineModule = new LineModule();

            if (id.HasValue && id.Value > 0)
                lineModule = _lineModuleService.GetEntityById(id.Value);

            lineModule.FactoryNames = _facService.GetFactories();
            return View(lineModule);
        }

        public ActionResult Details(int id)
        {
            return View(_lineModuleService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Detail(LineModule lineModule)
        {
            try
            {
                lineModule.FactoryNames = _facService.GetFactories();
                if (!ModelState.IsValid)
                    return View(lineModule);

                bool isAdded = false;

                if (lineModule.Id == 0)
                    isAdded = true;

                if (!_lineModuleService.Save(lineModule) && !string.IsNullOrWhiteSpace(lineModule.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", lineModule.BrokenBusinessRulesText);
                    return View(lineModule);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Line Module added successfully";

                    LineModule newModule = new LineModule();
                    newModule.FactoryNames = _facService.GetFactories();
                    return View(newModule);
                }
                else
                {
                    ViewBag.Message = "Line Module updated successfully";
                    return View(lineModule);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(lineModule);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_lineModuleService.GetEntityById(id));
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _lineModuleService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this line module as involved with some actions");
                return View(_lineModuleService.GetEntityById(id));
            }
        }
    }
}