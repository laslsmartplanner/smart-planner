﻿using Microsoft.Reporting.WebForms;
using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class ReportController : Controller
    {
        private ProductivityService _productivityService = new ProductivityService();
        private StyleDetailService _styleDetailService = new StyleDetailService();
        private ConfigurationTableValueService _configService = new ConfigurationTableValueService();
        private LineModuleService _lineModuleService = new LineModuleService();
        private FactoryService _factoryService = new FactoryService();

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FactoryEfficiencyByStyle()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult PlanVsActualProduction()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult SewingSummaryByLineStyle()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult ProductionProgress()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult ProductionHistory()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult OperationScheduleWeekly()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult CapacityVsLoad()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult DailyOutputPlan()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult PlannedHourSummary()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult SewingOutput()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult MonthlyFreezePlanVsCurrentPlan()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult GetLines(int? factory)
        {
            if (!factory.HasValue) return null;

            List<LineModule> lines = _lineModuleService.ListForFactoryId(factory.Value);
            return Json(lines, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductionCumalative()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult StyleWiseSMV()
        {
            SMVBankDTO reportDTO = new SMVBankDTO();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Buyers = new BuyerService().ListBuyers("Name", true);
            reportDTO.Customers = new CustomerService().ListCustomers("Name", true);

            return View(reportDTO);
        }

        public ActionResult SMVRevisionHistory()
        {
            StyleRevision reportDTO = new StyleRevision();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            
            return View(reportDTO);
        }

        public ActionResult StyleDetails()
        {
            StyleRevision reportDTO = new StyleRevision();
            reportDTO.StyleDetails = _styleDetailService.GetList();

            return View(reportDTO);
        }

        public ActionResult CumulativeProduction()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult CM()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

        public ActionResult ProductionEfficiency()
        {
            ReportDTOFactoryEfficiency reportDTO = new ReportDTOFactoryEfficiency();
            reportDTO.StyleDetails = _styleDetailService.GetList();
            reportDTO.Factories = _factoryService.GetList();
            reportDTO.LineModules = new List<LineModule>();

            return View(reportDTO);
        }

    }
}