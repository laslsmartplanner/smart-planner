﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class BuyersController : Controller
    {
        // GET: Buyers

        private BuyerService _buyerService = new BuyerService();

        public ActionResult Index()
        {
            return View(_buyerService.ListBuyers("Name", false));
        }

        public ActionResult Detail(int? id)
        {
            Buyer buyer = new Buyer();

            if (id.HasValue && id.Value > 0)
                buyer = _buyerService.GetEntityById(id.Value);

            return View(buyer);
        }

        [HttpPost]
        public ActionResult Detail(Buyer buyer)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(buyer);

                bool isAdded = false;

                if (buyer.Id == 0)
                    isAdded = true;

                if (!_buyerService.Save(buyer) && !string.IsNullOrWhiteSpace(buyer.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", buyer.BrokenBusinessRulesText);
                    return View(buyer);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Buyer added successfully";

                    Buyer newBuyer = new Buyer();
                    
                    return View(newBuyer);
                }
                else
                {
                    ViewBag.Message = "Buyer updated successfully";
                    return View(buyer);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(buyer);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_buyerService.GetEntityById(id));
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _buyerService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this buyer as involved with some actions");
                return View(_buyerService.GetEntityById(id));
            }
        }


    }
}