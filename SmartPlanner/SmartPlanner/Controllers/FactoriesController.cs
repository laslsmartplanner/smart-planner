﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class FactoriesController : Controller
    {
        private FactoryService _factoryService = new FactoryService();

        private void SetUsers(Factory factory)
        {
            factory.Users = new UserService().ListUsers("FullName", true);
        }

        // GET: Factories
        public ActionResult Index()
        {
            return View(_factoryService.GetFactories());
        }
        public ActionResult Detail(int? id)
        {
            Factory factory = new Factory();

            if (id.HasValue && id.Value > 0)
                factory = _factoryService.GetEntityById(id.Value);

            this.SetUsers(factory);

            return View(factory);
        }

        [HttpPost]
        public ActionResult Detail(Factory factory)
        {
            try
            {
                if (factory != null)
                    this.SetUsers(factory);
                if (!ModelState.IsValid)
                    return View(factory);
            
                bool isAdded = false;

                if (factory.Id == 0)
                    isAdded = true;

                if (!_factoryService.Save(factory) && !string.IsNullOrWhiteSpace(factory.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", factory.BrokenBusinessRulesText);

                    return View(factory);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Factory name added successfully";
                    factory = new Factory();
                    this.SetUsers(factory);
                    return View(factory);
                }
                else
                {
                    ViewBag.Message = "Factory name updated successfully";
                    factory = _factoryService.GetEntityById(factory.Id);
                    if (factory != null)
                        this.SetUsers(factory);
                    return View(factory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(factory);
            }
        }

        public ActionResult Delete(int id)
        {
            Factory factory = _factoryService.GetEntityById(id);
            this.SetUsers(factory);
            return View(factory);
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _factoryService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this factory as involved with some actions");
                return View(_factoryService.GetEntityById(id));
            }
        }
    }
}