﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class CustomersController : Controller
    {

        private CustomerService _customerService = new CustomerService();

        private void SetConfigTablesValues(Customer customer, bool isEdit = false)
        {
            ConfigurationTableValueService configService = new ConfigurationTableValueService();
            UserService userService = new UserService();

            customer.SizesList = configService.ListByConfigId((int)EnumConfigurationTables.Size, true);
            customer.ColorsList = configService.ListByConfigId((int)EnumConfigurationTables.Colors, true);
            customer.ResponsibilitiesList = _customerService.GetCustomerResponsibilities(customer.Id);
            customer.Coordinators = userService.ListUsers("FullName",true);
            customer.Commercials = userService.ListUsers("FullName",true);
            customer.Factories = new FactoryService().ListAvailableFactoriesForCustomerResponsibilities(customer.Id);

            if (!isEdit)
            {
                customer.SizeList = new List<IdNameDTO>();
                customer.ColorList = new List<IdNameDTO>();
            }

        }

        public ActionResult Index()
        {
            return View(_customerService.ListCustomers("Name", false));
        }

        public ActionResult Detail(int? id)
        {
            Customer customer = new Customer();

            if (id.HasValue && id.Value > 0)
            {
                customer = _customerService.GetEntityById(id.Value);
                this.SetConfigTablesValues(customer, true);
            }
            else
            {
                //customer.SizeList = new List<IdNameDTO>();
                //customer.SizeList.Add("No size added");

                this.SetConfigTablesValues(customer);
            }

            return View(customer);
        }

        [HttpPost]
        public ActionResult Detail(Customer customer)
        {
            this.SetConfigTablesValues(customer, true);

            try
            {
                if (!ModelState.IsValid)
                {
                    customer.SizeList = new List<IdNameDTO>();
                    customer.ColorList = new List<IdNameDTO>();
                    customer.ResponsibilitiesList = new List<CustomerResponsibility>();
                    return View(customer);
                }
                    

                bool isAdded = false;

                if (customer.Id == 0)
                    isAdded = true;

                if (!_customerService.Save(customer) && !string.IsNullOrWhiteSpace(customer.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", customer.BrokenBusinessRulesText);

                    return View(customer);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Customer added successfully";

                    Customer newCustomer = new Customer();
                    this.SetConfigTablesValues(newCustomer, true);

                    return View(newCustomer);
                }
                else
                {
                    ViewBag.Message = "Customer updated successfully";
                    customer = _customerService.GetEntityById(customer.Id);
                    this.SetConfigTablesValues(customer, true);
                    return View(customer);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(customer);
            }
        }

        [HttpPost]
        public JsonResult AddSize()
        {
            int customerId = Convert.ToInt32(Request["Id"]);
            string sizeName = Convert.ToString(Request["SizeName"]);

            var sizesList = _customerService.GetEntityById(customerId).SizeList.ToList();

            if (_customerService.SaveSizesList(sizeName, customerId))
            {
                sizesList = _customerService.GetEntityById(customerId).SizeList.ToList();
                return new JsonResult { Data = new { list = sizesList, success = true, message = "Size added successfully." } };
            }
            else
            {
                return new JsonResult { Data = new { list = sizesList, success = false, message = "Unable to add the size." } };
            }

        }

        public ActionResult DeleteSize(int customerId, int customerSizeId)
        {
            _customerService.DeleteCustomerSize(customerSizeId);

            return RedirectToAction("Detail", new { id = customerId });
        }


        [HttpPost]
        public JsonResult AddColor()
        {

            int customerId = Convert.ToInt32(Request["Id"]);
            string colorName = Convert.ToString(Request["ColorName"]);

            var colorsList = _customerService.GetEntityById(customerId).ColorList.ToList();

            if (_customerService.SaveColorsList(colorName, customerId))
            {
                colorsList = _customerService.GetEntityById(customerId).ColorList.ToList();
                return new JsonResult { Data = new { list = colorsList, success = true, message = "Color added successfully." } };
            }
            else
            {
                return new JsonResult { Data = new { list = colorsList, success = false, message = "Unable to add the color." } };
            }
        }

        public ActionResult DeleteColor(int customerId, int customerColorId)
        {
            _customerService.DeleteCustomerColor(customerColorId);

            return RedirectToAction("Detail", new { id = customerId });
        }

        public ActionResult Delete(int id)
        {
            return View(_customerService.GetEntityById(id));
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _customerService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this customer as involved with some actions");
                return View(_customerService.GetEntityById(id));
            }
        }

        [HttpPost]
        public JsonResult AddResponsibility()
        {
            CustomerResponsibility res = new CustomerResponsibility();
            res.CustomerId = Convert.ToInt32(Request["Id"]);
            res.FactoryId = Convert.ToInt32(Request["FactoryId"]);
            res.CoordinatorId = Convert.ToInt32(Request["CoordinatorId"]);
            res.CommercialId = Convert.ToInt32(Request["CommercialId"]);

            var responsibilitesList = _customerService.GetCustomerResponsibilities(res.CustomerId).ToList();

            if (_customerService.AddCustomerResponsibility(res))
            {
                responsibilitesList = _customerService.GetCustomerResponsibilities(res.CustomerId).ToList();
                return new JsonResult { Data = new { list = responsibilitesList, success = true, message = "Responsibility added successfully." } };
            }
            else
            {
                return new JsonResult { Data = new { list = responsibilitesList, success = false, message = "Unable to add the responsibility." } };
            }
        }
        public ActionResult DeleteResponsibility(int customerId, int customerResponsibilityId)
        {
            _customerService.DeleteCustomerResponsibility(customerResponsibilityId);

            return RedirectToAction("Detail", new { id = customerId });
        }
    }
}