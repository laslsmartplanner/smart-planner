﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class TnANotificationsController : Controller
    {
        private DTOService _service = new DTOService();

        // GET: TnANotifications
        public ActionResult Index()
        {
            TnADetailsDTO dto = new TnADetailsDTO() { StartDate = DateTime.Today };
            return View(dto);
        }

        public ActionResult LoadNotifications(DateTime notificationDate)
        {
            return PartialView("_TnANotifications", _service.ListTnADetailsForLoggedUser(notificationDate));
        }
    }
}