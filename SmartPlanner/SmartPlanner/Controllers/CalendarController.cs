﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class CalendarController : Controller
    {
        private CalendarDayService _service = new CalendarDayService();
        // GET: Calendar
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetCalendarData(double? start, double? end)
        {
            JsonResult result = new JsonResult();

            try
            {
                List<CalendarDay> data = _service.GetCalendarDays(Extensions.GetUnixDateTime(start.Value), Extensions.GetUnixDateTime(end.Value));

                result = this.Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            return result;
        }

        [HttpPost]
        public JsonResult DeleteEvent(string id, double start)
        {
            _service.Delete(Convert.ToInt32(id), Extensions.GetUnixDateTime(start));
            return new JsonResult { Data = new { status = true } };
        }

        [HttpPost]
        public JsonResult SaveEvent(CalendarDay calendarDay)
        {
            calendarDay.Start = Extensions.GetUnixDateTime(calendarDay.StartValue.Value);
            calendarDay.End = Extensions.GetUnixDateTime(calendarDay.EndValue.Value);

            _service.Save(calendarDay);
            return new JsonResult { Data = new { status = true } };
        }

    }
}