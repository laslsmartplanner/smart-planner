﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class OperationsController : Controller
    {
        private OperationService _operationService = new OperationService();

        // GET: Operations
        public ActionResult Index()
        {
            return View(_operationService.ListOperations("OperationName",false));
        }


        public ActionResult Detail(int? id)
        {
            Operation operation = new Operation();

            if (id.HasValue && id.Value > 0)
            {
                operation = _operationService.GetEntityById(id.Value);
            }
            
            return View(operation);
        }

        [HttpPost]
        public ActionResult Detail(Operation operation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(operation);
                }

                bool isAdded = false;

                if (operation.Id == 0)
                    isAdded = true;

                if (!_operationService.Save(operation) && !string.IsNullOrWhiteSpace(operation.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", operation.BrokenBusinessRulesText);

                    return View(operation);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Operation added successfully";

                    Operation newOperation = new Operation();
                    return View(newOperation);
                }
                else
                {
                    ViewBag.Message = "Operation updated successfully";
                    operation = _operationService.GetEntityById(operation.Id);                   
                    return View(operation);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(operation);
            }
        }


        public ActionResult Delete(int id)
        {
            return View(_operationService.GetEntityById(id));
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _operationService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this customer as involved with some actions");
                return View(_operationService.GetEntityById(id));
            }
        }
    }
}