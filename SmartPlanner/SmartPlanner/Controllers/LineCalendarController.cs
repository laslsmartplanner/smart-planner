﻿using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.Attributes;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class LineCalendarController : Controller
    {
        private LineModuleService _lineModuleService = new LineModuleService();
        private CalendarDayService _calendarService = new CalendarDayService();
        // GET: LineCalendar
        public ActionResult Index()
        {
            return View(_lineModuleService.GetLineModules());
        }
        public ActionResult Detail(int id)
        {
            return View(@"~/Views/Calendar/Index.cshtml", _lineModuleService.GetEntityById(id));
        }
        [HttpPost]
        public JsonResult SaveEvent(CalendarDay calendarDay,string lineModuleId)
        {
            calendarDay.Start = Extensions.GetUnixDateTime(calendarDay.StartValue.Value);
            calendarDay.End = Extensions.GetUnixDateTime(calendarDay.EndValue.Value);

            if (calendarDay.OtMinutes > 0)
                _calendarService.SaveOtMinutes(calendarDay, lineModuleId);
            else
                _calendarService.SaveLineHoliday(calendarDay, lineModuleId);
            return new JsonResult { Data = new { status = true } };
        }
        [HttpPost]
        public JsonResult DeleteEvent(string id, double start, string lineModuleId)
        {
            _calendarService.DeleteLineHoliday(Convert.ToInt32(id), Extensions.GetUnixDateTime(start), lineModuleId);
            return new JsonResult { Data = new { status = true } };
        }
        public ActionResult GetCalendarData(double? start, double? end,string lineModuleId)
        {
            JsonResult result = new JsonResult();

            try
            {
                List<CalendarDay> data = _calendarService.GetLineCalendarDays(Extensions.GetUnixDateTime(start.Value), Extensions.GetUnixDateTime(end.Value), Convert.ToInt32(lineModuleId));

                result = this.Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            return result;
        }
    }
}