﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class UsersController : Controller
    {
        private UserService _userService = new UserService();
        private UserLevelService _userLevelService = new UserLevelService();
        // GET: Users
        public ActionResult Index()
        {
            return View(_userService.GetList());
        }


        // GET: Users/Create
        [AuthAccess(EnumAccessModules.UserManagement)]
        public ActionResult Detail(int? id,bool isMyProfile=false)
        {
            LITUser user = new LITUser();
            
            if (id.HasValue && id.Value > 0)
                user = _userService.GetEntityById(id.Value);

            user.UserLevelList = _userLevelService.GetList();
            if(isMyProfile)
                ViewBag.Message = "Password reset request sent.";
            TempData["MyProfile"] = isMyProfile;
            return View(user);
        }

        // POST: Users/Create
        [HttpPost]
        public ActionResult Detail(LITUser user)
        {
            try
            {
                bool isMyProfile = Convert.ToBoolean(TempData["MyProfile"] ?? false);
                user.UserLevelList = _userLevelService.GetList();
                if (!ModelState.IsValidField("UerName") || !ModelState.IsValidField("Email") || !ModelState.IsValidField("Phone") || !ModelState.IsValidField("FullName"))
                    return View(user);
                
                if(isMyProfile)
                {
                    LITUser oldUser = _userService.GetEntityById(user.Id);
                    if (oldUser != null)
                        user.UserLevelId = oldUser.UserLevelId;
                }

                if (user.Id == 0 && _userService.Create(user))
                {
                    ViewBag.Message = "User added successfully";
                    LITUser newUser = new LITUser()
                    {
                        UserLevelList = _userLevelService.GetList()
                    };
                    ModelState.Clear();
                    return View(newUser);
                }
                if (user.Id > 0 && _userService.UpdateUser(user))
                {
                    ViewBag.Message = string.Format("{0} updated successfully", isMyProfile ? "Profile": "User");

                    return View(user);
                }
                if (!string.IsNullOrWhiteSpace(user.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("",user.BrokenBusinessRulesText);
                    return View(user);
                }

                return RedirectToAction("Detail", "User", user.Id > 0 ? user.Id : int.MinValue);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(user);
            }
        }

        // GET: Users/Delete/5
        [AuthAccess(EnumAccessModules.UserManagement)]
        public ActionResult Delete(int id)
        {
            LITUser user = _userService.GetEntityById(id);
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _userService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this user as involved with some actions");
                LITUser user = _userService.GetEntityById(id);
                return View(user);
            }
        }

        [AuthAccess(EnumAccessModules.UserManagement)]
        public ActionResult ResetPassword(int id)
        {
            LITUser user = _userService.GetEntityById(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult ResetPassword(int id, FormCollection collection)
        {
            try
            {
                LITUser user = _userService.GetEntityById(id);
                _userService.ResetPassword(user);
                ModelState.Clear();
                ViewBag.Message = $"New password sent to the '{user.Email}' for the user '{user.UserName}'";
                return View("Index", _userService.GetList());
            }
            catch (Exception ex)
            {
                ViewBag.Message(ex.Message);
                return View();
            }
        }

        public ActionResult MyProfile()
        {
            GV.LoggedUser().UserLevelList = _userLevelService.GetList();
            TempData["MyProfile"] = true;
            return View("Detail", GV.LoggedUser());
        }

    }
}
