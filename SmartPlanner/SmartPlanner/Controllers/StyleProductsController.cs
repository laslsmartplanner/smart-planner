﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class StyleProductsController : Controller
    {
        private StyleProductService _styleProductService = new StyleProductService();
        private StyleDetailService _styleDetailService = new StyleDetailService();
        private SubCategoryService _subCategoryService = new SubCategoryService();
        private StyleFeatureService _styleFeatureService = new StyleFeatureService();
        private OperationService _operationService = new OperationService();
        private StyleOperationService _styleOperationService = new StyleOperationService();
        private StyleAdditionalFileService _styleAdditionalFileService = new StyleAdditionalFileService();

        private void SetConfigTablesValues(StyleProduct styleProduct, bool isEdit = false)
        {
            ConfigurationTableValueService configService = new ConfigurationTableValueService();

            styleProduct.ProductTypes = configService.ListByConfigId((int)EnumConfigurationTables.ProductType, true);
            styleProduct.ProductGroups = configService.ListByConfigId((int)EnumConfigurationTables.ProductGroup, true);
            styleProduct.ProductCategories = configService.ListByConfigId((int)EnumConfigurationTables.ProductCategory, true);
            styleProduct.Categories = configService.ListByConfigId((int)EnumConfigurationTables.Category, true);
            styleProduct.FeaturesList = configService.ListByConfigId((int)EnumConfigurationTables.FeatureList, true);
            styleProduct.Products = configService.ListByConfigId((int)EnumConfigurationTables.Product, true);
            styleProduct.OperationsList = _operationService.GetList();
            styleProduct.FileTypeList = new AdditionalFileTypeService().GetList();

            if (isEdit)
            {
                styleProduct.SubCategories = _subCategoryService.ListForCategoryId(styleProduct.CategoryId);
            }
            else
            {
                styleProduct.SubCategories = new List<SubCategory>();
                styleProduct.AdditionalInformationFileNames = new List<StyleAdditionalFile>();
                styleProduct.FeatureList = new List<StyleFeature>();
                styleProduct.OperationBreakdowns = new List<StyleOperation>();
            }

        }

        // GET: StyleProducts
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int? id)
        {
            StyleProduct styleProduct = new StyleProduct();
            
            if (id.HasValue && id.Value > 0)
            {
                styleProduct = _styleProductService.GetEntityById(id.Value);
                this.SetConfigTablesValues(styleProduct, true);
            }
            else
            {
                this.SetConfigTablesValues(styleProduct);
            }

            return View(styleProduct);
        }

        [HttpPost]
        public ActionResult Detail(StyleProduct styleProduct)
        {
            try
            {
                this.SetConfigTablesValues(styleProduct, true);

                if (!ModelState.IsValid)
                    return View(styleProduct);

                bool isAdded = false;

                if (styleProduct.Id == 0)
                    isAdded = true;

                if (!_styleProductService.Save(styleProduct) && !string.IsNullOrWhiteSpace(styleProduct.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", styleProduct.BrokenBusinessRulesText);
                    return View(styleProduct);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Product details added successfully";

                    StyleProduct newStyleProduct = new StyleProduct();
                    this.SetConfigTablesValues(newStyleProduct);

                    return View(newStyleProduct);
                }
                else
                {
                    ViewBag.Message = "Product detail updated successfully";
                    styleProduct = _styleProductService.GetEntityById(styleProduct.Id);
                    this.SetConfigTablesValues(styleProduct, true);
                    return View(styleProduct);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint 'AK_StyleDetails_StyleNumber_Buyer'."))
                    ModelState.AddModelError("", "This style number already created for this buyer.");
                else
                    ModelState.AddModelError("", ex.Message);
                return View(styleProduct);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_styleProductService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                int styleId = _styleProductService.GetEntityById(id).StyleId;

                _styleProductService.DeleteById(id);

                return RedirectToAction("Detail", "StyleDetails", new { id = styleId });
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this style detail as involved with some actions");
                return View(_styleProductService.GetEntityById(id));
            }
        }

        public ActionResult GetSubCategories(int? category)
        {
            if (!category.HasValue) return null;

            List<SubCategory> subCategories = _subCategoryService.ListForCategoryId(category.Value);
            return Json(subCategories, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStyleProducts(int? style)
        {
            if (!style.HasValue) return null;

            List<IdNameDTO> products = _styleProductService.ListIdNameForStyleId(style.Value);
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddAdditionalFile()
        {
            int styleProductId = Convert.ToInt32(Request["Id"]);
            int fileTypeId = Convert.ToInt32(Request["FileTypeId"]);

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];

                _styleProductService.SaveAdditionalInformations(file, styleProductId, fileTypeId);
            }
            List<StyleAdditionalFile> additionalFiles = new List<StyleAdditionalFile>();

            additionalFiles = _styleProductService.GetEntityById(styleProductId).AdditionalInformationFileNames;

            return new JsonResult { Data = new { list = additionalFiles, success = true, message = "File added successfully." } };
        }

        public ActionResult DeleteAdditionalFile(int styleProductId, int additionalFileId)
        {
            _styleAdditionalFileService.DeleteById(additionalFileId);

            return RedirectToAction("Detail", new { id = styleProductId });
        }

        [HttpPost]
        public JsonResult AddFeature()
        {

            int styleProductId = Convert.ToInt32(Request["Id"]);
            int featureId = Convert.ToInt32(Request["FeatureId"]);

            //if(_styleFeatureService.IsFeatureExists(styleProductId, featureName))
            //{
            //    var featuresList = _styleProductService.GetEntityById(styleProductId).FeatureList.ToList();
            //    return new JsonResult { Data = new { list = featuresList, success = false, message = "Feature already exists." } };
            //}

            StyleFeature styleFeature = new StyleFeature();
            styleFeature.StyleProductId = styleProductId;
            styleFeature.FeatureId = featureId;

            List<StyleFeature> featuresList = new List<StyleFeature>();

            if (_styleFeatureService.Save(styleFeature))
            {
                featuresList = _styleFeatureService.ListForStyleProductId(styleProductId);
                return new JsonResult { Data = new { list = featuresList, success = true, message = "Feature added successfully." } };
            }
            else
            {
                featuresList = _styleFeatureService.ListForStyleProductId(styleProductId);
                return new JsonResult { Data = new { list = featuresList, success = false, message = "Unable to add the feature." } };
            }
        }

        public ActionResult DeleteFeature(int styleProductId, int styleFeatureId)
        {
            _styleFeatureService.DeleteById(styleFeatureId);

            return RedirectToAction("Detail", new { id = styleProductId });
        }

        [HttpPost]
        public JsonResult AddOperation()
        {

            int styleProductId = Convert.ToInt32(Request["Id"]);
            int operationId = Convert.ToInt32(Request["OperationId"]);
            decimal smv = Convert.ToDecimal(Request["SMV"]);

            //if(_styleOperationService.IsOperationExists(styleProductId, operationName))
            //{
            //    var operationsList = _styleProductService.GetEntityById(styleProductId).OperationList.ToList();
            //    return new JsonResult { Data = new { list = operationsList, success = false, message = "Operation already exists." } };
            //}

            StyleOperation styleOperation = new StyleOperation();
            styleOperation.StyleProductId = styleProductId;
            styleOperation.OperationId = operationId;
            styleOperation.SMV = smv;

            List<StyleOperation> operationsList = new List<StyleOperation>();
            decimal totalProductionSMV = 0;

            if (_styleOperationService.Save(styleOperation))
            {
                operationsList = _styleOperationService.ListForStyleProductId(styleProductId);
                totalProductionSMV = _styleOperationService.GetTotalSMVForStyleProductId(styleProductId);

                return new JsonResult { Data = new { list = operationsList, totalSMV = totalProductionSMV, success = true, message = "Operation added successfully." } };
            }
            else
            {
                operationsList = _styleOperationService.ListForStyleProductId(styleProductId);
                totalProductionSMV = _styleOperationService.GetTotalSMVForStyleProductId(styleProductId);

                return new JsonResult { Data = new { list = operationsList, totalSMV = totalProductionSMV, success = false, message = "Unable to add the operation." } };
            }
        }

        public ActionResult DeleteOperation(int styleProductId, int styleOperationId)
        {
            _styleOperationService.DeleteOperation(styleProductId,styleOperationId);

            return RedirectToAction("Detail", new { id = styleProductId });
        }

        public ActionResult Operation(int? id)
        {
            StyleOperation styleOperation = new StyleOperation();

            if (id.HasValue && id.Value > 0)
            {
                styleOperation = _styleOperationService.GetEntityById(id.Value);
            }

            return View(styleOperation);
        }

        [HttpPost]
        public ActionResult Operation(StyleOperation styleOperation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(styleOperation);
                }

                bool isAdded = false;

                if (styleOperation.Id == 0)
                    isAdded = true;

                if (!_styleOperationService.Save(styleOperation) && !string.IsNullOrWhiteSpace(styleOperation.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", styleOperation.BrokenBusinessRulesText);
                    return View(styleOperation);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Operation added successfully";
                    return View(styleOperation);
                }
                else
                {
                    return RedirectToAction("Detail", new { id = styleOperation.StyleProductId });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                return View(styleOperation);
            }
        }

    }
}