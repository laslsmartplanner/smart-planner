﻿using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    public class RequestResetPasswordController : Controller
    {
        private UserService _userService = new UserService();

        // GET: RequestResetPassword
        public ActionResult Index(int? id)
        {
            if(id.HasValue && id.Value>0)
            {
                LITUser user = _userService.GetEntityById(id.Value);
                if (user != null)
                {
                    this.RequestPassword(user);
                    GV.LoggedUser().UserLevelList = new UserLevelService().GetList();
                    TempData["MyProfile"] = true;
                    //ViewBag.Message = "Password reset request sent";
                    //return View("Create", GV.LoggedUser);
                    return RedirectToAction("Detail", "Users", new { id = GV.LoggedUser().Id, isMyProfile = true });
                }
            }
            return View(new LITUser());
        }
        [HttpPost]
        public ActionResult Index(LITUser user)
        {
            try
            {
                if (!ModelState.IsValidField("UserName"))
                {
                    return View(user);
                }

                if (_userService.GetByName(user.UserName) == null)
                {
                    ModelState.AddModelError("", "Username is invalid");
                    //ViewBag.Message = "Invalid username";
                    return View(user);
                }

                this.RequestPassword(user);
                return RedirectToAction("Index", "Login");
            }
            catch (Exception)
            {

                throw;
            }

            
        }

        public void RequestPassword(LITUser user)
        {
            LITUser admin = _userService.GetByName("Admin");
            if (admin == null)
                RedirectToAction("Index", "Login");
            new Email().SendUserPasswordResetRequestEmail(admin.Email, user.UserName);
            TempData["ReLoginMessage"] = "Password reset request sent";
        }
    }
}