﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;
using SmartPlanner.BusinessLayer.Common;
using System.Text;
using Newtonsoft.Json.Linq;
using Extensions = SmartPlanner.BusinessLayer.Common.Extensions;
using System.Data;
using System.IO;
using SmartPlanner.BusinessLayer.Excel;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class OrderDetailsController : Controller
    {
        private OrderDetailService _orderDetailService = new OrderDetailService();
        private TnACalendarService _tnACalendarService = new TnACalendarService();
        private OrderCombinationService _orderCombinationService = new OrderCombinationService();
        // GET: OrderDetails

        private void SetConfigTablesValues(OrderDetail orderDetail)
        {
            ConfigurationTableValueService configService = new ConfigurationTableValueService();
            orderDetail.PlanningSeasons = configService.ListByConfigId((int)EnumConfigurationTables.PlanningSeason, true);
            orderDetail.DestinationMedias = configService.ListByConfigId((int)EnumConfigurationTables.DestinationMedia, true);
            orderDetail.Destinations = configService.ListByConfigId((int)EnumConfigurationTables.Destination, true);
            orderDetail.BuyGroups = configService.ListByConfigId((int)EnumConfigurationTables.BuyGroup, true);

            orderDetail.Vendors = configService.ListByConfigId((int)EnumConfigurationTables.Vendor, true);
            orderDetail.Divisions = configService.ListByConfigId((int)EnumConfigurationTables.Division, true);
            orderDetail.Products = configService.ListByConfigId((int)EnumConfigurationTables.Product, true);
            orderDetail.DistributeFromList = configService.ListByConfigId((int)EnumConfigurationTables.DistributeFrom, true);
            orderDetail.DeliverToList = configService.ListByConfigId((int)EnumConfigurationTables.DeliverTo, true);

            orderDetail.Methods = configService.ListByConfigId((int)EnumConfigurationTables.Method, true);
            orderDetail.FactoryNames = new FactoryService().GetList();
            orderDetail.POTypes = configService.ListByConfigId((int)EnumConfigurationTables.POType, true);
            orderDetail.TimeTables = configService.ListByConfigId((int)EnumConfigurationTables.TimeTable, true);

            orderDetail.Customers = new CustomerService().ListCustomers("Name",true);
            orderDetail.Buyers = new BuyerService().ListBuyers("Name",true);

            orderDetail.EmblishmentPlants = configService.ListByConfigId((int)EnumConfigurationTables.EmblishmentPlant, true);
            orderDetail.Users = new UserService().ListUsers("UserName", true);
            orderDetail.Styles = new StyleDetailService().GetList();

            orderDetail.WashingPlants = configService.ListByConfigId((int)EnumConfigurationTables.WashingPlant, true);
            orderDetail.PrintingPlants = configService.ListByConfigId((int)EnumConfigurationTables.PrintingPlant, true);
            orderDetail.Sizes = configService.ListByConfigId((int)EnumConfigurationTables.Size, true);
            orderDetail.PriceTypes = configService.ListByConfigId((int)EnumConfigurationTables.PriceType, true);
            orderDetail.SewingPlants = configService.ListByConfigId((int)EnumConfigurationTables.SewingPlant, true);
            orderDetail.SampleStatuses = configService.ListByConfigId((int)EnumConfigurationTables.SampleStatus, true);
            orderDetail.OrderInquiryStatuses = new OrderInquiryStatusService().GetList();
            orderDetail.Merchants = new UserService().ListUsers("FullName", true);
            orderDetail.Planners = new UserService().ListUsers("FullName", true);

            orderDetail.Colors = configService.ListByConfigId((int)EnumConfigurationTables.Colors, true);
            orderDetail.CustomerSizes = new List<string>();
            orderDetail.CustomerColors = new List<string>();

        }

        public ActionResult Index()
        {
            return View(_orderDetailService.ListOrders());
        }

        public ActionResult TnaIndex(int type)
        {
            ViewBag.Type = type;
            return View(_orderDetailService.ListForType(type));
        }

        public ActionResult CutIndex()
        {
            return View(_orderDetailService.ListOrders());
        }

        [AuthAccess(EnumAccessModules.OrderManagement)]
        public ActionResult Detail(int? id)
        {
            OrderDetail orderDetail = new OrderDetail();
            //Session["OrderSizeColorCombinations"] = null;

            if (id.HasValue && id.Value > 0)
            {
                orderDetail = _orderDetailService.GetEntityById(id.Value);
            }

            this.SetConfigTablesValues(orderDetail);

            return View(orderDetail);
        }

        [AuthAccess(EnumAccessModules.OrderManagement)]
        public ActionResult ViewDetail(int? id)
        {
            OrderDetail orderDetail = new OrderDetail();

            if (id.HasValue && id.Value > 0)
            {
                orderDetail = _orderDetailService.GetEntityById(id.Value);
            }

            this.SetConfigTablesValues(orderDetail);

            return View(orderDetail);
        }

        [AuthAccess(EnumAccessModules.TnAManagement)]
        public ActionResult TnaDetail(int? id, int? productId, int isTna = 1)
        {
            OrderDetail orderDetail = new OrderDetail();

            if (id.HasValue && id.Value > 0 && productId.HasValue && productId.Value > 0)
                orderDetail = _orderDetailService.GetEntityById(id.Value);

            orderDetail.ProductId = productId.Value;
            orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id, orderDetail.ProductId);

            this.SetConfigTablesValues(orderDetail);

            ViewBag.IsTna = isTna;

            return View(orderDetail);
        }

        [AuthAccess(EnumAccessModules.TnAManagement)]
        public ActionResult ViewTnaDetail(int? id, int? productId, int isTna = 1)
        {
            OrderDetail orderDetail = new OrderDetail();

            if (id.HasValue && id.Value > 0 && productId.HasValue && productId.Value > 0)
                orderDetail = _orderDetailService.GetEntityById(id.Value);

            orderDetail.ProductId = productId.Value;
            orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id, orderDetail.ProductId);

            this.SetConfigTablesValues(orderDetail);

            ViewBag.IsTna = isTna;

            return View(orderDetail);
        }

        [AuthAccess(EnumAccessModules.OrderManagementCut)]
        public ActionResult CutDetail(int? id)
        {
            OrderDetail orderDetail = new OrderDetail();
            //Session["OrderSizeColorCombinations"] = null;

            if (id.HasValue && id.Value > 0)
            {
                orderDetail = _orderDetailService.GetEntityById(id.Value);

                //if(!(orderDetail.OrderCombinations.Count.Equals(1) && orderDetail.OrderCombinations[0].ColorId.Equals(0)))
                //    Session["OrderSizeColorCombinations"] = orderDetail.OrderCombinations;
            }

            //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);
            this.SetConfigTablesValues(orderDetail);

            return View(orderDetail);
        }

        [AuthAccess(EnumAccessModules.OrderManagementCut)]
        public ActionResult ViewCutDetail(int? id)
        {
            OrderDetail orderDetail = new OrderDetail();

            if (id.HasValue && id.Value > 0)
            {
                orderDetail = _orderDetailService.GetEntityById(id.Value);
            }

            this.SetConfigTablesValues(orderDetail);

            return View(orderDetail);
        }

        [AuthAccess(EnumAccessModules.TnAManagement)]
        public ActionResult StyleProducts(int? id, int isTna = 0)
        {
            if (!id.HasValue || id.Value < 1) return View();

            OrderDetail orderDetail = _orderDetailService.GetEntityById(id.Value);

            List<StyleProduct> styleProducts = new StyleProductService().ListForStyleId(orderDetail.StyleId);
            ViewBag.OrderId = id.Value;

            ViewBag.IsTna = isTna;

            return View(styleProducts);
        }

        [HttpPost]
        public ActionResult Detail(OrderDetail orderDetail)
        {
            try
            {
                this.SetConfigTablesValues(orderDetail);

                if (!ModelState.IsValid)
                {
                   //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id, orderDetail.ProductId);
                    return View(orderDetail);
                }

                bool isAdded = false;

                if (orderDetail.Id == 0)
                    isAdded = true;

                //if (Session["OrderSizeColorCombinations"] != null)
                //{
                //    orderDetail.OrderCombinations = (List<OrderCombination>)Session["OrderSizeColorCombinations"];
                //}

                orderDetail.OrderInquiryStatusId = 1;

                if (!_orderDetailService.Save(orderDetail) && !string.IsNullOrWhiteSpace(orderDetail.BrokenBusinessRulesText))
                {
                    //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);
                    ModelState.AddModelError("", orderDetail.BrokenBusinessRulesText);
                    return View(orderDetail);
                }
                //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Order Detail added successfully";
                    return View(orderDetail);
                }
                else
                {
                    ViewBag.Message = "Order Detail updated successfully";
                    orderDetail = _orderDetailService.GetEntityById(orderDetail.Id);
                    this.SetConfigTablesValues(orderDetail);
                    return View(orderDetail);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
               // orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);
                return View(orderDetail);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_orderDetailService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _orderDetailService.DeleteById(id);

                return RedirectToAction("Index","OrderDetails");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this order detail as involved with some actions");

                return View(_orderDetailService.GetEntityById(id));
            }
        }


        //[AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetStyleNumbers(string buyerIntKey)
        {
            List<StyleDetail> styles = new StyleDetailService().ListForBuyerId(Convert.ToInt32(buyerIntKey));
            return Json(styles, JsonRequestBehavior.AllowGet);
        }

        #region TnACalendar related

        public ActionResult PlanningCalendar(int id, int orderId, int tnaId,int productId, int isTna = 0)
        {
            TnACalendar tnACalendar = null;
            //if (id > 0)
            //    tnACalendar = _tnACalendarService.GetEntityById(id);
            //else
            tnACalendar = _tnACalendarService.GetCalendarForOrder(orderId, tnaId, productId);
            ViewBag.IsTna = isTna;

            //if (!tnACalendar.CanAddActualStartDate)
            //    ViewBag.Message = "Actual start date has not added to the previous planned order for the line module.";

            return View(tnACalendar);
        }
        [HttpPost]
        public ActionResult PlanningCalendar(TnACalendar tnACalendar, int isTna, FormCollection collection)
        {
            //if (!ModelState.IsValidField("StartDate") || !ModelState.IsValidField("EndDate") || !ModelState.IsValidField("ActualStartDate"))
            //    return View(tnACalendar);

            if (tnACalendar.Id < 1)
            {
                TnACalendar cal = _tnACalendarService.GetCalendarForOrder(tnACalendar.OrderDetailsId, tnACalendar.TnADefaultId, tnACalendar.ProductId);
                if (cal != null)
                {
                    tnACalendar.Id = cal.Id;
                    tnACalendar.PlannedStartDate = cal.StartDate;
                    tnACalendar.PlannedEndDate = cal.EndDate;
                }
            }
            //tnACalendar.SetEndDate();
            tnACalendar.Updated = 1;

            if (tnACalendar.ActualStartDate.HasValue && tnACalendar.TnADefaultId.Equals(16))
                tnACalendar.IsActualStartInHalfDay = new TnACalendarService().CanStartInHalfDay(tnACalendar.OrderDetailsId, tnACalendar.ProductId, tnACalendar.ActualStartDate.Value);
            else
                tnACalendar.IsActualStartInHalfDay = false;

            if (!tnACalendar.ActualEndDate.HasValue)
                tnACalendar.IsActualEndInHalfDay = false;

            if (!_tnACalendarService.Save(tnACalendar) && !string.IsNullOrWhiteSpace(tnACalendar.BrokenBusinessRulesText))
                ModelState.AddModelError("", tnACalendar.BrokenBusinessRulesText);
            ViewBag.IsTna = isTna;
            tnACalendar.CanAddActualStartDate = true;
            return View(tnACalendar);

            //return RedirectToAction("PlanningCalendar", new { id = tnACalendar.Id, orderId = tnACalendar.OrderDetailsId, tnaId = tnACalendar.TnADefaultId, productId = tnACalendar.ProductId, isTna = isTna });


            //if (isTna.Equals(1))
            //{
            //    return RedirectToAction("TnaDetail", new { id = tnACalendar.OrderDetailsId, productId = tnACalendar.ProductId });
            //}
            //else
            //{
            //    if (isTna.Equals(2))
            //        return RedirectToAction("Index", "TnaNotifications");
            //    else
            //        return RedirectToAction("Detail", new { id = tnACalendar.OrderDetailsId });
            //}
        }

        public ActionResult GetStartDate(double end , int days)
        {
            //TnACalendar tna = new TnACalendar() { StartDate = Extensions.GetUnixDateTime(start), Duration = days };
            //tna.SetEndDate();
            //return this.Json(tna, JsonRequestBehavior.AllowGet);
            System.DateTime dtJSStart = new System.DateTime(1970, 1, 1);

            DataRow dr = new TnACalendarService().GetStartDate(Extensions.GetUnixDateTime(end), days);

            DateTime startDate = Convert.ToDateTime(dr["StartDate"]);
            

            //Convert DateTime of .Net to Javascript's Date object
            //Find the difference between today's date to 1st Jan 1970 in milliseconds
            return this.Json(new { start = Convert.ToUInt64(startDate.Subtract(dtJSStart).TotalMilliseconds), startString=startDate.ToString(GV.DisplayDateFormat), holidays = Convert.ToDouble( dr["TotalHolidays"]) }, JsonRequestBehavior.AllowGet);
        }
         
        public ActionResult LoadUsers(int id)
        {
            List<LITUser> users = new UserService().ListUsers("UserName",true);
            StringBuilder sb = new StringBuilder();
            foreach(LITUser user in users)
                sb.Append($"'{user.Id}':'{user.FullName}',");
            //LITUser selected = users.FirstOrDefault(x => x.Id == id);
            //sb.Append($"'selected':'1'");
            return Content("{"+ sb.ToString() + "}");
        }

        public ActionResult SaveResponsibility(int id, int tnaDefaultId, int value,int orderId,int productId)
        {            
            _tnACalendarService.SaveResponsibility(id, Convert.ToInt32(value), tnaDefaultId, orderId,productId);

            LITUser user = new UserService().GetEntityById(value);

            var response = new { value, status = true, name = user?.FullName ?? string.Empty };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }

        public ActionResult GetHolidays(double start, double end,int orderId,int productId, int tnaDefaultId = 0)
        {
            double holidays = 0;
            double duration = 0;
            if (start > 0 && end > 0)
            {
                DateTime startDate = Extensions.GetUnixDateTime(start);
                DateTime endDate = Extensions.GetUnixDateTime(end);
                holidays = new CalendarDayService().GetCompanyHolidaysCount(startDate, endDate);

                bool startInHd = false;

                if(tnaDefaultId.Equals(16))
                    startInHd = new TnACalendarService().CanStartInHalfDay(orderId, productId, startDate);

                duration = (endDate - startDate).TotalDays - holidays + 1;
                duration = startInHd ? duration - 0.5 : duration;
                //duration = endInHalfDay ? duration - 0.5 : duration;
            }

            return this.Json(new { holidays, duration }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPlanDuration(double start, double end, int orderId, int productId, int tnaDefaultId = 0)
        {
            double holidays = 0;
            double duration = 0;
            if (start > 0 && end > 0)
            {
                DateTime startDate = Extensions.GetUnixDateTime(start);
                DateTime endDate = Extensions.GetUnixDateTime(end);
                holidays = new CalendarDayService().GetCompanyHolidaysCount(startDate, endDate);

                duration = Math.Ceiling((endDate - startDate).TotalDays - holidays + 1);
            }

            return this.Json(new { holidays, duration }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public JsonResult AddSizeColorCombinations()
        {
            List<OrderCombination> orderCombinations = new List<OrderCombination>();

            int orderDetailId = Convert.ToInt32(Request["OrderDetailId"]);
            int qty = Convert.ToInt32(Request["Qty"]);
            string colorName = Convert.ToString(Request["ColorName"]);
            int styleProductId = Convert.ToInt32(Request["StyleProductId"]);
            string sizeName = Convert.ToString(Request["SizeName"]);
            int planDeliveryQty = Convert.ToInt32(Request["PlanDeliveryQty"]);

            OrderCombination newOrderCombination = new OrderCombination();
            newOrderCombination.OrderDetailsId = orderDetailId;
            newOrderCombination.ColorName = colorName;
            newOrderCombination.SizeName = sizeName;
            newOrderCombination.StyleProductId = styleProductId;
            newOrderCombination.Qty = qty;
            newOrderCombination.PlanDeliveryQty = planDeliveryQty;

            OrderDetail orderDetail = new OrderDetail();

            if (_orderCombinationService.Save(newOrderCombination))
            {
                orderCombinations = _orderCombinationService.ListForOrderDetailId(orderDetailId);
                orderDetail = _orderDetailService.GetEntityById(orderDetailId);

                return new JsonResult { Data = new { list = orderCombinations, productQtyList = orderDetail.ProductQtySumForOrder, success = true, message = "Combination added successfully." } };
            }
            else
            {
                orderCombinations = _orderCombinationService.ListForOrderDetailId(orderDetailId);
                orderDetail = _orderDetailService.GetEntityById(orderDetailId);
                return new JsonResult { Data = new { list = orderCombinations, productQtyList = orderDetail.ProductQtySumForOrder, success = false, message = "Unable to add the combination." } };
            }

        }

        [HttpPost]
        public JsonResult UpdatePlanEfficiency()
        {
            int orderDetailId = Convert.ToInt32(Request["OrderDetailId"]);
            int productId = Convert.ToInt32(Request["ProductId"]);
            float planEfficiency = Convert.ToInt32(Request["PlanEfficiency"]);

            _tnACalendarService.UpdatePlanEfficiency(orderDetailId, productId, planEfficiency);

            return new JsonResult { Data = new { success = true, message = "First day efficiency added successfully." } };



        }

        public ActionResult DeleteSizeColorCombinations(int orderDetailId, int splitCombinationId)
        {
            _orderCombinationService.DeleteById(splitCombinationId);

            return RedirectToAction("Detail", new { id = orderDetailId });
        }

        public ActionResult Combination(int? id)
        {
            OrderCombination orderCombination = new OrderCombination();

            if (id.HasValue && id.Value > 0)
            {
                orderCombination = _orderCombinationService.GetEntityById(id.Value);

                OrderDetail orderDetail = _orderDetailService.GetEntityById(orderCombination.OrderDetailsId);
                orderCombination.Wastage = orderDetail.Wastage;
            }

            return View(orderCombination);
        }

        [HttpPost]
        public ActionResult Combination(OrderCombination orderCombination)
        {
            try
            {
                

                if (!ModelState.IsValid)
                {
                    return View(orderCombination);
                }

                bool isAdded = false;

                if (orderCombination.Id == 0)
                    isAdded = true;

                if (!_orderCombinationService.Save(orderCombination) && !string.IsNullOrWhiteSpace(orderCombination.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", orderCombination.BrokenBusinessRulesText);
                    return View(orderCombination);
                }
                //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Order combination added successfully";
                    return View(orderCombination);
                }
                else
                {
                    if (Helper.GetAccessType(Enums.EnumAccessModules.OrderManagementCut).Equals(Enums.UserAccessType.ReadWrite) && !Helper.GetAccessType(Enums.EnumAccessModules.OrderManagement).Equals(Enums.UserAccessType.ReadWrite))
                        return RedirectToAction("CutDetail", new { id = orderCombination.OrderDetailsId });
                    else
                        return RedirectToAction("Detail", new { id = orderCombination.OrderDetailsId });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                
                return View(orderCombination);
            }
        }

        [HttpPost]
        public ActionResult ExcelUpload(int orderId)
        {
            foreach (string inputTagName in Request.Files)
            {
                HttpPostedFileBase infile = Request.Files[inputTagName];
                if (infile.ContentLength > 0 && (Path.GetExtension(infile.FileName) == ".xls" || Path.GetExtension(infile.FileName) == ".xlsx" || Path.GetExtension(infile.FileName) == ".xlsm"))
                {
                    OrderDetail order = new OrderDetailService().GetEntityById(orderId);
                    _orderCombinationService.ImportOrderCombination(infile, order.StyleId, order.CustomerId, orderId);

                    List<OrderCombination> orderCombinations = _orderCombinationService.ListForOrderDetailId(orderId);
                    OrderDetail orderDetail = _orderDetailService.GetEntityById(orderId);
                    return new JsonResult { Data = new { list = orderCombinations, productQtyList = orderDetail.ProductQtySumForOrder, success = true, message = "Combination added successfully." } };
                    
                }

            }
            return Json(new
            {
                Valid = false,
                ErrorMessage = "Please try again."
            });
        }
    }
}