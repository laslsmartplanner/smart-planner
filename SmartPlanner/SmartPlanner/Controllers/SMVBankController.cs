﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class SMVBankController : Controller
    {
        private StyleDetailService _styleDetailService = new StyleDetailService();
        private StyleProductService _styleProductService = new StyleProductService();

        private void SetConfigTablesValues(SMVBankDTO smvBank)
        {
            ConfigurationTableValueService configService = new ConfigurationTableValueService();

            smvBank.Buyers = new BuyerService().ListBuyers("Name", true);
            smvBank.Customers = new CustomerService().ListCustomers("Name", true);
            //smvBank.Categories = configService.ListByConfigId((int)EnumConfigurationTables.Category, true);
            //smvBank.SubCategories = new List<SubCategory>();
            smvBank.StyleDetails = _styleDetailService.GetList();
            smvBank.StyleProducts = new List<StyleProduct>();

        }

        // GET: SMVBank
        public ActionResult Index()
        {
            SMVBankDTO smvBank = new SMVBankDTO();

            this.SetConfigTablesValues(smvBank);

            return View(smvBank);
        }

        public ActionResult Inquiry()
        {
            List<StyleRevision> styleRevisions = new StyleRevisionService().GetList();
            return View(styleRevisions);
        }


        public ActionResult LoadStyleProducts(int styleProductId)
        {
            return PartialView("_StyleDetail", _styleProductService.GetEntityById(styleProductId));
        }

        public ActionResult GetStyleList(int? buyerId, int? customerId)
        {
            List<IdNameDTO> styles = _styleDetailService.ListForSMV(buyerId.Value, customerId.Value);
            return Json(styles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStyleDetailList(int? buyerId, int? customerId)
        {
            List<StyleDetail> styles = _styleDetailService.ListForBuyerCustomer(buyerId.Value, customerId.Value);
            return Json(styles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CalculateChangeOver(int product1Id, int product2Id)
        {
            int changeOver = 0;

            StyleProduct product1 = _styleProductService.GetEntityById(product1Id);
            StyleProduct product2 = _styleProductService.GetEntityById(product2Id);


            if (product1.ProductId != product2.ProductId || product1.CategoryId != product2.CategoryId || product1.SubCategoryId != product2.SubCategoryId)
            {
                changeOver = 100;
            }
            else if (product1.FeatureList.Count.Equals(0) || product2.FeatureList.Count.Equals(0) || product1.OperationBreakdowns.Count.Equals(0) || product2.OperationBreakdowns.Count.Equals(0))
            {
                changeOver = 100;
            }
            else
            {
                int commonFeatures = product1.FeatureList.Intersect(product2.FeatureList).Count();

                int commonOperations = product1.OperationBreakdowns.Intersect(product2.OperationBreakdowns).Count();

                changeOver = ((commonFeatures * 50) / product2.FeatureList.Count) + ((commonOperations * 50) / product2.OperationBreakdowns.Count);

            }

            return Json(changeOver, JsonRequestBehavior.AllowGet);
        }
    }
}