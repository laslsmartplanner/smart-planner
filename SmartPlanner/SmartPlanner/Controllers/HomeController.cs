﻿using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.DTOs;
using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class HomeController : Controller
    {
        ProductivityService _productivityService = new ProductivityService();

        public ActionResult Index(string message="")
        {
            
            DashBoardDTO dashboardDTO = new DashBoardDTO();
            dashboardDTO.FactoryNames = new FactoryService().GetFactories();
            dashboardDTO.FactoryNameId = new CompanyService().GetCompany().DefaultFactoryId;
            
            ViewBag.Message = message;
            return View(dashboardDTO);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult LoadDashboardStats(int factoryId)
        {
            DashBoardDTO dashboardDTO = new DashBoardDTO();

            this.SetChartData(dashboardDTO, factoryId);


            return PartialView("_DashboardStats", dashboardDTO);
        }

        private void SetChartData(DashBoardDTO dashboardDTO, int factoryId)
        {
            dashboardDTO.LineEfficiencyChartData = new List<double>();
            dashboardDTO.LineEfficiencyChartLabelPoint = new List<string>();

            DateTime today = DateTime.Today;
            DateTime firstThisMonth = new DateTime(today.Year, today.Month, 1);
            
            DateTime month1 = firstThisMonth.AddMonths(-5);
            DateTime month2 = firstThisMonth.AddMonths(-4);
            DateTime month3 = firstThisMonth.AddMonths(-3);
            DateTime month4 = firstThisMonth.AddMonths(-2);
            DateTime month5 = firstThisMonth.AddMonths(-1);
            DateTime month6 = firstThisMonth;


            dashboardDTO.LineEfficiencyChartLabelPoint.Add(month1.ToString("MMMM"));
            dashboardDTO.LineEfficiencyChartLabelPoint.Add(month2.ToString("MMMM"));
            dashboardDTO.LineEfficiencyChartLabelPoint.Add(month3.ToString("MMMM"));
            dashboardDTO.LineEfficiencyChartLabelPoint.Add(month4.ToString("MMMM"));
            dashboardDTO.LineEfficiencyChartLabelPoint.Add(month5.ToString("MMMM"));
            dashboardDTO.LineEfficiencyChartLabelPoint.Add(month6.ToString("MMMM"));

            dashboardDTO.LineEfficiencyChartData.Add(_productivityService.GetPlantEffciencyForAMonth(factoryId, month1, new DateTime(month1.Year, month1.Month, DateTime.DaysInMonth(month1.Year, month1.Month))));
            dashboardDTO.LineEfficiencyChartData.Add(_productivityService.GetPlantEffciencyForAMonth(factoryId, month2, new DateTime(month2.Year, month2.Month, DateTime.DaysInMonth(month2.Year, month2.Month))));
            dashboardDTO.LineEfficiencyChartData.Add(_productivityService.GetPlantEffciencyForAMonth(factoryId, month3, new DateTime(month3.Year, month3.Month, DateTime.DaysInMonth(month3.Year, month3.Month))));
            dashboardDTO.LineEfficiencyChartData.Add(_productivityService.GetPlantEffciencyForAMonth(factoryId, month4, new DateTime(month4.Year, month4.Month, DateTime.DaysInMonth(month4.Year, month4.Month))));
            dashboardDTO.LineEfficiencyChartData.Add(_productivityService.GetPlantEffciencyForAMonth(factoryId, month5, new DateTime(month5.Year, month5.Month, DateTime.DaysInMonth(month5.Year, month5.Month))));
            dashboardDTO.LineEfficiencyChartData.Add(_productivityService.GetPlantEffciencyForAMonth(factoryId, month6, today));

            // For stat labels
            dashboardDTO.FactoryEfficiency = _productivityService.GetPlantEffciencyForAMonth(factoryId, month6, today);
            dashboardDTO.OnTimeDelivery = new TnADefaultService().GetOnTimeDelivery(factoryId, firstThisMonth, today);
            dashboardDTO.CutToShipRatio = new OrderDetailService().GetCutToShipRation(factoryId, firstThisMonth, today);

        }

        [HttpPost]
        public JsonResult SendPlannerReminder()
        {
            int factoryId = Convert.ToInt32(Request["FactoryId"]);

            DataTable notPlacedOrders = new OrderDetailService().ListOrdersNotPlaced(factoryId);

            string orderList = "<table style='border: 1px solid black;'>";
            orderList = orderList + "<thead><tr>";
            orderList = orderList + "<th style='border: 1px solid black;'>Buyer</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Customer</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Style</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>PONumber</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Order Ref.</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Order Qty</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Repeat</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Shipment Date</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>CM</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Current Process</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Possibility To Deliver</th>";
            orderList = orderList + "<th style='border: 1px solid black;'>Merchant</th>";
            orderList = orderList + "</tr></thead>";
            orderList = orderList + "<tbody>";

            foreach (DataRow dr in notPlacedOrders.Rows)
            {
                orderList = orderList + "<tr>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["Buyer"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["Customer"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["StyleNumber"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["CustomerPONumber"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["SalesOrderNumber"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["OrderQty"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["Repeat"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + Convert.ToDateTime(dr["ShipmentDate"]).ToString(GV.DisplayDateFormat) + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["CM"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["CurrentKeyProcess"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["PossibilityToDeliver"] + "</td>";
                orderList = orderList + "<td style='border: 1px solid black;'>" + dr["Merchant"] + "</td>";
                orderList = orderList + "</tr>";

            }

            orderList = orderList + "</tbody></table>";

            EmailPlanReminderDTO dto = new EmailPlanReminderDTO()
            {
                User = new RoleService().GetEntityById(17).UserName,
                OrdersList = orderList,
                
            };

            new Email().SendEmailPlanReminder(dto, (int)EnumEmailType.NotPlacedOrderReminder);

            return new JsonResult { Data = new { success = true, message = "Reminder to the planner sent successfully." } };
        }
    }
}