﻿using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;
using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.Attributes;
using Newtonsoft.Json.Linq;
using Extensions = SmartPlanner.BusinessLayer.Common.Extensions;
using SmartPlanner.BusinessLayer.DTOs;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class ProductivityController : Controller
    {
        ProductivityService _productivityService = new ProductivityService();
        ConfigurationTableValueService _configService = new ConfigurationTableValueService();
        LineModuleService _lineModuleService = new LineModuleService();
        FactoryService _facService = new FactoryService();
        DTOService _dtoService = new DTOService();

        private void SetConfigTableValues(Productivity productivity, bool isEdit = false)
        {
            if (isEdit && productivity.WorkingDate.HasValue)
            {
                productivity.Factories = _productivityService.GetFactoriesInBoard(productivity.WorkingDate.Value);
                productivity.LineModules = _productivityService.GetLinesInBoard(productivity.WorkingDate.Value, productivity.FactoryId);
                productivity.LineOutputs = _productivityService.GetLineOutputs(productivity.FactoryId, productivity.LineModuleId, productivity.WorkingDate.Value, productivity.Id);
                productivity.Orders = _productivityService.GetOrderDetailForLineModuleInBoard(productivity.WorkingDate.Value, productivity.FactoryId, productivity.LineModuleId, productivity.OrderId);
                productivity.Products = _productivityService.GetProductsInBoard(productivity.WorkingDate.Value, productivity.FactoryId, productivity.LineModuleId, productivity.OrderId);
            }
            else
            {
                productivity.Factories = new List<IdNameDTO>();
                productivity.LineModules = new List<IdNameDTO>();
                productivity.LineOutputs = new List<LineOutput>();
                productivity.Orders = new List<IdNameDTO>();
                productivity.Products = new List<IdNameDTO>();
            }
        }

        // GET: Productivity
        public ActionResult Index()
        {
            return View(_productivityService.ListProductivities());
        }

        public ActionResult Detail(int? id)
        {
            Productivity productivity = new Productivity();

            if (id.HasValue && id.Value > 0)
            {
                productivity = _productivityService.GetEntityById(id.Value);
                this.SetConfigTableValues(productivity, true);
            }
            else
                this.SetConfigTableValues(productivity);
            return View(productivity);
        }

        public ActionResult ViewDetail(int? id)
        {
            Productivity productivity = new Productivity();

            if (id.HasValue && id.Value > 0)
            {
                productivity = _productivityService.GetEntityById(id.Value);
                this.SetConfigTableValues(productivity, true);
            }
            else
                this.SetConfigTableValues(productivity);
            return View(productivity);
        }

        [HttpPost]
        public ActionResult Detail(Productivity productivity)
        {
            try
            {
                this.SetConfigTableValues(productivity, true);
                if (!ModelState.IsValid)
                    return View(productivity);

                bool isAdded = false;
                if (productivity.Id == 0)
                    isAdded = true;

                if (productivity.Order != null)
                    productivity.StyleId = productivity.Order.StyleId;

                if (!_productivityService.Save(productivity) && !string.IsNullOrWhiteSpace(productivity.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", productivity.BrokenBusinessRulesText);
                    return View(productivity);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Productivity Detail added successfully";
                }
                else
                    ViewBag.Message = "Productivity Detail updated successfully";

                //return View(productivity);

                if(isAdded)
                {
                    return RedirectToAction("Detail", new { id = productivity.Id });
                }
                else
                {
                    return RedirectToAction("Detail", new { id = "" });
                }
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(productivity);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_productivityService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Productivity productivity = _productivityService.GetEntityById(id);
                _productivityService.DeleteProductivity(productivity);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this productivity detail as involved with some actions");
                return View(_productivityService.GetEntityById(id));
            }
        }

        public ActionResult GetFactories(double? date)
        {
            if (double.IsNaN(date.Value)) return null;
            List<IdNameDTO> factories = _productivityService.GetFactoriesInBoard(Extensions.GetUnixDateTime(date.Value));
            return Json(factories, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetLines(double? date, int? factory)
        {
            if (double.IsNaN(date.Value) || !factory.HasValue) return null;
            List<IdNameDTO> factories = _productivityService.GetLinesInBoard(Extensions.GetUnixDateTime(date.Value), factory.Value);
            return Json(factories, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOrderDetails(double? date, int? factory, int lineId)
        {
            if (double.IsNaN(date.Value) || !factory.HasValue) return null;
            DateTime workingDate = Extensions.GetUnixDateTime(date.Value);
            List<IdNameDTO> factories = _productivityService.GetOrderDetailForLineModuleInBoard(workingDate, factory.Value, lineId);

            //List<LineOutput> outputs = _productivityService.GetLineOutputs(factory.Value, lineId, workingDate);
            return Json(factories, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOrdersInBoard(double? date, int? factory, int? lineId)
        {
            if (double.IsNaN(date.Value) || !factory.HasValue || !lineId.HasValue) return null;
            DateTime workingDate = Extensions.GetUnixDateTime(date.Value);
            List<IdNameDTO> styles = _productivityService.GetOrdersInBoard(workingDate, factory.Value, lineId.Value);

            //List<LineOutput> outputs = _productivityService.GetLineOutputs(factory.Value, lineId, workingDate);
            return Json(styles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductsInBoard(double? date, int? factory, int? lineId, int? orderId)
        {
            if (double.IsNaN(date.Value) || !factory.HasValue || !lineId.HasValue || !orderId.HasValue) return null;
            DateTime workingDate = Extensions.GetUnixDateTime(date.Value);
            List<IdNameDTO> products = _productivityService.GetProductsInBoard(workingDate, factory.Value, lineId.Value, orderId.Value);

            //List<LineOutput> outputs = _productivityService.GetLineOutputs(factory.Value, lineId, workingDate);
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveOutpt(int id, double fromTime, double toTime, double value)
        {
            var status = true;
            var message = "";

            LineOutput lineOutput = new LineOutput()
            {
                ProductivityId = id,
                FromTime = fromTime,
                ToTime = toTime,
                ProducedQty = value
            };

            Productivity productivity = _productivityService.GetEntityById(id);
            double totalProduced = _productivityService.GetTotalProducedForOrderId(productivity.OrderId,productivity.ProductId) - productivity.ProducedQty;

            if (productivity != null && productivity.Order != null && totalProduced + value > productivity.Order.PlanDeliveryQty)
            {
                status = false;
                double balanceAllowed = productivity.Order.PlanDeliveryQty - totalProduced;
                message = "Balance allowed is " + balanceAllowed + ". Produced quantity added exceeds the order cut quantity." ;
            }
            else
            {
                _productivityService.AddLineOutput(lineOutput, productivity);

                productivity = _productivityService.GetEntityById(id);
            }

            var response = new { value, status, message, productivity.ProducedQty };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }

        [HttpPost]
        public ActionResult ResetUpdateMode(double? date, int? factoryId, int? lineId, int? orderId, int? productId)
        {
            string url = string.Empty;
            if (!date.HasValue || double.IsNaN(date.Value) || !factoryId.HasValue || !lineId.HasValue || !orderId.HasValue || !productId.HasValue)
                return Json(new { Url = url });
            int prodId = _productivityService.GetProductivityId(Extensions.GetUnixDateTime(date.Value), factoryId.Value, lineId.Value, orderId.Value, productId.Value);
            if (prodId == 0) return Json(new { Url = url });

            url = new UrlHelper(Request.RequestContext).Action("Detail", "Productivity", new { id = prodId });
            return Json(new { Url = url });
        }
    }
}