﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class SubCategoriesController : Controller
    {
        private SubCategoryService _subCatService = new SubCategoryService();
        
        private void SetConfigTablesValues(SubCategory subCategory)
        {
            subCategory.Categories = new ConfigurationTableValueService().ListByConfigId((int)EnumConfigurationTables.Category, true);
        }

        public ActionResult Index()
        {
            return View(_subCatService.GetList());
        }

        public ActionResult Detail(int? id)
        {
            SubCategory subCategory = new SubCategory();

            if (id.HasValue && id.Value > 0)
                subCategory = _subCatService.GetEntityById(id.Value);

            this.SetConfigTablesValues(subCategory);
            return View(subCategory);
        }

        [HttpPost]
        public ActionResult Detail(SubCategory subCategory)
        {
            try
            {
                this.SetConfigTablesValues(subCategory);
                if (!ModelState.IsValid)
                    return View(subCategory);
                bool isAdded = false;
                if (subCategory.Id == 0)
                    isAdded = true;
                if (!_subCatService.Save(subCategory) && !string.IsNullOrWhiteSpace(subCategory.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", subCategory.BrokenBusinessRulesText);
                    return View(subCategory);
                }
                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Order Detail added successfully";

                    SubCategory newSubCategory = new SubCategory();
                    this.SetConfigTablesValues(newSubCategory);
                    return View(newSubCategory);
                }
                else
                {
                    ViewBag.Message = "Order Detail updated successfully";
                    return View(subCategory);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(subCategory);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_subCatService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _subCatService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this sub category as involved with some actions");
                return View(_subCatService.GetEntityById(id));
            }
        }
    }
}