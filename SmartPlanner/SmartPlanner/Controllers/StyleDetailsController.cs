﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class StyleDetailsController : Controller
    {
        private StyleDetailService _styleDetailService = new StyleDetailService();
        private SubCategoryService _subCategoryService = new SubCategoryService();
        private StyleProductService _styleProductService = new StyleProductService();

        private void SetConfigTablesValues(StyleDetail styleDetail, bool isEdit = false)
        {
            ConfigurationTableValueService configService = new ConfigurationTableValueService();
            styleDetail.AppearanceList = configService.ListByConfigId((int)EnumConfigurationTables.Appearance, true);
            
            styleDetail.FabricTypes = configService.ListByConfigId((int)EnumConfigurationTables.FabricType, true);
            styleDetail.Divisions = configService.ListByConfigId((int)EnumConfigurationTables.Division, true);
            styleDetail.Operations = configService.ListByConfigId((int)EnumConfigurationTables.Operation, true);
            styleDetail.TimeTables = configService.ListByConfigId((int)EnumConfigurationTables.TimeTable, true);
            
            styleDetail.Categories = configService.ListByConfigId((int)EnumConfigurationTables.Category, true);
            styleDetail.FeaturesList = configService.ListByConfigId((int)EnumConfigurationTables.FeatureList, true);
            styleDetail.Products = configService.ListByConfigId((int)EnumConfigurationTables.Product, true);

            styleDetail.Buyers = new BuyerService().ListBuyers("Name",true);
            styleDetail.Customers = new CustomerService().ListCustomers("Name", true);
            styleDetail.Merchants = new UserService().ListUsers("FullName", true);
            styleDetail.IndustrialEngineers = new UserService().ListUsers("FullName", true);
            styleDetail.SampleStatuses = new SampleApprovalStatusService().ListSampleApproval();


            if (isEdit)
            {
                //styleDetail.SubCategories = _subCategoryService.ListForCategoryId(styleDetail.CategoryId);
            }
            else
            {
                styleDetail.StyleProducts = new List<IdNameDTO>();

                //styleDetail.SubCategories = new List<SubCategory>();
                //styleDetail.AdditionalInformationFileNames = new List<AdditionalFileDTO>();
                //styleDetail.FeatureList = new List<string>();
            }

        }

        // GET: StyleDetails
        public ActionResult Index()
        {
            return View(_styleDetailService.GetList());
        }

        public ActionResult Detail(int? id, EnumSMVInquiryType isInquiry = EnumSMVInquiryType.None, int inquiryRevisionId = 0)
        {
            StyleDetail styleDetail = new StyleDetail();

            Session["StyleProducts"] = null;

            if (id.HasValue && id.Value > 0)
            {
                styleDetail = _styleDetailService.GetEntityById(id.Value);
                this.SetConfigTablesValues(styleDetail, true);
                ViewBag.IsInquiry = isInquiry;
            }
            else
            {
                ViewBag.IsInquiry = EnumSMVInquiryType.Request;
                this.SetConfigTablesValues(styleDetail);
            }

            ViewBag.InquiryRevisionId = inquiryRevisionId;
            return View(styleDetail);
        }

        [HttpPost]
        public ActionResult Detail(StyleDetail styleDetail, EnumSMVInquiryType isInquiry = EnumSMVInquiryType.None, int inquiryRevisionId = 0)
        {
            ViewBag.InquiryRevisionId = inquiryRevisionId;
            ViewBag.IsInquiry = isInquiry;

            try
            {
                this.SetConfigTablesValues(styleDetail,true);

                if (!ModelState.IsValid)
                    return View(styleDetail);

                bool isAdded = false;

                if (styleDetail.Id == 0)
                    isAdded = true;

                if (!_styleDetailService.Save(styleDetail, isInquiry, inquiryRevisionId) && !string.IsNullOrWhiteSpace(styleDetail.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", styleDetail.BrokenBusinessRulesText);

                    return View(styleDetail);
                }

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Style detail added successfully";

                    StyleDetail newStyle = new StyleDetail();
                    this.SetConfigTablesValues(newStyle,true);
                    return View(newStyle);
                }
                else
                {
                    ViewBag.Message = "Style detail updated successfully";
                    styleDetail = _styleDetailService.GetEntityById(styleDetail.Id);
                    this.SetConfigTablesValues(styleDetail,true);
                    return View(styleDetail);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint 'AK_StyleDetails_StyleNumber_Buyer'."))
                    ModelState.AddModelError("", "This style number already created for this buyer.");
                else
                    ModelState.AddModelError("", ex.Message);

                return View(styleDetail);
            }
        }

        [HttpPost]
        public JsonResult AddAdditionalFile()
        {

            int styleId = Convert.ToInt32(Request["Id"]);

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                //_styleDetailService.SaveAdditionalInformations(file, styleId);
            }


            var additionalFiles = new List<string>();// _styleDetailService.GetEntityById(styleId).AdditionalInformationFileNames.ToList();

            return Json(additionalFiles);
        }

        [HttpPost]
        public JsonResult AddFeature()
        {

            int styleId = Convert.ToInt32(Request["Id"]);
            string featureName = Convert.ToString(Request["FeatureName"]);

            //_styleDetailService.SaveFeaturesList(featureName, styleId);

            var featuresList = new List<string>();//= _styleDetailService.GetEntityById(styleId).FeatureList.ToList();

            return Json(featuresList);
        }

        public ActionResult Delete(int id)
        {
            return View(_styleDetailService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _styleDetailService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this style detail as involved with some actions");
                return View(_styleDetailService.GetEntityById(id));
            }
        }

        public ActionResult GetSubCategories(int? category)
        {
            if (!category.HasValue) return null;

            List<SubCategory> subCategories = _subCategoryService.ListForCategoryId(category.Value);
            return Json(subCategories, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AddProducts()
        {
            List<StyleProduct> styleProducts = new List<StyleProduct>();

            if (Session.Keys.Count > 0 && Session["StyleProducts"] != null)
            {
                styleProducts = (List<StyleProduct>)Session["StyleProducts"];
            }

            int styleId = Convert.ToInt32(Request["StyleId"]);
            int productId = Convert.ToInt32(Request["ProductId"]);

            StyleProduct newStyleProduct = new StyleProduct();
            newStyleProduct.StyleId = styleId;
            newStyleProduct.ProductId = productId;

            _styleProductService.Save(newStyleProduct);

            styleProducts = _styleProductService.ListForStyleId(styleId);
            Session["StyleProducts"] = styleProducts;

            return Json(styleProducts);
        }
    }
}