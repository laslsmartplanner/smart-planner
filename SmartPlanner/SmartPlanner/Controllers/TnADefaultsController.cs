﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class TnADefaultsController : Controller
    {
        private TnADefaultService _tnaDefaultService = new TnADefaultService();

        // GET: TnADefaults
        public ActionResult Index()
        {
            return View(_tnaDefaultService.GetList());
        }

        


        [HttpPost]
        public ActionResult Index(int id, string propertyName, string value)
        {
            var status = false;
            var message = "";

            TnADefault tnA = _tnaDefaultService.GetEntityById(id);
            if(tnA!=null)
            {
                if (propertyName.Equals("DisplayOrder"))
                    tnA.DisplayOrder = Convert.ToInt32(value);
                else
                    tnA.Duration = Convert.ToInt32(value);
                _tnaDefaultService.Update(tnA);
                status = true;
            }
            
            var response = new { value = value, status = status, message = message };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }
    }
}
