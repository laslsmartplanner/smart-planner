﻿using Newtonsoft.Json.Linq;
using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class RolesController : Controller
    {
        RoleService _roleService = new RoleService();
        // GET: Roles
        public ActionResult Index()
        {
            return View(_roleService.GetList());
        }
        public ActionResult SaveUser(int id, int value)
        {
            _roleService.UpdateUser(id, Convert.ToInt32(value));

            LITUser user = new UserService().GetEntityById(value);

            var response = new { value, status = true, name = user?.FullName ?? string.Empty };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }
    }
}