﻿using Newtonsoft.Json.Linq;
using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class ConfigValuesController : Controller
    {

        ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();
        TnADefaultService _tnaDefService = new TnADefaultService();

        // GET: ConfigValues
        public ActionResult Index(int? id)
        {
            if (!id.HasValue || id.Value < 1)
                return RedirectToAction("Index", "Home");
            ConfigurationTable configTable = new ConfigurationTableService().GetByConfigId(id.Value);
            ViewBag.ConfigTableName = configTable?.ConfigName;
            ViewBag.ConfigId = id.Value;

            return View(_configValueService.ListByConfigId(id.Value));
        }
        public ActionResult Detail(int configId, int? id)
        {
            ConfigurationTableValue configValue = new ConfigurationTableValue();

            if (id.HasValue && id.Value > 0)
            {
                configValue = _configValueService.GetEntityById(id.Value);
                if (configId == (int)EnumConfigurationTables.POType)
                    configValue.TnADefaults = _tnaDefService.ListForPOType(id.Value);
            }
            else
                configValue.ConfigId = configId;

            return View(configValue);
        }

        [HttpPost]
        public ActionResult Detail(ConfigurationTableValue configurationValue)
        {
            if(!ModelState.IsValid)
                return View(configurationValue);
            bool isAdded=false;
            if (configurationValue.Id == 0)
                isAdded = true;
            if (!_configValueService.Save(configurationValue) && !string.IsNullOrWhiteSpace(configurationValue.BrokenBusinessRulesText))
            {
                ModelState.AddModelError("", configurationValue.BrokenBusinessRulesText);
                return View(configurationValue);
            }
            if(isAdded)
            {
                ModelState.Clear();
                ViewBag.Message = $"{configurationValue.ConfigTable?.ConfigName} added successfully";
                ConfigurationTableValue configValue = new ConfigurationTableValue() { ConfigId = configurationValue.ConfigId };
                if (configValue.ConfigId == (int)EnumConfigurationTables.POType)
                    configValue.TnADefaults = _tnaDefService.ListForPOType(configValue.Id);
                return View(configValue);
            }
            else
            {
                ViewBag.Message = $"{configurationValue.ConfigTable?.ConfigName} updated successfully";
                return View(configurationValue);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_configValueService.GetEntityById(id));
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                ConfigurationTableValue val = _configValueService.GetEntityById(id);
                _configValueService.DeleteById(id);                
                return RedirectToAction("Index", new { id = val?.ConfigId });
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this value as involved with some actions");
                return View(_configValueService.GetEntityById(id));
            }
        }

        [HttpPost]
        public ActionResult SaveTnA(int id, int poTypeId, string value)
        {
            var status = false;
            var message = string.Empty;

            try
            {
                _tnaDefService.SaveTnAForPOType(poTypeId, id, Convert.ToDouble(value));
                status = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            var response = new { value, status, message };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }

    }
}