﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class BoardController :  Controller
    {
        private LineModuleService _lineModuleService = new LineModuleService();

        private PlacedOrderService _placesOrderService = new PlacedOrderService();
        private DTOService _dTOService = new DTOService();

        private CalendarDayService _calendarService = new CalendarDayService();

        // GET: Board
        public ActionResult Index()
        {
            TimeBoardDTO dto = new TimeBoardDTO();
            dto.FactoryNames = _lineModuleService.ListInvolvedFactoies();

            Company company = new CompanyService().GetCompany();
            if (company?.DefaultFactoryId > 0)
                dto.FactoryNameId = company.DefaultFactoryId;

            DateTime today = DateTime.Today;
            dto.FromDate = new DateTime(today.Year, today.Month,1);
            dto.ToDate = DateTime.Today.AddMonths(6);
            return View(dto);
        }
        public ActionResult LoadBoard(int factoryId, DateTime from, DateTime to)
        {
            if(Helper.GetAccessType(Enums.EnumAccessModules.PlanningManagement).Equals(Enums.UserAccessType.ReadWrite))
            {
                return PartialView("_Board", this.ListForFactoryId(factoryId, from, to));
            }
            else
            {
                return PartialView("_ViewBoard", this.ListForFactoryId(factoryId, from, to));
            }

        }

        public TimeBoardDTO ListForFactoryId(int factoryId, DateTime from, DateTime to)
        {
            List<string> columns = new List<string>();

            DateTime fromSelected = from;
            
            TimeBoardDTO dto = new TimeBoardDTO();
            DTOService dTOService = new DTOService();
            //foreach(LineModule lin in dto.LineModules)
            dto.LineModules = _lineModuleService.ListForBoard(factoryId, from, to);
            dto.HLineModules = new List<LineModule>();

            DateTime startDate = from;
            while (startDate <= to)
            {
                columns.Add($"{startDate.Month}-{startDate.Day}");
                startDate = startDate.AddDays(1);
            }
            dto.LineModules.FirstOrDefault().GridColumns = columns;
            

            dto.StyleNumbers = dTOService.GetStylesForAvailableOrders(factoryId);
            
            dto.Products = new List<IdNameDTO>();
            
            List<OrderDetailDTO> orders = new List<OrderDetailDTO>();
            
            dto.OrderDetails = orders;


            return dto;
        }

        public JsonResult LoadStyles(int? factoryId)
        {
            List<IdNameDTO> styles = factoryId.HasValue  ? new DTOService().GetStylesForAvailableOrders(factoryId.Value) : new List<IdNameDTO>();

            return Json(styles, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadProductsForStyle(int? factoryId, int? styleId)
        {
            List<IdNameDTO> products = factoryId.HasValue && styleId.HasValue ? new DTOService().GetProductsForAvailableStyle(factoryId.Value, styleId.Value) : new List<IdNameDTO>();

            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetChangeOver(int? factoryId,int? styleProductId)
        {
            List<IdNameDTO> products = factoryId.HasValue && styleProductId.HasValue ? new StyleProductService().GetChangeOverForBoard(factoryId.Value, styleProductId.Value) : new List<IdNameDTO>();

            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadOrders(int factoryId, int? styleId, int? productId)
        {
            List<OrderDetailDTO> orders = styleId.HasValue && productId.HasValue && styleId.Value > 0 && productId.Value > 0 ? _dTOService.GetOrderStartEnd(factoryId, styleId.Value, productId.Value) : new List<OrderDetailDTO>();

            return Json(orders, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadPlacedOrders(DateTime from, DateTime to,int factoryId)
        {
            List<PlacedOrder> orders = _placesOrderService.ListPlacedOrders(from, to, factoryId);
            //return Json(new { orders = orders.PlacedOrders, productivities = orders.Productivities}, JsonRequestBehavior.AllowGet);
            return Json(orders, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveBoard(PlacedOrder placedOrder)
        {
            int startCol = 0;
            if (placedOrder.LeftPosition > 103)
                startCol = Convert.ToInt32(Math.Truncate((placedOrder.LeftPosition - 103) / 40));
            placedOrder.StartDate = placedOrder.StartedFrom.AddDays(startCol);

            PlacedOrder order =  _placesOrderService.AddOrderToLine(placedOrder);
            //return this.Json(order);

            if (order == null)
                return new JsonResult { Data = new { isSuccess = false } };
            //return new JsonResult { Data = new { colCount = order.ColumnCount, startColumn = order.StartColumn, isTemp = order.IsTemp } };
            return new JsonResult { Data = new { savedOrder = order } };
        }

        public ActionResult GetOrderHistory(int orderId, int styleId,int productId)
        {
            List<OrderHistoryDTO> orderHistory = _dTOService.GetOrderHistory(orderId, productId);
            List<StyleHistoryDTO> styleHistory = _dTOService.GetStyleHistory(styleId, productId);
            return PartialView("_OrderStyleHistory", new OrderAndStyleHistoryDTO(orderHistory, styleHistory));
            //return Json(history, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeletePlacedOrder(int id)
        {
            _placesOrderService.DeleteById(id);
            return new JsonResult { Data = new { isSuccess = true } };
        }

        public ActionResult Search()
        {
            return View(new PlacedOrderService().PlacedOrderForSearch());
        }
    }
}