﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class UserLevelsController : Controller
    {
        private UserLevelService _userLevelService = new UserLevelService();
        // GET: UserLevels

        public ActionResult Index()
        {
            return View(_userLevelService.GetList());
        }

        // GET: UserLevels/Create
        [AuthAccess(EnumAccessModules.UserLevelManagement)]
        public ActionResult Detail(int? id)
        {
            UserLevel userLevel = new UserLevel();
            
            if(id.HasValue && id.Value > 0)
                userLevel = _userLevelService.GetEntityById(id.Value);
            userLevel.UserLevelAccessModules = userLevel.GetAccessModules();
            return View(userLevel);
        }
        [HttpPost]
        public ActionResult Detail(UserLevel userLevel, int? id)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    userLevel.UserLevelAccessModules = userLevel.GetAccessModules();
                    return View(userLevel);
                }
                if (id.HasValue && id.Value > 0)
                    userLevel.Id = id.Value;
                List<UserLevelAccessModule> accessModules = userLevel.GetAccessModules();
                foreach (UserLevelAccessModule levelAccessModule in accessModules)
                {
                    string value = Request.Form[$"{levelAccessModule.AccessModuleId}"];
                    if (string.IsNullOrEmpty(value))
                        continue;
                    levelAccessModule.AccessType = (UserAccessType)(Convert.ToInt32(value));
                }
                if (new UserLevelService().Save(userLevel, accessModules))
                {
                    if (id.HasValue && id.Value > 0)
                    {
                        ViewBag.Message = "User level updated successfully";
                        userLevel.UserLevelAccessModules = userLevel.GetAccessModules();
                        return View(userLevel);
                    }
                    else
                    {
                        ViewBag.Message = "User level added successfully";
                        UserLevel userLevelEmpty = new UserLevel() { LevelName = string.Empty };
                        userLevelEmpty.UserLevelAccessModules = userLevelEmpty.GetAccessModules();
                        ModelState.Clear();
                        return View(userLevelEmpty);
                    }
                }
                if (!string.IsNullOrWhiteSpace(userLevel.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", userLevel.BrokenBusinessRulesText);
                    userLevel.UserLevelAccessModules = userLevel.GetAccessModules();
                    return View(userLevel);
                }

                return RedirectToAction("Detail", "UserLevels");
            }
            catch (Exception)
            {

                throw;
            }
        }


        // GET: UserLevels/Delete/5
        [AuthAccess(EnumAccessModules.UserLevelManagement)]
        public ActionResult Delete(int id)
        {
            UserLevel userLevel = new UserLevelService().GetEntityById(id);
            userLevel.UserLevelAccessModules = userLevel.GetAccessModules();
            return View(userLevel);
        }

        // POST: UserLevels/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                new UserLevelService().DeleteByUserLevelId(id);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "This user level has used with users. Change user level for users before deleting this user level");
                UserLevel userLevel = new UserLevelService().GetEntityById(id);
                userLevel.UserLevelAccessModules = userLevel.GetAccessModules();
                return View(userLevel);
            }
        }
    }
}
