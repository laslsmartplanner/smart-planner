﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class CompanyController : Controller
    {
        CompanyService _companyService = new CompanyService();


        public ActionResult Detail()
        {
            Company comp = _companyService.GetCompany();
            comp.Factories = new FactoryService().GetFactories(true);
            return View(comp);
        }

        [HttpPost]
        public ActionResult Detail(Company company)
        {
            if (ModelState.IsValid)
            {
                _companyService.Save(company);
                ViewBag.Message = "Company detail updated successfully";
            }
            company.Factories = new FactoryService().GetFactories(true);
            return View(company);
        }
    }
}