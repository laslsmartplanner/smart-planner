﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class NotificationsController : Controller
    {
        private NotificationService _notificationService = new NotificationService();

        // GET: Notifications
        public ActionResult Index()
        {
            return View(_notificationService.ListForLoggedUser(0));
        }
        public JsonResult LoadNotificationsForUser()
        {
            List<Notification> orders = _notificationService.ListForLoggedUser(Convert.ToInt32(ConfigurationManager.AppSettings["NotificationCount"].ToString()));
            return Json(orders, JsonRequestBehavior.AllowGet);
        }
    }
}