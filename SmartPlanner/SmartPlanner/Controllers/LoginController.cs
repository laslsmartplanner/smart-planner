﻿using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SmartPlanner.BusinessLayer.Common;
using System.Threading.Tasks;
using System.Configuration;

namespace SmartPlanner.Controllers
{
    public class LoginController : Controller
    {
        private UserService _userService = new UserService();

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Message = TempData["ReLoginMessage"];
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LITUser user, bool chkRemember = false)
        {
            if (!ModelState.IsValidField("UserName") || !ModelState.IsValidField("Password"))
                return View(user);
            if (_userService.IsPasswordMatched(user.UserName, user.Password))
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
                //GV.LoggedUser = _userService.GetByName(user.UserName);
                Session["CurrentUser"] = _userService.GetByName(user.UserName);

                _userService.SetLogin(GV.LoggedUser());

                if (GV.LoggedUser().IsTempPassword)
                    return RedirectToAction("ChangePassword", new { userId = GV.LoggedUser().Id });
                
                var authTicket = new FormsAuthenticationTicket(
                      1,
                      user.Id.ToString(),
                      DateTime.Now,
                      DateTime.Now.AddMinutes(20),  
                      chkRemember,  
                      "", 
                      "/"
                    );
                
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                Response.Cookies.Add(cookie);

                return RedirectToAction("Index", "Home");
            }
            else
                ModelState.AddModelError("", "Username or password is invalid");
            return View(user);
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            Session["CurrentUser"] = null;
            return RedirectToAction("Index", "Login");
        }
        public ActionResult ChangePassword(int userId)
        {
            if (!Request.IsAuthenticated)
                return RedirectToAction("Index", "Login");
            LITUser user = _userService.GetEntityById(userId);
            user.Password = "";
            return View(user);
        }
        [HttpPost]
        public ActionResult ChangePassword(LITUser user)
        {
            try
            {
                if (!ModelState.IsValidField("Password") || !ModelState.IsValidField("ConfirmPassowrd"))
                    return View(user);
                if (user.Password.Length < 6)
                {
                    ModelState.AddModelError("", "Password should have at least 6 characters");
                    return View(user);
                }
                if(!_userService.ChangePassword(user) && !string.IsNullOrWhiteSpace(user.BrokenBusinessRulesText))
                {
                    ModelState.AddModelError("", user.BrokenBusinessRulesText);
                    return RedirectToAction("ChangePassword", user.Id);
                }
                TempData["ReLoginMessage"] = "Please login with new password.";
                return RedirectToAction("Index", "Login");

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}