﻿using SmartPlanner.Attributes;
using SmartPlanner.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Controllers
{
    [AuthenticateUserSession]
    public class OrderInquiriesController : Controller
    {
        private OrderInquiryService _orderInquiryService = new OrderInquiryService();
        private TnACalendarService _tnACalendarService = new TnACalendarService();
        private OrderCombinationService _splitCombinationService = new OrderCombinationService();
        private OrderInquirySplitService _orderInquirySplitService = new OrderInquirySplitService();

        private void SetConfigTablesValues(OrderInquiry orderInquiry)
        {
            ConfigurationTableValueService configService = new ConfigurationTableValueService();
            orderInquiry.PlanningSeasons = configService.ListByConfigId((int)EnumConfigurationTables.PlanningSeason, true);
            orderInquiry.Products = configService.ListByConfigId((int)EnumConfigurationTables.Product, true);

            orderInquiry.Customers = new CustomerService().ListCustomers("Name", true);
            orderInquiry.Buyers = new BuyerService().ListBuyers("Name", true);

            orderInquiry.EmblishmentPlants = configService.ListByConfigId((int)EnumConfigurationTables.EmblishmentPlant, true);
            orderInquiry.Users = new UserService().ListUsers("UserName", true);

            orderInquiry.Styles = new StyleDetailService().ListForBuyerCustomer(orderInquiry.BuyerId, orderInquiry.CustomerId);

            orderInquiry.WashingPlants = configService.ListByConfigId((int)EnumConfigurationTables.WashingPlant, true);
            orderInquiry.PrintingPlants = configService.ListByConfigId((int)EnumConfigurationTables.PrintingPlant, true);
            orderInquiry.SewingPlants = configService.ListByConfigId((int)EnumConfigurationTables.SewingPlant, true);
            orderInquiry.OrderInquiryStatuses = new OrderInquiryStatusService().GetList();
            orderInquiry.Merchants = new UserService().ListUsers("FullName", true);
            orderInquiry.Planners = new UserService().ListUsers("FullName", true);
            orderInquiry.Colors = configService.ListByConfigId((int)EnumConfigurationTables.Colors, true);
            orderInquiry.CustomerSizes = new List<string>();

        }

        // GET: OrderInqueries
        public ActionResult Index()
        {
            return View(_orderInquiryService.ListForType(1));
        }

        [AuthAccess(EnumAccessModules.OrderInquiryManagement)]
        public ActionResult Detail(int? id)
        {
            Session["OrderInquirySplits"] = null;

            OrderInquiry orderInquiry = new OrderInquiry();
            orderInquiry.Split = new OrderInquirySplit();

            if (id.HasValue && id.Value > 0)
            {
                orderInquiry = _orderInquiryService.GetEntityById(id.Value);
                orderInquiry.Split = new OrderInquirySplit();
                Session["OrderInquirySplits"] = orderInquiry.Splits;
            }
            else
            {
                orderInquiry.OrderInquiryStatusId = 3;
                orderInquiry.Splits = new List<OrderInquirySplit>();

                //OrderInquirySplit newSplit = new OrderInquirySplit();
                //newSplit.SplitId = 0;
                //orderInquiry.Splits.Add(newSplit);
            }

            //orderInquiry.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderInquiry.Id);
            this.SetConfigTablesValues(orderInquiry);
            return View(orderInquiry);
        }

        [AuthAccess(EnumAccessModules.OrderInquiryManagement)]
        public ActionResult ViewDetail(int? id)
        {
            Session["OrderInquirySplits"] = null;

            OrderInquiry orderInquiry = new OrderInquiry();
            orderInquiry.Split = new OrderInquirySplit();

            if (id.HasValue && id.Value > 0)
            {
                orderInquiry = _orderInquiryService.GetEntityById(id.Value);
                orderInquiry.Split = new OrderInquirySplit();
                Session["OrderInquirySplits"] = orderInquiry.Splits;
            }
            else
            {
                orderInquiry.OrderInquiryStatusId = 3;
                orderInquiry.Splits = new List<OrderInquirySplit>();
            }

            this.SetConfigTablesValues(orderInquiry);
            return View(orderInquiry);
        }

        [HttpPost]
        public ActionResult Detail(OrderInquiry orderInquiry)
        {
            try
            {
                this.SetConfigTablesValues(orderInquiry);

                if (!ModelState.IsValid)
                {
                    //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);
                    return View(orderInquiry);
                }

                bool isAdded = false;

                if (orderInquiry.Id == 0)
                    isAdded = true;

                if (Session["OrderInquirySplits"] != null)
                {
                    orderInquiry.Splits = (List<OrderInquirySplit>)Session["OrderInquirySplits"];
                }
                else
                {
                    orderInquiry.Splits = new List<OrderInquirySplit>();
                }

                if (!_orderInquiryService.Save(orderInquiry) && !string.IsNullOrWhiteSpace(orderInquiry.BrokenBusinessRulesText))
                {
                    
                    ModelState.AddModelError("", orderInquiry.BrokenBusinessRulesText);
                    return View(orderInquiry);
                }
                

                if (isAdded)
                {
                    ModelState.Clear();
                    ViewBag.Message = "Order Inquiry added successfully";

                    OrderInquiry newOrder = new OrderInquiry();
                    Session["OrderInquirySplits"] = null;

                    newOrder.OrderInquiryStatusId = 3;
                    newOrder.Splits = new List<OrderInquirySplit>();

                    OrderInquirySplit newSplit = new OrderInquirySplit();
                    newSplit.SplitId = 0;
                    newOrder.Splits.Add(newSplit);

                    newOrder.Split = new OrderInquirySplit();

                    this.SetConfigTablesValues(newOrder);

                    return View(newOrder);
                }
                else
                {
                    ViewBag.Message = "Order Inquiry updated successfully";
                    orderInquiry = _orderInquiryService.GetEntityById(orderInquiry.Id);
                    this.SetConfigTablesValues(orderInquiry);
                    orderInquiry.Split = new OrderInquirySplit();
                    return View(orderInquiry);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                //orderDetail.TnACalendar = _tnACalendarService.GetCalendarsForOrder(orderDetail.Id);
                orderInquiry = _orderInquiryService.GetEntityById(orderInquiry.Id);
                this.SetConfigTablesValues(orderInquiry);
                orderInquiry.Split = new OrderInquirySplit();
                return View(orderInquiry);
            }
        }

        [HttpPost]
        public JsonResult AddSplitBreakdown()
        {
            List<OrderInquirySplit> orderInquirySplits = new List<OrderInquirySplit>();
            if (Session.Keys.Count > 0 && Session["OrderInquirySplits"] != null)
            {
                orderInquirySplits = (List<OrderInquirySplit>)Session["OrderInquirySplits"];
            }

            int splitNo = orderInquirySplits.Count() + 1;
            int orderInquiryId = Convert.ToInt32(Request["OrderInquiryId"]);
            int splitQty = Convert.ToInt32(Request["SplitQty"]);
            DateTime shipmentDate = Convert.ToDateTime(Request["ShipmentDate"]);

            OrderInquirySplit newSplit = new OrderInquirySplit();
            newSplit.SplitId = splitNo;
            newSplit.OrderInquiryId = orderInquiryId;
            newSplit.Qty = splitQty;
            newSplit.ShipmentDate = shipmentDate;

            orderInquirySplits.Add(newSplit);

            Session["OrderInquirySplits"] = orderInquirySplits;

            return Json(orderInquirySplits);
        }

        [HttpPost]
        public JsonResult DeleteSplitBreakdown()
        {
            int splitNo = Convert.ToInt32(Request["SplitId"]);

            List<OrderInquirySplit> orderInquirySplits = new List<OrderInquirySplit>();
            if (Session.Keys.Count > 0 && Session["OrderInquirySplits"] != null)
            {
                orderInquirySplits = (List<OrderInquirySplit>)Session["OrderInquirySplits"];
            }

            orderInquirySplits.RemoveAt(splitNo-1);

            int newSplitNo = 1;

            foreach(OrderInquirySplit split in orderInquirySplits)
            {
                split.SplitId = newSplitNo;
                newSplitNo++;
            }

            Session["OrderInquirySplits"] = orderInquirySplits;

            return Json(orderInquirySplits);
        }

        public ActionResult Delete(int id)
        {
            return View(_orderInquiryService.GetEntityById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _orderInquiryService.DeleteById(id);

                return RedirectToAction("Index", "OrderInquiries");
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("The DELETE statement conflicted with the REFERENCE constraint"))
                    ModelState.AddModelError("", "Unable to delete this order detail as involved with some actions");

                return View(_orderInquiryService.GetEntityById(id));
            }
        }

        public ActionResult GetCustomerSizes(int? customer)
        {
            if (!customer.HasValue) return null;

            List<String> customerSizes = _splitCombinationService.ListSizesForCustomer(customer.Value);
            return Json(customerSizes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerColors(int? customer)
        {
            if (!customer.HasValue) return null;

            List<String> customerColors = _splitCombinationService.ListColorsForCustomer(customer.Value);
            return Json(customerColors, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateSplit(int id, int qty,DateTime shipmentDate)
        {
            OrderInquirySplit inquirySplit = null;
            inquirySplit = _orderInquirySplitService.GetEntityById(id);

            return View(inquirySplit);
        }

        [HttpPost]
        public ActionResult UpdateSplit(OrderInquirySplit inquirySplit, FormCollection collection)
        {
            if (!ModelState.IsValidField("Qty") || !ModelState.IsValidField("ShipmentDate"))
                return View(inquirySplit);

            _orderInquiryService.UpdateSplit(inquirySplit);

            return RedirectToAction("Detail", new { id = inquirySplit.OrderInquiryId });
        }

        [HttpPost]
        public JsonResult GetStyleInfo(int styleId)
        {
            StyleProductService styleProductService = new StyleProductService();
            StyleDetail styleDetail = new StyleDetailService().GetEntityById(styleId);

            decimal cm = 0;
            decimal productSMV = 0;

            foreach(IdNameDTO idNameDTO in styleDetail.StyleProducts)
            {
                StyleProduct sp = styleProductService.GetEntityById(idNameDTO.Id);
                cm = cm + sp.CM;
                productSMV = productSMV + sp.ProductionSMV;
            }

            return new JsonResult { Data = new { CM = cm, ProductionSMV = productSMV} };
        }
    }
}