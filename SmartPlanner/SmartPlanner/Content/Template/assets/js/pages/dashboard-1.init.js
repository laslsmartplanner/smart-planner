!function (o) {
    "use strict"; var t = function ()
    { this.$body = o("body") };
    t.prototype.createCombineGraph = function (t, a, e, i)
    {
        var r = [
            { label: e[0], data: i[0], lines: { show: !0, fill: !0 }, points: { show: !0 } },
            { label: e[1], data: i[1], lines: { show: !0 }, points: { show: !0 } },
            { label: e[2], data: i[2], bars: { show: !0, barWidth: .7 } }],
            l = {
                series: { shadowSize: 0 }, grid: { hoverable: !0, clickable: !0, tickColor: "#f9f9f9", borderWidth: 1, borderColor: "#eeeeee" },
                colors: ["#e3eaef", "#4a81d4", "#1abc9c"], tooltip: !0, tooltipOpts: { defaultTheme: !1 },
                legend: {
                    position: "ne", margin: [0, -32], noColumns: 0, labelBoxBorderColor: null, labelFormatter: function (o, t) { return o + "&nbsp;&nbsp;" },
                    width: 30, height: 2
                },
                yaxis: { axisLabel: "Efficiency", tickColor: "#f5f5f5", font: { color: "#bdbdbd" } },
                xaxis: { axisLabel: "Months", ticks: a, tickColor: "#f5f5f5", font: { color: "#bdbdbd" } }
            }; o.plot(o(t), r, l)
    },
        t.prototype.init = function ()
        {
        var o = [
            [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0]],
            [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0]],
            [[5, 0], [4, 0], [3, 0], [2, 0], [1, 0], [0, 0]]];
        this.createCombineGraph("#sales-analytics",
            [[0, "February"], [1, "March"], [2, "April"], [3, "May"], [4, "June"], [5, "July"]],
            ["Efficiency"], o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
}(window.jQuery), function (o) { "use strict"; o.Dashboard1.init() }(window.jQuery), $("#dash-daterange").flatpickr({ altInput: !0, altFormat: "F j, Y", dateFormat: "Y-m-d", defaultDate: "today" });