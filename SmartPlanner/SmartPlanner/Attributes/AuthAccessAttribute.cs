﻿using SmartPlanner.BusinessLayer;
using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.Attributes
{
    public class AuthAccessAttribute: AuthorizeAttribute
    {
        private EnumAccessModules _accessModule;
        public AuthAccessAttribute(EnumAccessModules accessModule)
        {
            this._accessModule = accessModule;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if(GV.LoggedUser()!=null)
            {
                List<UserLevelAccessModule> accModules = GV.LoggedUser().UserLevel.GetAccessModules();
                if (accModules.FirstOrDefault(x => x.AccessModuleId == (int)this._accessModule && x.NoAccessChecked == false) != null)
                    return;
            }

            SmartPlanner.BusinessLayer.SystemConfigurations.AccessModule module = new SmartPlanner.BusinessLayer.SystemConfigurations.AccessModules.AccessModuleService().GetAccessModuleByModuleNumber((int)_accessModule);

            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            {
                controller = "Home",
                action = "Index",
                message = $"Access denied for the '{module.ModuleName}'"
            }));
        }
    }
}