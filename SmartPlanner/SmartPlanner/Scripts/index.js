﻿
var specialKeys = new Array();
specialKeys.push(8); //Backspace
specialKeys.push(43); //plus sign
specialKeys.push(45); //Minus sign

function IsNumber(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) !== -1);
    return ret;
}


function AddOnChangeEvent() {
    $(".form-control, .custom-control-input, .select-control").attr("onchange", "onChangeInTabControl()");
}

function onChangeInTabControl() {

    var modified = $('#tabWasModified').val();

    if (modified === undefined) {
        console.log('Dev note: You forgot to put the hidden "tabWasModified" input element in the root view.');
        return;
    }

    $('#tabWasModified').val(true);
}