﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class FactoryDA : EntityDataAccessBase<Factory>
    {
        private DataAccess _dataAccess = new DataAccess();

        private void SetParameters(SqlCommand command, Factory factory)
        {
            command.Parameters.AddWithValue("@FactoryName", factory.FactoryName);
            command.Parameters.AddWithValue("@AvailableMinutes", factory.AvailableMinutes);

            command.Parameters.AddWithValue("@ConsumptionCalculationResponsibilityId", factory.ConsumptionCalculationResponsibilityId);
            command.Parameters.AddWithValue("@SizeSetsubmisionResponsibilityId", factory.SizeSetsubmisionResponsibilityId);
            command.Parameters.AddWithValue("@SizeSetCommentResponsibilityId", factory.SizeSetCommentResponsibilityId);
            command.Parameters.AddWithValue("@ProductionPalningResponsibilityId", factory.ProductionPalningResponsibilityId);
            command.Parameters.AddWithValue("@PatternGradingResponsibilityId", factory.PatternGradingResponsibilityId);
            command.Parameters.AddWithValue("@CuttingResponsibilityId", factory.CuttingResponsibilityId);
            command.Parameters.AddWithValue("@PrintingResponsibilityId", factory.PrintingResponsibilityId);
            command.Parameters.AddWithValue("@EmbroideryResponsibilityId", factory.EmbroideryResponsibilityId);
            command.Parameters.AddWithValue("@ReCuttingShapingResponsibilityId", factory.ReCuttingShapingResponsibilityId);
            command.Parameters.AddWithValue("@FinishingResponsibilityId", factory.FinishingResponsibilityId);
            command.Parameters.AddWithValue("@PackingResponsibilityId", factory.PackingResponsibilityId);
            command.Parameters.AddWithValue("@InspectionResponsibilityId", factory.InspectionResponsibilityId);
            command.Parameters.AddWithValue("@PlanEmployees", factory.PlanEmployees);
            command.Parameters.AddWithValue("@PlanEfficiency", factory.PlanEfficiency);
        }

        internal bool Add(Factory factory)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO Factories
                      (FactoryName,AvailableMinutes, CreatedById, CreatedOn, ConsumptionCalculationResponsibilityId, SizeSetsubmisionResponsibilityId, SizeSetCommentResponsibilityId, 
                      ProductionPalningResponsibilityId, PatternGradingResponsibilityId, CuttingResponsibilityId, PrintingResponsibilityId, EmbroideryResponsibilityId, ReCuttingShapingResponsibilityId, 
                      FinishingResponsibilityId, PackingResponsibilityId, InspectionResponsibilityId, PlanEmployees, PlanEfficiency)
               VALUES (@FactoryName,@AvailableMinutes, @CreatedById, @CreatedOn, @ConsumptionCalculationResponsibilityId, @SizeSetsubmisionResponsibilityId, @SizeSetCommentResponsibilityId, 
                      @ProductionPalningResponsibilityId, @PatternGradingResponsibilityId, @CuttingResponsibilityId, @PrintingResponsibilityId, @EmbroideryResponsibilityId, @ReCuttingShapingResponsibilityId, 
                      @FinishingResponsibilityId, @PackingResponsibilityId, @InspectionResponsibilityId, @PlanEmployees, @PlanEfficiency)");

            this.SetParameters(command, factory);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                factory.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal bool Update(Factory factory)
        {
            SqlCommand command = new SqlCommand(@"UPDATE Factories
                                                SET FactoryName =@FactoryName, AvailableMinutes=@AvailableMinutes , ConsumptionCalculationResponsibilityId =@ConsumptionCalculationResponsibilityId, SizeSetsubmisionResponsibilityId =@SizeSetsubmisionResponsibilityId , 
                      SizeSetCommentResponsibilityId =@SizeSetCommentResponsibilityId , ProductionPalningResponsibilityId =@ProductionPalningResponsibilityId , PatternGradingResponsibilityId =@PatternGradingResponsibilityId , CuttingResponsibilityId =@CuttingResponsibilityId , PrintingResponsibilityId =@PrintingResponsibilityId , EmbroideryResponsibilityId =@EmbroideryResponsibilityId , 
                      ReCuttingShapingResponsibilityId =@ReCuttingShapingResponsibilityId , FinishingResponsibilityId =@FinishingResponsibilityId , PackingResponsibilityId =@PackingResponsibilityId , InspectionResponsibilityId =@InspectionResponsibilityId , PlanEmployees =@PlanEmployees , PlanEfficiency=@PlanEfficiency
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", factory.Id);
            this.SetParameters(command, factory);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<Factory> ListInvolvedFactoiesInLineModules()
        {
            SqlCommand sqlCommand = new SqlCommand(@"Select c.* 
                                                    From LineModules l WITH(NOLOCK) 
	                                                    INNER JOIN (Select MAX(ID) lineId
				                                                    From LineModules WITH(NOLOCK)
				                                                    GROUP BY FactoryNameId)i ON l.Id = i.lineId 
	                                                    INNER JOIN Factories c WITH(NOLOCK) ON l.FactoryNameId = c.Id 
                                                    order by c.FactoryName ");
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<Factory> ListAvailableFactoriesForCustomerResponsibilities(int customerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * FROM Factories WHERE Id NOT IN (SELECT FactoryId From CustomerResponsibilities WHERE CustomerId=@Id)");
            sqlCommand.Parameters.AddWithValue("@Id", customerId);

            return this.GetList(sqlCommand, true);
        }

    }
}
