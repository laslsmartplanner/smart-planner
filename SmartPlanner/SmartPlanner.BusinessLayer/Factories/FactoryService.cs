﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class FactoryService : ServiceBase<Factory>
    {
        private FactoryDA _dataAccess = new FactoryDA();

        public bool Save(Factory factory)
        {
            if (!this.IsValidToSave(factory)) return false;
            if (factory.Id > 0)
                _dataAccess.Update(factory);
            else
                _dataAccess.Add(factory);
            return true;
        }

        public List<Factory> GetFactories(bool addEmpty=false)
        {
            return _dataAccess.GetList("FactoryName",addEmpty);
        }
        public List<Factory> ListAvailableFactoriesForCustomerResponsibilities(int customerId)
        {
            return _dataAccess.ListAvailableFactoriesForCustomerResponsibilities(customerId);
        }
    }
}
