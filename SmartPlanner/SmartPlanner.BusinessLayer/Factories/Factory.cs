﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Factory : EntityBase<Factory>
    {
        public override string TableName => "Factories";
        public override string NameField => "FactoryName";

        [Required]
        [Display(Name = "Factory Name")]
        public string FactoryName { get; set; }

        [Required]
        [Display(Name = "Available Minutes")]
        public double AvailableMinutes { get; set; }

        [Display(Name = "Consumption Calculation")]
        public int ConsumptionCalculationResponsibilityId { get; set; }

        [Display(Name = "Size Set Submision")]
        public int SizeSetsubmisionResponsibilityId { get; set; }

        [Display(Name = "Size Set Comment")]
        public int SizeSetCommentResponsibilityId { get; set; }

        [Display(Name = "Production palning update & Circulation")]
        public int ProductionPalningResponsibilityId { get; set; }

        [Display(Name = "Pattern Grading")]
        public int PatternGradingResponsibilityId { get; set; }

        [Display(Name = "Cutting")]
        public int CuttingResponsibilityId { get; set; }

        [Display(Name = "Printing")]
        public int PrintingResponsibilityId { get; set; }

        [Display(Name = "Embroidery")]
        public int EmbroideryResponsibilityId { get; set; }

        [Display(Name = "Re-Cutting Shaping")]
        public int ReCuttingShapingResponsibilityId { get; set; }

        [Display(Name = "Finishing")]
        public int FinishingResponsibilityId { get; set; }

        [Display(Name = "Packing")]
        public int PackingResponsibilityId { get; set; }

        [Display(Name = "Inspection / Audit")]
        public int InspectionResponsibilityId { get; set; }
        [Display(Name = "Plan Employees")]
        public int PlanEmployees { get; set; }

        [Display(Name = "Plan Efficiency")]
        public decimal PlanEfficiency { get; set; }
        public List<LITUser> Users { get; set; }
    }
}
