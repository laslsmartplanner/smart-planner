﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class OrderInquirySplitService : ServiceBase<OrderInquirySplit>
    {
        private OrderInquirySplitDA _dataAccess = new OrderInquirySplitDA();
        
        public List<OrderInquirySplit> ListForOrderInquiryId(int orderInquiryId)
        {
            return _dataAccess.ListForOrderInquiryId(orderInquiryId);
        }

        public void Delete(int id)
        {
            _dataAccess.DeleteById(id);
        }
    }
}
