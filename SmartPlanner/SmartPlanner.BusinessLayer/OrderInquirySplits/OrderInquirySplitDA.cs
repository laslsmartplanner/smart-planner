﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    internal class OrderInquirySplitDA : EntityDataAccessBase<OrderInquirySplit>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal List<OrderInquirySplit> ListForOrderInquiryId(int orderInquiryId)
        {
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM OrderInquirySplits o WITH(NOLOCK) WHERE OrderInquiryId=@OrderInquiryId ");
            sqlCommand.Parameters.AddWithValue("@OrderInquiryId", orderInquiryId);

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void DeleteSplitsForOrderInquiry(int orderInquiryId)
        {
            SqlCommand sqlCommand = new SqlCommand("DELETE OrderInquirySplits WHERE OrderInquiryId=@OrderInquiryId ");
            sqlCommand.Parameters.AddWithValue("@OrderInquiryId", orderInquiryId);

            try
            {
                _dataAccess.ExecuteNonQuery(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal void Delete(int id)
        {
            new OrderCombinationDA().DeleteForOrderDetailId(id);

            SqlCommand sqlCommand = new SqlCommand("DELETE OrderInquirySplits WHERE Id=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", id);

            try
            {
                _dataAccess.BeginTransaction();
                _dataAccess.ExecuteNonQuery(sqlCommand);

                sqlCommand.CommandText = "DELETE OrderStyleProductSMVs where SplitKey=@Id";
                _dataAccess.ExecuteNonQuery(sqlCommand);

                _dataAccess.CommitTransaction();
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
        }

    }
}
