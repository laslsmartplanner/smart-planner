﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OrderInquirySplit : EntityBase<OrderInquirySplit>
    {
        public override string TableName => "OrderInquirySplits";
        public int OrderInquiryId { get; set; }

        [Display(Name = "No.")]
        public int SplitId { get; set; }

        [Display(Name = "Quantity")]
        public int Qty { get; set; }

        [Display(Name = "Shipment Date")]
        public DateTime? ShipmentDate { get; set; }

        [Display(Name = "No.")]
        public string SplitIdValue {
            get
            {
                if (this.SplitId > 0)
                    return this.SplitId.ToString();
                else
                    return string.Empty;
            }
        }

        [Display(Name = "Quantity")]
        public string QtyValue {
            get
            {
                if (this.Qty>0)
                    return this.Qty.ToString();
                else
                    return string.Empty;
            }
        }

        [Display(Name = "Shipment Date")]
        public string ShipmentDateText {
            get
            {
                if (this.ShipmentDate.HasValue)
                    return this.ShipmentDate.Value.ToString("yyyy-MM-dd");
                else
                    return string.Empty;
            }
        }
        
        internal override void LoadAdditionalProperties(DataRow row)
        {


        }
    }
   
}
