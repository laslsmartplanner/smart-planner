﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class BuyerService : ServiceBase<Buyer>
    {
        private BuyerDA _dataAccess = new BuyerDA();

        public bool Save(Buyer buyer)
        {
            if (!this.IsValidToSave(buyer)) return false;
            if (buyer.Id > 0)
                _dataAccess.Update(buyer);
            else
                _dataAccess.Add(buyer);
            return true;
        }

        public List<Buyer> ListBuyers(string orderBy, bool addEmpty)
        {
            return _dataAccess.GetList(orderBy, addEmpty);
        }
    }
}
