﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Buyer : EntityBase<Buyer>
    {
        public override string TableName => "Buyers";
        public override string NameField => "Name";

        [Required]
        [Display(Name = "Buyer Name")]
        public string Name { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Planned Color")]
        public string PlannedColor { get; set; }
        [Display(Name = "Actual Color")]
        public string ActualColor { get; set; }
        [Display(Name = "Changed Color")]
        public string ChangedColor { get; set; }
        
    }
}
