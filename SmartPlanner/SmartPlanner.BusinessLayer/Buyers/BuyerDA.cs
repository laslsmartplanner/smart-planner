﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class BuyerDA : EntityDataAccessBase<Buyer>
    {

        private DataAccess _dataAccess = new DataAccess();

        private void SetParameters(SqlCommand command, Buyer buyer)
        {
            command.Parameters.AddWithValue("@Name", buyer.Name);
            command.Parameters.AddWithValue("@Country", buyer.Country ?? string.Empty);
            command.Parameters.AddWithValue("@Address", buyer.Address ?? string.Empty);
            command.Parameters.AddWithValue("@Mobile", buyer.Mobile ?? string.Empty);
            command.Parameters.AddWithValue("@Email", buyer.Email ?? string.Empty);
            command.Parameters.AddWithValue("@PlannedColor", buyer.PlannedColor ?? string.Empty);
            command.Parameters.AddWithValue("@ActualColor", buyer.ActualColor ?? string.Empty);
            command.Parameters.AddWithValue("@ChangedColor", buyer.ChangedColor ?? string.Empty);
        }

        internal bool Add(Buyer buyer)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO Buyers
                      (Name, Country, Address, Mobile, Email, CreatedById, CreatedOn,PlannedColor,ActualColor,ChangedColor)
               VALUES (@Name, @Country, @Address, @Mobile, @Email, @CreatedById, @CreatedOn,@PlannedColor,@ActualColor,@ChangedColor)");
            this.SetParameters(command, buyer);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                buyer.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal bool Update(Buyer buyer)
        {
            SqlCommand command = new SqlCommand(@"UPDATE Buyers
                                                SET Name = @Name, Country = @Country, Address = @Address, Mobile = @Mobile, Email = @Email , UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn , PlannedColor=@PlannedColor, ActualColor=@ActualColor, ChangedColor=@ChangedColor
                                                WHERE   (Id = @Id)");
            command.Parameters.AddWithValue("@Id", buyer.Id);
            this.SetParameters(command, buyer);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

    }
}
