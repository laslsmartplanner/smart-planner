﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class CustomerResponsibility : EntityBase<CustomerResponsibility>
    {
        private LITUser _coordinator = null;
        private LITUser _commercial = null;
        private Factory _factory = null;

        public override string TableName => "CustomerResponsibilities";

        [Display(Name = "Factory")]
        public int FactoryId { get; set; }

        public int CustomerId { get; set; }

        [Display(Name = "Coordinator")]
        public int CoordinatorId { get; set; }

        [Display(Name = "Commercial")]
        public int CommercialId { get; set; }

        public Factory Factory
        {
            get
            {
                if (this._factory == null && this.FactoryId > 0)
                    this._factory = new FactoryDA().GetEntityById(this.FactoryId);
                return this._factory;
            }
        }

        public string FactoryName
        {
            get
            {
                return this.Factory?.FactoryName ?? string.Empty;
            }
        }

        public string CoordinatoryName
        {
            get
            {
                return this.Coordinator?.FullName ?? string.Empty;
            }
        }

        public string CommercialName
        {
            get
            {
                return this.Commercial?.FullName ?? string.Empty;
            }
        }


        public LITUser Coordinator
        {
            get
            {
                if (this.CoordinatorId > 0 && this._coordinator == null)
                    this._coordinator = new UserDA().GetEntityById(this.CoordinatorId);
                return this._coordinator;
            }
        }

        public LITUser Commercial
        {
            get
            {
                if (this.CommercialId > 0 && this._commercial == null)
                    this._commercial = new UserDA().GetEntityById(this.CommercialId);
                return this._commercial;
            }
        }
    }
}
