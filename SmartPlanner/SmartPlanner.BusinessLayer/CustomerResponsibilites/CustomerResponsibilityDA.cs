﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class CustomerResponsibilityDA : EntityDataAccessBase<CustomerResponsibility>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal List<CustomerResponsibility> ListForCustomerId(int customerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * FROM CustomerResponsibilities WHERE CustomerId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", customerId);

            return this.GetList(sqlCommand);
        }

        internal bool Add(CustomerResponsibility customerResponsibility)
        {
            SqlCommand sqlCommand = new SqlCommand(@"INSERT INTO CustomerResponsibilities(CustomerId,FactoryId,CoordinatorId,CommercialId, CreatedById, CreatedOn)
                                                                                                VALUES(@CustomerId,@FactoryId,@CoordinatorId,@CommercialId, @CreatedById, @CreatedOn)");
            try
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerResponsibility.CustomerId);
                sqlCommand.Parameters.AddWithValue("@FactoryId", customerResponsibility.FactoryId);
                sqlCommand.Parameters.AddWithValue("@CoordinatorId", customerResponsibility.CoordinatorId);
                sqlCommand.Parameters.AddWithValue("@CommercialId", customerResponsibility.CommercialId);
                this.SetCreatedDetails(sqlCommand);
                _dataAccess.ExecuteNonQuery(sqlCommand);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal bool DeleteCustomerResponsibility(int customerResponsibilityId)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"DELETE FROM  CustomerResponsibilities WHERE Id=@Id");

                command.Parameters.AddWithValue("@Id", customerResponsibilityId);

                _dataAccess.ExecuteNonQuery(command);
                command.Parameters.Clear();
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
