﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SmartPlanner.BusinessLayer.Attributes
{
    public class CompareWithAttribute: ValidationAttribute
    {
        private string _compareWith = string.Empty;
        private string _errorMessage = string.Empty;

        public CompareWithAttribute(string compareWith,string errorMessage)
        {
            this._compareWith = compareWith;
            this._errorMessage = errorMessage;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string text = Convert.ToString(value);
            PropertyInfo property = validationContext.ObjectInstance.GetType().GetProperty(this._compareWith);
            if (property == null) return new ValidationResult(this._errorMessage);
            if (string.IsNullOrWhiteSpace(text)) return new ValidationResult(this._errorMessage);
            string compareValue = Convert.ToString(property.GetValue(validationContext.ObjectInstance));
            if (compareValue.Equals(text))
                return ValidationResult.Success;
            return new ValidationResult(this._errorMessage);
        }
    }
}