﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.Common.Emails
{
   internal class EmailDA
    {
        private DataAccess _dataAccess = new DataAccess();
        internal DataRow GetById(int id)
        {
            SqlCommand sqlCommand = new SqlCommand("Select * FROM Emails Where Id=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", id);
            DataTable dt = _dataAccess.GetDataTable(sqlCommand);
            return dt.Rows.Count > 0 ? dt.Rows[0] : null;
        }

    }
}
