﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using static SmartPlanner.BusinessLayer.Common.Enums;
using SmartPlanner.BusinessLayer.Common.Emails;
using System.Data;

namespace SmartPlanner.BusinessLayer.Common
{
    public class Email
    {
        private EmailDA _dataAccess = new EmailDA();

        private void Send(string toEmail, string subject,string message)
        {
            Send(toEmail, string.Empty, subject, message);
        }

        private void Send(string to,string cc, string subject, string message)
        {
            try
            {
                MailMessage msg = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                msg.To.Add(to);
                if (!string.IsNullOrWhiteSpace(cc))
                    msg.CC.Add(cc);
                msg.Subject = subject;

                msg.Body = message;
                msg.IsBodyHtml = true;

                smtp.Send(msg);
            }
            catch (Exception)
            { }
        }

        public void SendUserCreatedEmail(string toEmail,string userName,string password)
        {
            DataRow emailData = _dataAccess.GetById((int)EnumEmailType.UserCreated);
            if (emailData == null) return;
            string message = Convert.ToString(emailData["Message"]);
            Send(toEmail, Convert.ToString(emailData["Subject"]), string.Format(message, userName, password));
        }

        public void SendUserPasswordResetRequestEmail(string toEmail, string userName)
        {
            DataRow emailData = _dataAccess.GetById((int)EnumEmailType.PasswordResetRequest);
            if (emailData == null) return;
            string message = Convert.ToString(emailData["Message"]);
            Send(toEmail, Convert.ToString(emailData["Subject"]), string.Format(message, userName));
        }

        public void SendUserPasswordResetEmail(string toEmail, string userName, string password)
        {
            DataRow emailData = _dataAccess.GetById((int)EnumEmailType.PasswordReset);
            if (emailData == null) return;
            string message = Convert.ToString(emailData["Message"]);
            Send(toEmail, Convert.ToString(emailData["Subject"]), string.Format(message, userName, password));
        }

        private string LoadEmailBody(EmailNotificationDTO dto,string body)
        {
            var fields = body.Split('<', '>').Where((item, index) => index % 2 != 0).ToList();

            foreach(string field in fields)
            {
                string value = Convert.ToString(dto.GetType().GetProperty(field)?.GetValue(dto));
                body = body.Replace($"<{field}>",  value);
            }
            return body;
        }

        public void SendChangedNotification(EmailNotificationDTO dto,int emailId)
        {
            DataRow emailData = _dataAccess.GetById(emailId);
            if (emailData == null) return;
            string message = LoadEmailBody(dto, Convert.ToString(emailData["Message"]));

            List<EmailReceiver> receivers = new EmailReceiverService().ListByEmailId(emailId);

            string to = string.Join(",", receivers.Where(x => x.ToOrCC == EnumEmailSendingOptions.To).Select(e=>e.ReceiverEmail));
            string cc = string.Join(",", receivers.Where(x => x.ToOrCC == EnumEmailSendingOptions.CC).Select(e => e.ReceiverEmail));

            if (!string.IsNullOrWhiteSpace(to))
                Send(to, cc, Convert.ToString(emailData["Subject"]), message);
        }
        public void SendEmailNotification(EmailNotificationDTO dto, int emailId, string to)
        {
            DataRow emailData = _dataAccess.GetById(emailId);
            if (emailData == null) return;
            string message = LoadEmailBody(dto, Convert.ToString(emailData["Message"]));

            if (!string.IsNullOrWhiteSpace(to))
                Send(to, string.Empty, Convert.ToString(emailData["Subject"]), message);
        }

        private string LoadEmailBody(EmailPlanReminderDTO dto, string body)
        {
            var fields = body.Split('<', '>').Where((item, index) => index % 2 != 0).ToList();

            foreach (string field in fields)
            {
                string value = Convert.ToString(dto.GetType().GetProperty(field)?.GetValue(dto));
                body = body.Replace($"<{field}>", value);
            }
            return body;
        }

        public void SendEmailPlanReminder(EmailPlanReminderDTO dto, int emailId)
        {
            DataRow emailData = _dataAccess.GetById(emailId);
            if (emailData == null) return;
            string message = LoadEmailBody(dto, Convert.ToString(emailData["Message"]));

            List<EmailReceiver> receivers = new EmailReceiverService().ListByEmailId(emailId);

            string to = string.Join(",", receivers.Where(x => x.ToOrCC == EnumEmailSendingOptions.To).Select(e => e.ReceiverEmail));

            if (!string.IsNullOrWhiteSpace(to))
                Send(to, string.Empty, Convert.ToString(emailData["Subject"]), message);
        }
    }
}
