﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.Common
{
    public static class Enums
    {
        public enum UserAccessType
        {
            NoAccess = 0,
            ReadOnly = 1,
            ReadWrite = 2
        }

        public enum EnumEmailType
        {
            UserCreated = 1,
            PasswordResetRequest = 2,
            PasswordReset = 3,
            FabricInhouseDateChanged = 4,
            FtrimsInhouseDateChanged = 5,
            PlannedCutDateChanged = 6,
            FabricStichingDateChanged = 7,
            FabricFinishingDateChanged = 8,
            OrderInquiryCreated = 9,
            OrderInquiryUpdated = 10,
            SMVNewInquiry = 11,
            SMVRevisionInquiry = 12,
            SMVInquiryUpdated = 13,
            CompanyHolidayFDAdded = 14,
            CompanyHolidayHDAdded = 15,
            LineHolidayFDAdded = 16,
            LineHolidayHDAdded = 17,
            NotPlacedOrderReminder = 18
        }

        public enum EnumAccessModules
        {
            UserLevelManagement = 1,
            UserManagement = 2,
            SystemConfiguration = 3,
            OrderManagement = 4,
            StyleManagement = 5,
            SMVManagement = 6,
            CalendarManagement = 7,
            DailyOutput = 8,
            Reports = 9,
            TnAManagement = 10,
            PlanningManagement = 11,
            StyleManagementIE = 12,
            SMVManagementIE = 13,
            OrderManagementCut = 14,
            SizeColorManagement = 15,
            FeatureOperationManagement = 16,
            OrderInquiryManagement = 17

        }
        public enum EnumConfigurationTables
        {
            FactoryNames = 1,
            OrderStatus = 2,
            PlanningSeason = 3,
            DestinationMedia = 4,
            Destination = 5,
            BuyGroup = 6,
            Vendor = 7,
            Division = 8,
            Product = 9,
            Location = 10,
            Method = 11,
            POType = 12,
            Appearance = 13,
            ProductType = 14,
            ProductGroup = 15,
            ProductCategory = 16,
            FabricType = 17,
            TimeTable = 18,
            Operation = 19,
            SampleStatus = 20,
            Category = 21,
            EmblishmentPlant = 22,
            FeatureList = 23,
            DeliverTo = 24,
            DistributeFrom = 25,
            WashingPlant = 26,
            PrintingPlant = 27,
            Size = 28,
            PriceType = 29,
            SewingPlant = 30,
            Colors = 31
        }

        public enum EnumEmailSendingOptions
        {
            To = 0,
            CC = 1
        }
        public enum EnumOrderInquiryStatus
        {
            Confirmed = 1,
            NotConfirmed = 2,
            Pending = 3
        }

        public enum EnumSampleApprovalStatus
        {
            Approved = 1,
            NotApproved = 2
        }

        public enum EnumAdditionalFileType
        {
            SketchFile = 1,
            ThreadSheetFile = 2,
            TrimConsumptionFile = 3,
            PPRAReportFile = 4,
            Other = 5
        }

        public enum EnumSMVInquiryType
        {
            None = 1,
            Request = 2,
            Accept = 3
        }

        public enum EnumRoles
        {
            CEO = 1,
            COO = 2,
            OperationsDirectors = 3,
            GroupPM = 4,
            GroupSceduler = 5,
            GroupTechnical = 6,
            GroupIE = 7,
            HeadCommer = 8,
            GM = 9,
            FM = 10,
            PM = 11,
            SourcingManager = 12,
            CuttingMgr = 13,
            WashingMgr = 14,
            FinishingMgr = 15,
            HeadQA = 16,
            PlaningMgr = 17,
            HeadFabrictec = 18,
            HeadPattern = 19,
            Merchant = 20,
            IEPPcell = 21,
            FinishingINC = 22
        }

        public enum EnumOrderInquiryConfirmationStatuses
        {
            None = 0,
            PlannerConfirmed = 1,
            PendingConfirmation = 2
        }

        public enum EnumTnADefaults
        {
            FabricStitching = 16
        }

    }
}
