﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SmartPlanner.BusinessLayer.Common;

namespace SmartPlanner.BusinessLayer
{
    public abstract class EntityBase<T> where T: class
    {
        public int Id { get ; set; }
        public int CreatedById { get; internal set; }
        public DateTime CreatedOn { get; internal set; }
        public int UpdatedById { get; internal set; }
        public DateTime? UpdatedOn { get; internal set; }
        public List<string> BrokenBusinessRules { get; set; }
        public virtual List<string> IsAlreadyExistsProperties { get; }
        public virtual string TableName { get; }
        public virtual string NameField { get; }

        public string BrokenBusinessRulesText
        {
            get
            {
                if (this.BrokenBusinessRules?.Count > 0)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (string rule in this.BrokenBusinessRules)
                    {
                        if (stringBuilder.Length > 0)
                            stringBuilder.AppendLine(",");
                        stringBuilder.AppendLine(rule);
                    }
                    return stringBuilder.ToString();
                }
                return string.Empty;
            }
        }

        internal virtual void Load(DataRow row, DataColumnCollection columns)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (DataColumn column in columns)
            {
                PropertyInfo prop = properties.FirstOrDefault(x => x.Name.Equals(column.ColumnName, StringComparison.OrdinalIgnoreCase));
                if (prop != null && prop.CanWrite && row[column.ColumnName] != DBNull.Value)
                {
                    try
                    {
                        prop.SetValue(this, row[column.ColumnName]);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Column {row[column.ColumnName]} {ex.Message}", ex.InnerException);
                    }
                }
            }
            this.LoadAdditionalProperties(row);
        }

        internal virtual List<T> Load(DataTable dt)
        {
            List<T> list = new List<T>();
            DataColumnCollection columns = dt.Columns;
            foreach (DataRow row in dt.Rows)
            {
                dynamic newEntity = Activator.CreateInstance<T>();
                newEntity.Load(row, columns);
                list.Add(newEntity);
            }
            return list;
        }

        internal virtual void LoadAdditionalProperties(DataRow row )
        {
        }

        internal void SetId(Object  id)
        {
            int intKey = 0;
            int.TryParse(Convert.ToString(id), out intKey);
            this.Id = intKey;
        }
        internal virtual bool IsValidToSave()
        {
            if (this.BrokenBusinessRules == null) this.BrokenBusinessRules = new List<string>();
            this.BrokenBusinessRules.Clear();
            if (this.IsAlreadyExistsProperties?.Count > 0)
            {
                EntityDataAccessBase<T> da = new EntityDataAccessBase<T>();
                foreach (string field in this.IsAlreadyExistsProperties)
                {
                    PropertyInfo property = typeof(T).GetProperty(field);
                    if (property == null) continue;
                    object value = property.GetValue(this);
                    if (value == null) continue;
                    int id = da.GetIdByField(field, value);
                    if ((this.Id > 0 && id > 0 && id != this.Id) || (this.Id < 1 && id > 0))
                        this.BrokenBusinessRules.Add($"{value} already exists");
                }
            }
            this.ValidateOnSave();
            return this.BrokenBusinessRules.Count < 1;
        }

        internal virtual void ValidateOnSave()
        { }
    }
}
