﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public abstract class ServiceBase<T> where T : class
    {
        private EntityDataAccessBase<T> _dataAccess = new EntityDataAccessBase<T>();
        internal virtual bool  IsValidToSave(T entity)
        {
            MethodInfo method = entity.GetType().GetMethod("IsValidToSave", BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Instance);
            return Convert.ToBoolean(method?.Invoke(entity, null) ?? true);
        }

        public List<T> GetList()
        {
            return _dataAccess.GetList();
        }

        public T GetEntityById(int id)
        {
            return _dataAccess.GetEntityById(id);
        }

        public virtual void DeleteById(int id)
        {
            _dataAccess.DeleteById(id);
        }
    }
}
