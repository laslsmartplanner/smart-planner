﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.Common
{
    internal  class EntityDataAccessBase<T> where T:class
    {
        private DataAccess dataAccess = new DataAccess();
        private string tableName = string.Empty;
        private string nameField = string.Empty;

        private string TableName
        {
            get
            {
                if(string.IsNullOrEmpty(this.tableName))
                {
                    T entity = Activator.CreateInstance<T>();
                    this.tableName = Convert.ToString(typeof(T).GetProperty("TableName")?.GetValue(entity));
                }
                return this.tableName;
            }
        }

        private string NameField
        {
            get
            {
                if (string.IsNullOrEmpty(this.nameField))
                {
                    T entity = Activator.CreateInstance<T>();
                    this.nameField = Convert.ToString(typeof(T).GetProperty("NameField")?.GetValue(entity));
                }
                return this.nameField;
            }
        }

        internal List<T> GetList(string orderBy = "", bool addEmpty = false)
        {
            List<T> list = new List<T>();
            if (string.IsNullOrWhiteSpace(this.TableName))
                return list;

            string order = string.Empty;

            if (!string.IsNullOrWhiteSpace(orderBy))
                order = $"ORDER BY {orderBy}";

            SqlCommand sqlCommand = new SqlCommand($"Select * From {this.TableName} {order}");
            try
            {
                if (!string.IsNullOrEmpty(this.NameField) && addEmpty)
                {
                    T entity = Activator.CreateInstance<T>();
                    typeof(T).GetProperty(NameField)?.SetValue(entity, "Select One...");
                    list.Add(entity);
                }
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                DataColumnCollection columns = dt.Columns;
                foreach (DataRow row in dt.Rows)
                {
                    dynamic newEntity = Activator.CreateInstance<T>();
                    newEntity.Load(row, columns);
                    list.Add(newEntity);
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal List<T> GetList(string condition,List<SqlParameter> parameters)
        {
            List<T> list = new List<T>();
            if (string.IsNullOrWhiteSpace(this.TableName))
                return list;

            string where = string.IsNullOrWhiteSpace(condition) ? string.Empty : $" Where {condition}";           

            SqlCommand sqlCommand = new SqlCommand($"Select * From {this.TableName} {where}");
            if (parameters != null && parameters.Count > 0)
                sqlCommand.Parameters.AddRange(parameters.ToArray());
            try
            {
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                DataColumnCollection columns = dt.Columns;
                foreach (DataRow row in dt.Rows)
                {
                    dynamic newEntity = Activator.CreateInstance<T>();
                    newEntity.Load(row, columns);
                    list.Add(newEntity);
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal List<T> GetList(SqlCommand sqlCommand, bool addEmpty = false)
        {
            List<T> list = new List<T>();
            if (sqlCommand == null) return list;
            try
            {
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                DataColumnCollection columns = dt.Columns;
                foreach (DataRow row in dt.Rows)
                {
                    dynamic newEntity = Activator.CreateInstance<T>();
                    newEntity.Load(row, columns);
                    list.Add(newEntity);
                }
                if (list.Count > 0 && addEmpty)
                    list.Insert(0, Activator.CreateInstance<T>());

                return list;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal List<T> GetList(DataTable dt,bool addEmpty=false)
        {
            List<T> list = new List<T>();

            try
            {
                DataColumnCollection columns = dt.Columns;
                foreach (DataRow row in dt.Rows)
                {
                    dynamic newEntity = Activator.CreateInstance<T>();
                    newEntity.Load(row, columns);
                    list.Add(newEntity);
                }
                if (list.Count > 0 && addEmpty)
                    list.Insert(0, Activator.CreateInstance<T>());

                return list;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal T GetEntity(string condition, List<SqlParameter> parameters)
        {
            T entity = Activator.CreateInstance<T>();
            string tableName = Convert.ToString(typeof(T).GetProperty("TableName")?.GetValue(entity));
            if (string.IsNullOrWhiteSpace(tableName))
                return null;

            string where = string.IsNullOrWhiteSpace(condition) ? string.Empty : $" Where {condition}";

            SqlCommand sqlCommand = new SqlCommand($"Select * From {tableName} {where}");
            if (parameters != null && parameters.Count > 0)
                sqlCommand.Parameters.AddRange(parameters.ToArray());
            try
            {
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                if (dt.Rows.Count == 0)
                    return null;
                DataColumnCollection columns = dt.Columns;

                dynamic newEntity = Activator.CreateInstance<T>();
                newEntity.Load(dt.Rows[0], columns);

                return newEntity;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal T GetEntity(SqlCommand sqlCommand)
        {           
            try
            {
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                if (dt.Rows.Count == 0)
                    return null;
                DataColumnCollection columns = dt.Columns;

                dynamic newEntity = Activator.CreateInstance<T>();
                newEntity.Load(dt.Rows[0], columns);

                return newEntity;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal T GetEntityById(int id)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * From {this.TableName} Where Id = @Id");
            sqlCommand.Parameters.AddWithValue("@Id", id);
            try
            {
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                if (dt.Rows.Count == 0)
                    return null;
                DataColumnCollection columns = dt.Columns;

                dynamic newEntity = Activator.CreateInstance<T>();
                newEntity.Load(dt.Rows[0], columns);

                return newEntity;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal void SetCreatedDetails(SqlCommand command)
        {
            command.Parameters.AddWithValue("@CreatedById", GV.LoggedUser().Id);
            command.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
        }
        internal void SetUpdatedDetails(SqlCommand command)
        {
            command.Parameters.AddWithValue("@UpdatedById", GV.LoggedUser().Id);
            command.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
        }
        internal bool IsExists(string field, object value)
        {
            SqlCommand command = new SqlCommand($"Select 1 from {this.TableName} Where {field}=@Value");
            command.Parameters.AddWithValue("@Value", value);
            return dataAccess.IsExists(command);
        }
        internal int GetIdByField(string field,object value)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select Id From {this.TableName} Where {field} = @Value");
            sqlCommand.Parameters.AddWithValue("@Value", value);
            try
            {
                int key = 0;
                object ret = dataAccess.ExecuteScalar(sqlCommand);
                if (ret == null) return key;
                int.TryParse(Convert.ToString(ret), out key);
                return key;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }
        internal object GetValue(string selectField,string conditionField,object conditionValue)
        {
            SqlCommand command = new SqlCommand($"Select {selectField} From {this.TableName} Where {conditionField}=@{conditionField}");
            command.Parameters.AddWithValue($"@{conditionField}", conditionValue);
            return dataAccess.ExecuteScalar(command);
        }
        internal void DeleteById(int id)
        {            
            SqlCommand command = new SqlCommand($"Delete {this.TableName} Where Id=@Id");
            command.Parameters.AddWithValue("@Id", id);
            dataAccess.ExecuteNonQuery(command);
        }

        internal List<T> SearchBy(string field, string value)
        {
            List<T> list = new List<T>();
            if (string.IsNullOrWhiteSpace(this.TableName))
                return list;

            string where = $"Where {field} like @Value";

            SqlCommand sqlCommand = new SqlCommand($"Select * From {this.TableName} {where}");
            sqlCommand.Parameters.AddWithValue("@Value", $"%{value}%");
            try
            {
                DataTable dt = dataAccess.GetDataTable(sqlCommand);
                DataColumnCollection columns = dt.Columns;
                foreach (DataRow row in dt.Rows)
                {
                    dynamic newEntity = Activator.CreateInstance<T>();
                    newEntity.Load(row, columns);
                    list.Add(newEntity);
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataAccess.DBDisconnect();
            }
        }

        internal object GetNullIfLessOne(int value)
        {
            return value < 1 ? (object)DBNull.Value : value;
        }
    }
}
