﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer.Common
{
    public class Helper
    {
        public static UserAccessType GetAccessType(EnumAccessModules accessModule)
        {
            if (GV.LoggedUser() == null)
                return UserAccessType.NoAccess;
            List<UserLevelAccessModule> modules = GV.LoggedUser().UserLevel.GetAccessModules();
            return modules.FirstOrDefault(x => x.AccessModuleId == (int)accessModule)?.AccessType ?? UserAccessType.NoAccess;
        }

        public static string SaveFile(HttpPostedFileBase file)
        {
            string uploadPath = Path.Combine(HttpContext.Current.Server.MapPath("~"), ConfigurationManager.AppSettings["DocumentsDirectory"].ToString());

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);
            
            string fileName = file.FileName;

            int count = 1;
            string fileNameOnly = Path.GetFileNameWithoutExtension(fileName);
            string extension = Path.GetExtension(fileName);

            while (File.Exists(Path.Combine(uploadPath, fileName)))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                fileName = Path.Combine(uploadPath, tempFileName + extension);
            }
            fileName = Path.Combine(uploadPath, fileName);

            file.SaveAs(fileName);
            return fileName;
        }
    }
}
