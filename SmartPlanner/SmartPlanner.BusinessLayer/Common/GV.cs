﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartPlanner.BusinessLayer.Common
{
    public static class GV
    {
        //public static LITUser LoggedUser { get; set; }
        public static string DisplayDateFormat { get { return ConfigurationManager.AppSettings["DisplayDateFormat"].ToString(); } }

        public static LITUser LoggedUser()
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                return (LITUser)HttpContext.Current.Session["CurrentUser"];
            }
            return null;
        }

    }
}
