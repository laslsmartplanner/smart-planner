﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class TnADefaultDA : EntityDataAccessBase<TnADefault>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal List<TnADefault> ListForPOType(int poTypeId)
        {
            SqlCommand command = new SqlCommand(@"select d.Id,d.KeyProcess,d.DisplayOrder,ISNULL(p.Duration,d.Duration) Duration 
                                                from TnADefaults d WITH(NOLOCK) 
                                                LEFT JOIN POTypeTnADurations p WITH(NOLOCK) ON d.Id = p.TnaDefaultId and p.PoTypeId=@POTypeId
                                                order by DisplayOrder");
            command.Parameters.AddWithValue("@POTypeId", poTypeId);
            return this.GetList(command);
        }

        internal List<TnADefault> ListTnADefaults()
        {
            return this.GetList("DisplayOrder");
        }
        internal void Update(TnADefault tnaDefault)
        {
            SqlCommand command = new SqlCommand(@"UPDATE TnADefaults
                                                SET Duration =@Duration , DisplayOrder =@DisplayOrder 
                                                WHERE(Id = @Id )");
            command.Parameters.AddWithValue("@Duration", tnaDefault.Duration);
            command.Parameters.AddWithValue("@Id", tnaDefault.Id);
            command.Parameters.AddWithValue("@DisplayOrder", tnaDefault.DisplayOrder);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal int GetPlannedShipments(int factoryId, DateTime fromDate, DateTime toDate)
        {
            int noOfShipments = 0;

            string whereCondition = factoryId > 0 ? " FactoryNameId = @FactoryId" : string.Empty;


            if (fromDate != null && toDate != null)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " Cast(EndDate as Date) >= Cast(@FromDate as Date) AND Cast(EndDate as Date) <= Cast(@ToDate as Date)" : " AND Cast(EndDate as Date) >= Cast(@FromDate as Date) AND Cast(EndDate as Date) <= Cast(@ToDate as Date)";
            }

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " AND " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"SELECT ISNULL(count(OrderDetailsId),0) FROM TnACalendars tc, OrderDetails od, Factories f where TnADefaultId = 21 and tc.OrderDetailsId = od.Id and od.FactoryNameId = f.Id { whereCondition} ");

            if (factoryId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            }

            if (fromDate != null && toDate != null)
            {
                sqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
                sqlCommand.Parameters.AddWithValue("@ToDate", toDate);
            }

            try
            {
                object ret = _dataAccess.ExecuteScalar(sqlCommand);
                if (ret == null)
                    noOfShipments = 0;
                else
                    noOfShipments = Convert.ToInt32(ret);

            }
            catch (Exception ex)
            {
                throw;
            }

            return noOfShipments;
        }

        internal int GetOnTimeShipments(int factoryId, DateTime fromDate, DateTime toDate)
        {
            int noOfShipments = 0;

            string whereCondition = factoryId > 0 ? " FactoryNameId = @FactoryId" : string.Empty;


            if (fromDate != null && toDate != null)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " Cast(EndDate as Date) >= Cast(@FromDate as Date) AND Cast(EndDate as Date) <= Cast(@ToDate as Date) AND Cast(EndDate as Date) >= Cast(ActualEndDate as Date)" : " AND Cast(EndDate as Date) >= Cast(@FromDate as Date) AND Cast(EndDate as Date) <= Cast(@ToDate as Date) AND Cast(EndDate as Date) >= Cast(ActualEndDate as Date)";
            }

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " AND " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"SELECT ISNULL(count(OrderDetailsId),0) FROM TnACalendars tc, OrderDetails od, Factories f where TnADefaultId = 21 and tc.OrderDetailsId = od.Id and od.FactoryNameId = f.Id { whereCondition} ");

            if (factoryId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            }

            if (fromDate != null && toDate != null)
            {
                sqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
                sqlCommand.Parameters.AddWithValue("@ToDate", toDate);
            }

            try
            {
                object ret = _dataAccess.ExecuteScalar(sqlCommand);
                if (ret == null)
                    noOfShipments = 0;
                else
                    noOfShipments = Convert.ToInt32(ret);

            }
            catch (Exception ex)
            {
                throw;
            }

            return noOfShipments;
        }

        internal void SaveTnAForPOType(int poTypeId, int tnaDefId, double duration)
        {
            SqlCommand command = new SqlCommand(@"DELETE POTypeTnADurations
                                                  WHERE PoTypeId=@PoTypeId AND TnADefaultId=@TnADefaultId");
            command.Parameters.AddWithValue("@PoTypeId", poTypeId);
            command.Parameters.AddWithValue("@TnADefaultId", tnaDefId);

            _dataAccess.ExecuteNonQuery(command);

            command.CommandText = @"INSERT INTO POTypeTnADurations
                                      (PoTypeId, TnADefaultId, Duration, CreatedById, CreatedOn)
                                    VALUES  (@PoTypeId, @TnADefaultId, @Duration, @CreatedById, @CreatedOn)";
            command.Parameters.AddWithValue("@Duration", duration);
            command.Parameters.AddWithValue("@CreatedById", GV.LoggedUser().Id);
            command.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            _dataAccess.ExecuteNonQuery(command);
        }
    }

}
