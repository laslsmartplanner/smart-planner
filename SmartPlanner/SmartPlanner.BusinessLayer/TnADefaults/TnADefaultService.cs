﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class TnADefaultService : ServiceBase<TnADefault>
    {
        private TnADefaultDA _dataAccess = new TnADefaultDA();

        public List<TnADefault> ListForPOType(int poTypeId)
        {
            return _dataAccess.ListForPOType(poTypeId);
        }

        public List<TnADefault> ListTnADefaults()
        {
            return _dataAccess.ListTnADefaults();
        }
        public void Update(TnADefault tnaDefault)
        {
            _dataAccess.Update(tnaDefault);
        }

        public decimal GetOnTimeDelivery(int factoryId, DateTime fromDate, DateTime toDate)
        {
            int plannedShipments = _dataAccess.GetPlannedShipments(factoryId, fromDate, toDate);
            int onTimeShipments = _dataAccess.GetOnTimeShipments(factoryId, fromDate, toDate);

            if (onTimeShipments.Equals(0))
                return 0;
            else
                return onTimeShipments * 100/ plannedShipments;
        }
        public void SaveTnAForPOType(int poTypeId, int tnaDefId, double duration)
        {
            _dataAccess.SaveTnAForPOType(poTypeId, tnaDefId, duration);
        }
    }
}
