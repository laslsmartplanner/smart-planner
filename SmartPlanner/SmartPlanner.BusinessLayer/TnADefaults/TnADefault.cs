﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class TnADefault : EntityBase<TnADefault>
    {
        public override string TableName => "TnADefaults";

        public string KeyProcess { get; set; }
        public int Duration { get; set; }
        public int DisplayOrder { get; set; }
    }
}
