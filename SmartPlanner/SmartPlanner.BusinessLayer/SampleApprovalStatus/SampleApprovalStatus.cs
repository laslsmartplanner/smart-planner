﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class SampleApprovalStatus : EntityBase<SampleApprovalStatus>
    {
        public override string TableName => "SampleApprovalStatus";

        public string StatusName { get; set; }

    }
}
