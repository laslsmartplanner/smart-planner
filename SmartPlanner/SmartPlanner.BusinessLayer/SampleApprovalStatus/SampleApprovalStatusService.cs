﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class SampleApprovalStatusService : ServiceBase<SampleApprovalStatus>
    {
        private SampleApprovalStatusDA _dataAccess = new SampleApprovalStatusDA();

        public List<SampleApprovalStatus> ListSampleApproval()
        {
            return _dataAccess.GetList();
        }
    }
}
