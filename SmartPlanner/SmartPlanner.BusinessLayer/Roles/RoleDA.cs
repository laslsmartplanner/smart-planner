﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class RoleDA : EntityDataAccessBase<Role>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool UpdateUser(int id, int userId)
        {
            SqlCommand command = new SqlCommand(@"UPDATE    Roles
                                                    SET UserId =@UserId, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn 
                                                    WHERE     (Id = @Id)");

            command.Parameters.AddWithValue("@UserId", userId);
            command.Parameters.AddWithValue("@Id", id);

            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
    }
}
