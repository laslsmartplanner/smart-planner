﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class RoleService : ServiceBase<Role>
    {
        private RoleDA _dataAccess = new RoleDA();

        public bool UpdateUser(int id, int userId)
        {
            return _dataAccess.UpdateUser(id, userId);
        }
    }
}
