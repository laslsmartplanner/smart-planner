﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Role : EntityBase<Role>
    {
        private LITUser user = null;

        public override string TableName => "Roles";
        
        [Display(Name ="Role")]
        public string RoleName { get; set; }

        [Display(Name ="User")]
        public int UserId { get; set; }

        public LITUser User
        {
            get
            {
                if (this.user == null)
                    this.user = new UserDA().GetEntityById(this.UserId);
                return this.user;
            }
        }
        public string UserName
        {
            get
            {
                return this.User?.FullName ?? string.Empty;
            }
        }
    }

}
