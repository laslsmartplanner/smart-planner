﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartPlanner.BusinessLayer
{
    public class StyleDetail : EntityBase<StyleDetail>
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        private ConfigurationTableValue _appearance;
        private ConfigurationTableValue _productType;
        private ConfigurationTableValue _productGroup;
        private ConfigurationTableValue _productCategory;
        private ConfigurationTableValue _fabricType;
        private ConfigurationTableValue _division;
        private ConfigurationTableValue _operation;
        private ConfigurationTableValue _timeTable;
        private SampleApprovalStatus _sampleStatus;
        private ConfigurationTableValue _category;
        
        private SubCategory _subCategory;
        private Buyer _buyer;
        private Customer _customer;
        private LITUser _merchant;
        private LITUser _industrialEngineer;

        private ConfigurationTableValue _product;
        private StyleProduct _styleProduct;
        

        public override string TableName => "StyleDetails";

        #region Properties

        [Required]
        [Display(Name = "Style Number")]
        public string StyleNumber { get; set; }

        [Display(Name = "Is Multipiece")]
        public bool IsMultiPiece { get; set; }

        [Required]
        [Display(Name ="Buyer")]
        public int BuyerId { get; set; }

        [Display(Name = "Appearance")]
        public int AppearanceId { get; set; }

        [Display(Name = "Material Code")]
        public string MaterialCode { get; set; }

        [Display(Name = "Color Code")]
        public string ColorCode { get; set; }

        [Display(Name = "Order Code")]
        public string OrderCode { get; set; }

        [Display(Name = "FG Code")]
        public string FGCode { get; set; }
                
        [Display(Name = "Sketch File")]
        public HttpPostedFileBase SketchFile { get; set; }

        [Display(Name = "Sketch File Name")]
        public string SketchFileName { get; set; }

        [Display(Name = "Fabric Type")]
        public int FabricTypeId { get; set; }

        [Display(Name = "Order Qty.")]
        public int OrderQty { get; set; }

        [Display(Name = "Product Reference")]
        public string ProductReference { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Base Style")]
        public string BaseStyle { get; set; }

        [Display(Name = "Design Reference")]
        public string DesignReference { get; set; }

        [Display(Name = "Division")]
        public int DivisionId { get; set; }

        [Display(Name = "E Description")]
        public string EDescription { get; set; }

        [Display(Name = "Graphic Number")]
        public string GraphicNumber { get; set; }

        [Display(Name = "No of Sheets")]
        public int NoOfSheets { get; set; }

        [Display(Name = "Operation")]
        public int OperationId { get; set; }

        [Display(Name = "Process")]
        public string Process { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Set")]
        public string SetValue { get; set; }

        [Display(Name = "SG BOM")]
        public string SgBom { get; set; }

        [Display(Name = "Time Table")]
        public int TimeTableId { get; set; }

        [Display(Name = "Launched Date")]
        public DateTime? LaunchedDate { get; set; }
        
        [Display(Name = "Total SMV")]
        public decimal TotalSMV { get; set; }

        [Display(Name = "Sample Status")]
        public int SampleStatusId { get; set; }
        
        [Display(Name = "Confirmed?")]
        public bool IsConfirmed { get; set; }

        [Display(Name = "Merchant")]
        public int MerchantId { get; set; }

        [Display(Name = "Department")]
        public string Department { get; set; }

        [Display(Name = "Sample Stage")]
        public string SampleStage { get; set; }

        [Display(Name = "Special Concern")]
        public string SpecialConcern { get; set; }

        [Display(Name = "Special Instruction")]
        public string SpecialInstruction { get; set; }

        [Display(Name = "Costing Deadline")]
        public DateTime? CostingDeadline { get; set; }

        [Display(Name = "Revision Sample Stage")]
        public string RevisionSampleStage { get; set; }

        [Display(Name = "Reason to Revise")]
        public string ReasonToRevise { get; set; }

        [Display(Name = "Option Request")]
        public string OptionRequest { get; set; }
        
        [Display(Name = "Sketch File Name")]
        public string OldSketchFileName { get; set; }
        
        [Display(Name = "Sketch File Name")]
        public string SketchFilePath { get; set; }

        [Display(Name = "Constructive Comments")]
        public string CustomerComment { get; set; }
         
        [Display(Name = "Customer Code")]
        public string CustomerCode { get; set; }

        [Display(Name = "Product Description")]
        public string ProductDesc { get; set; }

        [Display(Name = "Planned Color")]
        public string PlannedColor { get; set; }

        [Display(Name = "Actual Color")]
        public string ActualColor { get; set; }

        [Display(Name = "Changed Color")]
        public string ChangedColor { get; set; }

        [Display(Name = "Industrial Engineer")]
        public int IEId { get; set; }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        [Display(Name = "Production SMV")]
        public decimal ProductionSMV { get; set; }

        [Display(Name = "Cost SMV")]
        public decimal CostSMV { get; set; }

        [Display(Name = "CM")]
        public decimal CM { get; set; }

        #endregion

        public Buyer Buyer
        {
            get
            {
                if (this._buyer == null && this.BuyerId > 0)
                    this._buyer = new BuyerService().GetEntityById(this.BuyerId);
                return this._buyer;
            }
        }

        #region Configuration Properties

        public ConfigurationTableValue Appearance
        {
            get
            {
                if (this._appearance == null && this.AppearanceId > 0)
                    this._appearance = _configValueService.GetEntityById(this.AppearanceId);
                return this._appearance;
            }
        }

        public ConfigurationTableValue FabricType
        {
            get
            {
                if (this._fabricType == null && this.FabricTypeId> 0)
                    this._fabricType = _configValueService.GetEntityById(this.FabricTypeId);
                return this._fabricType;
            }
        }

        public ConfigurationTableValue Division
        {
            get
            {
                if (this._division == null && this.DivisionId > 0)
                    this._division = _configValueService.GetEntityById(this.DivisionId);
                return this._division;
            }
        }

        public ConfigurationTableValue Operation
        {
            get
            {
                if (this._operation == null && this.OperationId > 0)
                    this._operation = _configValueService.GetEntityById(this.OperationId);
                return this._operation;
            }
        }

        public ConfigurationTableValue TimeTable
        {
            get
            {
                if (this._timeTable == null && this.TimeTableId > 0)
                    this._timeTable = _configValueService.GetEntityById(this.TimeTableId);
                return this._timeTable;
            }
        }

        public SampleApprovalStatus SampleStatus
        {
            get
            {
                if (this._sampleStatus == null && this.SampleStatusId > 0)
                    this._sampleStatus = new SampleApprovalStatusService().GetEntityById(this.SampleStatusId);
                return this._sampleStatus;
            }
        }
        
        public Customer Customer
        {
            get
            {
                if (this._customer == null && this.CustomerId > 0)
                    this._customer = new CustomerService().GetEntityById(this.CustomerId);
                return this._customer;
            }
        }

        public LITUser Merchant
        {
            get
            {
                if (this._merchant == null && this.MerchantId > 0)
                    this._merchant = new UserService().GetEntityById(this.MerchantId);
                return this._merchant;
            }
        }

        public LITUser IndustrialEngineer
        {
            get
            {
                if (this._industrialEngineer == null && this.IEId > 0)
                    this._industrialEngineer = new UserService().GetEntityById(this.IEId);
                return this._industrialEngineer;
            }
        }

        public List<ConfigurationTableValue> Products { get; set; }

        //public StyleProduct StyleProduct { get; set; }

        public List<IdNameDTO> StyleProducts { get; set; }


        #endregion

        #region Support properties

        public List<ConfigurationTableValue> AppearanceList { get; set; }        
        public List<ConfigurationTableValue> FabricTypes { get; set; }
        public List<ConfigurationTableValue> Divisions { get; set; }
        public List<ConfigurationTableValue> Operations { get; set; }
        public List<ConfigurationTableValue> TimeTables { get; set; }
        public List<SampleApprovalStatus> SampleStatuses { get; set; }
        public List<ConfigurationTableValue> Categories { get; set; }
        
        public List<Buyer> Buyers { get; set; }
        public List<Customer> Customers { get; set; }
        public List<LITUser> Merchants { get; set; }
        public List<ConfigurationTableValue> FeaturesList { get; set; }
        public List<LITUser> IndustrialEngineers { get; set; }

        #endregion

        internal override void ValidateOnSave()
        {
            if (new StyleDetailDA().IsStyleExists(this.StyleNumber, this.CustomerId))
                this.BrokenBusinessRules.Add("Style already added to this customer.");
        }

        internal override void LoadAdditionalProperties(DataRow row)
        {
            StyleProductService styleProductService = new StyleProductService();

            string uploadPath = Path.Combine(ConfigurationManager.AppSettings["StyleSkethDirectory"].ToString());
            uploadPath = Path.Combine(uploadPath, Convert.ToInt32(row["Id"]).ToString());
            
            //Get the image upload path
            string sketchPath = Path.Combine(uploadPath, "Sketch");
            string tnaPath = Path.Combine(uploadPath, "TnA");

            this.SketchFileName = Convert.ToString(row["Sketch"]);
            this.OldSketchFileName = Convert.ToString(row["Sketch"]);
            this.SketchFilePath = Path.Combine(sketchPath, this.SketchFileName);

            this.StyleProducts = styleProductService.ListIdNameForStyleId(this.Id);

            ProductionCostSMVDTO productionCostSMVDTO = styleProductService.GetSMVSumForStyleId(this.Id);
            this.ProductionSMV = productionCostSMVDTO.ProductionSMV;
            this.CostSMV = productionCostSMVDTO.CostSMV;
            this.CM = productionCostSMVDTO.CM;

        }
    }
}
