﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class StyleDetailService : ServiceBase<StyleDetail>
    {
        private StyleDetailDA _dataAccess = new StyleDetailDA();
        private StyleRevisionService _styleRevisionService = new StyleRevisionService();
        private Email _email = new Email();

        public bool Save(StyleDetail styleDetail, EnumSMVInquiryType isInquiry = EnumSMVInquiryType.None, int inquiryRevisionId = 0)
        {
            //if (!this.IsValidToSave(styleDetail)) return false;

            if (styleDetail.Id > 0)
            {
                if (styleDetail.SketchFile != null && !styleDetail.SketchFileName.Equals(styleDetail.OldSketchFileName))
                    styleDetail.SketchFileName = this.SaveFile(styleDetail.SketchFile, styleDetail.Id, "Sketch");
            }

            if (styleDetail.Id > 0)
            {
                _dataAccess.Update(styleDetail);

                if(isInquiry.Equals(EnumSMVInquiryType.Request))
                {
                    StyleRevision styleRevision = new StyleRevision();
                    styleRevision.StyleId = styleDetail.Id;
                    styleRevision.RequestedById = GV.LoggedUser().Id;
                    styleRevision.InquiryType = "Revision";
                    styleRevision.RequestedDate = DateTime.Now;

                    _styleRevisionService.Reqeust(styleRevision);

                    if (styleDetail.Merchant != null && styleDetail.IndustrialEngineer != null && GV.LoggedUser().Id == styleDetail.Merchant.Id)
                    {
                        _email.SendEmailNotification(new EmailNotificationDTO() { Style = styleDetail.StyleNumber, Merchant = styleDetail.Merchant.FullName },
                                                    (int)EnumEmailType.SMVRevisionInquiry, styleDetail.IndustrialEngineer.Email);

                        new NotificationDA().Add(new Notification()
                        {
                            ToId = styleDetail.IndustrialEngineer.Id,
                            Message = $"SMV revision inquiry for '{styleDetail.StyleNumber}' has added by {GV.LoggedUser().UserName}",
                        });
                    }
                }

                if (isInquiry.Equals(EnumSMVInquiryType.Accept) && inquiryRevisionId>0)
                {
                    StyleRevision styleRevision = _styleRevisionService.GetEntityById(inquiryRevisionId);
                    styleRevision.AcceptedById = GV.LoggedUser().Id;
                    styleRevision.RevisedDate = DateTime.Now;

                    _styleRevisionService.Accept(styleRevision);

                    if (styleDetail.Merchant != null && styleDetail.IndustrialEngineer != null && GV.LoggedUser().Id == styleDetail.IndustrialEngineer.Id)
                    {
                        _email.SendEmailNotification(new EmailNotificationDTO() { Style = styleDetail.StyleNumber, Merchant = styleDetail.IndustrialEngineer.FullName },
                                                    (int)EnumEmailType.SMVInquiryUpdated, styleDetail.Merchant.Email);

                        new NotificationDA().Add(new Notification()
                        {
                            ToId = styleDetail.Merchant.Id,
                            Message = $"SMV revision inquiry for '{styleDetail.StyleNumber}' has accepted by {GV.LoggedUser().UserName}",
                        });
                    }
                }
            }
            else
            {
                if (!this.IsValidToSave(styleDetail)) return false;

                int styleId = _dataAccess.Add(styleDetail);

                if (isInquiry.Equals(EnumSMVInquiryType.Request))
                {
                    StyleRevision styleRevision = new StyleRevision();
                    styleRevision.StyleId = styleId;
                    styleRevision.RequestedById = GV.LoggedUser().Id;
                    styleRevision.InquiryType = "New";
                    styleRevision.RequestedDate = DateTime.Now;

                    _styleRevisionService.Reqeust(styleRevision);

                    if (styleDetail.Merchant != null && styleDetail.IndustrialEngineer != null && GV.LoggedUser().Id == styleDetail.Merchant.Id)
                    {
                        _email.SendEmailNotification(new EmailNotificationDTO() { Style = styleDetail.StyleNumber, Merchant = styleDetail.Merchant.FullName },
                                                    (int)EnumEmailType.SMVNewInquiry, styleDetail.IndustrialEngineer.Email);

                        new NotificationDA().Add(new Notification()
                        {
                            ToId = styleDetail.IndustrialEngineer.Id,
                            Message = $"SMV new inquiry for '{styleDetail.StyleNumber}' has added by {GV.LoggedUser().UserName}",
                        });
                    }
                }
            }

            return true;
        }

        private string SaveFile(HttpPostedFileBase file, int styleNo, string folderName)
        {

            string uploadPath = Path.Combine(HttpContext.Current.Server.MapPath("~"), ConfigurationManager.AppSettings["StyleSkethDirectory"].ToString());
            uploadPath = Path.Combine(uploadPath, styleNo.ToString());

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            uploadPath = Path.Combine(uploadPath, folderName);

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            string fileName = file.FileName;

            int count = 1;
            string fileNameOnly = Path.GetFileNameWithoutExtension(fileName);
            string extension = Path.GetExtension(fileName);

            while (File.Exists(Path.Combine(uploadPath, fileName)))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                fileName = Path.Combine(uploadPath, tempFileName + extension);
            }
            fileName = Path.Combine(uploadPath, fileName);

            file.SaveAs(fileName);
            return Path.GetFileName(fileName);
        }
        

        public List<StyleDetail> GetList(string orderBy = "")
        {
            return _dataAccess.GetList(orderBy);
        }

        public List<StyleDetail> ListForBuyerId(int buyerId)
        {
            return _dataAccess.ListForBuyerId(buyerId);
        }

        public List<IdNameDTO> ListForSMV(int buyerId, int customerId)
        {
            return _dataAccess.ListForSMV(buyerId, customerId);
        }

        public List<StyleDetail> ListForBuyerCustomer(int buyerId, int customerId)
        {
            return _dataAccess.ListForBuyerCustomer(buyerId, customerId);
        }
    }
}
