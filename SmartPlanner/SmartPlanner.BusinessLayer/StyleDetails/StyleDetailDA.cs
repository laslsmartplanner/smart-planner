﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class StyleDetailDA : EntityDataAccessBase<StyleDetail>
    {
        private DataAccess _dataAccess = new DataAccess();

        
        internal int Add(StyleDetail styleDetail)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO StyleDetails(StyleNumber, BuyerId, AppearanceId, MaterialCode, ColorCode, OrderCode, FGCode, 
                      FabricTypeId, OrderQty, ProductReference, Description, CreatedDate, BaseStyle, DesignReference, DivisionId, EDescription, GraphicNumber, MerchantId, NoOfSheets, CreatedById, CreatedOn, OperationId, Process, 
                      CustomerId, SetValue, SgBom, TimeTableId, LaunchedDate, TotalSMV, SampleStatusId,IsConfirmed,Department,SampleStage,SpecialConcern,SpecialInstruction,
                        RevisionSampleStage,ReasonToRevise,OptionRequest,CostingDeadline,Sketch, CustomerComment, CustomerCode, ProductDesc,PlannedColor,ActualColor,ChangedColor,IsMultiPiece,IEId) 
                      VALUES (@StyleNumber, @BuyerId, @AppearanceId, @MaterialCode, @ColorCode, @OrderCode, @FGCode,
                      @FabricTypeId, @OrderQty, @ProductReference, @Description, @CreatedDate, @BaseStyle, @DesignReference, @DivisionId, @EDescription, @GraphicNumber, @MerchantId, @NoOfSheets, @CreatedById, @CreatedOn, @OperationId, @Process, 
                      @CustomerId, @SetValue, @SgBom, @TimeTableId, @LaunchedDate,  @TotalSMV, @SampleStatusId, @IsConfirmed, @Department,@SampleStage,@SpecialConcern,@SpecialInstruction,
                        @RevisionSampleStage,@ReasonToRevise,@OptionRequest,@CostingDeadline,@Sketch, @CustomerComment, @CustomerCode, @ProductDesc,@PlannedColor,@ActualColor,@ChangedColor,@IsMultiPiece,@IEId)");
            this.SetParameters(command, styleDetail);
            this.SetCreatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return 0;

                //this.SaveAdditionalInformations(Convert.ToInt32(ret), styleDetail.AdditionalInformationFileNames);

                styleDetail.SetId(ret);
                _dataAccess.CommitTransaction();
                return styleDetail.Id;
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private void SetParameters(SqlCommand command, StyleDetail styleDetail)
        {
            command.Parameters.AddWithValue("@IsMultiPiece", styleDetail.IsMultiPiece);
            command.Parameters.AddWithValue("@StyleNumber", styleDetail.StyleNumber);
            command.Parameters.AddWithValue("@BuyerId", styleDetail.BuyerId);
            command.Parameters.AddWithValue("@AppearanceId", GetNullIfLessOne(styleDetail.AppearanceId));
            command.Parameters.AddWithValue("@MaterialCode", styleDetail.MaterialCode ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ColorCode", styleDetail.ColorCode ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@OrderCode", styleDetail.OrderCode ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@FGCode", styleDetail.FGCode ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@Sketch", styleDetail.SketchFileName ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@FabricTypeId", GetNullIfLessOne(styleDetail.FabricTypeId));
            command.Parameters.AddWithValue("@OrderQty", styleDetail.OrderQty);

            command.Parameters.AddWithValue("@ProductReference", styleDetail.ProductReference ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Description", styleDetail.Description ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@CreatedDate", styleDetail.CreatedDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@BaseStyle", styleDetail.BaseStyle ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@DesignReference", styleDetail.DesignReference ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@DivisionId", GetNullIfLessOne(styleDetail.DivisionId));
            command.Parameters.AddWithValue("@EDescription", styleDetail.EDescription ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@GraphicNumber", styleDetail.GraphicNumber ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@MerchantId", GetNullIfLessOne(styleDetail.MerchantId));
            command.Parameters.AddWithValue("@NoOfSheets", styleDetail.NoOfSheets);

            command.Parameters.AddWithValue("@OperationId", GetNullIfLessOne(styleDetail.OperationId));
            command.Parameters.AddWithValue("@Process", styleDetail.Process ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@CustomerId", GetNullIfLessOne(styleDetail.CustomerId));
            command.Parameters.AddWithValue("@SetValue", styleDetail.SetValue ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SgBom", styleDetail.SgBom ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@TimeTableId", GetNullIfLessOne(styleDetail.TimeTableId));
            command.Parameters.AddWithValue("@LaunchedDate", styleDetail.LaunchedDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@TotalSMV", styleDetail.TotalSMV);
            command.Parameters.AddWithValue("@SampleStatusId", GetNullIfLessOne(styleDetail.SampleStatusId));

            command.Parameters.AddWithValue("@IsConfirmed", styleDetail.IsConfirmed);
            command.Parameters.AddWithValue("@Department", styleDetail.Department ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SampleStage", styleDetail.SampleStage ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SpecialConcern", styleDetail.SpecialConcern ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SpecialInstruction", styleDetail.SpecialInstruction ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@RevisionSampleStage", styleDetail.RevisionSampleStage ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ReasonToRevise", styleDetail.ReasonToRevise ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@OptionRequest", styleDetail.OptionRequest ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@CostingDeadline", styleDetail.CostingDeadline ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@CustomerComment", styleDetail.CustomerComment ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@CustomerCode", styleDetail.CustomerCode ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ProductDesc", styleDetail.ProductDesc ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@PlannedColor", styleDetail.PlannedColor ?? string.Empty);
            command.Parameters.AddWithValue("@ActualColor", styleDetail.ActualColor ?? string.Empty);
            command.Parameters.AddWithValue("@ChangedColor", styleDetail.ChangedColor ?? string.Empty);
            command.Parameters.AddWithValue("@IEId", GetNullIfLessOne(styleDetail.IEId));
        }

        internal bool Update(StyleDetail styleDetail)
        {
            SqlCommand command = new SqlCommand(@"UPDATE StyleDetails
                                                SET  IsMultiPiece=@IsMultiPiece, StyleNumber =@StyleNumber, BuyerId =@BuyerId , AppearanceId =@AppearanceId , MaterialCode =@MaterialCode , ColorCode =@ColorCode , OrderCode =@OrderCode , FGCode =@FGCode , 
                                                FabricTypeId =@FabricTypeId , OrderQty =@OrderQty , ProductReference =@ProductReference , Description =@Description , CreatedDate =@CreatedDate , BaseStyle =@BaseStyle , DesignReference =@DesignReference , DivisionId =@DivisionId , EDescription =@EDescription , GraphicNumber =@GraphicNumber , MerchantId =@MerchantId , NoOfSheets =@NoOfSheets , OperationId=@OperationId, Process=@Process, 
                                                CustomerId=@CustomerId, SetValue=@SetValue, SgBom=@SgBom, TimeTableId=@TimeTableId, LaunchedDate=@LaunchedDate, TotalSMV=@TotalSMV, SampleStatusId=@SampleStatusId, IsConfirmed=@IsConfirmed, 
                                                Department=@Department,SampleStage=@SampleStage,SpecialConcern=@SpecialConcern,
                                                SpecialInstruction=@SpecialInstruction,RevisionSampleStage=@RevisionSampleStage,ReasonToRevise=@ReasonToRevise,
                                                OptionRequest=@OptionRequest,CostingDeadline=@CostingDeadline,Sketch =@Sketch,
                                                CustomerComment=@CustomerComment, CustomerCode=@CustomerCode, ProductDesc=@ProductDesc, PlannedColor=@PlannedColor, ActualColor=@ActualColor, ChangedColor=@ChangedColor, IEId=@IEId WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", styleDetail.Id);
            this.SetParameters(command, styleDetail);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                _dataAccess.ExecuteNonQuery(command);
                //this.SaveAdditionalInformations(styleDetail.Id, styleDetail.AdditionalInformationFileNames);
                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        
        internal List<StyleDetail> ListForBuyerId(int buyerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * From StyleDetails WITH(NOLOCK) Where BuyerId=@BuyerId ORDER BY StyleNumber");
            sqlCommand.Parameters.AddWithValue("@BuyerId", buyerId);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<IdNameDTO> ListForSMV(int buyerId, int customerId)
        {
            string whereCondition = buyerId > 0 ? " BuyerId = @BuyerId" : string.Empty;

            if (customerId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " CustomerId = @CustomerId" : " AND CustomerId = @CustomerId";
            }

            //if (categoryId > 0)
            //{
            //    whereCondition += string.IsNullOrEmpty(whereCondition) ? " CategoryId = @CategoryId" : " AND CategoryId = @CategoryId";
            //}

            //if (subCategoryId > 0)
            //{
            //    whereCondition += string.IsNullOrEmpty(whereCondition) ? " SubCategoryId = @SubCategoryId" : " AND SubCategoryId = @SubCategoryId";
            //}

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " WHERE " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"Select Id, StyleNumber as Name From StyleDetails {whereCondition} ORDER BY StyleNumber");

            if (buyerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@BuyerId", buyerId);
            }

            if (customerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);
            }

            try
            {
                if (buyerId < 1 && customerId < 1)
                {
                    return new List<IdNameDTO>();
                }
                else
                {
                    return IdNameDTO.Load(_dataAccess.GetDataTable(sqlCommand));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<StyleDetail> ListForBuyerCustomer(int buyerId, int customerId)
        {
            string whereCondition = buyerId > 0 ? " BuyerId = @BuyerId" : string.Empty;

            if (customerId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " CustomerId = @CustomerId" : " AND CustomerId = @CustomerId";
            }

            //if (categoryId > 0)
            //{
            //    whereCondition += string.IsNullOrEmpty(whereCondition) ? " CategoryId = @CategoryId" : " AND CategoryId = @CategoryId";
            //}

            //if (subCategoryId > 0)
            //{
            //    whereCondition += string.IsNullOrEmpty(whereCondition) ? " SubCategoryId = @SubCategoryId" : " AND SubCategoryId = @SubCategoryId";
            //}

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " WHERE " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"Select * From StyleDetails WITH(NOLOCK) {whereCondition} ORDER BY StyleNumber");

            if (buyerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@BuyerId", buyerId);
            }

            if (customerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);
            }

            try
            {
                if (buyerId < 1 && customerId < 1)
                {
                    return new List<StyleDetail>();
                }
                else
                {
                    return this.GetList(sqlCommand);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal bool IsStyleExists(string styleNo, int customerId)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM StyleDetails WITH(NOLOCK) WHERE StyleNumber=@StyleNumber AND CustomerId=@CustomerId");
            command.Parameters.AddWithValue("@StyleNumber", styleNo);
            command.Parameters.AddWithValue("@CustomerId", customerId);

            return _dataAccess.ExecuteScalar(command) != null;
        }
    }
}
