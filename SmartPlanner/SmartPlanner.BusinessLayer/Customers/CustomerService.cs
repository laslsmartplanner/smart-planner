﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class CustomerService : ServiceBase<Customer>
    {
        private CustomerDA _dataAccess = new CustomerDA();

        public bool Save(Customer customer)
        {
            if (!this.IsValidToSave(customer)) return false;
            if (customer.Id > 0)
                _dataAccess.Update(customer);
            else
                _dataAccess.Add(customer);
            return true;
        }

        public List<Customer> ListCustomers(string orderBy,bool addEmpty)
        {
            return _dataAccess.GetList(orderBy,addEmpty);
        }

        public bool SaveSizesList(string sizeName, int customerId)
        {
            if (_dataAccess.IsCustomerSizeExists(customerId, sizeName)) return false;

            return _dataAccess.SaveSizesList(customerId, sizeName);
        }

        public bool DeleteCustomerSize(int customerSizeId)
        {
            return _dataAccess.DeleteCustomerSize(customerSizeId);
        }

        public bool SaveColorsList(string colorName, int customerId)
        {
            if (_dataAccess.IsCustomerColorExists(customerId, colorName)) return false;

            return _dataAccess.SaveColorsList(customerId, colorName);
        }

        public bool DeleteCustomerColor(int customerColorId)
        {
            return _dataAccess.DeleteCustomerColor(customerColorId);
        }

        public List<CustomerResponsibility> GetCustomerResponsibilities(int customerId)
        {
            return new CustomerResponsibilityDA().ListForCustomerId(customerId);
        }

        public bool AddCustomerResponsibility(CustomerResponsibility customerResponsibility)
        {
            return new CustomerResponsibilityDA().Add(customerResponsibility);
        }

        public bool DeleteCustomerResponsibility(int customerResponsibilityId)
        {
            return new CustomerResponsibilityDA().DeleteCustomerResponsibility(customerResponsibilityId);
        }
    }
}
