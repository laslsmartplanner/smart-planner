﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class CustomerDA : EntityDataAccessBase<Customer>
    {
        private DataAccess _dataAccess = new DataAccess();

        private void SetParameters(SqlCommand command, Customer customer)
        {
            command.Parameters.AddWithValue("@Name", customer.Name);
            command.Parameters.AddWithValue("@Country", customer.Country);
            command.Parameters.AddWithValue("@Address", customer.Address);
            command.Parameters.AddWithValue("@Mobile", customer.Mobile);
            command.Parameters.AddWithValue("@Email", customer.Email);

        }

        internal bool Add(Customer customer)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO Customers
                      (Name, Country, Address, Mobile, Email, CreatedById, CreatedOn)
               VALUES (@Name, @Country, @Address, @Mobile, @Email, @CreatedById, @CreatedOn)");
            this.SetParameters(command, customer);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                customer.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(Customer customer)
        {
            SqlCommand command = new SqlCommand(@"UPDATE Customers
                                                SET Name = @Name, Country = @Country, Address = @Address, Mobile = @Mobile, Email = @Email , UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn 
                                                WHERE   (Id = @Id)");
            command.Parameters.AddWithValue("@Id", customer.Id);
            this.SetParameters(command, customer);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<IdNameDTO> GetSizesList(int customerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select Id, SizeName as Name FROM Customers_SizesList WHERE CustomerId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", customerId);
            try
            {
                return IdNameDTO.Load(new DataAccess().GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal bool SaveSizesList(Int32 id, string sizeName)
        {
            SqlCommand sqlCommand = new SqlCommand(@"INSERT INTO Customers_SizesList(CustomerId,SizeName)
                                                                                                VALUES(@CustomerId,@SizeName)");
            try
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", id);
                sqlCommand.Parameters.AddWithValue("@SizeName", sizeName);
                _dataAccess.ExecuteNonQuery(sqlCommand);
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }

        internal bool IsCustomerSizeExists(int customerId, string sizeName)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM Customers_SizesList WITH(NOLOCK) WHERE CustomerId=@CustomerId AND SizeName=@SizeName");
            command.Parameters.AddWithValue("@CustomerId", customerId);
            command.Parameters.AddWithValue("@SizeName", sizeName);

            return _dataAccess.ExecuteScalar(command) != null;
        }

        internal bool DeleteCustomerSize(int customerSizeId)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"DELETE FROM  Customers_SizesList WHERE Id=@Id");

                command.Parameters.AddWithValue("@Id", customerSizeId);

                _dataAccess.ExecuteNonQuery(command);
                command.Parameters.Clear();
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

        internal List<IdNameDTO> GetColorsList(int customerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select  Id, ColorName as Name  FROM Customers_ColorsList WHERE CustomerId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", customerId);
            try
            {
                return IdNameDTO.Load(new DataAccess().GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal bool SaveColorsList(Int32 id, string colorName)
        {
            SqlCommand sqlCommand = new SqlCommand(@"INSERT INTO Customers_ColorsList(CustomerId,ColorName)
                                                                                                VALUES(@CustomerId,@ColorName)");
            try
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", id);
                sqlCommand.Parameters.AddWithValue("@ColorName", colorName);
                _dataAccess.ExecuteNonQuery(sqlCommand);
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }

        internal bool IsCustomerColorExists(int customerId, string colorName)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM Customers_ColorsList WITH(NOLOCK) WHERE CustomerId=@CustomerId AND ColorName=@ColorName");
            command.Parameters.AddWithValue("@CustomerId", customerId);
            command.Parameters.AddWithValue("@ColorName", colorName);

            return _dataAccess.ExecuteScalar(command) != null;
        }

        internal bool DeleteCustomerColor(int customerColorId)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"DELETE FROM  Customers_ColorsList WHERE Id=@Id");

                command.Parameters.AddWithValue("@Id", customerColorId);

                _dataAccess.ExecuteNonQuery(command);
                command.Parameters.Clear();
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
