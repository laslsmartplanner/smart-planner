﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Customer : EntityBase<Customer>
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        public override string TableName => "Customers";
        public override string NameField => "Name";

        [Required]
        [Display(Name = "Customer Name")]
        public string Name { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Size")]
        public int SizeId { get; set; }

        [Display(Name = "Size List")]
        public List<IdNameDTO> SizeList { get; set; }

        public List<ConfigurationTableValue> SizesList { get; set; }

        [Display(Name = "Color")]
        public int ColorId { get; set; }

        [Display(Name = "Color List")]
        public List<IdNameDTO> ColorList { get; set; }

        public List<ConfigurationTableValue> ColorsList { get; set; }

        public List<CustomerResponsibility> ResponsibilitiesList { get; set; }

        public List<Factory> Factories { get; set; }

        public List<LITUser> Coordinators { get; set; }

        public List<LITUser> Commercials { get; set; }

        [Display(Name = "Factory")]
        public int FactoryId { get; set; }

        [Display(Name = "Coordinator")]
        public int CoordinatorId { get; set; }

        [Display(Name = "Commercial")]
        public int CommercialId { get; set; }

        internal override void LoadAdditionalProperties(DataRow row)
        {
            CustomerDA da = new CustomerDA();
            this.SizeList = da.GetSizesList(Convert.ToInt32(row["Id"]));

            this.ColorList = da.GetColorsList(Convert.ToInt32(row["Id"]));

            this.ResponsibilitiesList = new CustomerResponsibilityDA().ListForCustomerId(this.Id);
        }
    }
}
