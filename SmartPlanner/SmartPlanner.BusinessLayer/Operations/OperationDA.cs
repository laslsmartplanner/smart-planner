﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class OperationDA : EntityDataAccessBase<Operation>
    {
        private DataAccess _dataAccess = new DataAccess();

        private void SetParameters(SqlCommand command, Operation operation)
        {
            command.Parameters.AddWithValue("@OperationName", operation.OperationName);
            command.Parameters.AddWithValue("@SMV", operation.SMV);

        }

        internal bool Add(Operation operation)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO Operations
                      (OperationName, SMV, CreatedById, CreatedOn)
               VALUES (@OperationName, @SMV, @CreatedById, @CreatedOn)");
            this.SetParameters(command, operation);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                operation.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(Operation operation)
        {
            SqlCommand command = new SqlCommand(@"UPDATE Operations
                                                SET OperationName = @OperationName, SMV = @SMV, UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn 
                                                WHERE   (Id = @Id)");
            command.Parameters.AddWithValue("@Id", operation.Id);
            this.SetParameters(command, operation);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        
    }
}
