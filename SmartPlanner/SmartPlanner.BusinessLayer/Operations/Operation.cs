﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Operation : EntityBase<Operation>
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        public override string TableName => "Operations";
        public override string NameField => "OperationName";
        public override List<string> IsAlreadyExistsProperties => new List<string>() { "OperationName" };

        [Required]
        [Display(Name = "Operation")]
        public string OperationName { get; set; }

        [Display(Name = "SMV")]
        public decimal SMV { get; set; }

    }
}
