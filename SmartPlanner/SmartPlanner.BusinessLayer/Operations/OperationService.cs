﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OperationService : ServiceBase<Operation>
    {
        private OperationDA _dataAccess = new OperationDA();

        public bool Save(Operation operation)
        {
            operation.OperationName = operation.OperationName.Trim();

            if (!this.IsValidToSave(operation)) return false;
            if (operation.Id > 0)
                _dataAccess.Update(operation);
            else
                _dataAccess.Add(operation);
            return true;
        }

        public List<Operation> ListOperations(string orderBy,bool addEmpty)
        {
            return _dataAccess.GetList(orderBy,addEmpty);
        }

        public List<Operation> GetList()
        {
            return _dataAccess.GetList("OperationName");
        }

    }
}
