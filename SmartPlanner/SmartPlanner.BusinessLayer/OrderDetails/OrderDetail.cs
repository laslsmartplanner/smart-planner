﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OrderDetail : EntityBase<OrderDetail>
    {
        private StyleDetail style = null;

        public override string TableName => "OrderDetails";

        public override List<string> IsAlreadyExistsProperties => new List<string>() { "POId" };

        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        private ConfigurationTableValue _planningSeason = null;
        private ConfigurationTableValue _destinationMedia = null;
        private ConfigurationTableValue _destination = null;
        private ConfigurationTableValue _buyGroup = null;
        private ConfigurationTableValue _vendor = null;
        private ConfigurationTableValue _division = null;
        private ConfigurationTableValue _product = null;
        private ConfigurationTableValue _distributeFrom = null;
        private ConfigurationTableValue _deliverTo = null;
        private ConfigurationTableValue _method = null;
        private Factory _factoryName = null;
        private ConfigurationTableValue _poType = null;
        private ConfigurationTableValue _timeTable = null;
        private ConfigurationTableValue _emblishmentPlant = null;
        private ConfigurationTableValue _washingPlant = null;
        private ConfigurationTableValue _printingPlant = null;
        private ConfigurationTableValue _size = null;
        private ConfigurationTableValue _priceType = null;
        private ConfigurationTableValue _sewingPlant = null;
        private ConfigurationTableValue _sampleStatus = null;
        private ConfigurationTableValue _color = null;

        private Customer _customer = null;
        private Buyer _buyer = null;
        private OrderInquiryStatus _orderInquiryStatus = null;
        private LITUser _merchant;
        private LITUser _planner;

        #region Properties

        [Display(Name = "PO No.")]
        public string POId { get; set; }

        [Display(Name = "Order Reference")]
        public string SalesOrderNumber { get; set; }

        [Display(Name = "Order Confirmation Date")]
        public DateTime? OrderConfirmationDate { get; set; }

        [Display(Name = "Is Repeat Order?")]
        public bool Repeat { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Customer PO No.")]
        public string CustomerPONumber { get; set; }

        [Display(Name = "Planning Season")]
        public int PlanningSeasonId { get; set; }

        [Display(Name = "Year")]
        public int Year { get; set; }

        [Display(Name = "Uplift")]
        public string Uplift { get; set; }

        [Display(Name = "Order Quantity")]
        public int OrderQty { get; set; }

        [Display(Name = "Actual Cut Quantity")]
        public int CutQty { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "CM")]
        public decimal CM { get; set; }

        [Display(Name = "Price")]
        public decimal CMF { get; set; }

        [Display(Name = "Destination Media")]
        public int DestinationMediaId { get; set; }

        [Display(Name = "Destination")]
        public int DestinationId { get; set; }

        [Display(Name = "Trading PO Number")]
        public string TradingPONumber { get; set; }

        [Display(Name = "Buy Group")]
        public int BuyGroupId { get; set; }

        [Display(Name = "Vendor")]
        public int VendorId { get; set; }

        [Display(Name = "Posibile to Deliver?")]
        public bool PossibilityToDeliver { get; set; }

        [Display(Name = "Posibile to Deliver?")]
        public string PossibilityToDeliverString { get { return (string.IsNullOrEmpty(this.CurrentKeyProcess)) ? string.Empty : this.PossibilityToDeliver ? "Yes" : "No"; } }

        [Display(Name = "Cut Quantity")]
        public int PlanDeliveryQty { get; set; }

        [Display(Name = "Wastage")]
        public decimal Wastage { get; set; }

        [Display(Name = "Division")]
        public int DivisionId { get; set; }

        [Display(Name = "Shipment Date")]
        public DateTime? ShipmentDate { get; set; }

        [Display(Name = "Shipment Date")]
        public string ShipmentDateText { get; set; }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        [Display(Name = "Order Set")]
        public string OrderSet { get; set; }

        [Display(Name = "Sales Year")]
        public int SalesYear { get; set; }

        [Display(Name = "Efficiency")]
        public decimal Efficiency { get; set; }

        [Display(Name = "Reference")]
        public string Reference { get; set; }

        [Display(Name = "Distribute From")]
        public int DistributeFromId { get; set; }

        [Display(Name = "Deliver To")]
        public int DeliverToId { get; set; }

        [Display(Name = "Method")]
        public int MethodId { get; set; }

        [Display(Name = "Plan In Group")]
        public string PlanInGroup { get; set; }

        [Display(Name = "User Route")]
        public string UserRoute { get; set; }

        [Display(Name = "Factory")]
        public int FactoryNameId { get; set; }

        [Display(Name = "Fabric In House")]
        public string FabricIH { get; set; }

        [Display(Name = "FG Code")]
        public string FGCode { get; set; }

        [Display(Name = "Merchant")]
        public int MerchantId { get; set; }

        [Display(Name = "Pilot Run")]
        public string PilotRun { get; set; }

        [Display(Name = "PO Type")]
        public int POTypeId { get; set; }

        [Display(Name = "Rev. Delivery")]
        public string RevDelivery { get; set; }

        [Display(Name = "Time Table")]
        public int TimeTableId { get; set; }

        [Display(Name = "Buyer")]
        public int BuyerId { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Progress")]
        public string Progress { get; set; }

        [Display(Name = "Consolidated")]
        public string Consolidated { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Substitute")]
        public int Substitute { get; set; }

        [Display(Name = "Embellishment Plant")]
        public int EmblishmentPlantId { get; set; }

        [Display(Name = "No. of Modules")]
        public int NoOfModules { get; set; }

        [Display(Name = "Buy Date")]
        public DateTime? BuyDate { get; set; }

        [Display(Name = "Style")]
        public int StyleId { get; set; }

        [Display(Name = "Washing Plant")]
        public int WashingPlantId { get; set; }

        [Display(Name = "Printing Plant")]
        public int PrintingPlantId { get; set; }

        [Display(Name = "Size")]
        public int SizeId { get; set; }

        [Display(Name = "Price Type")]
        public int PriceTypeId { get; set; }

        [Display(Name = "Sewing Plant")]
        public int SewingPlantId { get; set; }

        [Display(Name = "Contribution")]
        public decimal Contribution { get; set; }

        [Display(Name = "CPM")]
        public decimal CPM { get; set; }

        [Display(Name = "EPM")]
        public decimal EPM { get; set; }

        [Display(Name = "Sample Status")]
        public int SampleStatusId { get; set; }

        [Display(Name = "Order Status")]
        public int OrderInquiryStatusId { get; set; }

        [Display(Name = "Receive Date")]
        public DateTime? ConfirmationDate { get; set; }

        [Display(Name = "Current Key Process")]
        public string CurrentKeyProcess { get; set; }

        public int CurrentTnADefaultId { get; set; }

        public DateTime PlanStartDate { get; set; }

        public DateTime PlanEndDate { get; set; }

        public DateTime ActualStartDate { get; set; }

        [Display(Name = "Is Split Order?")]
        public bool IsSplitOrder { get; set; }

        [Display(Name = "Sewing Plant Required?")]
        public bool IsSewingPlant { get; set; }

        [Display(Name = "Emblishment Plant Required?")]
        public bool IsEmblishmentPlant { get; set; }

        [Display(Name = "Printing Plant Required?")]
        public bool IsPrintingPlant { get; set; }

        [Display(Name = "Washing Plant Required?")]
        public bool IsWashingPlant { get; set; }

        [Display(Name = "Planner")]
        public int PlannerId { get; set; }

        public int SplitKey { get; set; }

        public OrderCombination SplitCombination { get; set; }

        [Display(Name = "Color")]
        public int ColorId { get; set; }

        public ConfigurationTableValue Color
        {
            get
            {
                if (this._color == null && this.ColorId > 0)
                    this._color = _configValueService.GetEntityById(this.ColorId);
                return this._color;
            }
        }

        [Display(Name = "Production SMV")]
        public decimal ProductionSMV { get; set; }

        public List<OrderCombination> OrderCombinations { get; set; }

        [Display(Name = "Repeat Number")]
        public string RepeatNumber { get; set; }

        [Display(Name = "Delivery Contract Number")]
        public string DeliveryContractNumber { get; set; }

        #endregion

        public List<IdNameDTO> ProductsForStyle { get; set; }
        public List<OrderQtyPlanQtyDTO> ProductQtySumForOrder { get; set; }

        public ConfigurationTableValue PlanningSeason
        {
            get
            {
                if (this._planningSeason == null && this.PlanningSeasonId > 0)
                    this._planningSeason = _configValueService.GetEntityById(this.PlanningSeasonId);
                return this._planningSeason;
            }
        }

        public ConfigurationTableValue DestinationMedia
        {
            get
            {
                if (this._destinationMedia == null && this.DestinationMediaId > 0)
                    this._destinationMedia = _configValueService.GetEntityById(this.DestinationMediaId);
                return this._destinationMedia;
            }
        }

        public ConfigurationTableValue Destination
        {
            get
            {
                if (this._destination == null && this.DestinationId > 0)
                    this._destination = _configValueService.GetEntityById(this.DestinationId);
                return this._destination;
            }
        }

        public ConfigurationTableValue BuyGroup
        {
            get
            {
                if (this._buyGroup == null && this.BuyGroupId > 0)
                    this._buyGroup = _configValueService.GetEntityById(this.BuyGroupId);
                return this._buyGroup;
            }
        }

        public ConfigurationTableValue Vendor
        {
            get
            {
                if (this._vendor == null && this.VendorId > 0)
                    this._vendor = _configValueService.GetEntityById(this.VendorId);
                return this._vendor;
            }
        }

        public ConfigurationTableValue Division
        {
            get
            {
                if (this._division == null && this.DivisionId > 0)
                    this._division = _configValueService.GetEntityById(this.DivisionId);
                return this._division;
            }
        }

        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = _configValueService.GetEntityById(this.ProductId);
                return this._product;
            }
        }

        public ConfigurationTableValue DistributeFrom
        {
            get
            {
                if (this._distributeFrom == null && this.DistributeFromId > 0)
                    this._distributeFrom = _configValueService.GetEntityById(this.DistributeFromId);
                return this._distributeFrom;
            }
        }

        public ConfigurationTableValue DeliverTo
        {
            get
            {
                if (this._deliverTo == null && this.DeliverToId > 0)
                    this._deliverTo = _configValueService.GetEntityById(this.DeliverToId);
                return this._deliverTo;
            }
        }

        public ConfigurationTableValue Method
        {
            get
            {
                if (this._method == null && this.MethodId > 0)
                    this._method = _configValueService.GetEntityById(this.MethodId);
                return this._method;
            }
        }

        public Factory FactoryName
        {
            get
            {
                if (this._factoryName == null && this.FactoryNameId > 0)
                    this._factoryName = new FactoryService().GetEntityById(this.FactoryNameId);
                return this._factoryName;
            }
        }

        public ConfigurationTableValue POType
        {
            get
            {
                if (this._poType == null && this.POTypeId > 0)
                    this._poType = _configValueService.GetEntityById(this.POTypeId);
                return this._poType;
            }
        }

        public ConfigurationTableValue TimeTable
        {
            get
            {
                if (this._timeTable == null && this.TimeTableId > 0)
                    this._timeTable = _configValueService.GetEntityById(this.TimeTableId);
                return this._timeTable;
            }
        }

        public ConfigurationTableValue EmblishmentPlant
        {
            get
            {
                if (this._emblishmentPlant == null && this.EmblishmentPlantId > 0)
                    this._emblishmentPlant = _configValueService.GetEntityById(this.EmblishmentPlantId);
                return this._emblishmentPlant;
            }
        }

        public ConfigurationTableValue WashingPlant
        {
            get
            {
                if (this._washingPlant == null && this.WashingPlantId > 0)
                    this._washingPlant = _configValueService.GetEntityById(this.WashingPlantId);
                return this._washingPlant;
            }
        }

        public ConfigurationTableValue PrintingPlant
        {
            get
            {
                if (this._printingPlant == null && this.PrintingPlantId > 0)
                    this._printingPlant = _configValueService.GetEntityById(this.PrintingPlantId);
                return this._printingPlant;
            }
        }

        public ConfigurationTableValue Size
        {
            get
            {
                if (this._size == null && this.SizeId > 0)
                    this._size = _configValueService.GetEntityById(this.SizeId);
                return this._size;
            }
        }

        public ConfigurationTableValue PriceType
        {
            get
            {
                if (this._priceType == null && this.PriceTypeId > 0)
                    this._priceType = _configValueService.GetEntityById(this.PriceTypeId);
                return this._priceType;
            }
        }

        public ConfigurationTableValue SewingPlant
        {
            get
            {
                if (this._sewingPlant == null && this.SewingPlantId > 0)
                    this._sewingPlant = _configValueService.GetEntityById(this.SewingPlantId);
                return this._sewingPlant;
            }
        }

        public ConfigurationTableValue SampleStatus
        {
            get
            {
                if (this._sampleStatus == null && this.SampleStatusId > 0)
                    this._sampleStatus = _configValueService.GetEntityById(this.SampleStatusId);
                return this._sampleStatus;
            }
        }

        public OrderInquiryStatus OrderInquiryStatus
        {
            get
            {
                if (this._orderInquiryStatus == null && this.OrderInquiryStatusId > 0)
                    this._orderInquiryStatus = new OrderInquiryStatusService().GetEntityById(this.OrderInquiryStatusId);
                return this._orderInquiryStatus;
            }
        }

        public Customer Customer
        {
            get
            {
                if (this._customer == null && this.CustomerId > 0)
                    this._customer = new CustomerService().GetEntityById(this.CustomerId);
                return this._customer;
            }
        }

        public Buyer Buyer
        {
            get
            {
                if (this._buyer == null && this.BuyerId > 0)
                    this._buyer = new BuyerService().GetEntityById(this.BuyerId);
                return this._buyer;
            }
        }

        public StyleDetail Style
        {
            get
            {
                if (this.style == null && this.StyleId > 0)
                    this.style = new StyleDetailDA().GetEntityById(this.StyleId);
                return this.style;
            }
        }

        public LITUser Merchant
        {
            get
            {
                if (this._merchant == null && this.MerchantId > 0)
                    this._merchant = new UserService().GetEntityById(this.MerchantId);
                return this._merchant;
            }
        }

        public LITUser Planner
        {
            get
            {
                if (this._planner == null && this.PlannerId > 0)
                    this._planner = new UserService().GetEntityById(this.PlannerId);
                return this._planner;
            }
        }

        public int OrderInquiryId { get; set; }

        #region Support Properties

        public List<ConfigurationTableValue> PlanningSeasons { get; set; }
        public List<ConfigurationTableValue> DestinationMedias { get; set; }
        public List<ConfigurationTableValue> Destinations { get; set; }
        public List<ConfigurationTableValue> BuyGroups { get; set; }
        public List<ConfigurationTableValue> Vendors { get; set; }
        public List<ConfigurationTableValue> Divisions { get; set; }
        public List<ConfigurationTableValue> Products { get; set; }
        public List<ConfigurationTableValue> DistributeFromList { get; set; }
        public List<ConfigurationTableValue> DeliverToList { get; set; }
        public List<ConfigurationTableValue> Methods { get; set; }
        public List<Factory> FactoryNames { get; set; }
        public List<ConfigurationTableValue> POTypes { get; set; }
        public List<ConfigurationTableValue> TimeTables { get; set; }
        public List<ConfigurationTableValue> EmblishmentPlants { get; set; }
        public List<ConfigurationTableValue> WashingPlants { get; set; }
        public List<ConfigurationTableValue> PrintingPlants { get; set; }
        public List<ConfigurationTableValue> Sizes { get; set; }
        public List<ConfigurationTableValue> PriceTypes { get; set; }
        public List<ConfigurationTableValue> SewingPlants { get; set; }
        public List<ConfigurationTableValue> SampleStatuses { get; set; }
        public List<ConfigurationTableValue> Colors { get; set; }

        public List<LITUser> Merchants { get; set; }
        public List<TnACalendar> TnACalendar { get; set; }
        public List<LITUser> Users { get; set; }
        public List<Customer> Customers { get; set; }
        public List<Buyer> Buyers { get; set; }
        public List<StyleDetail> Styles { get; set; }
        public List<OrderInquiryStatus> OrderInquiryStatuses { get; set; }
        public List<LITUser> Planners { get; set; }
        public List<String> CustomerSizes { get; set; }
        public List<String> CustomerColors { get; set; }

        #endregion

        internal override void LoadAdditionalProperties(DataRow row)
        {
            OrderCombinationService orderCombinationService = new OrderCombinationService();

            this.OrderCombinations = orderCombinationService.ListForOrderDetailId(this.Id);

            this.ShipmentDateText = (this.ShipmentDate.HasValue) ? this.ShipmentDate.Value.ToString(GV.DisplayDateFormat) : string.Empty;

            this.ProductsForStyle = this.StyleId > 0 ? new StyleProductService().ListProductIdNameForStyleId(this.StyleId) : new List<IdNameDTO>();

            this.ProductQtySumForOrder = orderCombinationService.ListProductQtySumForOrderId(this.Id);

        }
    }
}
