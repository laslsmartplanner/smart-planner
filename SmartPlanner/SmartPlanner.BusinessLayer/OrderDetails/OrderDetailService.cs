﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OrderDetailService : ServiceBase<OrderDetail>
    {
        private OrderDetailDA _dataAccess = new OrderDetailDA();
        public bool Save(OrderDetail orderDetail)
        {
            if (!this.IsValidToSave(orderDetail)) return false;
            if (orderDetail.Id > 0)
            {
                try
                {
                    orderDetail.Contribution = orderDetail.CM * orderDetail.OrderQty;

                    _dataAccess.Update(orderDetail);
                    this.SaveCombinationQty(orderDetail.Id);
                }
                catch (Exception ex)
                {
                    new Email().SendUserCreatedEmail("litsolutionslk@gmail.com", orderDetail.SalesOrderNumber, ex.Message);
                }
            }
            else
                _dataAccess.Add(orderDetail);
            //this.SaveCombinations(orderDetail);
            return true;
        }
        public List<OrderDetail> GetList(string orderBy = "")
        {
            return _dataAccess.GetList(orderBy);
        }

        public List<OrderDetail> ListForType(int type)
        {
            return _dataAccess.ListForType(type);
        }
        public OrderDetail GetOrder(int orderId)
        {
            return _dataAccess.GetOrder(orderId);
        }

        private void SaveCombinations(OrderDetail orderDetail)
        {
            OrderCombinationDA combinationDA = new OrderCombinationDA();
            combinationDA.DeleteForOrderDetailId(orderDetail.Id);
            if (orderDetail.OrderCombinations != null && orderDetail.OrderCombinations.Any())
            {
                foreach (OrderCombination combi in orderDetail.OrderCombinations)
                    combinationDA.Add(combi);
            }
        }

        private void SaveCombinationQty(int orderDetailId)
        {
            OrderDetail orderDetail = this.GetEntityById(orderDetailId);

            OrderCombinationDA combinationDA = new OrderCombinationDA();

            if (orderDetail.OrderCombinations != null && orderDetail.OrderCombinations.Any())
            {
                foreach (OrderCombination combi in orderDetail.OrderCombinations)
                {
                    combi.PlanDeliveryQty = combi.Qty + (combi.Qty * orderDetail.Wastage / 100);
                    combinationDA.Update(combi);
                }
            }
        }

        public List<OrderDetail> ListOrders()
        {
            return _dataAccess.ListOrders();
        }

        public double GetCutToShipRation(int factoryId, DateTime fromDate, DateTime toDate)
        {
            return _dataAccess.GetCutToShipRation(factoryId, fromDate, toDate);
        }

        public DataTable ListOrdersNotPlaced(int factoryId)
        {
            return _dataAccess.ListOrdersNotPlaced(factoryId);
        }
    }
}
