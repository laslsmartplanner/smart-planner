﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    internal class OrderDetailDA : EntityDataAccessBase<OrderDetail>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Add(OrderDetail orderDetail)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO OrderDetails(POId, SalesOrderNumber, Repeat, CustomerId, CustomerPONumber, PlanningSeasonId, Year, Uplift, OrderQty, Description, CM, CMF, DestinationMediaId, 
                      DestinationId, TradingPONumber, BuyGroupId, VendorId,  PlanDeliveryQty, DivisionId, OrderConfirmationDate, ShipmentDate,  OrderSet, SalesYear, Efficiency, 
                      Reference, DistributeFromId, DeliverToId, MethodId, PlanInGroup, UserRoute, FactoryNameId, FabricIH, FGCode, MerchantId, PilotRun, POTypeId, RevDelivery, CreatedById, CreatedOn, CreatedDate, TimeTableId, Color, 
                      Progress, Consolidated, Notes, Substitute, BuyerId, EmblishmentPlantId,NoOfModules,BuyDate,StyleId,Contribution, CPM, EPM, WashingPlantId, PrintingPlantId, SizeId, PriceTypeId, SewingPlantId, SampleStatusId,OrderInquiryStatusId, ConfirmationDate,
                      IsSplitOrder, IsSewingPlant, IsEmblishmentPlant, IsPrintingPlant, IsWashingPlant, PlannerId,SplitKey,ProductionSMV,CutQty, Wastage,RepeatNumber,DeliveryContractNumber)
                    VALUES (@POId, @SalesOrderNumber, @Repeat, @CustomerCodeId, @CustomerPONumber, @PlanningSeasonId, @Year, @Uplift, @OrderQty, @Description, @CM, @CMF, @DestinationMediaId, 
                      @DestinationId, @TradingPONumber, @BuyGroupId, @VendorId,  @PlanDeliveryQty, @DivisionId, @OrderConfirmationDate, @ShipmentDate,  @OrderSet, @SalesYear, @Efficiency, 
                      @Reference, @DistributeFromId, @DeliverToId, @MethodId, @PlanInGroup, @UserRoute, @FactoryNameId, @FabricIH, @FGCode, @MerchantId, @PilotRun, @POTypeId, @RevDelivery, @CreatedById, @CreatedOn, @CreatedDate, @TimeTableId, @Color, 
                      @Progress, @Consolidated, @Notes, @Substitute, @BuyerId, @EmblishmentPlantId,@NoOfModules,@BuyDate,@StyleId,@Contribution, @CPM, @EPM, @WashingPlantId, @PrintingPlantId, @SizeId, @PriceTypeId, @SewingPlantId, @SampleStatusId, @OrderInquiryStatusId, @ConfirmationDate,
                      @IsSplitOrder, @IsSewingPlant, @IsEmblishmentPlant, @IsPrintingPlant, @IsWashingPlant, @PlannerId,@SplitKey, @ProductionSMV, @CutQty, @Wastage,@RepeatNumber,@DeliveryContractNumber)");

            this.SetParameters(command, orderDetail);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                orderDetail.SetId(ret);
                this.ResetResponsibilitiesForOrder(orderDetail);

                command.CommandText = "UPDATE OrderStyleProductSMVs SET OrderId = @OrderId WHERE OrderInquiryId=@InqId AND SplitKey=@SplitKey";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@OrderId", orderDetail.Id);
                command.Parameters.AddWithValue("@InqId", orderDetail.OrderInquiryId);
                command.Parameters.AddWithValue("@SplitKey", orderDetail.SplitKey);
                _dataAccess.ExecuteNonQuery(command);

                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private void SetParameters(SqlCommand command, OrderDetail orderDetail)
        {

            command.Parameters.AddWithValue("@POId", orderDetail.POId ?? string.Empty);
            command.Parameters.AddWithValue("@SalesOrderNumber", orderDetail.SalesOrderNumber ?? string.Empty);
            command.Parameters.AddWithValue("@Repeat", orderDetail.Repeat);
            command.Parameters.AddWithValue("@CustomerCodeId", orderDetail.CustomerId);
            command.Parameters.AddWithValue("@CustomerPONumber", orderDetail.CustomerPONumber ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@PlanningSeasonId", GetNullIfLessOne(orderDetail.PlanningSeasonId));
            command.Parameters.AddWithValue("@Year", orderDetail.Year);
            command.Parameters.AddWithValue("@OrderQty", orderDetail.OrderQty);
            command.Parameters.AddWithValue("@Description", orderDetail.Description ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@CM", orderDetail.CM);
            command.Parameters.AddWithValue("@CMF", orderDetail.CMF);
            command.Parameters.AddWithValue("@DestinationMediaId", GetNullIfLessOne(orderDetail.DestinationMediaId));
            command.Parameters.AddWithValue("@DestinationId", GetNullIfLessOne(orderDetail.DestinationId));
            command.Parameters.AddWithValue("@TradingPONumber", orderDetail.TradingPONumber ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@BuyGroupId", GetNullIfLessOne(orderDetail.BuyGroupId));
            command.Parameters.AddWithValue("@VendorId", GetNullIfLessOne(orderDetail.VendorId));
            command.Parameters.AddWithValue("@PlanDeliveryQty", orderDetail.PlanDeliveryQty);
            command.Parameters.AddWithValue("@DivisionId", GetNullIfLessOne(orderDetail.DivisionId));

            command.Parameters.AddWithValue("@OrderConfirmationDate", orderDetail.OrderConfirmationDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ShipmentDate", orderDetail.ShipmentDate ?? (object)DBNull.Value);
            //command.Parameters.AddWithValue("@ProductId", GetNullIfLessOne(orderDetail.ProductId));
            command.Parameters.AddWithValue("@OrderSet", orderDetail.OrderSet ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SalesYear", orderDetail.SalesYear);

            command.Parameters.AddWithValue("@Efficiency", orderDetail.Efficiency);
            command.Parameters.AddWithValue("@Reference", orderDetail.Reference ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@DistributeFromId", GetNullIfLessOne(orderDetail.DistributeFromId));
            command.Parameters.AddWithValue("@DeliverToId", GetNullIfLessOne(orderDetail.DeliverToId));
            command.Parameters.AddWithValue("@MethodId", GetNullIfLessOne(orderDetail.MethodId));

            command.Parameters.AddWithValue("@PlanInGroup", orderDetail.PlanInGroup ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@UserRoute", orderDetail.UserRoute ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@FactoryNameId", GetNullIfLessOne(orderDetail.FactoryNameId));
            command.Parameters.AddWithValue("@FabricIH", orderDetail.FabricIH ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@FGCode", orderDetail.FGCode ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@MerchantId", GetNullIfLessOne(orderDetail.MerchantId));
            command.Parameters.AddWithValue("@RevDelivery", orderDetail.RevDelivery ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Uplift", orderDetail.Uplift ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@PilotRun", orderDetail.PilotRun ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@POTypeId", GetNullIfLessOne(orderDetail.POTypeId));

            command.Parameters.AddWithValue("@CreatedDate", orderDetail.CreatedDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@TimeTableId", GetNullIfLessOne(orderDetail.TimeTableId));
            command.Parameters.AddWithValue("@Color", orderDetail.Color ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Progress", orderDetail.Progress ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Consolidated", orderDetail.Consolidated ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@Notes", orderDetail.Notes ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Substitute", GetNullIfLessOne(orderDetail.Substitute));
            command.Parameters.AddWithValue("@NoOfModules", GetNullIfLessOne(orderDetail.NoOfModules));            
            command.Parameters.AddWithValue("@EmblishmentPlantId", GetNullIfLessOne(orderDetail.EmblishmentPlantId));
            command.Parameters.AddWithValue("@BuyerId", GetNullIfLessOne(orderDetail.BuyerId));

            command.Parameters.AddWithValue("@BuyDate", orderDetail.BuyDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@StyleId", GetNullIfLessOne(orderDetail.StyleId));

            command.Parameters.AddWithValue("@WashingPlantId", GetNullIfLessOne(orderDetail.WashingPlantId));
            command.Parameters.AddWithValue("@PrintingPlantId", GetNullIfLessOne(orderDetail.PrintingPlantId));
            command.Parameters.AddWithValue("@SizeId", GetNullIfLessOne(orderDetail.SizeId));
            command.Parameters.AddWithValue("@PriceTypeId", GetNullIfLessOne(orderDetail.PriceTypeId));
            command.Parameters.AddWithValue("@SewingPlantId", GetNullIfLessOne(orderDetail.SewingPlantId));
            command.Parameters.AddWithValue("@SampleStatusId", GetNullIfLessOne(orderDetail.SampleStatusId));
            command.Parameters.AddWithValue("@OrderInquiryStatusId", GetNullIfLessOne(orderDetail.OrderInquiryStatusId));
            
            command.Parameters.AddWithValue("@Contribution", orderDetail.Contribution);
            command.Parameters.AddWithValue("@CPM", orderDetail.CPM);
            command.Parameters.AddWithValue("@EPM", orderDetail.EPM);
            command.Parameters.AddWithValue("@ConfirmationDate", orderDetail.ConfirmationDate ?? (object)DBNull.Value);

            command.Parameters.AddWithValue("@IsSplitOrder", orderDetail.IsSplitOrder);
            command.Parameters.AddWithValue("@IsSewingPlant", orderDetail.IsSewingPlant);
            command.Parameters.AddWithValue("@IsEmblishmentPlant", orderDetail.IsEmblishmentPlant);
            command.Parameters.AddWithValue("@IsPrintingPlant", orderDetail.IsPrintingPlant);
            command.Parameters.AddWithValue("@IsWashingPlant", orderDetail.IsWashingPlant);

            command.Parameters.AddWithValue("@PlannerId", GetNullIfLessOne(orderDetail.PlannerId));
            command.Parameters.AddWithValue("@SplitKey", orderDetail.SplitKey);
            command.Parameters.AddWithValue("@ProductionSMV", orderDetail.ProductionSMV);

            command.Parameters.AddWithValue("@CutQty", orderDetail.CutQty);
            command.Parameters.AddWithValue("@Wastage", orderDetail.Wastage);

            command.Parameters.AddWithValue("@RepeatNumber", orderDetail.RepeatNumber ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@DeliveryContractNumber", orderDetail.DeliveryContractNumber ?? (object)DBNull.Value);
        }

        internal bool Update(OrderDetail orderDetail)
        {
            OrderDetail currentOrder = GetEntityById(orderDetail.Id);
            bool hasSetToRepeat = currentOrder != null && !currentOrder.Repeat && orderDetail.Repeat;
            
            SqlCommand command = new SqlCommand(@"UPDATE   OrderDetails
                                            SET     POId =@POId, SalesOrderNumber =@SalesOrderNumber, Repeat =@Repeat , CustomerId =@CustomerCodeId , CustomerPONumber =@CustomerPONumber , PlanningSeasonId =@PlanningSeasonId, Year =@Year , Uplift =@Uplift, OrderQty =@OrderQty, Description =@Description , CM =@CM , CMF =@CMF , 
                                                    DestinationMediaId =@DestinationMediaId , DestinationId =@DestinationId , TradingPONumber =@TradingPONumber , BuyGroupId =@BuyGroupId , VendorId =@VendorId ,  PlanDeliveryQty =@PlanDeliveryQty , DivisionId =@DivisionId , OrderConfirmationDate =@OrderConfirmationDate , ShipmentDate =@ShipmentDate , 
                                                    OrderSet =@OrderSet , SalesYear =@SalesYear , Efficiency =@Efficiency , Reference =@Reference , DistributeFromId =@DistributeFromId , DeliverToId =@DeliverToId , MethodId =@MethodId , PlanInGroup =@PlanInGroup , UserRoute =@UserRoute , FactoryNameId =@FactoryNameId , FabricIH =@FabricIH , FGCode =@FGCode , MerchantId =@MerchantId , 
                                                    PilotRun =@PilotRun , POTypeId =@POTypeId , RevDelivery =@RevDelivery , TimeTableId=@TimeTableId, Color=@Color, Progress=@Progress, Consolidated=@Consolidated, Notes=@Notes, Substitute=@Substitute, BuyerId=@BuyerId, EmblishmentPlantId=@EmblishmentPlantId, NoOfModules=@NoOfModules, BuyDate=@BuyDate, StyleId=@StyleId ,
                                                    Contribution=@Contribution, CPM=@CPM, EPM=@EPM, WashingPlantId=@WashingPlantId, PrintingPlantId=@PrintingPlantId, SizeId=@SizeId, PriceTypeId=@PriceTypeId, SewingPlantId=@SewingPlantId, SampleStatusId=@SampleStatusId, OrderInquiryStatusId=@OrderInquiryStatusId, ConfirmationDate=@ConfirmationDate,
                                                    IsSplitOrder=@IsSplitOrder, IsSewingPlant=@IsSewingPlant, IsEmblishmentPlant=@IsEmblishmentPlant, IsPrintingPlant=@IsPrintingPlant, IsWashingPlant=@IsWashingPlant, PlannerId=@PlannerId, ProductionSMV =@ProductionSMV, CutQty =@CutQty, Wastage=@Wastage,RepeatNumber = @RepeatNumber,DeliveryContractNumber =@DeliveryContractNumber     
                                            WHERE     (Id = @Id )");
            command.Parameters.AddWithValue("@Id", orderDetail.Id);
            this.SetParameters(command, orderDetail);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                this.ResetResponsibilitiesForOrder(orderDetail);

                if (orderDetail.POTypeId > 0 && hasSetToRepeat)
                    this.ResetDuration(orderDetail.Id);

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<OrderDetail> ListForType(int type)
        {
            string whereCondition = "OrderInquiryStatusId = 1";

            if(type !=2)
            {
                whereCondition = "OrderInquiryStatusId != 1";
            }

            SqlCommand sqlCommand = new SqlCommand($"select * from OrderDetails o WITH(NOLOCK) LEFT JOIN OnGoingOrderProcesses p WITH(NOLOCK) ON o.Id = p.orderdetailsId WHERE {whereCondition} AND o.PoTypeId>0 ");

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal OrderDetail GetOrder(int orderId)
        {
            SqlCommand sqlCommand = new SqlCommand($"select * from OrderDetails o WITH(NOLOCK) LEFT JOIN OnGoingOrderProcesses p WITH(NOLOCK) ON o.Id = p.orderdetailsId WHERE o.Id=@Id ");

            try
            {
                sqlCommand.Parameters.AddWithValue("@Id", orderId);
                return this.GetList(sqlCommand).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal List<OrderDetail> ListForOrderInquiryId(int orderInquiryId)
        {
            SqlCommand sqlCommand = new SqlCommand("select * from OrderDetails o WITH(NOLOCK) WHERE OrderInquiryId=@OrderInquiryId ");
            sqlCommand.Parameters.AddWithValue("@OrderInquiryId", orderInquiryId);

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        
        internal List<OrderDetail> ListOrders()
        {
            SqlCommand sqlCommand = new SqlCommand($"select * from OrderDetails o WITH(NOLOCK) LEFT JOIN OnGoingOrderProcesses p WITH(NOLOCK) ON o.Id = p.orderdetailsId ");

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal DataTable ListOrdersNotPlaced(int factoryId)
        {
            SqlCommand sqlCommand = new SqlCommand("GetOrderStartEnd");
            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@ReturnOrderDetails", 1);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            return _dataAccess.GetDataTable(sqlCommand);
        }


        internal void ResetResponsibilitiesForOrder(OrderDetail order)
        {
            if (order.POTypeId < 1) return;

            SqlCommand sqlCommand = new SqlCommand("ResetResponsibilitiesForOrder");
            sqlCommand.Parameters.AddWithValue("@OrderId", order.Id);
            sqlCommand.Parameters.AddWithValue("@UserId", GV.LoggedUser().Id);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            _dataAccess.ExecuteNonQuery(sqlCommand);
        }

        internal void ResetDuration(int orderId)
        {
            SqlCommand sqlCommand = new SqlCommand("ResetDuration");
            sqlCommand.Parameters.AddWithValue("@OrderId", orderId);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            _dataAccess.ExecuteNonQuery(sqlCommand);
        }

        internal double GetCutToShipRation(int factoryId, DateTime fromDate, DateTime toDate)
        {
            double factoryEfficiency = 0;

            SqlCommand sqlCommand = new SqlCommand(@"SELECT 
                    isnull(SUM(
                    CASE
                    WHEN oc.ShipQty is null then 0
                    WHEN oc.ShipQty = 0 then 0
                    ELSE oc.CutQty/isnull(oc.ShipQty,1)
                    end)/count(*),0)
                     FROM OrderCombinations oc WITH(NOLOCK)
                    INNER JOIN OrderDetails o WITH(NOLOCK) ON oc.OrderDetailsId = o.Id
                    INNER JOIN PlacedOrders po WITH(NOLOCK) ON oc.OrderDetailsId = po.OrderDetailId and po.IsInquiry=0
                    WHERE Cast(o.ShipmentDate as Date) >= Cast(@FromDate as Date) AND Cast(o.ShipmentDate as Date) <= Cast(@ToDate as Date) and o.FactoryNameId = @FactoryId");

            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
            sqlCommand.Parameters.AddWithValue("@ToDate", toDate);

            try
            {
                object ret = _dataAccess.ExecuteScalar(sqlCommand);
                if (ret == null)
                    factoryEfficiency = 0;
                else
                    factoryEfficiency = Convert.ToDouble(ret) * 100;

            }
            catch (Exception ex)
            {
                throw;
            }


            return Math.Round(factoryEfficiency, 2);
        }

        internal List<IdNameDTO> GetOrdersIdNameDTO()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select ID,SalesOrderNumber Name from OrderDetails with(nolock) order by SalesOrderNumber");
                return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal List<IdNameDTO> GetProductsIdNameDTO()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id,ConfigValue Name from ConfigurationTableValues with(nolock) where ConfigId = @ConfigId order by ConfigValue");
                cmd.Parameters.AddWithValue("@ConfigId", (int)EnumConfigurationTables.Product);
                return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
    }
}
