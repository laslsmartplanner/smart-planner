﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class StyleAdditionalFile : EntityBase<StyleAdditionalFile>
    {
        private AdditionalFileType _fileType = null;

        public override string TableName => "StyleProducts_AdditionalInformations";

        [Required]
        [Display(Name = "File Type")]
        public int FileTypeId { get; set; }

        [Required]
        [Display(Name = "Style Product")]
        public int StyleProductId { get; set; }

        [Required]
        [Display(Name = "File Name")]
        public string FileName { get; set; }

        [Display(Name = "File Path")]
        public string FilePath { get; set; }

        public AdditionalFileType FileType
        {
            get
            {
                if (this._fileType == null && this.FileTypeId > 0)
                    this._fileType = new AdditionalFileTypeService().GetEntityById(this.FileTypeId);
                return this._fileType;
            }
        }

    }
}
