﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class StyleAdditionalFileService : ServiceBase<StyleAdditionalFile>
    {
        private StyleAdditionalFileDA _dataAccess = new StyleAdditionalFileDA();

        public bool Save(StyleAdditionalFile styleAdditionalFile)
        {
            if (!this.IsValidToSave(styleAdditionalFile)) return false;

            if (styleAdditionalFile.Id > 0)
                _dataAccess.Update(styleAdditionalFile);
            else
                _dataAccess.Add(styleAdditionalFile);

            return true;
        }

        public List<StyleAdditionalFile> ListForStyleProductId(int styleProductId)
        {
            return _dataAccess.ListForStyleProductId(styleProductId);
        }

    }
}
