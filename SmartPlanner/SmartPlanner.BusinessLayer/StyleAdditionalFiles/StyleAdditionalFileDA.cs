﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class StyleAdditionalFileDA : EntityDataAccessBase<StyleAdditionalFile>
    {
        private DataAccess _dataAccess = new DataAccess();


        private void SetParameters(SqlCommand command, StyleAdditionalFile styleAdditionalFile)
        {
            command.Parameters.AddWithValue("@StyleProductId", styleAdditionalFile.StyleProductId);
            command.Parameters.AddWithValue("@FileTypeId", styleAdditionalFile.FileTypeId);
            command.Parameters.AddWithValue("@FileName", styleAdditionalFile.FileName);
        }

        internal bool Add(StyleAdditionalFile styleAdditionalFile)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO StyleProducts_AdditionalInformations (StyleProductId,FileTypeId,FileName,CreatedById, CreatedOn)
                                                                                                VALUES(@StyleProductId,@FileTypeId,@FileName, @CreatedById, @CreatedOn)");

            this.SetParameters(command, styleAdditionalFile);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                styleAdditionalFile.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(StyleAdditionalFile styleAdditionalFile)
        {
            SqlCommand command = new SqlCommand(@"UPDATE StyleProducts_AdditionalInformations
                                                SET StyleProductId =@StyleProductId , FileTypeId =@FileTypeId, FileName=@FileName , UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn 
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", styleAdditionalFile.Id);
            this.SetParameters(command, styleAdditionalFile);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }


        internal List<StyleAdditionalFile> ListForStyleProductId(int styleProductId)
        {
            SqlCommand sqlCommand = new SqlCommand($"SELECT * FROM StyleProducts_AdditionalInformations WHERE StyleProductId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", styleProductId);

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
