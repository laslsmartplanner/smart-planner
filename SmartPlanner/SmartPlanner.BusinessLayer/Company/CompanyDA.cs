﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class CompanyDA : EntityDataAccessBase<Company>
    {
        private DataAccess _dataAccess = new DataAccess();
        
        internal void Update(Company company)
        {
            SqlCommand command = new SqlCommand(@"UPDATE    CompanyDetails
                                                SET  ReportLogoName =@ReportLogoName , OrderPrefix =@OrderPrefix , Address =@Address , Phone =@Phone , 
                                                     StartTime =@StartTime , TotalMinutes =@TotalMinutes,DefaultFactoryId=@DefaultFactoryId , UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn 
                                                WHERE   (Id = @Id)");
            command.Parameters.AddWithValue("@Id", company.Id);
            command.Parameters.AddWithValue("@ReportLogoName", company.ReportLogoName ?? string.Empty);
            command.Parameters.AddWithValue("@OrderPrefix", company.OrderPrefix ?? string.Empty);
            command.Parameters.AddWithValue("@Address", company.Address ?? string.Empty);
            command.Parameters.AddWithValue("@Phone", company.Phone ?? string.Empty);
            command.Parameters.AddWithValue("@StartTime", company.StartTime);
            command.Parameters.AddWithValue("@TotalMinutes", company.TotalMinutes);
            command.Parameters.AddWithValue("@DefaultFactoryId", company.DefaultFactoryId);

            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

    }
}
