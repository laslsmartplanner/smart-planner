﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Web;
using System.Data;
using System.IO;
using System.Configuration;

namespace SmartPlanner.BusinessLayer
{
    public class Company : EntityBase<Company>
    {
        public override string TableName => "CompanyDetails";

        private Factory _factory = null;

        public string Address { get; set; }        
        [Display(Name ="Report Logo")]
        public string ReportLogoName { get; set; }
        [Display(Name = "Report Logo")]
        public HttpPostedFileBase ReportLogo { get; set; }
        [Display(Name = "Order Prefix")]
        public string OrderPrefix { get; set; }
        public string Phone { get; set; }
        [Display(Name = "Start Time")]
        public double StartTime { get; set; }
        [Display(Name = "Default Factory")]
        public int DefaultFactoryId { get; set; }
        [Display(Name = "Total Minutes")]
        public double TotalMinutes { get; set; }
        public string ReportLogoPath { get; set; }

        [Display(Name = "Report Logo Name")]
        public string OldReportLogoName { get; set; }

        public Factory DefaultFactory
        {
            get
            {
                if (this._factory == null && this.DefaultFactoryId > 0)
                    this._factory = new FactoryService().GetEntityById(this.DefaultFactoryId);
                return this._factory;
            }
        }

        public List<Factory> Factories { get; set; }
        internal override void LoadAdditionalProperties(DataRow row)
        {

            string uploadPath = Path.Combine(ConfigurationManager.AppSettings["StyleSkethDirectory"].ToString());

            this.OldReportLogoName = Convert.ToString(row["ReportLogoName"]);

            this.ReportLogoPath = Path.Combine(uploadPath, this.ReportLogoName);
        }
    }
}
