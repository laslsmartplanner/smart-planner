﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartPlanner.BusinessLayer
{
    public class CompanyService
    {
        private  CompanyDA dataAccess = new CompanyDA();
        public Company GetCompany()
        {
            return dataAccess.GetList().FirstOrDefault();
        }
        public void Save(Company company)
        {

            if (company.ReportLogo != null && !company.ReportLogoName.Equals(company.OldReportLogoName))
            {
                string fileName = Path.Combine(HttpContext.Current.Server.MapPath("~"), company.ReportLogo.FileName);

                company.ReportLogo.SaveAs(fileName);
                company.ReportLogoName = Path.GetFileName(fileName);
            }
            new CompanyDA().Update(company);
        }
    }
}
