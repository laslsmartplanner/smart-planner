﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public  class OrderCombination : EntityBase<OrderCombination>
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        private ConfigurationTableValue _color = null;
        private StyleProduct _styleProduct = null;

        public override string TableName => "OrderCombinations";

        public int OrderDetailsId { get; set; }

        [Display(Name = "Size")]
        public string SizeName { get; set; }

        [Display(Name = "Color")]
        public string ColorName { get; set; }

        [Display(Name = "Quantity")]
        public int Qty { get; set; }

        [Display(Name = "Product")]
        public int StyleProductId { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        [Display(Name = "Cut Quantity")]
        public decimal PlanDeliveryQty { get; set; }

        [Display(Name = "Wastage%")]
        public decimal Wastage { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        [Display(Name = "Actual Cut Quantity")]
        public decimal CutQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ship Quantity")]
        public decimal ShipQty { get; set; }

        public StyleProduct StyleProduct
        {
            get
            {
                if (this._styleProduct == null && this.StyleProductId > 0)
                    this._styleProduct = new StyleProductService().GetEntityById(this.StyleProductId);
                return this._styleProduct;
            }
        }

    }
}
