﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    internal class OrderCombinationDA : EntityDataAccessBase<OrderCombination>
    {
        private DataAccess _dataAccess = new DataAccess();

        private void SetParameters(SqlCommand command, OrderCombination combination)
        {
            command.Parameters.AddWithValue("@OrderDetailId", combination.OrderDetailsId);
            command.Parameters.AddWithValue("@SizeName", combination.SizeName);
            command.Parameters.AddWithValue("@Qty", combination.Qty);
            command.Parameters.AddWithValue("@ColorName", combination.ColorName);
            command.Parameters.AddWithValue("@StyleProductId", combination.StyleProductId);
            command.Parameters.AddWithValue("@PlanDeliveryQty", combination.PlanDeliveryQty);
            command.Parameters.AddWithValue("@CutQty", combination.CutQty);
            command.Parameters.AddWithValue("@ShipQty", combination.ShipQty);
            
        }

        internal bool Add(OrderCombination combination)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO OrderCombinations
                                                (OrderDetailsId, SizeName, ColorName,StyleProductId, Qty,PlanDeliveryQty,CutQty,ShipQty,CreatedById, CreatedOn)
                                     VALUES     (@OrderDetailId, @SizeName, @ColorName,@StyleProductId, @Qty,@PlanDeliveryQty,@CutQty,@ShipQty, @CreatedById, @CreatedOn)");

            this.SetParameters(command, combination);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                combination.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        

        internal bool Update(OrderCombination combination)
        {
            SqlCommand command = new SqlCommand(@"UPDATE  OrderCombinations
                                            SET OrderDetailsId =@OrderDetailId , SizeName =@SizeName , ColorName =@ColorName, StyleProductId =@StyleProductId , Qty =@Qty, PlanDeliveryQty =@PlanDeliveryQty, CutQty=@CutQty, ShipQty=@ShipQty, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn     
                                            WHERE     (Id = @Id )");
            command.Parameters.AddWithValue("@Id", combination.Id);
            this.SetParameters(command, combination);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<OrderCombination> ListForOrderDetailId(int orderDetailId)
        {
            SqlCommand sqlCommand = new SqlCommand($"select * from OrderCombinations WHERE OrderDetailsId = @OrderDetailId ORDER BY StyleProductId, SizeName, ColorName");
            sqlCommand.Parameters.AddWithValue("@OrderDetailId", orderDetailId);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void DeleteForOrderDetailId(int orderDetailId)
        {
            SqlCommand sqlCommand = new SqlCommand($"DELETE OrderCombinations WHERE OrderDetailsId = @OrderDetailId ");
            sqlCommand.Parameters.AddWithValue("@OrderDetailId", orderDetailId);
            try
            {
                _dataAccess.ExecuteNonQuery(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<string> ListSizesForCustomer(int customerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"select * from  Customers_SizesList WHERE CustomerId = @CustomerId ORDER BY SizeName");
            sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);
            try
            {
                DataTable dt = _dataAccess.GetDataTable(sqlCommand);

                List<string> list = new List<string>();
                if (dt == null || dt.Rows.Count == 0) return list;
                foreach (DataRow row in dt.Rows)
                    list.Add(Convert.ToString(row["SizeName"]));

                return list;

            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<string> ListColorsForCustomer(int customerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"select * from  Customers_ColorsList WHERE CustomerId = @CustomerId  ORDER BY ColorName");
            sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);
            try
            {
                DataTable dt = _dataAccess.GetDataTable(sqlCommand);

                List<string> list = new List<string>();
                if (dt == null || dt.Rows.Count == 0) return list;
                foreach (DataRow row in dt.Rows)
                    list.Add(Convert.ToString(row["ColorName"]));

                return list;

            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<OrderQtyPlanQtyDTO> ListProductQtySumForOrderId(int orderDetailId)
        {
            SqlCommand sqlCommand = new SqlCommand(@"SELECT ISNULL(SUM(oc.Qty),0) as Qty,ISNULL(SUM(oc.PlanDeliveryQty),0) as PlanDeliveryQty,ISNULL(SUM(oc.CutQty),0) as ActualCutQty,ISNULL(SUM(oc.ShipQty),0) as ShipQty, p.ConfigValue as ProductName from OrderCombinations oc WITH(NOLOCK)
                INNER JOIN StyleProducts sp WITH(NOLOCK) on oc.StyleProductId = sp.id
                INNER JOIN ConfigurationTableValues p WITH(NOLOCK) on p.id = sp.ProductId
                 WHERE oc.OrderDetailsId = @OrderDetailsId GROUP BY p.ConfigValue");

            sqlCommand.Parameters.AddWithValue("@OrderDetailsId", orderDetailId);
            try
            {
                return OrderQtyPlanQtyDTO.Load(_dataAccess.GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal int GetTotalCutQtySumForOrderId(int orderDetailId)
        {
            SqlCommand command = new SqlCommand(@"SELECT SUM(CutQty) FROM OrderCombinations WITH(NOLOCK) WHERE OrderDetailsId=@OrderDetailsId");
            command.Parameters.AddWithValue("@OrderDetailsId", orderDetailId);

            object cutTotal = _dataAccess.ExecuteScalar(command);
            return Convert.ToInt32(cutTotal);
        }

        internal int GetTotalShipQtySumForOrderId(int orderDetailId)
        {
            SqlCommand command = new SqlCommand(@"SELECT SUM(ShipQty) FROM OrderCombinations WITH(NOLOCK) WHERE OrderDetailsId=@OrderDetailsId");
            command.Parameters.AddWithValue("@OrderDetailsId", orderDetailId);

            object shipTotal = _dataAccess.ExecuteScalar(command);
            return Convert.ToInt32(shipTotal);
        }
    }
}
