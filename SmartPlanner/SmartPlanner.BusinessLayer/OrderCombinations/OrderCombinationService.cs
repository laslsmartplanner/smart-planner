﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.BusinessLayer.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class OrderCombinationService : ServiceBase<OrderCombination>
    {
        private OrderCombinationDA _dataAccess = new OrderCombinationDA();

        public bool Save(OrderCombination orderCombination)
        {
            //if (!this.IsValidToSave(orderCombination)) return false;

            if (orderCombination.Id > 0)
            {
                _dataAccess.Update(orderCombination);

                OrderDetailService orderDetailService = new OrderDetailService();

                OrderDetail orderDetail = orderDetailService.GetEntityById(orderCombination.OrderDetailsId);
                orderDetail.CutQty = _dataAccess.GetTotalCutQtySumForOrderId(orderCombination.OrderDetailsId);
                orderDetailService.Save(orderDetail);
            }
            else
                _dataAccess.Add(orderCombination);

            
            return true;
        }

        public List<OrderCombination> ListForOrderDetailId(int orderDetailId)
        {
            return _dataAccess.ListForOrderDetailId(orderDetailId);
        }
        public List<string> ListSizesForCustomer(int customerId)
        {
            return _dataAccess.ListSizesForCustomer(customerId);
        }

        public List<string> ListColorsForCustomer(int customerId)
        {
            return _dataAccess.ListColorsForCustomer(customerId);
        }

        public List<OrderQtyPlanQtyDTO> ListProductQtySumForOrderId(int orderDetailId)
        {
            return _dataAccess.ListProductQtySumForOrderId(orderDetailId);
        }

        public void ImportOrderCombination(HttpPostedFileBase infile, int styleId, int customerId, int orderId)
        {
            if (infile.ContentLength > 0 && (Path.GetExtension(infile.FileName) == ".xls" || Path.GetExtension(infile.FileName) == ".xlsx" || Path.GetExtension(infile.FileName) == ".xlsm"))
            {
                ExcelData excelData = new ExcelData();
                IEnumerable<DataRow> dt = excelData.GetData(infile);

                if (dt == null || dt.Count() == 0 || !dt.FirstOrDefault().Table.Columns.Contains("Product") || !dt.FirstOrDefault().Table.Columns.Contains("Color") || !dt.FirstOrDefault().Table.Columns.Contains("Size") || !dt.FirstOrDefault().Table.Columns.Contains("Quantity")) return;
                
                ConfigurationTableValueService configService = new ConfigurationTableValueService();
                List<ConfigurationTableValue> products = configService.ListByConfigId((int)EnumConfigurationTables.Product);

                StyleProductService styleProductService = new StyleProductService();

                List<StyleProduct> sps = styleProductService.ListForStyleId(styleId);
                OrderCombinationService orderCombinationService = new OrderCombinationService();
                List<String> customerColors = orderCombinationService.ListColorsForCustomer(customerId);
                List<String> customerSizes = orderCombinationService.ListSizesForCustomer(customerId);
                List<OrderCombination> combinations = orderCombinationService.ListForOrderDetailId(orderId);

                if (sps == null || !sps.Any()) //|| customerColors == null || !customerColors.Any()
                    return;

                List<OrderCombination> lst = new List<OrderCombination>();

                CustomerService customerService = new CustomerService();

                foreach (DataRow row in dt)
                {
                    string productName = Convert.ToString(row["Product"]);
                    string cusColor = Convert.ToString(row["Color"]);
                    string cusSize = Convert.ToString(row["Size"]);

                    if (string.IsNullOrWhiteSpace(productName) 
                        || string.IsNullOrWhiteSpace(cusColor)
                        || string.IsNullOrWhiteSpace(cusSize)) continue;

                    ConfigurationTableValue product = products.FirstOrDefault(x => x.ConfigValue.Equals(productName, StringComparison.OrdinalIgnoreCase));
                    if (product == null)
                    {
                        product = new ConfigurationTableValue() { ConfigValue = productName, ConfigId = (int)EnumConfigurationTables.Product };
                        configService.Save(product);
                        products.Add(product);
                    }

                    StyleProduct styleProduct = sps.FirstOrDefault(x => x.ProductName.Equals(productName, StringComparison.OrdinalIgnoreCase));
                    if (styleProduct == null)
                    {
                        styleProduct = new StyleProduct() { StyleId = styleId, ProductId = product.Id };
                        styleProductService.Save(styleProduct);
                        sps.Add(styleProduct);
                    }
                    
                    if (customerColors.FirstOrDefault(x => x.Equals(cusColor, StringComparison.OrdinalIgnoreCase)) == null)
                    {
                        customerService.SaveColorsList(cusColor, customerId);
                        customerColors.Add(cusColor);
                    }

                    if (customerSizes.FirstOrDefault(x => x.Equals(cusSize, StringComparison.OrdinalIgnoreCase)) == null)
                    {
                        customerService.SaveSizesList(cusSize, customerId);
                        customerSizes.Add(cusSize);
                    }

                    int.TryParse(Convert.ToString(row["Quantity"]), out int qty);

                    OrderCombination orderCombi = combinations.FirstOrDefault(x => x.StyleProductId == styleProduct.Id && x.SizeName.Equals(cusSize) && x.ColorName.Equals(cusColor));

                    if (orderCombi == null)
                    {
                        orderCombi = new OrderCombination()
                        {
                            OrderDetailsId = orderId,
                            StyleProductId = styleProduct.Id,
                            SizeName = cusSize,
                            ColorName = cusColor,
                            Qty = qty
                        };
                        combinations.Add(orderCombi);
                    }
                    else
                        orderCombi.Qty = qty;

                    orderCombinationService.Save(orderCombi);
                }

            }
        }
    }
}
