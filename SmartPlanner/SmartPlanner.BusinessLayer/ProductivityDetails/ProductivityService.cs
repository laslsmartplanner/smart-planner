﻿using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class ProductivityService : ServiceBase<Productivity>
    {
        private ProductivityDA _dataAccess = new ProductivityDA();
        public List<IdNameDTO> GetFactoriesInBoard(DateTime date)
        {
            return _dataAccess.GetFactoriesInBoard(date);
        }
        public List<IdNameDTO> GetLinesInBoard(DateTime date,int factoryId)
        {
            return _dataAccess.GetLinesInBoard(date,factoryId);
        }
        public List<IdNameDTO> GetOrderDetailForLineModuleInBoard(DateTime date, int factoryId,int lineId, int orderId = 0)
        {
            return _dataAccess.GetOrderDetailsInBoard(date, factoryId, lineId, orderId);
        }
        public List<IdNameDTO> GetOrdersInBoard(DateTime date, int factoryId, int lineId)
        {
            return _dataAccess.GetOrdersInBoard(date, factoryId, lineId);
        }
        public List<IdNameDTO> GetProductsInBoard(DateTime date, int factoryId, int lineId, int orderId)
        {
            return _dataAccess.GetProductsInBoard(date, factoryId, lineId, orderId);
        }
        public bool Save(Productivity productivity)
        {
            if (!this.IsValidToSave(productivity)) return false;
            if (productivity.Id > 0)
                return _dataAccess.Update(productivity);
            return _dataAccess.Add(productivity);
        }

        public List<Productivity> ListForSFactoryEfficiency(int factoryId, int lineModuleId, int styleDetailId, DateTime? fromDate = null, DateTime? toDate = null)
        {
            return _dataAccess.ListForSFactoryEfficiency(factoryId, lineModuleId, styleDetailId, fromDate, toDate);
        }

        public List<LineOutput> GetLineOutputs(int factoryId, int lineId, DateTime workingDate, int productivityId = 0)
        {
            return new LineOutputDA().GetLineOutputs(factoryId, lineId, workingDate, productivityId);
        }

        public void AddLineOutput(LineOutput output,Productivity productivity)
        {
            _dataAccess.AddLineOutput(output, productivity);
        }

        public List<Productivity> ListProductivities()
        {
            return _dataAccess.ListProductivities();
        }

        public double GetPlantEffciencyForAMonth(int factoryId, DateTime fromDate, DateTime toDate)
        {
            return _dataAccess.GetPlantEffciencyForAMonth(factoryId, fromDate, toDate);
        }
        public void DeleteProductivity(Productivity productivity)
        {
            _dataAccess.DeleteProductivity(productivity);
        }
        public DataRow GetPlannedQty(int orderId, int productId, int lineModuleId, int factoryId, double duration)
        {
            return _dataAccess.GetPlannedQty(orderId, productId, lineModuleId, factoryId, duration);
        }
        public int GetProductivityId(DateTime date, int factoryId, int lineId, int orderId, int productId)
        {
            return _dataAccess.GetProductivityId(date, factoryId, lineId, orderId, productId);
        }

        public int GetTotalProducedForOrderId(int orderDetailId, int productId)
        {
            return _dataAccess.GetTotalProducedForOrderId(orderDetailId, productId);
        }

        public int GetTotalProducedForFactoryId(DateTime workingDate, int factoryId)
        {
            return _dataAccess.GetTotalProducedForFactoryId(workingDate, factoryId);
        }


        public List<ReportDTOProductivity> GetProductivityForReport(DateTime workingDate, int factoryId)
        {
            return _dataAccess.GetProductivityForReport(workingDate, factoryId);
        }

        public DateTime GetPreviousWorkingDay(DateTime workingDate)
        {
            return _dataAccess.GetPreviousWorkingDay(workingDate);

        }
    }
}
