﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Productivity : EntityBase<Productivity>
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();
        private LineModuleService _lineModuleService = new LineModuleService();
        private OrderDetailService _orderService = new OrderDetailService();
        private StyleDetailService _styleService = new StyleDetailService();

        private Factory _factory = null;
        private LineModule _lineModule = null;
        private OrderDetail _order = null;
        private StyleDetail _style = null;
        private ConfigurationTableValue _product = null;

        public override string TableName => "ProductivityDetails";

        [Required]
        [Display(Name = "Date")]
        public DateTime? WorkingDate { get; set; }

        [Display(Name = "Date")]
        public string WorkingDateText { get; set; }

        [Required]
        [Display(Name = "Factory")]
        public int FactoryId { get; set; }

        [Required]
        [Display(Name = "Line Module")]
        public int LineModuleId { get; set; }

        [Required]
        [Display(Name ="Order Reference")]
        public int OrderId { get; set; }

        [Display(Name ="Style Number")]
        public int StyleId { get; set; }

        [Required]
        [Display(Name = "Product")]
        public int ProductId { get; set; }

        public int Attendance { get; set; }

        [Display(Name = "Produced Qty")]
        public double ProducedQty { get; set; }

        public List<IdNameDTO> Products { get; set; }

        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = _configValueService.GetEntityById(this.ProductId);
                return this._product;
            }
        }

        public Factory Factory
        {
            get
            {
                if (this._factory == null && this.FactoryId > 0)
                    this._factory = new FactoryService().GetEntityById(this.FactoryId);
                return this._factory;
            }   
        }

        public LineModule LineModule
        {
            get
            {
                if (this._lineModule == null && this.LineModuleId > 0)
                    this._lineModule = _lineModuleService.GetEntityById(this.LineModuleId);
                return this._lineModule;
            }
        }

        public OrderDetail Order
        {
            get
            {
                if (this._order == null && this.OrderId> 0)
                    this._order = _orderService.GetEntityById(this.OrderId);
                return this._order;
            }
        }

        public StyleDetail Style
        {
            get
            {
                if (this._style == null && this.StyleId > 0)
                    this._style = _styleService.GetEntityById(this.StyleId);
                return this._style;
            }
        }

        public string StyleNumber
        {
            get
            {
                return this.Style?.StyleNumber??string.Empty;
            }
        }

        public decimal AvailableMinutes
        {
            get
            {
                return this.LineModule?.AvailableMinutes ?? 0;
            }
        }

        public decimal TotalSMV
        {
            get
            {
                return this.Style?.TotalSMV ?? 0;
            }
        }

        public string ModuleNumber
        {
            get
            {
                return this.LineModule?.ModuleNumber ?? string.Empty;
            }
        }

        public string WorkingDateString
        {
            get
            {
                if (this.WorkingDate.HasValue)
                    return this.WorkingDate.Value.ToString("dd-MMM-yyyy");
                else
                    return string.Empty;
            }
        }

        public List<IdNameDTO> Factories { get; set; }
        public List<IdNameDTO> LineModules { get; set; }
        public List<StyleDetail> Styles { get; set; }
        public List<LineOutput> LineOutputs { get; set; }

        public List<IdNameDTO> Orders { get; set; }

        internal override void ValidateOnSave()
        {
            if (new ProductivityDA().IsExists(this.Id, this.FactoryId, this.LineModuleId, this.OrderId, this.WorkingDate.Value, this.ProductId))
                this.BrokenBusinessRules.Add("Productivity already added to this date. Update or Delete existing productivity.");
        }

        public string FactoryName { get { return this.Factory?.FactoryName ?? string.Empty; } }

        public string ProductName { get { return this.Product?.ConfigValue ?? string.Empty; } }

        internal override void LoadAdditionalProperties(DataRow row)
        {
            this.WorkingDateText = (this.WorkingDate.HasValue) ? this.WorkingDate.Value.ToString(GV.DisplayDateFormat) : string.Empty;
        }

    }

    public class LineOutput: EntityBase<LineOutput>
    {
        public override string TableName => "LineOutputs";

        public int ProductivityId { get; set; }
        public double FromTime { get; set; }
        public double ToTime { get; set; }
        public double? ProducedQty { get; set; }
    }
}
