﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.BusinessLayer.DTOs;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class ProductivityDA : EntityDataAccessBase<Productivity>
    {
        private DataAccess _dataAccess = new DataAccess();
        
        internal List<IdNameDTO> GetFactoriesInBoard(DateTime date)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetFactoriesInBoard");
                cmd.Parameters.AddWithValue("@Date", date);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<IdNameDTO> GetOrderDetailsInBoard(object date, object factoryId, int lineId, int orderId = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetOrderDetailForLineModuleInBoard");
                cmd.Parameters.AddWithValue("@Date", date);
                cmd.Parameters.AddWithValue("@FactoryId", factoryId);
                cmd.Parameters.AddWithValue("@LineModuleId", lineId);
                cmd.Parameters.AddWithValue("@OrderDetailId", orderId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<IdNameDTO> GetOrdersInBoard(DateTime date, int factoryId, int lineId)
        {
            SqlCommand cmd = new SqlCommand("GetOrdersForLineModuleInBoard");
            cmd.Parameters.AddWithValue("@Date", date);
            cmd.Parameters.AddWithValue("@FactoryId", factoryId);
            cmd.Parameters.AddWithValue("@LineModuleId", lineId);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            
        }

        internal List<IdNameDTO> GetProductsInBoard(DateTime date, int factoryId, int lineId,int orderId)
        {
            SqlCommand cmd = new SqlCommand("GetProductDetailsForLineModuleInBoard");
            cmd.Parameters.AddWithValue("@Date", date);
            cmd.Parameters.AddWithValue("@FactoryId", factoryId);
            cmd.Parameters.AddWithValue("@LineModuleId", lineId);
            cmd.Parameters.AddWithValue("@OrderId", orderId);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            
        }

        internal List<IdNameDTO> GetLinesInBoard(DateTime date, int factoryId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetLineModulesForFactoryInBaord");
                cmd.Parameters.AddWithValue("@Date", date);
                cmd.Parameters.AddWithValue("@FactoryId", factoryId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return IdNameDTO.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<Productivity> ListProductivities()
        {
            SqlCommand sqlCommand = new SqlCommand(@"select p.Id,p.WorkingDate,p.FactoryId,p.LineModuleId,p.OrderId,p.StyleId,p.ProductId , p.Attendance,p.CreatedById,p.CreatedOn ,p.UpdatedById,p.UpdatedOn ,ISNULL(l.qty,0) ProducedQty
                                                    from ProductivityDetails p WITH(NOLOCK)
                                                    left join ( select ProductivityId, SUM(qty) qty
			                                                     from LineOutputs with(nolock)
			                                                     group by ProductivityId )l ON p.Id = l.ProductivityId 
                                                    ORDER BY WorkingDate");
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal bool Add(Productivity productivity)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO ProductivityDetails
                                            (WorkingDate, FactoryId, LineModuleId, OrderId, StyleId, Attendance,  CreatedById, CreatedOn,ProductId)
                                            VALUES (@WorkingDate, @FactoryId, @LineModuleId, @OrderId, @StyleId, @Attendance,@CreatedById, @CreatedOn,@ProductId)");
            this.SetParameters(command, productivity);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                productivity.SetId(ret);
                
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal void AddLineOutput(LineOutput output,Productivity productivity)
        {
            try
            {
                SqlCommand command = new SqlCommand("Delete LineOutputs where ProductivityId=@ProductivityId AND FromTime=@FromTime and ToTime=@ToTime");
                command.Parameters.AddWithValue("@ProductivityId", output.ProductivityId);
                command.Parameters.AddWithValue("@FromTime", output.FromTime);
                command.Parameters.AddWithValue("@ToTime", output.ToTime);

                _dataAccess.BeginTransaction();

                _dataAccess.ExecuteScalar(command);

                command.CommandText = @"INSERT INTO LineOutputs
                                                    (ProductivityId, FromTime, ToTime, Qty)
                                                VALUES (@ProductivityId, @FromTime, @ToTime, @Qty)";

                command.Parameters.AddWithValue("@Qty", output.ProducedQty.HasValue ? output.ProducedQty.Value : 0);

                _dataAccess.ExecuteNonQuery(command);

                command.Parameters.Clear();

                command.CommandText = @"Update ProductivityDetails 
                                    SET ProducedQty=(select SUM(qty)
                                                     from LineOutputs with(nolock)
                                                     where ProductivityId=@ProductivityId)
                                    Where Id=@ProductivityId";

                command.Parameters.AddWithValue("@ProductivityId", output.ProductivityId);

                _dataAccess.ExecuteNonQuery(command);

                StyleProduct styleProduct = new StyleProductDA().GetByStyleAndProductId(productivity.StyleId, productivity.ProductId);
                if(styleProduct!=null)
                {
                    command.CommandText = "Update LineModules set LastStyleProductId=@StyleProductId where Id=@LineId";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@StyleProductId", styleProduct.Id);
                    command.Parameters.AddWithValue("@LineId", productivity.LineModuleId);

                    _dataAccess.ExecuteNonQuery(command);
                }

                
                if (productivity != null)
                {
                    command = GetResetPlacedOrderCommand(productivity);

                    _dataAccess.ExecuteNonQuery(command);
                }
                _dataAccess.CommitTransaction();
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }

        }

        private SqlCommand GetResetPlacedOrderCommand(Productivity productivity)
        {
            SqlCommand command = new SqlCommand("ResetPlacedOrdersForLine");
            
            command.Parameters.AddWithValue("@OrderId", productivity.OrderId);
            command.Parameters.AddWithValue("@ProductId", productivity.ProductId);
            command.Parameters.AddWithValue("@LineId", productivity.LineModuleId);
            command.Parameters.AddWithValue("@FactoryId", productivity.FactoryId);

            command.CommandType = System.Data.CommandType.StoredProcedure;
            return command;
        }

        private void SetParameters(SqlCommand command, Productivity productivity)
        {
            command.Parameters.AddWithValue("@WorkingDate", productivity.WorkingDate);
            command.Parameters.AddWithValue("@FactoryId", productivity.FactoryId);
            command.Parameters.AddWithValue("@LineModuleId", productivity.LineModuleId);
            command.Parameters.AddWithValue("@OrderId", productivity.OrderId);
            command.Parameters.AddWithValue("@StyleId", productivity.StyleId);
            command.Parameters.AddWithValue("@Attendance", productivity.Attendance);
            command.Parameters.AddWithValue("@ProductId", productivity.ProductId);
        }

        internal bool Update(Productivity productivity)
        {
            SqlCommand command = new SqlCommand(@"UPDATE    ProductivityDetails
                                                SET Attendance =@Attendance ,  UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn 
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", productivity.Id);
            command.Parameters.AddWithValue("@Attendance", productivity.Attendance);
            //command.Parameters.AddWithValue("@ProducedQty", productivity.ProducedQty);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool IsExists(int id, int factoryId, int lineModuleId, int orderDetailId, DateTime workingDate,int productId)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM productivitydetails WITH(NOLOCK) 
                                                 WHERE Id<>@Id AND FactoryId=@FactoryId AND LineModuleId=@LineModuleId AND OrderId=@OrderId and Cast(workingDate as Date) = Cast(@WorkingDate as Date) and ProductId=@ProductId");
            command.Parameters.AddWithValue("@Id", id);
            command.Parameters.AddWithValue("@FactoryId", factoryId);
            command.Parameters.AddWithValue("@LineModuleId", lineModuleId);
            command.Parameters.AddWithValue("@OrderId", orderDetailId);
            command.Parameters.AddWithValue("@WorkingDate", workingDate);
            command.Parameters.AddWithValue("@ProductId", productId);
            return _dataAccess.ExecuteScalar(command) != null;
        }

        internal int GetProductivityId(DateTime date, int factoryId, int lineId, int orderId, int productId)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM productivitydetails WITH(NOLOCK) 
                                                 WHERE FactoryId=@FactoryId AND LineModuleId=@LineModuleId AND OrderId=@OrderId and Cast(workingDate as Date) = Cast(@WorkingDate as Date) and ProductId=@ProductId");

            command.Parameters.AddWithValue("@FactoryId", factoryId);
            command.Parameters.AddWithValue("@LineModuleId", lineId);
            command.Parameters.AddWithValue("@OrderId", orderId);
            command.Parameters.AddWithValue("@WorkingDate", date);
            command.Parameters.AddWithValue("@ProductId", productId);
            object ret = _dataAccess.ExecuteScalar(command);
            return ret == null ? 0 : Convert.ToInt32(ret);
        }

        internal List<Productivity> ListForSFactoryEfficiency(int factoryId, int lineModuleId, int styleDetailId, DateTime? fromDate = null, DateTime? toDate = null)
        {
            string whereCondition = factoryId > 0 ? " FactoryId = @FactoryId" : string.Empty;

            if (lineModuleId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " LineModuleId = @LineModuleId" : " AND LineModuleId = @LineModuleId";
            }

            if (styleDetailId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " StyleId = @StyleId" : " AND StyleId = @StyleId";
            }

            if (fromDate !=null && toDate != null)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " Cast(workingDate as Date) >= Cast(@FromDate as Date) AND Cast(workingDate as Date) <= Cast(@ToDate as Date)" : " AND Cast(workingDate as Date) >= Cast(@FromDate as Date) AND Cast(workingDate as Date) <= Cast(@ToDate as Date)";
            }

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " WHERE " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"Select * From ProductivityDetails {whereCondition} ORDER BY WorkingDate");

            if (factoryId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            }

            if (lineModuleId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@LineModuleId", lineModuleId);
            }

            if (styleDetailId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@StyleId", styleDetailId);
            }

            if (fromDate != null && toDate != null)
            {
                sqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
                sqlCommand.Parameters.AddWithValue("@ToDate", toDate);
            }

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal double GetPlantEffciencyForAMonth(int factoryId, DateTime fromDate, DateTime toDate)
        {
            double factoryEfficiency = 0;

            SqlCommand sqlCommand = new SqlCommand(@"select isnull((sum (p.ProducedQty * sp.ProductionSMV / (p.Attendance * l.AvailableMinutes))/count(*) ),0) from ProductivityDetails p WITH(NOLOCK)
            INNER JOIN OrderDetails o WITH(NOLOCK) ON p.OrderId = o.Id
            INNER JOIN StyleProducts sp WITH(NOLOCK) ON p.StyleId = sp.StyleId and p.OrderId = o.Id and p.ProductId = sp.ProductId
            INNER JOIN LineModules l WITH(NOLOCK) ON l.Id = p.LineModuleId
            WHERE Cast(p.WorkingDate as Date) >= Cast(@FromDate as Date) AND Cast(p.WorkingDate as Date) <= Cast(@ToDate as Date) and p.FactoryId = @FactoryId");

            

            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
            sqlCommand.Parameters.AddWithValue("@ToDate", toDate);

            try
            {
                object ret = _dataAccess.ExecuteScalar(sqlCommand);
                if (ret == null)
                    factoryEfficiency = 0;
                else
                    factoryEfficiency = Convert.ToDouble(ret)*100;
                
            }
            catch (Exception ex)
            {
                return 0;
            }


            return Math.Round(factoryEfficiency,2);
        }

        internal void DeleteProductivity(Productivity productivity)
        {
            try
            {
                _dataAccess.BeginTransaction();
                SqlCommand command = new SqlCommand("DELETE productivitydetails WHERE Id=@Id");
                command.Parameters.AddWithValue("@Id", productivity.Id);
                _dataAccess.ExecuteNonQuery(command);

                if (productivity != null)
                {
                   command = GetResetPlacedOrderCommand(productivity);

                    _dataAccess.ExecuteNonQuery(command);
                }
                _dataAccess.CommitTransaction();
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool HasProductivityAddedAfterDate(DateTime date,int orderId,int productId)
        {
            SqlCommand sqlCommand = new SqlCommand(@"SELECT * FROM ProductivityDetails WITH(NOLOCK) 
                                                     WHERE WorkingDate > @Date and OrderId=@OrderId and ProductId = @ProductId ");

            sqlCommand.Parameters.AddWithValue("@Date", date);
            sqlCommand.Parameters.AddWithValue("@OrderId", orderId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);

            return _dataAccess.IsExists(sqlCommand);
        }

        internal DataRow GetPlannedQty(int orderId, int productId, int lineModuleId, int factoryId, double duration)
        {
            SqlCommand sqlCommand = new SqlCommand("GetPlannedQty");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@OrderDetailId", orderId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.Parameters.AddWithValue("@LineModuleId", lineModuleId);
            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@Duration", duration);

            DataTable dt = _dataAccess.GetDataTable(sqlCommand);

            return dt?.Rows.Count > 0 ? dt.Rows[0] : new DataTable().NewRow();
        }

        internal int GetTotalProducedForOrderId(int orderDetailId, int productId)
        {
            SqlCommand command = new SqlCommand(@"SELECT SUM(ProducedQty) FROM ProductivityDetails WITH(NOLOCK) WHERE OrderId=@OrderId and ProductId=@ProductId");
            command.Parameters.AddWithValue("@OrderId", orderDetailId);
            command.Parameters.AddWithValue("@ProductId", productId);

            object producedTotal = _dataAccess.ExecuteScalar(command);
            return Convert.ToInt32(producedTotal);
        }

        internal int GetTotalProducedForFactoryId(DateTime workingDate, int factoryId)
        {
            DateTime firstTheMonth = new DateTime(workingDate.Year, workingDate.Month, 1);

            SqlCommand command = new SqlCommand(@"SELECT SUM(ProducedQty) FROM ProductivityDetails WITH(NOLOCK) WHERE FactoryId = @FactoryId AND WorkingDate <= @WorkingDate AND WorkingDate >= @FirstWorkingDate");
            command.Parameters.AddWithValue("@FactoryId", factoryId);
            command.Parameters.AddWithValue("@WorkingDate", workingDate);
            command.Parameters.AddWithValue("@FirstWorkingDate", firstTheMonth);

            object producedTotal = _dataAccess.ExecuteScalar(command);
            return Convert.ToInt32(producedTotal);
        }

        internal List<ReportDTOProductivity> GetProductivityForReport(DateTime workingDate, int factoryId)
        {
            SqlCommand cmd = new SqlCommand("GetProductivityForReport");
            cmd.Parameters.AddWithValue("@WorkingDate", workingDate);
            cmd.Parameters.AddWithValue("@FactoryId", factoryId);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            return ReportDTOProductivity.Load(_dataAccess.GetDataTable(cmd));

        }

        internal DateTime GetPreviousWorkingDay(DateTime workingDate)
        {
            SqlCommand cmd = new SqlCommand(@"SELECT TOP(1) WorkingDate FROM ProductivityDetails where WorkingDate<@WorkingDate order by WorkingDate desc");
            cmd.Parameters.AddWithValue("@WorkingDate", workingDate);

            object ret = _dataAccess.ExecuteScalar(cmd);
            return Convert.ToDateTime(ret);


        }
    }

    internal class LineOutputDA : EntityDataAccessBase<LineOutput>
    {
        private DataAccess _dataAccess = new DataAccess();
        internal List<LineOutput> GetLineOutputs(int factoryId, int lineId, DateTime workingDate, int productivityId = 0)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetLineOutputs");
                cmd.Parameters.AddWithValue("@FactoryId", factoryId);
                cmd.Parameters.AddWithValue("@LineId", lineId);
                cmd.Parameters.AddWithValue("@WorkingDate", workingDate);
                cmd.Parameters.AddWithValue("@ProductivityId", productivityId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return this.GetList(cmd);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        
    }
}
