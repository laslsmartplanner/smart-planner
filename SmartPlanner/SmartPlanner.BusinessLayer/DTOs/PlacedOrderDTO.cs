﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class PlacedOrderDTO
    {
        public List<PlacedOrder> PlacedOrders { get; set; }
        public List<ProductivityDTO> Productivities { get; set; }
    }
}
