﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class OrderDetailDTO
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double Duration { get; set; }
        public double Holidays { get; set; }
        public string SalesOrderNumber { get; set; }
        public string BuyerName { get; set; }
        public string StyleNumber { get; set; }
        public string PlannedColor { get; set; }
        public string ActualColor { get; set; }
        public string ChangedColor { get; set; }
        public string ProductName { get; set; }

        public string StartDateString { get { return this.StartDate.HasValue ? this.StartDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }
        public string EndDateString { get { return this.EndDate.HasValue ? this.EndDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public string ShipmentDateString { get { return this.ShipmentDate.HasValue ? this.ShipmentDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public int OrderQty { get; set; }

        public string CustomerPONumber { get; set; }

        public decimal ProductionSMV { get; set; }

        public DateTime? ShipmentDate { get; set; }

        public int StyleProductId { get; set; }
        public double PlanEfficiency { get; set; }
        public int IsInquiry { get; set; }

        internal static List<OrderDetailDTO> Load(DataTable dt)
        {
            List<OrderDetailDTO> list = new List<OrderDetailDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach(DataRow row in dt.Rows)
            {
                //if(row["StartDate"] == DBNull.Value)
                //{
                //    continue;
                //}

                list.Add(new OrderDetailDTO()
                {
                    OrderId = Convert.ToInt32(row["OrderId"]),
                    ProductId = Convert.ToInt32(row["ProductId"]),
                    StartDate = Extensions.GetDateTime(row["StartDate"]),
                    EndDate = Extensions.GetDateTime(row["EndDate"]),
                    Duration = Convert.ToDouble(row["Duration"]),
                    Holidays = Convert.ToDouble(row["Holidays"]),
                    SalesOrderNumber = Convert.ToString(row["SalesOrderNumber"]),
                    BuyerName = Convert.ToString(row["Buyer"]),
                    StyleNumber = Convert.ToString(row["StyleNumber"]),
                    PlannedColor = Convert.ToString(row["PlannedColor"]),
                    ActualColor = Convert.ToString(row["ActualColor"]),
                    ChangedColor = Convert.ToString(row["ChangedColor"]),
                    ProductName = Convert.ToString(row["Product"]),
                    OrderQty = Convert.ToInt32(row["OrderQty"]),
                    CustomerPONumber = Convert.ToString(row["CustomerPONumber"]),
                    ProductionSMV = Convert.ToDecimal(row["ProductionSMV"]),
                    ShipmentDate = GetNullableDateValue(row, "ShipmentDate"),
                    StyleProductId = Convert.ToInt32(row["StyleProductId"]),
                    PlanEfficiency = Convert.ToDouble(row["PlanEfficiency"]),
                    IsInquiry = Convert.ToInt32(row["IsInquiry"])
                });
            }
            return list;
        }
        private static DateTime? GetNullableDateValue(DataRow row, string columnName)
        {
            if (row[columnName] != DBNull.Value)
                return Convert.ToDateTime(row[columnName]);
            return null;
        }
    }

    internal class OrderDetailDTODA
    {
        internal List<OrderDetailDTO> GetOrderStartEnd(int factoryId,int styleId,int productId)
        {

            SqlCommand sqlCommand = new SqlCommand("GetOrderStartEnd");
            //sqlCommand.Parameters.AddWithValue("@From", fromDate);
            //sqlCommand.Parameters.AddWithValue("@To", toDate);
            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            return OrderDetailDTO.Load(new DataAccess().GetDataTable(sqlCommand));
        }

        internal List<IdNameDTO> GetStylesForAvailableOrders(int factoryId)
        {

            SqlCommand sqlCommand = new SqlCommand("GetOrderStartEnd");

            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@ReturnStyles", true);

            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            return IdNameDTO.Load(new DataAccess().GetDataTable(sqlCommand));
        }

        internal List<IdNameDTO> GetProductsForAvailableStyle(int factoryId,int styleId)
        {

            SqlCommand sqlCommand = new SqlCommand("GetOrderStartEnd");

            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            sqlCommand.Parameters.AddWithValue("@ReturnProducts", true);

            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            return IdNameDTO.Load(new DataAccess().GetDataTable(sqlCommand));
        }
    }
}
