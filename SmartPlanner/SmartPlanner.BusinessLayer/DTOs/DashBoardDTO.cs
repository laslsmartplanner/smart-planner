﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class DashBoardDTO
    {
        public int FactoryNameId { get; set; }
        public List<Factory> FactoryNames { get; set; }

        public List<string> LineEfficiencyChartLabelPoint { get; set; }
        public List<double> LineEfficiencyChartData { get; set; }

        public double FactoryEfficiency { get; set; }
        public decimal OnTimeDelivery { get; set; }
        public double CutToShipRatio { get; set; }

    }
}
