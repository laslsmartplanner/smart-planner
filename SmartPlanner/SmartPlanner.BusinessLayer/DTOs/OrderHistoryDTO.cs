﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class OrderHistoryDTO
    {
        [Display(Name ="From Line")]
        public string FromLine { get; set; }
        [Display(Name = "To Line")]
        public string ToLine { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        [Display(Name = "Changed By")]
        public string ChangedBy { get; set; }
        public DateTime ChangedOn { get; set; }
        [Display(Name = "Start Date From")]
        public string FromDateString
        {
            get
            {
                return this.FromDate.ToString("dd/MMM/yyyy");
            }
        }
        [Display(Name = "Start Date To")]
        public string ToDateString
        {
            get
            {
                return this.ToDate.ToString("dd/MMM/yyyy");
            }
        }
        [Display(Name = "Changed On")]
        public string ChangedOnString
        {
            get
            {
                return this.ChangedOn.ToString("dd/MMM/yyyy HH:mm:ss");
            }
        }
        public string SalesOrderNumber { get; set; }

        internal static List<OrderHistoryDTO> Load(DataTable dt)
        {
            List<OrderHistoryDTO> list = new List<OrderHistoryDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new OrderHistoryDTO()
                {
                    FromLine = Convert.ToString(row["FromLine"]),
                    ToLine = Convert.ToString(row["ToLine"]),
                    FromDate = Convert.ToDateTime (row["FromDate"]),
                    ToDate = Convert.ToDateTime(row["ToDate"]),
                    ChangedBy = Convert.ToString(row["ChangedBy"]),
                    ChangedOn = Convert.ToDateTime(row["ChangedOn"]),
                    SalesOrderNumber = Convert.ToString(row["SalesOrderNumber"])
                });
            }
            return list;
        }
    }
    internal class OrderHistoryDTODA
    {
        internal List<OrderHistoryDTO> GetOrderHistory(int orderId,int productId)
        {

            SqlCommand sqlCommand = new SqlCommand("GetOrderHistory");
            sqlCommand.Parameters.AddWithValue("@OrderId", orderId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            return OrderHistoryDTO.Load(new DataAccess().GetDataTable(sqlCommand));
        }
    }
}
