﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class TimeBoardDTO
    {
        public int FactoryNameId { get; set; }
        public List<string> Columns { get; set; }
        public List<LineModule> LineModules { get; set; }
        public List<Factory> FactoryNames { get; set; }
        public List<OrderDetailDTO> OrderDetails { get; set; }
        public List<OrderHistoryDTO> OrderHistory { get { return new List<OrderHistoryDTO>(); } }
        public List<StyleHistoryDTO> StyleHistory { get { return new List<StyleHistoryDTO>(); } }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<IdNameDTO> StyleNumbers { get; set; }
        public List<IdNameDTO> Products { get; set; }
        public int StyleId { get; set; }
        public int ProductId { get; set; }
        public List<LineModule> HLineModules { get; set; }
    }
}
