﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class OrderAndStyleHistoryDTO
    {
        public List<OrderHistoryDTO> OrderHistory { get; set; }
        public List<StyleHistoryDTO> StyleHistory { get; set; }
        public OrderAndStyleHistoryDTO(List<OrderHistoryDTO> orderHistory, List<StyleHistoryDTO> styleHistory)
        {
            this.OrderHistory = orderHistory;
            this.StyleHistory = styleHistory;
        }
    }
}
