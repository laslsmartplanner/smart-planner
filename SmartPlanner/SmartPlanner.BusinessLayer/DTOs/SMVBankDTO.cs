﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class SMVBankDTO
    {
        [Display(Name = "Style Number")]
        public int StyleDetailId { get; set; }

        [Display(Name = "Buyer")]
        public int BuyerId { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Product Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Product Sub Category")]
        public int SubCategoryId { get; set; }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        public List<StyleDetail> StyleDetails { get; set; }

        public List<Buyer> Buyers { get; set; }

        public List<Customer> Customers { get; set; }

        public List<ConfigurationTableValue> Categories { get; set; }

        public List<SubCategory> SubCategories { get; set; }

        public List<StyleProduct> StyleProducts { get; set; }
    }
}
