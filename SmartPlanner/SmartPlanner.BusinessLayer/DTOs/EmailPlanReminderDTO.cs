﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class EmailPlanReminderDTO
    {
        
        public string User { get; set; }
        public string OrdersList { get; set; }
        
    }
}
