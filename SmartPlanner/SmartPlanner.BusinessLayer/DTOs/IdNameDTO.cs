﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class IdNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        internal static List<IdNameDTO> Load(DataTable dt, bool addEmpty = false)
        {
            List<IdNameDTO> list = new List<IdNameDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            if (addEmpty)
                list.Add(new IdNameDTO() { Id = 0, Name = "Select One..." });
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new IdNameDTO()
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Name = Convert.ToString(row["Name"])
                });
            }
            return list;
        }
    }
}
