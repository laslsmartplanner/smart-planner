﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class ReportDTOPlacedOrders
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();
        private LineModuleService _lineModuleService = new LineModuleService();
        private OrderDetailService _orderService = new OrderDetailService();
        private StyleDetailService _styleService = new StyleDetailService();

        private Factory _factory = null;
        private LineModule _lineModule = null;
        private OrderDetail _order = null;
        private StyleDetail _style = null;
        private ConfigurationTableValue _product = null;

        public string CombinationColor { get; set; }

        [Display(Name = "Date")]
        public DateTime? WorkingDate { get; set; }

        [Display(Name = "Date")]
        public string WorkingDateText
        {
            get
            {
                if (this.WorkingDate.HasValue)
                    return this.WorkingDate.Value.ToString(GV.DisplayDateFormat);
                else
                    return string.Empty;
            }
        }

        [Display(Name = "Factory")]
        public int FactoryId { get; set; }

        public string FactoryName { get { return this.Factory?.FactoryName ?? string.Empty; } }

        [Display(Name = "Line Module")]
        public int LineModuleId { get; set; }

        [Display(Name = "Line Module")]
        public string ModuleNumber
        {
            get
            {
                return this.LineModule?.ModuleNumber ?? string.Empty;
            }
        }

        [Display(Name = "Sales Order Number")]
        public int OrderId { get; set; }

        [Display(Name = "Customer PO Number")]
        public string CustomerPONumber
        {
            get
            {
                return this.Order?.CustomerPONumber ?? string.Empty;
            }
        }

        [Display(Name = "Customer")]
        public string CustomerName
        {
            get
            {
                return this.Order ?.Customer.Name ?? string.Empty;
            }
        }

        [Display(Name = "Style Number")]
        public int StyleId { get; set; }

        public string StyleNumber
        {
            get
            {
                return this.Style?.StyleNumber ?? string.Empty;
            }
        }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        public string ProductName
        {
            get
            {
                return this.Product?.ConfigValue ?? string.Empty;
            }
        }

        [Display(Name = "Order Quantity")]
        public int OrderQty
        {
            get
            {
                return this.Order.OrderQty;
            }
        }

        [Display(Name = "Production SMV")]
        public double ProductionSMV
        {
            get; set;
        }

        [Display(Name = "Season")]
        public string SeasonName
        {
            get
            {
                return this.Order.PlanningSeason.ConfigValue;
            }
        }

        [Display(Name = "Delivery Contract Number")]
        public string DeliveryContractNumber {
            get
            {
                return this.Order.DeliveryContractNumber;
            }
        }

        [Display(Name = "Shipment Date")]
        public string ShipmentDateText {
            get
            {
                return (this.Order.ShipmentDate.HasValue) ? this.Order.ShipmentDate.Value.ToShortDateString() : string.Empty;
            }
        }

        [Display(Name = "Planned Production")]
        public double PlannedQty { get; set; }

        [Display(Name = "CM")]
        public decimal ProductCM { get; set; }


        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = _configValueService.GetEntityById(this.ProductId);
                return this._product;
            }
        }

        public Factory Factory
        {
            get
            {
                if (this._factory == null && this.FactoryId > 0)
                    this._factory = new FactoryService().GetEntityById(this.FactoryId);
                return this._factory;
            }
        }

        public LineModule LineModule
        {
            get
            {
                if (this._lineModule == null && this.LineModuleId > 0)
                    this._lineModule = _lineModuleService.GetEntityById(this.LineModuleId);
                return this._lineModule;
            }
        }

        public OrderDetail Order
        {
            get
            {
                if (this._order == null && this.OrderId > 0)
                    this._order = _orderService.GetEntityById(this.OrderId);
                return this._order;
            }
        }

        public StyleDetail Style
        {
            get
            {
                if (this._style == null && this.StyleId > 0)
                    this._style = _styleService.GetEntityById(this.StyleId);
                return this._style;
            }
        }

        public string PlanCuttingDate { get; internal set; }
    }
}
