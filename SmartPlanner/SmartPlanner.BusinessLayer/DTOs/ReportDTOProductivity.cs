﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class ReportDTOProductivity
    {
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();
        private LineModuleService _lineModuleService = new LineModuleService();
        private OrderDetailService _orderService = new OrderDetailService();
        private StyleDetailService _styleService = new StyleDetailService();

        private LineModule _lineModule = null;
        private OrderDetail _order = null;
        private StyleDetail _style = null;
        private ConfigurationTableValue _product = null;

        [Display(Name = "Date")]
        public DateTime? WorkingDate { get; set; }

        [Display(Name = "Factory")]
        public int FactoryId { get; set; }

        [Display(Name = "Line Module")]
        public int LineModuleId { get; set; }

        [Display(Name = "Line Module")]
        public string ModuleNumber
        {
            get
            {
                return this.LineModule?.ModuleNumber ?? string.Empty;
            }
        }

        [Display(Name = "Order Ref.")]
        public int OrderId { get; set; }

        [Display(Name = "Production SMV")]
        public double ProductionSMV
        {
            get; set;
        }

        [Display(Name = "No. of days")]
        public int NoOfDays { get; set; }

        [Display(Name = "Day Efficiency")]
        public double DayEfficiency { get; set; }

        [Display(Name = "CM Produced")]
        public decimal CMProduced { get; set; }

        [Display(Name = "Target")]
        public int TargetQty { get; set; }

        [Display(Name = "Produced Qty")]
        public double ProducedQty { get; set; }

        [Display(Name = "Total OPR")]
        public int Attendance { get; set; }

        [Display(Name = "Min Work")]
        public int AvailableMin { get; set; }

        [Display(Name = "Total in Line")]
        public decimal PlanEmployees { get; set; }

        [Display(Name = "Style Number")]
        public int StyleId { get; set; }

        public string StyleNumber
        {
            get
            {
                return this.Style?.StyleNumber ?? string.Empty;
            }
        }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        public LineModule LineModule
        {
            get
            {
                if (this._lineModule == null && this.LineModuleId > 0)
                    this._lineModule = _lineModuleService.GetEntityById(this.LineModuleId);
                return this._lineModule;
            }
        }

        public OrderDetail Order
        {
            get
            {
                if (this._order == null && this.OrderId > 0)
                    this._order = _orderService.GetEntityById(this.OrderId);
                return this._order;
            }
        }

        public StyleDetail Style
        {
            get
            {
                if (this._style == null && this.StyleId > 0)
                    this._style = _styleService.GetEntityById(this.StyleId);
                return this._style;
            }
        }

        internal static List<ReportDTOProductivity> Load(DataTable dt)
        {
            List<ReportDTOProductivity> list = new List<ReportDTOProductivity>();

            if (dt == null || dt.Rows.Count == 0) return list;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ReportDTOProductivity()
                {
                    WorkingDate = GetNullableDateValue(row, "WorkingDate"),
                    FactoryId = Convert.ToInt32(row["FactoryId"]),
                    LineModuleId = Convert.ToInt32(row["LineModuleId"]),
                    OrderId = Convert.ToInt32(row["OrderId"]),
                    StyleId = Convert.ToInt32(row["StyleId"]),
                    ProductId = Convert.ToInt32(row["ProductId"]),
                    ProductionSMV = Convert.ToDouble(row["ProductionSMV"]),
                    NoOfDays = Convert.ToInt32(row["NoOfDays"]),
                    DayEfficiency = Convert.ToDouble(row["DayEfficiency"]),
                    CMProduced = Convert.ToDecimal(row["CMProduced"]),
                    TargetQty = Convert.ToInt32(row["TargetQty"]),
                    ProducedQty = Convert.ToDouble(row["ProducedQty"]),
                    Attendance = Convert.ToInt32(row["Attendance"]),
                    AvailableMin = Convert.ToInt32(row["AvailableMin"]),
                    PlanEmployees = Convert.ToInt32(row["PlanEmployees"])
                });
            }
            return list;
        }

        private static DateTime? GetNullableDateValue(DataRow row, string columnName)
        {
            if (row[columnName] != DBNull.Value)
                return Convert.ToDateTime(row[columnName]);
            return null;
        }

    }
}
