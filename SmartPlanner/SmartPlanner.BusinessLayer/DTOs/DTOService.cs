﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class DTOService
    {
        public List<OrderDetailDTO> GetOrderStartEnd(int factoryId, int styleId = 0, int productId = 0)
        {
            return new OrderDetailDTODA().GetOrderStartEnd(factoryId, styleId, productId);
        }
        public List<StyleHistoryDTO> GetStyleHistory(int styleId,int productId)
        {
            return new StyleHistoryDTODA().GetStyleHistory(styleId, productId);
        }
        public List<OrderHistoryDTO> GetOrderHistory(int orderId,int productId)
        {
            return new OrderHistoryDTODA().GetOrderHistory(orderId, productId);
        }
        public List<TnADetailsDTO> ListTnADetailsForLoggedUser(DateTime date)
        {
            return new TnADetaislDTODA().ListForLoggedUser(date);
        }
        public List<IdNameDTO> GetOrders()
        {
            return new OrderDetailDA().GetOrdersIdNameDTO();
        }
        public List<IdNameDTO> GetProductsIdNameDTO()
        {
            return new OrderDetailDA().GetProductsIdNameDTO();
        }
        public List<IdNameDTO> GetStylesForAvailableOrders(int factoryId)
        {
            return new OrderDetailDTODA().GetStylesForAvailableOrders(factoryId);
        }
        public List<IdNameDTO> GetProductsForAvailableStyle(int factoryId, int styleId)
        {
            return new OrderDetailDTODA().GetProductsForAvailableStyle(factoryId, styleId);
        }
    }
}
