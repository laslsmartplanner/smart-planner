﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class ProductionCostSMVDTO
    {

        [Display(Name = "Production SMV")]
        public decimal ProductionSMV { get; set; }

        [Display(Name = "Cost SMV")]
        public decimal CostSMV { get; set; }

        [Display(Name = "CM")]
        public decimal CM { get; set; }

        internal static List<ProductionCostSMVDTO> Load(DataTable dt)
        {
            List<ProductionCostSMVDTO> list = new List<ProductionCostSMVDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ProductionCostSMVDTO()
                {
                    ProductionSMV = Convert.ToDecimal(row["ProductionSMV"]),
                    CostSMV = Convert.ToDecimal(row["CostSMV"]),
                    CM = Convert.ToDecimal(row["CM"])
                });
            }
            return list;
        }
    }
}
