﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class AdditionalFileDTO
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
