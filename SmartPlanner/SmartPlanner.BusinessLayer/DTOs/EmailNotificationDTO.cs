﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class EmailNotificationDTO
    {
        public string OrderNumber { get; set; }
        public string Buyer { get; set; }
        public string Style { get; set; }
        public string StyleDescription { get; set; }
        public string OrderQty { get; set; }
        public string Merchant { get; set; }
        public string ReceivedDate { get; set; }
        public string KeyProcess { get; set; }
        public string PlanStartDate { get; set; }
        public string PlanEndDate { get; set; }
        public string Status { get; set; }
        public string Planner { get; set; }
        public string User { get; set; }
        public string LineModule { get; set; }
        public string Holiday { get; set; }
    }
}
