﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class ProductivityDTO
    {
        public int Id { get; set; }
        public DateTime WorkingDate { get; set; }
        public int ColumnNumber { get; set; }
        public string ProducedQty { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int LineModuleId { get; set; }

        internal static List<ProductivityDTO> Load(DataTable dt)
        {
            List<ProductivityDTO> list = new List<ProductivityDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ProductivityDTO()
                {
                    Id = Convert.ToInt32(row["Id"]),
                    ColumnNumber = Convert.ToInt32(row["ColumnNumber"]),
                    WorkingDate = Convert.ToDateTime(row["WorkingDate"]),
                    ProducedQty = Convert.ToString(row["ProducedQty"]),
                    OrderId = Convert.ToInt32(row["OrderDetailId"]),
                    ProductId = Convert.ToInt32(row["ProductId"]),
                    LineModuleId = Convert.ToInt32(row["LineModuleId"])
                });
            }
            return list;
        }

        internal static List<ProductivityDTO> Load(DataRow[] rows)
        {
            List<ProductivityDTO> list = new List<ProductivityDTO>();
            if (rows == null || rows.Length == 0) return list;
            foreach (DataRow row in rows)
            {
                list.Add(new ProductivityDTO()
                {
                    Id = Convert.ToInt32(row["Id"]),
                    ColumnNumber = Convert.ToInt32(row["ColumnNumber"]),
                    WorkingDate = Convert.ToDateTime(row["WorkingDate"]),
                    ProducedQty = Convert.ToString(row["ProducedQty"]),
                    OrderId = Convert.ToInt32(row["OrderDetailId"]),
                    ProductId = Convert.ToInt32(row["ProductId"]),
                    LineModuleId = Convert.ToInt32(row["LineModuleId"])
                });
            }
            return list;
        }
    }
}
