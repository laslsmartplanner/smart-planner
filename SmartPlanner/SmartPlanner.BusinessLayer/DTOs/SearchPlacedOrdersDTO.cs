﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class SearchPlacedOrdersDTO
    {
        [Display(Name = "Factory Name")]
        public string FactoryName { get; set; }

        [Display(Name = "Line Module")]
        public string ModuleNumber { get; set; }

        [Display(Name = "Order Ref.")]
        public string SalesOrderNumber { get; set; }

        [Display(Name = "Style")]
        public string StyleNumber { get; set; }

        [Display(Name = "Product")]
        public string ProductName { get; set; }

        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Shipment Date")]
        public DateTime ShipmentDate { get; set; }

        [Display(Name = "Start Date")]
        public string StartDateString { get { return this.StartDate.ToString(GV.DisplayDateFormat); } }

        [Display(Name = "End Date")]
        public string EndDateString { get { return this.EndDate.ToString(GV.DisplayDateFormat); } }

        [Display(Name = "Shipment Date")]
        public string ShipmentDateString { get { return this.ShipmentDate.ToString(GV.DisplayDateFormat); } }

        internal static List<SearchPlacedOrdersDTO> Load(DataTable dt)
        {
            List<SearchPlacedOrdersDTO> list = new List<SearchPlacedOrdersDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SearchPlacedOrdersDTO()
                {
                    FactoryName = Convert.ToString(row["FactoryName"]),
                    ModuleNumber = Convert.ToString(row["ModuleNumber"]),
                    SalesOrderNumber = Convert.ToString(row["SalesOrderNumber"]),
                    StyleNumber = Convert.ToString(row["StyleNumber"]),
                    ProductName = Convert.ToString(row["ProductName"]),
                    StartDate = Convert.ToDateTime(row["StartDate"]),
                    EndDate = Convert.ToDateTime(row["EndDate"]),
                    ShipmentDate = Convert.ToDateTime(row["ShipmentDate"])
                });
            }
            return list;
        }
    }
}
