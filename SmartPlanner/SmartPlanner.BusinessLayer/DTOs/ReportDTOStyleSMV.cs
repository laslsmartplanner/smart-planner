﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class ReportDTOStyleSMV
    {

        private StyleDetailService _styleService = new StyleDetailService();
        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        private StyleDetail _style = null;
        private ConfigurationTableValue _product = null;

        [Display(Name = "Style Number")]
        public int StyleId { get; set; }

        public string StyleNumber
        {
            get
            {
                return this.Style?.StyleNumber ?? string.Empty;
            }
        }

        [Display(Name = "Production SMV")]
        public decimal ProductionSMV { get; set; }

        public StyleDetail Style
        {
            get
            {
                if (this._style == null && this.StyleId > 0)
                    this._style = _styleService.GetEntityById(this.StyleId);
                return this._style;
            }
        }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        public string ProductName
        {
            get
            {
                return this.Product?.ConfigValue ?? string.Empty;
            }
        }

        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = _configValueService.GetEntityById(this.ProductId);
                return this._product;
            }
        }

        internal static List<ReportDTOStyleSMV> Load(DataTable dt)
        {
            List<ReportDTOStyleSMV> list = new List<ReportDTOStyleSMV>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ReportDTOStyleSMV()
                {
                    StyleId = Convert.ToInt32(row["StyleId"]),
                    ProductionSMV = Convert.ToInt32(row["ProductionSMV"]),
                    ProductId = Convert.ToInt32(row["ProductId"])
                });
            }
            return list;
        }

    }
}
