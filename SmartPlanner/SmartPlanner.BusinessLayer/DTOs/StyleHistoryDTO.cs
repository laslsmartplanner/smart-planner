﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class StyleHistoryDTO
    {
        [Display(Name = "Style Numer")]
        public string StyleNumber { get; set; }
        public DateTime WorkingDate { get; set; }
        [Display(Name = "Produces Qty")]
        public double ProducedQty { get; set; }
        [Display(Name = "Sales Order Number")]
        public string SalesOrderNumber { get; set; }
        [Display(Name = "Module Number")]
        public string ModuleNumber { get; set; }
        [Display(Name = "Working Date")]
        public string WorkingDateString
        {
            get
            {
                return this.WorkingDate.ToString("dd/MMM/yyyy");
            }
        }

        internal static List<StyleHistoryDTO> Load(DataTable dt)
        {
            List<StyleHistoryDTO> list = new List<StyleHistoryDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new StyleHistoryDTO()
                {
                    StyleNumber = Convert.ToString(row["StyleNumber"]),
                    WorkingDate = Convert.ToDateTime(row["WorkingDate"]),
                    ProducedQty = Convert.ToDouble(row["ProducedQty"]),
                    SalesOrderNumber = Convert.ToString(row["SalesOrderNumber"]),
                    ModuleNumber = Convert.ToString(row["ModuleNumber"])
                });
            }
            return list;
        }
    }

    internal class StyleHistoryDTODA
    {
        internal List<StyleHistoryDTO> GetStyleHistory(int styleId,int productId)
        {

            SqlCommand sqlCommand = new SqlCommand("GetStyleHistory");
            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            return StyleHistoryDTO.Load(new DataAccess().GetDataTable(sqlCommand));
        }
    }
}
