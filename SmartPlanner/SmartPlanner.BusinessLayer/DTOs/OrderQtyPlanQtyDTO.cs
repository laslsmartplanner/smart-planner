﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OrderQtyPlanQtyDTO
    {
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Qty { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal PlanDeliveryQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal ActualCutQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal ShipQty { get; set; }

        public string ProductName { get; set; }

        internal static List<OrderQtyPlanQtyDTO> Load(DataTable dt)
        {
            List<OrderQtyPlanQtyDTO> list = new List<OrderQtyPlanQtyDTO>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new OrderQtyPlanQtyDTO()
                {
                    Qty = Convert.ToDecimal(row["Qty"]),
                    PlanDeliveryQty = Convert.ToDecimal(row["PlanDeliveryQty"]),
                    ActualCutQty = Convert.ToDecimal(row["ActualCutQty"]),
                    ShipQty = Convert.ToDecimal(row["ShipQty"]),
                    ProductName = Convert.ToString(row["ProductName"])
                });
            }
            return list;
        }
    }
}
