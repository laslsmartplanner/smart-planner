﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class TnADetailsDTO : EntityBase<TnADetailsDTO>
    {
        public override string TableName => "TnACalendarDetails";

        public int TnADefaultId { get; set; }

        [Display(Name = "Key Process")]
        public string KeyProcess { get; set; }

        public int OrderDetailsId { get; set; }

        [Display(Name = "Order Number")]
        public string SalesOrderNumber { get; set; }

        public int ProductId { get; set; }

        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        public int ResponsibilityId { get; set; }

        public string ResponsibilityName { get; set; }

        public DateTime StartDate { get; set; }

        [Display(Name = "Plan Date")]
        public string StartDateString { get { return this.StartDate.ToString(GV.DisplayDateFormat); } }

        public double Duration { get; set; }

        public DateTime? EndDate { get; set; }

        [Display(Name = "End Date")]
        public string EndDateString { get { return this.EndDate.HasValue ? this.EndDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public double Holidays { get; set; }

        public DateTime? ActualStartDate { get; set; }

        public DateTime? ActualEndDate { get; set; }

        public int FactoryNameId { get; set; }

        [Display(Name = "Factory")]
        public string FactoryName { get; set; }

        [Display(Name = "Buyer")]
        public string BuyerName { get; set; }

        [Display(Name = "Customer")]
        public string CustomerName { get; set; }

        [Display(Name = "Style")]
        public string StyleNumber { get; set; }

        [Display(Name = "Customer PO Number")]
        public string CustomerPONumber { get; set; }

        [Display(Name = "Order Qty")]
        public int OrderQty { get; set; }

        [Display(Name = "Repeat")]
        public bool Repeat { get; set; }

    }

    internal class TnADetaislDTODA : EntityDataAccessBase<TnADetailsDTO>
    {
        internal List<TnADetailsDTO> ListForLoggedUser(DateTime date)
        {
            SqlCommand sqlCommand = new SqlCommand(@"SELECT * 
                                                    FROM TnACalendarDetails with(nolock)
                                                    WHERE startdate<=@Date and responsibilityid=@User and Duration>0 and (ActualStartDate IS NULL OR ActualEndDate IS NULL) ORDER BY startdate");
            sqlCommand.Parameters.AddWithValue("@Date", date);
            sqlCommand.Parameters.AddWithValue("@User", GV.LoggedUser().Id);

            return new TnADetailsDTO().Load(new DataAccess().GetDataTable(sqlCommand));
        }
    }
}
