﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.DTOs
{
    public class ReportDTOFactoryEfficiency
    {
        [Display(Name = "Style Number")]
        public int StyleDetailId { get; set; }

        [Display(Name = "Factory")]
        public int FactoryId { get; set; }

        [Display(Name = "Line Module")]
        public int LineModuleId { get; set; }

        [Display(Name = "From Date")]
        public DateTime? FromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime? ToDate { get; set; }

        public List<StyleDetail> StyleDetails { get; set; }

        public List<Factory> Factories { get; set; }

        public List<LineModule> LineModules { get; set; }

    }
}
