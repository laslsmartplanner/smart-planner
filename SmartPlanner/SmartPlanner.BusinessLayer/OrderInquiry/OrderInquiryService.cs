﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class OrderInquiryService : ServiceBase<OrderInquiry>
    {
        private OrderInquiryDA _dataAccess = new OrderInquiryDA();
        private Email _email = new Email();
        public bool Save(OrderInquiry orderDetail)
        {
            if (!this.IsValidToSave(orderDetail)) return false;
            if (orderDetail.Id > 0)
            {
                OrderInquiry current = _dataAccess.GetEntityById(orderDetail.Id);

                //if (orderDetail.PlannerId > 0 && orderDetail.PlannerId == GV.LoggedUser().Id && current != null && current.ShipmentDate.HasValue && orderDetail.ShipmentDate.HasValue && current.ShipmentDate.Value != orderDetail.ShipmentDate.Value)
                if (orderDetail.PlannerId > 0 && orderDetail.PlannerId == GV.LoggedUser().Id)
                {
                    //orderDetail.RequestedDate = current.ShipmentDate;
                    orderDetail.ConfirmationStatusId = (int)EnumOrderInquiryConfirmationStatuses.PlannerConfirmed;
                }
                else
                {
                    if (orderDetail.MerchantId > 0 && GV.LoggedUser().Id == orderDetail.MerchantId)
                    {
                        orderDetail.ConfirmationStatusId = (int)EnumOrderInquiryConfirmationStatuses.PendingConfirmation;
                        orderDetail.RequestedDate = DateTime.Now;
                    }
                }

                _dataAccess.Update(orderDetail);
                if (orderDetail.Merchant != null && orderDetail.Planner != null && GV.LoggedUser().Id == orderDetail.Planner.Id)
                    _email.SendEmailNotification(new EmailNotificationDTO() { OrderNumber = orderDetail.SalesOrderNumber, Planner = orderDetail.Planner.FullName },
                                                (int)EnumEmailType.OrderInquiryUpdated, orderDetail.Merchant.Email);

                if (orderDetail.Merchant != null)
                {
                    new NotificationDA().Add(new Notification()
                    {
                        ToId = orderDetail.Merchant.Id,
                        Message = $"Order inquiry '{orderDetail.SalesOrderNumber}' has updated by {GV.LoggedUser().UserName}",
                    });
                }

                if (orderDetail.Merchant != null && orderDetail.Planner != null && GV.LoggedUser().Id == orderDetail.Merchant.Id && orderDetail.OrderInquiryStatusId == (int)EnumOrderInquiryStatus.Pending)
                    _email.SendEmailNotification(new EmailNotificationDTO() { OrderNumber = orderDetail.SalesOrderNumber, Planner = orderDetail.Merchant.FullName },
                                                (int)EnumEmailType.OrderInquiryUpdated, orderDetail.Planner.Email);
            }
            else
            {
                if (orderDetail.MerchantId > 0 && GV.LoggedUser().Id == orderDetail.MerchantId)
                {
                    orderDetail.ConfirmationStatusId = (int)EnumOrderInquiryConfirmationStatuses.PendingConfirmation;
                    orderDetail.RequestedDate = DateTime.Now;
                }


                _dataAccess.Add(orderDetail);
                if (orderDetail.Planner != null)
                {
                    _email.SendEmailNotification(new EmailNotificationDTO() { OrderNumber = orderDetail.SalesOrderNumber, Merchant = orderDetail.Merchant.FullName },
                                                (int)EnumEmailType.OrderInquiryCreated, orderDetail.Planner.Email);

                    new NotificationDA().Add(new Notification()
                    {
                        ToId = orderDetail.Planner.Id,
                        Message = $"Order inquiry '{orderDetail.SalesOrderNumber}' has created by {GV.LoggedUser().UserName}",
                    });
                }
            }
            if (orderDetail.IsSplitOrder)
                _dataAccess.SaveSplits(orderDetail);
            else
            {
                if (orderDetail.OrderInquiryStatusId == (int)EnumOrderInquiryStatus.Confirmed)
                {
                    OrderInquirySplit split = new OrderInquirySplit();
                    split.OrderInquiryId = orderDetail.Id;
                    split.SplitId = 1;
                    split.Qty = orderDetail.OrderQty;
                    split.ShipmentDate = orderDetail.ShipmentDate;

                    _dataAccess.AddSplit(split);

                    List<OrderDetail> orders = new OrderDetailDA().ListForOrderInquiryId(orderDetail.Id);
                    string orderNo = orderDetail.SalesOrderNumber;
                    if (!orders.Exists(x => x.SalesOrderNumber.Equals(orderNo)))
                        _dataAccess.AddOrder(orderDetail, orderNo, orderDetail.OrderQty, orderDetail.ShipmentDate, split.Id);
                }
            }
            if (orderDetail.OrderInquiryStatusId == (int)EnumOrderInquiryStatus.Confirmed || orderDetail.OrderInquiryStatusId == (int)EnumOrderInquiryStatus.NotConfirmed)
                _dataAccess.DeletePlacedOrderInquiries(orderDetail.Id);
            return true;
        }
        public List<OrderInquiry> GetList(string orderBy = "")
        {
            return _dataAccess.GetList(orderBy);
        }

        public List<OrderInquiry> ListForType(int type)
        {
            return _dataAccess.ListForType(type);
        }

        public bool UpdateSplit(OrderInquirySplit split)
        {
            _dataAccess.UpdateSplit(split);
            return true;
        }
    }
}
