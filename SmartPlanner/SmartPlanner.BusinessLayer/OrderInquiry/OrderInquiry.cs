﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class OrderInquiry : EntityBase<OrderInquiry>
    {
        private StyleDetail style = null;

        public override string TableName => "OrderInquiries";
        
        public override List<string> IsAlreadyExistsProperties => new List<string>() { "POId" };

        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        private ConfigurationTableValue _planningSeason = null;
        private ConfigurationTableValue _product = null;
        private ConfigurationTableValue _emblishmentPlant = null;
        private ConfigurationTableValue _washingPlant = null;
        private ConfigurationTableValue _printingPlant = null;
        private ConfigurationTableValue _sewingPlant = null;
        private ConfigurationTableValue _color = null;

        private Customer _customer = null;
        private Buyer _buyer = null;
        private OrderInquiryStatus _orderInquiryStatus = null;
        private LITUser _merchant;
        private LITUser _planner;

        #region Properties

        [Display(Name = "PO No.")]
        public string POId { get; set; }

        [Display(Name = "Order Reference")]
        public string SalesOrderNumber { get; set; }

        [Display(Name = "Order Confirmation Date")]
        public DateTime? OrderConfirmationDate { get; set; }

        [Display(Name = "Is Repeat Order?")]
        public bool Repeat { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Planning Season")]
        public int PlanningSeasonId { get; set; }

        [Display(Name = "Order Quantity")]
        public int OrderQty { get; set; }

        [Display(Name = "CM")]
        public decimal CM { get; set; }

        [Display(Name = "Shipment Date")]
        public DateTime? ShipmentDate { get; set; }

        [Display(Name = "Shipment Date")]
        public string ShipmentDateText { get; set; }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        [Display(Name = "Merchant")]
        public int MerchantId { get; set; }

        [Display(Name = "Buyer")]
        public int BuyerId { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Embellishment Plant")]
        public int EmblishmentPlantId { get; set; }

        [Display(Name = "Style")]
        public int StyleId { get; set; }

        [Display(Name = "Washing Plant")]
        public int WashingPlantId { get; set; }

        [Display(Name = "Printing Plant")]
        public int PrintingPlantId { get; set; }

        [Display(Name = "Sewing Plant")]
        public int SewingPlantId { get; set; }

        [Display(Name = "Order Status")]
        public int OrderInquiryStatusId { get; set; }

        [Display(Name = "Is Split Order?")]
        public bool IsSplitOrder { get; set; }

        [Display(Name = "Sewing Plant Required?")]
        public bool IsSewingPlant { get; set; }

        [Display(Name = "Embellishment Plant Required?")]
        public bool IsEmblishmentPlant { get; set; }

        [Display(Name = "Printing Plant Required?")]
        public bool IsPrintingPlant { get; set; }

        [Display(Name = "Washing Plant Required?")]
        public bool IsWashingPlant { get; set; }

        [Display(Name = "Planner")]
        public int PlannerId { get; set; }

        [Display(Name = "Color")]
        public int ColorId { get; set; }

        public List<OrderInquirySplit> Splits { get; set; }

        public OrderInquirySplit Split { get; set; }

        public OrderCombination SplitCombination { get; set; }

        [Display(Name = "Production SMV")]
        public decimal ProductionSMV { get; set; }

        public DateTime? RequestedDate { get; set; }

        [Display(Name ="Requested Date")]
        public string ReqeustedDateString { get { return this.RequestedDate.HasValue ? this.RequestedDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        [Display(Name = "Created Date")]
        public string CreatedDateString { get { return this.CreatedDate.HasValue ? this.CreatedDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        [Display(Name = "Order Confirmation Date")]
        public string OrderConfirmationDateString { get { return this.OrderConfirmationDate.HasValue ? this.OrderConfirmationDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public int ConfirmationStatusId { get; set; }

        [Display(Name = "Confirmation Status")]
        public string ConfirmationStatus
        {
            get
            {
                string sts = string.Empty;
                switch (this.ConfirmationStatusId)
                {
                    case (int)EnumOrderInquiryConfirmationStatuses.PlannerConfirmed:
                        sts = "Planner Confirmed";
                        break;
                    case (int)EnumOrderInquiryConfirmationStatuses.PendingConfirmation:
                        sts = "Pending Confirmation";
                        break;
                    default:
                        sts = "Pending Confirmation";
                        break;

                }
                return sts;
            }
        }

        #endregion

        public ConfigurationTableValue PlanningSeason
        {
            get
            {
                if (this._planningSeason == null && this.PlanningSeasonId > 0)
                    this._planningSeason = _configValueService.GetEntityById(this.PlanningSeasonId);
                return this._planningSeason;
            }
        }

        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = _configValueService.GetEntityById(this.ProductId);
                return this._product;
            }
        }
        
        public ConfigurationTableValue EmblishmentPlant
        {
            get
            {
                if (this._emblishmentPlant == null && this.EmblishmentPlantId > 0)
                    this._emblishmentPlant = _configValueService.GetEntityById(this.EmblishmentPlantId);
                return this._emblishmentPlant;
            }
        }

        public ConfigurationTableValue WashingPlant
        {
            get
            {
                if (this._washingPlant == null && this.WashingPlantId > 0)
                    this._washingPlant = _configValueService.GetEntityById(this.WashingPlantId);
                return this._washingPlant;
            }
        }

        public ConfigurationTableValue PrintingPlant
        {
            get
            {
                if (this._printingPlant == null && this.PrintingPlantId > 0)
                    this._printingPlant = _configValueService.GetEntityById(this.PrintingPlantId);
                return this._printingPlant;
            }
        }

        public ConfigurationTableValue SewingPlant
        {
            get
            {
                if (this._sewingPlant == null && this.SewingPlantId > 0)
                    this._sewingPlant = _configValueService.GetEntityById(this.SewingPlantId);
                return this._sewingPlant;
            }
        }

        public OrderInquiryStatus OrderInquiryStatus
        {
            get
            {
                if (this._orderInquiryStatus == null && this.OrderInquiryStatusId > 0)
                    this._orderInquiryStatus = new OrderInquiryStatusService().GetEntityById(this.OrderInquiryStatusId);
                return this._orderInquiryStatus;
            }
        }

        public Customer Customer
        {
            get
            {
                if (this._customer == null && this.CustomerId > 0)
                    this._customer = new CustomerService().GetEntityById(this.CustomerId);
                return this._customer;
            }
        }

        public Buyer Buyer
        {
            get
            {
                if (this._buyer == null && this.BuyerId > 0)
                    this._buyer = new BuyerService().GetEntityById(this.BuyerId);
                return this._buyer;
            }
        }

        public StyleDetail Style
        {
            get
            {
                if (this.style == null && this.StyleId > 0)
                    this.style = new StyleDetailDA().GetEntityById(this.StyleId);
                return this.style;
            }
        }

        public LITUser Merchant
        {
            get
            {
                if (this._merchant == null && this.MerchantId > 0)
                    this._merchant = new UserService().GetEntityById(this.MerchantId);
                return this._merchant;
            }
        }

        public LITUser Planner
        {
            get
            {
                if (this._planner == null && this.PlannerId > 0)
                    this._planner = new UserService().GetEntityById(this.PlannerId);
                return this._planner;
            }
        }

        public ConfigurationTableValue Color
        {
            get
            {
                if (this._color == null && this.ColorId > 0)
                    this._color = _configValueService.GetEntityById(this.ColorId);
                return this._color;
            }
        }

        #region Support Properties

        public List<ConfigurationTableValue> PlanningSeasons { get; set; }
        public List<ConfigurationTableValue> Products { get; set; }
        public List<ConfigurationTableValue> EmblishmentPlants { get; set; }
        public List<ConfigurationTableValue> WashingPlants { get; set; }
        public List<ConfigurationTableValue> PrintingPlants { get; set; }
        public List<ConfigurationTableValue> SewingPlants { get; set; }
        public List<ConfigurationTableValue> Colors { get; set; }

        public List<LITUser> Merchants { get; set; }
        public List<LITUser> Users { get; set; }
        public List<Customer> Customers { get; set; }
        public List<Buyer> Buyers { get; set; }
        public List<StyleDetail> Styles { get; set; }
        public List<OrderInquiryStatus> OrderInquiryStatuses { get; set; }
        public List<LITUser> Planners { get; set; }
        public List<String> CustomerSizes { get; set; }

        #endregion

        internal override void LoadAdditionalProperties(DataRow row)
        {
            OrderInquirySplitService orderInquirySplitService = new OrderInquirySplitService();

            this.Split = new OrderInquirySplit();
            this.ShipmentDateText = (this.ShipmentDate.HasValue) ? this.ShipmentDate.Value.ToString(GV.DisplayDateFormat) : string.Empty;
            
            this.Splits = orderInquirySplitService.ListForOrderInquiryId(this.Id);

            //if(this.Splits.Count.Equals(0))
            //{
            //    OrderInquirySplit newSplit = new OrderInquirySplit();
            //    newSplit.SplitId = 0;
            //    this.Splits.Add(newSplit);
            //}

        }
    }
}
