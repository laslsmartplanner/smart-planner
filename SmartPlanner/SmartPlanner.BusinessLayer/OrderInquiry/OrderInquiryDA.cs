﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    internal class OrderInquiryDA : EntityDataAccessBase<OrderInquiry>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Add(OrderInquiry orderDetail)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO OrderInquiries
                      (POId, SalesOrderNumber, Repeat, CustomerId, PlanningSeasonId, OrderQty, CM, ShipmentDate, ProductId,  MerchantId, CreatedDate, BuyerId, StyleId, CreatedById, 
                      CreatedOn, EmblishmentPlantId, WashingPlantId, PrintingPlantId, SewingPlantId, OrderInquiryStatusId, IsSplitOrder, IsSewingPlant, IsEmblishmentPlant, 
                      IsPrintingPlant, IsWashingPlant, PlannerId,ProductionSMV,OrderConfirmationDate)
                    VALUES (@POId, @SalesOrderNumber, @Repeat, @CustomerId, @PlanningSeasonId, @OrderQty, @CM, @ShipmentDate, @ProductId, @MerchantId, @CreatedDate, @BuyerId, @StyleId, @CreatedById, 
                      @CreatedOn, @EmblishmentPlantId, @WashingPlantId, @PrintingPlantId, @SewingPlantId, @OrderInquiryStatusId, @IsSplitOrder, @IsSewingPlant, @IsEmblishmentPlant, 
                      @IsPrintingPlant, @IsWashingPlant, @PlannerId, @ProductionSMV,@OrderConfirmationDate)");

            this.SetParameters(command, orderDetail);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                orderDetail.SetId(ret);
                this.SetSalesOrderNumber(orderDetail);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        
        private void SetParameters(SqlCommand command, OrderInquiry orderDetail)
        {

            command.Parameters.AddWithValue("@POId", orderDetail.POId ?? string.Empty);
            command.Parameters.AddWithValue("@SalesOrderNumber", orderDetail.SalesOrderNumber ?? string.Empty);
            command.Parameters.AddWithValue("@Repeat", orderDetail.Repeat);
            command.Parameters.AddWithValue("@CustomerId", orderDetail.CustomerId);
            
            command.Parameters.AddWithValue("@PlanningSeasonId", GetNullIfLessOne(orderDetail.PlanningSeasonId));
            command.Parameters.AddWithValue("@OrderQty", orderDetail.OrderQty);

            command.Parameters.AddWithValue("@CM", orderDetail.CM);
            command.Parameters.AddWithValue("@ShipmentDate", orderDetail.ShipmentDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ProductId", GetNullIfLessOne(orderDetail.ProductId));

            
            command.Parameters.AddWithValue("@CreatedDate", orderDetail.CreatedDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@BuyerId", GetNullIfLessOne(orderDetail.BuyerId));
            command.Parameters.AddWithValue("@StyleId", GetNullIfLessOne(orderDetail.StyleId));
            command.Parameters.AddWithValue("@EmblishmentPlantId", GetNullIfLessOne(orderDetail.EmblishmentPlantId));

            command.Parameters.AddWithValue("@WashingPlantId", GetNullIfLessOne(orderDetail.WashingPlantId));
            command.Parameters.AddWithValue("@PrintingPlantId", GetNullIfLessOne(orderDetail.PrintingPlantId));
            command.Parameters.AddWithValue("@SewingPlantId", GetNullIfLessOne(orderDetail.SewingPlantId));
            command.Parameters.AddWithValue("@MerchantId", GetNullIfLessOne(orderDetail.MerchantId));

            command.Parameters.AddWithValue("@OrderInquiryStatusId", GetNullIfLessOne(orderDetail.OrderInquiryStatusId));
            
            command.Parameters.AddWithValue("@IsSplitOrder", orderDetail.IsSplitOrder);
            command.Parameters.AddWithValue("@IsSewingPlant", orderDetail.IsSewingPlant);

            command.Parameters.AddWithValue("@IsEmblishmentPlant", orderDetail.IsEmblishmentPlant);
            command.Parameters.AddWithValue("@IsPrintingPlant", orderDetail.IsPrintingPlant);
            command.Parameters.AddWithValue("@IsWashingPlant", orderDetail.IsWashingPlant);

            command.Parameters.AddWithValue("@PlannerId", GetNullIfLessOne(orderDetail.PlannerId));
            command.Parameters.AddWithValue("@ProductionSMV", orderDetail.ProductionSMV);
            command.Parameters.AddWithValue("@OrderConfirmationDate", orderDetail.OrderConfirmationDate ?? (object)DBNull.Value);

        }

        internal bool Update(OrderInquiry orderDetail)
        {
            SqlCommand command = new SqlCommand(@"UPDATE    OrderInquiries
                                            SET POId =@POId , SalesOrderNumber =@SalesOrderNumber , Repeat =@Repeat , CustomerId =@CustomerId , PlanningSeasonId =@PlanningSeasonId , OrderQty =@OrderQty , CM =@CM , ShipmentDate =@ShipmentDate , ProductId =@ProductId , 
                                                CreatedDate =@CreatedDate, BuyerId =@BuyerId , StyleId =@StyleId , UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn , EmblishmentPlantId =@EmblishmentPlantId , WashingPlantId =@WashingPlantId , PrintingPlantId =@PrintingPlantId , 
                                                SewingPlantId =@SewingPlantId , OrderInquiryStatusId =@OrderInquiryStatusId , MerchantId =@MerchantId ,IsSplitOrder =@IsSplitOrder , IsSewingPlant =@IsSewingPlant , IsEmblishmentPlant =@IsEmblishmentPlant , IsPrintingPlant =@IsPrintingPlant , IsWashingPlant =@IsWashingPlant , PlannerId =@PlannerId, ProductionSMV=@ProductionSMV ,OrderConfirmationDate=@OrderConfirmationDate
                                                ,RequestedDate=@RequestedDate,ConfirmationStatusId=@ConfirmationStatusId
                                            WHERE     (Id = @Id )");
            command.Parameters.AddWithValue("@Id", orderDetail.Id);
            command.Parameters.AddWithValue("@RequestedDate", orderDetail.RequestedDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ConfirmationStatusId", orderDetail.ConfirmationStatusId);
            this.SetParameters(command, orderDetail);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<OrderInquiry> ListForType(int type)
        {
            string whereCondition = "OrderInquiryStatusId = 1";

            if(type !=2)
            {
                whereCondition = "OrderInquiryStatusId != 1";
            }

            SqlCommand sqlCommand = new SqlCommand($"select * from OrderInquiries WHERE {whereCondition} ");

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetSalesOrderNumber(OrderInquiry order)
        {
            SqlCommand sqlCommand = new SqlCommand("SetSalesOrderNumber");
            sqlCommand.Parameters.AddWithValue("@OrderInquiryId", order.Id);
            sqlCommand.Parameters.AddWithValue("@OrderNumberPrefix", ConfigurationManager.AppSettings["OrderNumberPrefix"].ToString());
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                object ret = _dataAccess.ExecuteScalar(sqlCommand);
                if (ret != null)
                    order.SalesOrderNumber = Convert.ToString(ret);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void SaveSplits(OrderInquiry order)
        {
            if (order.Splits == null || !order.Splits.Any()) return;

            OrderInquirySplitDA splitDA = new OrderInquirySplitDA();
                       
            List<OrderDetail> orders = new OrderDetailDA().ListForOrderInquiryId(order.Id);

            List<OrderInquirySplit> currentSplits = splitDA.ListForOrderInquiryId(order.Id);

            if(currentSplits.Any())
            {
                List<OrderInquirySplit> deleted = currentSplits.Where(f => !order.Splits.Any(t => t.SplitId == f.SplitId)).ToList();
                if(deleted.Any())
                {
                    foreach (OrderInquirySplit iSplit in deleted)
                        splitDA.Delete(iSplit.Id);
                }
            }
            

            foreach (OrderInquirySplit split in order.Splits)
            {
                split.SplitId = order.Splits.IndexOf(split) + 1;
                if (split.Id == 0)
                {
                    split.OrderInquiryId = order.Id;
                    this.AddSplit(split);
                }
                else
                    this.UpdateSplit(split);
                
                if (order.OrderInquiryStatusId == (int)EnumOrderInquiryStatus.Confirmed)
                {
                    string orderNo = $"{order.SalesOrderNumber}-{split.SplitId.ToString().PadLeft(2, '0')}";
                    if (!orders.Exists(x => x.SalesOrderNumber.Equals(orderNo)))
                        this.AddOrder(order, orderNo, split.Qty, split.ShipmentDate,split.Id);
                }
            }
        }

        internal void DeletePlacedOrderInquiries(int orderDetailsId)
        {
            SqlCommand command = new SqlCommand(@"Delete PlacedOrders
                                                  WHERE OrderDetailId = @OrderDetailsId AND IsInquiry=1 ");
            command.Parameters.AddWithValue("@OrderDetailsId", orderDetailsId);
            _dataAccess.ExecuteNonQuery(command);
        }

        internal void UpdateSplit(OrderInquirySplit split)
        {
            SqlCommand command = new SqlCommand(@"UPDATE    OrderInquirySplits
                                            SET OrderInquiryId =@OrderInquiryId, SplitId =@SplitId , Qty =@Qty , ShipmentDate =@ShipmentDate , UpdatedById =@UpdatedById , UpdatedOn =  @UpdatedOn
                                            WHERE (Id = @Id )");
            command.Parameters.AddWithValue("@Id", split.Id);
            this.SetSplitParameters(command, split);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal void AddSplit(OrderInquirySplit split)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO OrderInquirySplits
                                                    (OrderInquiryId, SplitId, Qty, ShipmentDate, CreatedById, CreatedOn)
                                                  VALUES (@OrderInquiryId, @SplitId, @Qty, @ShipmentDate, @CreatedById, @CreatedOn)");

            this.SetSplitParameters(command, split);
            this.SetCreatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null)
                {
                    _dataAccess.RollbackTransaction();
                    return;
                }
                split.SetId(ret);

                command.CommandText = "AddProductionSMV";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@OrderInquiryId", split.OrderInquiryId);
                command.Parameters.AddWithValue("@SplitKey", split.Id);
                _dataAccess.ExecuteNonQuery(command);
                _dataAccess.CommitTransaction();
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private void SetSplitParameters(SqlCommand command, OrderInquirySplit split)
        {
            command.Parameters.AddWithValue("@OrderInquiryId", split.OrderInquiryId);
            command.Parameters.AddWithValue("@SplitId", split.SplitId);
            command.Parameters.AddWithValue("@Qty", split.Qty);
            command.Parameters.AddWithValue("@ShipmentDate", split.ShipmentDate ?? (object)DBNull.Value);

        }

        internal void AddOrder(OrderInquiry inquiry,string orderNo,int orderQty,DateTime? shipmentDate,int splitKey)
        {
            OrderDetail order = new OrderDetail()
            {
                POId = inquiry.POId,
                SalesOrderNumber = orderNo,
                Repeat = inquiry.Repeat,
                CustomerId = inquiry.CustomerId,
                PlanningSeasonId = inquiry.PlanningSeasonId,
                OrderQty = orderQty,
                CM = inquiry.CM,
                OrderConfirmationDate = inquiry.OrderConfirmationDate,
                ShipmentDate = shipmentDate,
                ProductId = inquiry.ProductId,
                MerchantId = inquiry.MerchantId,
                CreatedDate = inquiry.CreatedDate,
                BuyerId = inquiry.BuyerId,
                EmblishmentPlantId = inquiry.EmblishmentPlantId,
                StyleId = inquiry.StyleId,
                WashingPlantId = inquiry.WashingPlantId,
                PrintingPlantId = inquiry.PrintingPlantId,
                SewingPlantId = inquiry.SewingPlantId,
                OrderInquiryStatusId = inquiry.OrderInquiryStatusId,
                ConfirmationDate = DateTime.Now,
                IsSplitOrder = inquiry.IsSplitOrder,
                IsSewingPlant = inquiry.IsSewingPlant,
                IsEmblishmentPlant = inquiry.IsEmblishmentPlant,
                IsPrintingPlant = inquiry.IsPrintingPlant,
                IsWashingPlant = inquiry.IsWashingPlant,
                PlannerId = inquiry.PlannerId,
                SplitKey = splitKey,
                OrderInquiryId = inquiry.Id,
                Contribution = inquiry.CM * orderQty,
                EPM = (inquiry.ProductionSMV==0)?0:inquiry.CM/ inquiry.ProductionSMV
            };
            new OrderDetailDA().Add(order);
        }
        
    }
}
