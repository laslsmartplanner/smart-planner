﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class Notification : EntityBase<Notification>
    {
        public override string TableName => "Notifications";

        public int ToId { get; set; }
        public string Message { get; set; }
        public bool Watched { get; set; }
        public DateTime? WatchedOn { get; set; }

        [Display(Name = "Created On")]
        public string CretedOnString { get { return this.CreatedOn.ToString("dd-MMM-yyyy HH:mm:ss"); } }
    }
}
