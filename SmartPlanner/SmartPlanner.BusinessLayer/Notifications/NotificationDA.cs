﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class NotificationDA : EntityDataAccessBase<Notification>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Add(Notification notification)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO Notifications (ToId, Message, CreatedById, CreatedOn)
                                                VALUES (@ToId, @Message, @CreatedById, @CreatedOn)");
            command.Parameters.AddWithValue("@ToId", notification.ToId);
            command.Parameters.AddWithValue("@Message", notification.Message);

            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                notification.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool UpdateAsWatched(Notification notification)
        {
            SqlCommand command = new SqlCommand(@"UPDATE Notifications
                                                SET Watched =1, WatchedOn = @UpdatedOn 
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", notification.Id);
            command.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
            
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal List<Notification> ListForLoggedUser(int numberOfNotifications)
        {
            SqlCommand sqlCommand = new SqlCommand(string.Format(@"SELECT {0} *
                                                                    FROM         Notifications
                                                                    WHERE     (ToId = @ToId) ORDER BY ID desc", numberOfNotifications>0 ? $"TOP ({numberOfNotifications})": string.Empty));
            sqlCommand.Parameters.AddWithValue("@ToId", GV.LoggedUser().Id);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
