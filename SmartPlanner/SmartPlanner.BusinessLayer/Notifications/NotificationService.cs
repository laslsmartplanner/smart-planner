﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class NotificationService : ServiceBase<Notification>
    {
        private NotificationDA _dataAccess = new NotificationDA();
        public bool UpdateAsWatched(Notification notification)
        {
            return _dataAccess.UpdateAsWatched(notification);
        }

        public List<Notification> ListForLoggedUser(int numberOfNotifications)
        {
            return _dataAccess.ListForLoggedUser(numberOfNotifications);
        }
    }
}
