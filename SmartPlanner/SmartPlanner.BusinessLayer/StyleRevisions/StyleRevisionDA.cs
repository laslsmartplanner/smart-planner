﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class StyleRevisionDA : EntityDataAccessBase<StyleRevision>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Reqeust(StyleRevision styleRevision)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO StyleRevisions(StyleId, RequestedById, InquiryType, RequestedDate, CreatedById, CreatedOn)
                                                VALUES (@StyleId, @RequestedById, @InquiryType, @RequestedDate, @CreatedById, @CreatedOn)");

            command.Parameters.AddWithValue("@StyleId", styleRevision.StyleId);
            command.Parameters.AddWithValue("@RequestedById", styleRevision.RequestedById);
            command.Parameters.AddWithValue("@InquiryType", styleRevision.InquiryType);
            command.Parameters.AddWithValue("@RequestedDate", styleRevision.RequestedDate);

            this.SetCreatedDetails(command);

            try
            {
                _dataAccess.BeginTransaction();
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                

                styleRevision.SetId(ret);

                SqlCommand history = this.GetStyleRevisionHistoryCommand(styleRevision.StyleId, styleRevision.Id, styleRevision.RequestedById, 0);
                _dataAccess.ExecuteNonQuery(history);
                

                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Accept(StyleRevision styleRevision)
        {
            SqlCommand command = new SqlCommand(@"UPDATE StyleRevisions
                                                SET RevisedDate =@RevisedDate , AcceptedById =@AcceptedById, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn WHERE Id=@Id");

            command.Parameters.AddWithValue("@RevisedDate", styleRevision.RevisedDate);
            command.Parameters.AddWithValue("@AcceptedById", styleRevision.AcceptedById);
            command.Parameters.AddWithValue("@Id", styleRevision.Id);

            this.SetUpdatedDetails(command);

            try
            {
                _dataAccess.BeginTransaction();
                _dataAccess.ExecuteNonQuery(command);
                
                SqlCommand history = this.GetStyleRevisionHistoryCommand(styleRevision.StyleId,styleRevision.Id,0,styleRevision.AcceptedById);
                _dataAccess.ExecuteNonQuery(history);


                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private SqlCommand GetStyleRevisionHistoryCommand(int styleId,int styleRevisionId,int requestedById,int acceptedById)
        {
            SqlCommand command = new SqlCommand("AddStyleRevisionHistory");
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@StyleId", styleId);
            command.Parameters.AddWithValue("@RequestedBy", requestedById);
            command.Parameters.AddWithValue("@AcceptedBy", acceptedById);
            command.Parameters.AddWithValue("@RevisionId", styleRevisionId);

            return command;
        }

        internal List<StyleRevision> ListStyleSMV(int styleDetailId)
        {
            string whereCondition = string.Empty;

            if (styleDetailId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " StyleId = @StyleId" : " AND StyleId = @StyleId";
            }

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " WHERE " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"SELECT * FROM StyleRevisions {whereCondition} ORDER BY StyleId");

            if (styleDetailId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@StyleId", styleDetailId);
            }

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
