﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class StyleRevision : EntityBase<StyleRevision>
    {
        public override string TableName => "StyleRevisions";

        private StyleDetail _styleDetail;
        private LITUser _requestedBy;
        private LITUser _acceptedBy;

        [Display(Name = "Style Number")]
        public int StyleId { get; set; }

        [Display(Name = "Requested By")]
        public int RequestedById { get; set; }

        [Display(Name = "Inquiry Type")]
        public string InquiryType { get; set; }

        [Display(Name = "Requested Date")]
        public DateTime RequestedDate { get; set; }

        [Display(Name = "Requested Date")]
        public string RequestedDateText { get; set; }

        [Display(Name = "Accepted Date")]
        public DateTime? RevisedDate { get; set; }

        [Display(Name = "Accepted Date")]
        public string RevisedDateText { get; set; }

        [Display(Name = "Accepted By")]
        public int AcceptedById { get; set; }

        public string StyleNumber
        {
            get
            {
                return this.StyleDetail?.StyleNumber ?? string.Empty;
            }
        }

        public string RequestedUser
        {
            get
            {
                return this.RequestedBy?.FullName ?? string.Empty;
            }
        }

        public string AcceptedUser
        {
            get
            {
                return this.AcceptedBy?.FullName ?? string.Empty;
            }
        }

        public StyleDetail StyleDetail
        {
            get
            {
                if (this._styleDetail == null && this.StyleId > 0)
                    this._styleDetail = new StyleDetailService().GetEntityById(this.StyleId);
                return this._styleDetail;
            }
        }

        public LITUser RequestedBy
        {
            get
            {
                if (this._requestedBy == null && this.RequestedById > 0)
                    this._requestedBy = new UserService().GetEntityById(this.RequestedById);
                return this._requestedBy;
            }
        }

        public LITUser AcceptedBy
        {
            get
            {
                if (this._acceptedBy == null && this.AcceptedById > 0)
                    this._acceptedBy = new UserService().GetEntityById(this.AcceptedById);
                return this._acceptedBy;
            }
        }

        public List<StyleDetail> StyleDetails { get; set; }

        internal override void LoadAdditionalProperties(DataRow row)
        {
            this.RequestedDateText = this.RequestedDate.ToShortDateString();
            this.RevisedDateText = (this.RevisedDate.HasValue) ? this.RevisedDate.Value.ToShortDateString() : string.Empty;
        }

    }
}
