﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class StyleRevisionService : ServiceBase<StyleRevision>
    {
        private StyleRevisionDA _dataAccess = new StyleRevisionDA();

        public bool Reqeust(StyleRevision styleRevision)
        {
            return _dataAccess.Reqeust(styleRevision);
        }
        public bool Accept(StyleRevision styleRevision)
        {
            return _dataAccess.Accept(styleRevision);
        }

        public List<StyleRevision> ListStyleSMV(int styleDetailId)
        {
            return _dataAccess.ListStyleSMV(styleDetailId);
        }
    }
}
