﻿using SmartPlanner.BusinessLayer.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class UserLevel : EntityBase<UserLevel>
    {
        #region Properties

        public override string TableName => "UserLevels";
        [Required(ErrorMessage = "Please enter user level")]
        [Display(Name = "Level Name")]
        public string LevelName { get; set; }
        public override List<string> IsAlreadyExistsProperties => new List<string>() { "LevelName" };

        public List<UserLevelAccessModule> UserLevelAccessModules { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return this.LevelName;
        }

        public List<UserLevelAccessModule> GetAccessModules()
        {
            return new UserLevelAccessModuleService().LoadForUserLevel(this.Id);
        }
       
        #endregion

    }
}
