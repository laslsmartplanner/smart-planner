﻿using SmartPlanner.BusinessLayer.SystemConfigurations;
using SmartPlanner.BusinessLayer.SystemConfigurations.AccessModules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class UserLevelAccessModule : EntityBase<UserLevelAccessModule>
    {
        #region Attributes

        private AccessModuleService _accessModuleService = new AccessModuleService();
        private AccessModule _accessModule = null;

        #endregion

        #region Properties

        public int UserLevelId { get; set; }
        public int AccessModuleId { get; set; }
        public UserAccessType AccessType { get; set; }
        public AccessModule AccessModule {
            get {
                if (this._accessModule == null && this.AccessModuleId > 0)
                    this._accessModule = _accessModuleService.GetAccessModuleByModuleNumber(this.AccessModuleId);
                return this._accessModule;
            }
        }
        public bool NoAccessChecked { get { return this.AccessType == UserAccessType.NoAccess; } }
        public bool ReadOnlyChecked { get { return this.AccessType == UserAccessType.ReadOnly; } }
        public bool ReadWriteChecked { get { return this.AccessType == UserAccessType.ReadWrite; } }

        public string UserAccessTypeText
        {
            get
            {
                return this.AccessType == UserAccessType.NoAccess ? "No Access" : this.AccessType == UserAccessType.ReadOnly ? "Read Only" : "Read Write";
            }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return $"{this.AccessModule?.ModuleName ?? string.Empty} - {this.AccessType.ToString()} ";
        }

        internal override void LoadAdditionalProperties(DataRow row)
        {
            this.AccessType = (UserAccessType)row["AccessTypeId"];
        }

        #endregion


    }
}
