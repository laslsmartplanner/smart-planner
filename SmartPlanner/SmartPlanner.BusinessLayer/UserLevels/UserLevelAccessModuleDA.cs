﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class UserLevelAccessModuleDA: EntityDataAccessBase<UserLevelAccessModule>
    {
        internal List<UserLevelAccessModule> LoadForUserLevel(int userLevelId)
        {
            SqlCommand sqlCommand = new SqlCommand("GetAccessModulesForUserLevel");
            sqlCommand.Parameters.AddWithValue("@UserLevelId", userLevelId);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            return GetList(sqlCommand);
        }
        internal List<UserLevelAccessModule> ListAccessModules()
        {
            SqlCommand sqlCommand = new SqlCommand(@"Select Id AccessModuleId,0 AccessTypeId From SysConfig_AccessModules");
            return GetList(sqlCommand);
        }
        
    }
}
