﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class UserLevelAccessModuleService
    {
        private UserLevelAccessModuleDA _dataAccess = new UserLevelAccessModuleDA();
        internal List<UserLevelAccessModule> LoadForUserLevel(int userLevelId)
        {
            return _dataAccess.LoadForUserLevel(userLevelId);
        }
        public List<UserLevelAccessModule> ListAccessModules()
        {
            return _dataAccess.GetList("ModuleNumber");
        }
        
    }
}
