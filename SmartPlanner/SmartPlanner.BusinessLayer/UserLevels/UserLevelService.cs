﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class UserLevelService:ServiceBase<UserLevel>
    {
        private UserLevelDA _dataAccess = new UserLevelDA();

        public bool Save(UserLevel userLevel, List<UserLevelAccessModule> accessModules)
        {
            if (!this.IsValidToSave(userLevel)) return false;
            if (userLevel.Id > 0)
                _dataAccess.Update(userLevel, accessModules);
            else
                _dataAccess.Add(userLevel, accessModules);
            return true;
        }
        
        public List<UserLevel> SearchByUserLevelName(string name)
        {
            return _dataAccess.SearchBy("LevelName", name);
        }
        public void DeleteByUserLevelId(int id)
        {
            _dataAccess.DeleteByUserLevelId(id);
        }
    }
}
