﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class UserLevelDA : EntityDataAccessBase<UserLevel>
    {
        private DataAccess _dataAccess = new DataAccess();
        internal void Add(UserLevel userLevel, List<UserLevelAccessModule> accessModules)
        {
            SqlCommand command = new SqlCommand(@"Insert into UserLevels(LevelName,CreatedById,CreatedOn) Values(@LevelName,@CreatedById,@CreatedOn)");
            command.Parameters.AddWithValue("@LevelName", userLevel.LevelName);
            this.SetCreatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();

                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return;
                userLevel.SetId(ret);
                AddAccessModules(userLevel, accessModules);
                _dataAccess.CommitTransaction();
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private void AddAccessModules(UserLevel userLevel, List<UserLevelAccessModule> accessModules)
        {
            SqlCommand command = new SqlCommand(@"Insert into UserLevelAccessModules(UserLevelId,AccessModuleId,AccessTypeId) 
                                        Values(@UserLevelId,@AccessModuleId,@AccessTypeId)");

            foreach (UserLevelAccessModule accessModule in accessModules)
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@UserLevelId", userLevel.Id);
                command.Parameters.AddWithValue("@AccessModuleId", accessModule.AccessModule.ModuleNumber);
                command.Parameters.AddWithValue("@AccessTypeId", (int)accessModule.AccessType);
                _dataAccess.ExecuteScalar(command);
            }
        }

        internal void Update(UserLevel userLevel, List<UserLevelAccessModule> accessModules)
        {
            SqlCommand command = new SqlCommand(@"Update UserLevels set LevelName=@LevelName,UpdatedById=@UpdatedById,UpdatedOn=@UpdatedOn Where Id = @Id");
            command.Parameters.AddWithValue("@Id", userLevel.Id);
            command.Parameters.AddWithValue("@LevelName", userLevel.LevelName);
            this.SetUpdatedDetails(command);

            try
            {
                _dataAccess.BeginTransaction();
                if (_dataAccess.ExecuteNonQuery(command) < 1) return;
                
                command.CommandText = @"Delete UserLevelAccessModules Where UserLevelId=@UserLevelId";
                command.Parameters.AddWithValue("@UserLevelId", userLevel.Id);
                _dataAccess.ExecuteNonQuery(command);
                this.AddAccessModules(userLevel, accessModules);
                _dataAccess.CommitTransaction();
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal void DeleteByUserLevelId(int id)
        {
            SqlCommand command = new SqlCommand($"Delete UserLevelAccessModules Where UserLevelId=@Id");
            command.Parameters.AddWithValue("@Id", id);
            _dataAccess.ExecuteNonQuery(command);
            command.CommandText = "Delete UserLevels Where Id=@Id";
            _dataAccess.ExecuteNonQuery(command);
        }

    }
}
