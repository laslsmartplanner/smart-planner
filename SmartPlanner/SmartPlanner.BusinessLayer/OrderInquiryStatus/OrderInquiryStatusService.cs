﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OrderInquiryStatusService : ServiceBase<OrderInquiryStatus>
    {
        private OrderInquiryStatusDA _dataAccess = new OrderInquiryStatusDA();

        public List<OrderInquiryStatus> ListOrderInquiryStatus()
        {
            return _dataAccess.GetList();
        }
    }
}
