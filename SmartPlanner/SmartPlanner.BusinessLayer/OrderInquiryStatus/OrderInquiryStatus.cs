﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class OrderInquiryStatus : EntityBase<OrderInquiryStatus>
    {
        public override string TableName => "OrderInquiryStatus";

        public string StatusName { get; set; }

    }
}
