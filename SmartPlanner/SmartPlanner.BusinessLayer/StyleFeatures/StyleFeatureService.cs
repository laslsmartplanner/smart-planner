﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class StyleFeatureService : ServiceBase<StyleFeature>
    {
        private StyleFeatureDA _dataAccess = new StyleFeatureDA();

        public bool Save(StyleFeature styleFeature)
        {
            if (!this.IsValidToSave(styleFeature)) return false;
            if (styleFeature.Id > 0)
                _dataAccess.Update(styleFeature);
            else
                _dataAccess.Add(styleFeature);

            return true;
        }

        public List<StyleFeature> ListForStyleProductId(int styleProductId)
        {
            return _dataAccess.ListForStyleProductId(styleProductId);
        }
    }
}
