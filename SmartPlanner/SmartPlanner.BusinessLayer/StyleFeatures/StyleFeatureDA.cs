﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class StyleFeatureDA : EntityDataAccessBase<StyleFeature>
    {
        private DataAccess _dataAccess = new DataAccess();


        private void SetParameters(SqlCommand command, StyleFeature styleFeature)
        {
            command.Parameters.AddWithValue("@StyleProductId", styleFeature.StyleProductId);
            command.Parameters.AddWithValue("@FeatureId", styleFeature.FeatureId);
        }

        internal bool Add(StyleFeature styleFeature)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO StyleProducts_FeaturesList (StyleProductId,FeatureId, CreatedById, CreatedOn)
                                                                                                VALUES(@StyleProductId,@FeatureId, @CreatedById, @CreatedOn)");

            this.SetParameters(command, styleFeature);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                styleFeature.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(StyleFeature styleFeature)
        {
            SqlCommand command = new SqlCommand(@"UPDATE StyleProducts_FeaturesList
                                                SET StyleProductId =@StyleProductId , FeatureId =@FeatureId , UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn 
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", styleFeature.Id);
            this.SetParameters(command, styleFeature);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }


        internal List<StyleFeature> ListForStyleProductId(int styleProductId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * FROM StyleProducts_FeaturesList WHERE StyleProductId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", styleProductId);

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal bool IsFeatureExists(int styleProductId, int featureId)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM StyleProducts_FeaturesList WITH(NOLOCK) WHERE StyleProductId=@StyleProductId AND FeatureId=@FeatureId");
            command.Parameters.AddWithValue("@StyleProductId", styleProductId);
            command.Parameters.AddWithValue("@FeatureId", featureId);

            return _dataAccess.ExecuteScalar(command) != null;
        }

    }
}
