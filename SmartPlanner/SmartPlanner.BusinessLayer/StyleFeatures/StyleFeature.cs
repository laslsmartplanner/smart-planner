﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class StyleFeature : EntityBase<StyleFeature>
    {
        private ConfigurationTableValue _feature = null;

        public override string TableName => "StyleProducts_FeaturesList";

        [Required]
        [Display(Name ="Feature")]
        public int FeatureId { get; set; }

        [Required]
        [Display(Name = "Style Product")]
        public int StyleProductId { get; set; }

        public ConfigurationTableValue Feature
        {
            get
            {
                if (this._feature == null && this.FeatureId > 0)
                    this._feature = new ConfigurationTableValueService().GetEntityById(this.FeatureId);
                return this._feature;
            }
        }

        internal override void ValidateOnSave()
        {
            if (new StyleFeatureDA().IsFeatureExists(this.StyleProductId, this.FeatureId))
                this.BrokenBusinessRules.Add("Feature already added to this product. Please select another.");
        }

    }
}
