﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using SmartPlanner.BusinessLayer.Common;

namespace SmartPlanner.BusinessLayer
{
    
    public class UserService : ServiceBase<LITUser>
    {
        private UserDA _dataAccess = new UserDA();
        private Email _email = new Email();
        public bool Create(LITUser user)
        {
            if (!this.IsValidToSave(user)) return false;
            string password = this.GetGuid();
            if (!_dataAccess.Add(user,password))
                return false;
            _email.SendUserCreatedEmail(user.Email, user.UserName, password);
            return true;
        }

        private string GetGuid()
        {
            string password = Membership.GeneratePassword(8, 6);
            //return string.Format("{0:00000000}", Guid.NewGuid().ToString());

            return password;
        }

        public bool ChangePassword(LITUser user)
        {
            if (!this.IsValidToSave(user)) return false;
            _dataAccess.UpdatePassword(user, Extensions.Encode(user.Password), false);
            return true;
        }
        public bool ResetPassword(LITUser user)
        {
            if (!this.IsValidToSave(user)) return false;
            string password = this.GetGuid();
            _dataAccess.UpdatePassword(user, password, true);
            _email.SendUserPasswordResetEmail(user.Email, user.UserName, password);
            return true;
        }
        public bool UpdateUser(LITUser user)
        {
            if (!this.IsValidToSave(user)) return false;
            _dataAccess.UpdateUser(user);
            return true;
        }
        public LITUser GetByName(string userName)
        {
            return _dataAccess.GetByName(userName);
        }
        public bool IsPasswordMatched(string userName,string password)
        {
            string pwd = _dataAccess.GetPassword(userName);
            LITUser user = _dataAccess.GetByName(userName);

            if (user == null)
                return false;

            if (user.IsTempPassword)
                return pwd.Equals(password);
            return pwd.Equals(Extensions.Encode(password));
        }
        public void SetLogin(LITUser user)
        {
            _dataAccess.SetLogin(user);
        }
        public List<LITUser> SearchByUserName(string name)
        {
            return _dataAccess.SearchBy("UserName", name);
        }
        public List<LITUser> ListUsers(string orderBy, bool addEmpty)
        {
            return _dataAccess.GetList(orderBy, addEmpty);
        }
    }
}
