﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class UserDA : EntityDataAccessBase<LITUser>
    {
        private DataAccess _dataAccess = new DataAccess();
        internal bool Add(LITUser user,string password)
        {
            SqlCommand command = new SqlCommand(@"Insert into Users(UserName,Password,FullName,Email,Phone,IsTempPassword,UserLevelId,CreatedById,CreatedOn) 
                                                Values(@UserName,@Password,@FullName,@Email,@Phone,@IsTempPassword,@UserLevelId,@CreatedById,@CreatedOn)");
            command.Parameters.AddWithValue("@UserName", user.UserName);
            command.Parameters.AddWithValue("@Password", password);
            command.Parameters.AddWithValue("@FullName", user.FullName);
            command.Parameters.AddWithValue("@Email", user.Email);
            command.Parameters.AddWithValue("@Phone", user.Phone);
            command.Parameters.AddWithValue("@UserLevelId", user.UserLevelId);
            command.Parameters.AddWithValue("@IsTempPassword", true);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                user.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal void UpdatePassword(LITUser user,string password,bool isReset)
        {
            SqlCommand command = new SqlCommand(@"Update Users set LastPasswordChangedOn=@LastPasswordChangedOn, Password=@Password,UpdatedById=@UpdatedById,UpdatedOn=@UpdatedOn, IsTempPassword=@IsTempPassword where id = @Id");
            command.Parameters.AddWithValue("@Password", password);
            command.Parameters.AddWithValue("@Id", user.Id );
            command.Parameters.AddWithValue("@IsTempPassword", isReset ? true : false);
            command.Parameters.AddWithValue("@LastPasswordChangedOn", DateTime.Now);
            this.SetUpdatedDetails (command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal bool UpdateUser(LITUser user)
        {
            SqlCommand command = new SqlCommand(@"Update Users SET FullName=@FullName, Email=@Email,Phone=@Phone,UserLevelId=@UserLevelId,UpdatedById=@UpdatedById,UpdatedOn=@UpdatedOn Where Id=@Id");
            command.Parameters.AddWithValue("@Id", user.Id);
            command.Parameters.AddWithValue("@Email", user.Email);
            command.Parameters.AddWithValue("@Phone", user.Phone);
            command.Parameters.AddWithValue("@UserLevelId", user.UserLevelId);
            command.Parameters.AddWithValue("@FullName", user.FullName);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal LITUser GetByName(string userName)
        {
            SqlCommand sqlCommand = new SqlCommand("Select * FROM Users Where UserName=@UserName");
            sqlCommand.Parameters.AddWithValue("@UserName", userName);
            return this.GetList(sqlCommand).FirstOrDefault();
        }
        internal string GetPassword(string userName)
        {
            return Convert.ToString(this.GetValue("Password", "UserName", userName));
        }
        internal void SetLogin(LITUser user)
        {
            SqlCommand command = new SqlCommand(@"Update Users set LastLoginOn=@LastLoginOn where id = @Id");
            command.Parameters.AddWithValue("@Id", user.Id);
            command.Parameters.AddWithValue("@LastLoginOn", DateTime.Now);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
    }
}
