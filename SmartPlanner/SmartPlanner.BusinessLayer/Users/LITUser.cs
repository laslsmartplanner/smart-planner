﻿using SmartPlanner.BusinessLayer.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class LITUser : EntityBase<LITUser>
    {
        private UserLevel _userLevel = null;
        public override string TableName => "Users";

        public override string NameField => "UserName";

        public override List<string> IsAlreadyExistsProperties => new List<string>() { "UserName","Email" };

        [Required(ErrorMessage = "Please enter username")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        [DataType(DataType.Password)]
       // [StringLength(15, MinimumLength = 6, ErrorMessage = "Password should have at least 6 characters")]
        public string Password { get; set; }

        [Required(ErrorMessage ="Please enter full name")]
        [Display(Name ="Full Name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter phone number")]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please select user level")]
        [Display(Name = "User Level")]
        public int UserLevelId { get; set; }

        public bool IsTempPassword { get; set; }

        public UserLevel UserLevel
        {
            get
            {
                if (this._userLevel == null && this.UserLevelId > 0)
                    this._userLevel = new UserLevelDA().GetEntityById(this.UserLevelId);
                return this._userLevel;
            }
        }

        public List<UserLevel> UserLevelList { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Required(ErrorMessage = "Please enter confirm password")]
        [CompareWithAttribute("Password", "Password does not match with the Confirm Password")]
        public string ConfirmPassowrd { get; set; }
    }
}
