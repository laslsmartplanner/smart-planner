﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class TnACalendar : EntityBase<TnACalendar>
    {
        private TnADefault _tnaDefault = null;
        private OrderDetail _orderDetail = null;
        private LITUser _responsiblity = null;

        public override string TableName => "TnACalendars";

        public int OrderDetailsId { get; set; }
        public int ProductId { get; set; }

        [Display(Name="Process Name")]
        public int TnADefaultId { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Required]
        public double Duration { get; set; }
        public double Holidays { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        //[Required]
        [Display(Name = "Actual Start Date")]
        public DateTime? ActualStartDate { get; set; }
        //[Required]
        [Display(Name = "Actual End Date")]
        public DateTime? ActualEndDate { get; set; }
        [Display(Name = "Holidays")]
        public double ActualHolidays { get; set; }
        [Display(Name = "Duration")]
        public int ActualDuration { get; set; }
        [Display(Name = "Responsibility")]
        public int ResponsibilityId { get; set; }
        public bool CanAddActualStartDate { get; set; }
        public TnADefault TnADefault
        {
            get
            {
                if (this._tnaDefault == null && this.TnADefaultId > 0)
                    this._tnaDefault = new TnADefaultService().GetEntityById(this.TnADefaultId);
                return this._tnaDefault;
            }
        }
        public OrderDetail OrderDetail
        {
            get
            {
                if (this._orderDetail == null && this.OrderDetailsId> 0)
                    this._orderDetail = new OrderDetailService().GetEntityById(this.OrderDetailsId);
                return this._orderDetail;
            }
        }
        public int DisplayOrder
        {
            get
            {
                return this.TnADefault?.DisplayOrder ?? 0;
            }
        }
        public string KeyProcess
        {
            get { return this.TnADefault?.KeyProcess; }
        }
        public string PlanStartDate
        {
            get
            {
                return this.StartDate?.ToString(GV.DisplayDateFormat) ?? string.Empty;
            }
        }
        public string PlanEndDate
        {
            get
            {
                return this.EndDate?.ToString(GV.DisplayDateFormat) ?? string.Empty;
            }
        }
        public string PlanActualStartDate
        {
            get
            {
                return this.ActualStartDate?.ToString(GV.DisplayDateFormat) ?? string.Empty;
            }
        }
        public string PlanActualEndDate
        {
            get
            {
                return this.ActualEndDate?.ToString(GV.DisplayDateFormat) ?? string.Empty;
            }
        }
        public LITUser Responsibility
        {
            get
            {
                if (this._responsiblity == null)
                    this._responsiblity = new UserService().GetEntityById(this.ResponsibilityId);
                return this._responsiblity;

            }
        }
        public string ResponsibilityName { get { return this.Responsibility?.FullName ?? string.Empty; } }
        public bool CanEdit
        {
            get
            {
                return (this.Responsibility == null ? true : this.Responsibility.Id == GV.LoggedUser().Id);
            }
        }
        public int Updated { get; set; }

        public DateTime? PlannedStartDate { get; set; }
        public DateTime? PlannedEndDate { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Ends in Half Day")]
        public bool IsActualEndInHalfDay { get; set; }

        public bool IsActualStartInHalfDay { get; set; }

        public void SetEndDate()
        {
            DataRow dr = new TnACalendarService().GetEndDate(this.StartDate.Value, this.Duration);
            if (dr == null) return;
            this.EndDate = Convert.ToDateTime(dr["EndDate"]);
            this.Holidays = Convert.ToDouble(dr["TotalHolidays"]);
        }

        internal override void ValidateOnSave()
        {
            if (this.TnADefaultId.Equals(16))
            {
                if (this.ActualEndDate.HasValue &&
                    new ProductivityDA().HasProductivityAddedAfterDate(this.ActualEndDate.Value, this.OrderDetailsId, this.ProductId))
                    this.BrokenBusinessRules.Add($"Outputs has added after {this.ActualEndDate.Value.ToString(GV.DisplayDateFormat)}. Delete outputs and change Actual End Date again.");
                if (this.ActualStartDate.HasValue && !new TnACalendarDA().IsValidActualStartDate(this.OrderDetailsId, this.ProductId, this.ActualStartDate.Value))
                    this.BrokenBusinessRules.Add("There is already ongoing order for this actual start date.");
            }
        }
    }
}

