﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class TnACalendarDA: EntityDataAccessBase<TnACalendar>
    {
        private DataAccess _dataAccess = new DataAccess();
        internal bool Add(TnACalendar tnACalendar)
        {


            SqlCommand command = new SqlCommand(@"INSERT INTO TnACalendars
                                                    (OrderDetailsId, TnADefaultId, StartDate, Duration, Holidays, EndDate, ActualStartDate, ActualEndDate, CreatedById, CreatedOn, ActualHolidays, ActualDuration,Updated,ProductId, Remarks,IsActualStartInHalfDay,IsActualEndInHalfDay)
                                                  VALUES(@OrderDetailsId, @TnADefaultId, @StartDate, @Duration, @Holidays, @EndDate, @ActualStartDate, @ActualEndDate, @CreatedById, @CreatedOn, @ActualHolidays, @ActualDuration,@Updated,@ProductId, @Remarks,@IsActualStartInHalfDay,@IsActualEndInHalfDay)");

            command.Parameters.AddWithValue("@OrderDetailsId", tnACalendar.OrderDetailsId);
            command.Parameters.AddWithValue("@TnADefaultId", tnACalendar.TnADefaultId);
            command.Parameters.AddWithValue("@StartDate", tnACalendar.StartDate);
            command.Parameters.AddWithValue("@Duration", tnACalendar.Duration);

            command.Parameters.AddWithValue("@Holidays", tnACalendar.Holidays);
            command.Parameters.AddWithValue("@EndDate", tnACalendar.EndDate);
            command.Parameters.AddWithValue("@ActualStartDate", tnACalendar.ActualStartDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ActualEndDate", tnACalendar.ActualEndDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ActualHolidays", tnACalendar.ActualHolidays);
            command.Parameters.AddWithValue("@ActualDuration", tnACalendar.ActualDuration);
            command.Parameters.AddWithValue("@Updated", tnACalendar.Updated);
            command.Parameters.AddWithValue("@ProductId", tnACalendar.ProductId);
            command.Parameters.AddWithValue("@Remarks", tnACalendar.Remarks ?? string.Empty);
            command.Parameters.AddWithValue("@IsActualStartInHalfDay", tnACalendar.IsActualStartInHalfDay);
            command.Parameters.AddWithValue("@IsActualEndInHalfDay", tnACalendar.IsActualEndInHalfDay);


            this.SetCreatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;

                tnACalendar.SetId(ret);

                this.SetActuallyEndDate(tnACalendar,_dataAccess);

                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(TnACalendar tnACalendar)
        {
            SqlCommand command = new SqlCommand(@"UPDATE TnACalendars
                                                  SET StartDate=@StartDate,Duration=@Duration,Holidays=@Holidays,EndDate=@EndDate, Updated=@Updated, ActualStartDate = @ActualStartDate, ActualEndDate = @ActualEndDate, ActualHolidays=@ActualHolidays, ActualDuration=@ActualDuration, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn, Remarks =@Remarks  ,
                                                   IsActualStartInHalfDay = @IsActualStartInHalfDay,IsActualEndInHalfDay=@IsActualEndInHalfDay
                                                  WHERE Id=@Id");

            command.Parameters.AddWithValue("@ActualStartDate", tnACalendar.ActualStartDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ActualEndDate", tnACalendar.ActualEndDate ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ActualHolidays", tnACalendar.ActualHolidays);
            command.Parameters.AddWithValue("@ActualDuration", tnACalendar.ActualDuration);
            command.Parameters.AddWithValue("@Id", tnACalendar.Id);
            command.Parameters.AddWithValue("@Updated", tnACalendar.Updated);

            command.Parameters.AddWithValue("@StartDate", tnACalendar.StartDate);
            command.Parameters.AddWithValue("@Duration", tnACalendar.Duration);

            command.Parameters.AddWithValue("@Holidays", tnACalendar.Holidays);
            command.Parameters.AddWithValue("@EndDate", tnACalendar.EndDate);
            command.Parameters.AddWithValue("@Remarks", tnACalendar.Remarks ?? string.Empty);
            command.Parameters.AddWithValue("@IsActualStartInHalfDay", tnACalendar.IsActualStartInHalfDay);
            command.Parameters.AddWithValue("@IsActualEndInHalfDay", tnACalendar.IsActualEndInHalfDay);

            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                _dataAccess.ExecuteNonQuery(command);

                this.ResetDuration(tnACalendar, _dataAccess);
                this.SetActuallyEndDate(tnACalendar, _dataAccess);

                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal void ResetDuration(TnACalendar tna, DataAccess dataAccess)
        {
            if (tna.ActualEndDate.HasValue) return;
            SqlCommand sqlCommand = new SqlCommand("ResetDuration");
            sqlCommand.Parameters.AddWithValue("@OrderId", tna.OrderDetailsId);
            sqlCommand.Parameters.AddWithValue("@OrderProductId", tna.ProductId);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            dataAccess.ExecuteNonQuery(sqlCommand);
        }

        private void SetActuallyEndDate(TnACalendar tna, DataAccess dataAccess)
        {
            if (tna.TnADefaultId != (int)Enums.EnumTnADefaults.FabricStitching || !tna.EndDate.HasValue) return;

            PlacedOrder placedOrder = new PlacedOrderDA().GetPlacedOrder(tna.OrderDetailsId, tna.ProductId);

            if (placedOrder == null) return;

            SqlCommand command = new SqlCommand();
            if (!tna.ActualEndDate.HasValue && placedOrder.ActuallyEnded.HasValue)
            {
                command.CommandText = "RemoveActuallyEnded";
                command.Parameters.AddWithValue("@PlacedOrderId", placedOrder.Id);
                command.CommandType = CommandType.StoredProcedure;
                dataAccess.ExecuteNonQuery(command);
                return;
            }

            if (!tna.ActualEndDate.HasValue || (placedOrder.ActuallyEnded.HasValue && tna.ActualEndDate.Value.Date == placedOrder.ActuallyEnded.Value.Date && tna.IsActualEndInHalfDay == placedOrder.EndInHalfDay)) return;

            command = new SqlCommand("ResetPlacedOrdersWithActualChange");
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@OrderId", tna.OrderDetailsId);
            command.Parameters.AddWithValue("@LineId", placedOrder.LineModuleId);
            command.Parameters.AddWithValue("@FactoryId", placedOrder.FactoryId);
            command.Parameters.AddWithValue("@PlacedOrderId", placedOrder.Id);
            command.Parameters.AddWithValue("@ShiftedOrderStartDate", placedOrder.StartDate);
            command.Parameters.AddWithValue("@ShiftedOrderEndDate", placedOrder.EndDate);
            command.Parameters.AddWithValue("@ShiftedOrderActualEndDate", tna.ActualEndDate.Value);
            command.Parameters.AddWithValue("@CurrentlyEndInHalfDay", placedOrder.EndInHalfDay);
            command.Parameters.AddWithValue("@ActualEndInHalfDay", tna.IsActualEndInHalfDay);
            command.Parameters.AddWithValue("@ActualStartInHalfDay", tna.IsActualStartInHalfDay);
            command.CommandType = CommandType.StoredProcedure;
            dataAccess.ExecuteNonQuery(command);
        }

        internal List<TnACalendar> GetCalendarsForOrder(int orderDetailsIntKey, int productId, int tnADefaultId = 0)
        {
            SqlCommand sqlCommand = new SqlCommand("GetTnACalendar");
            sqlCommand.Parameters.AddWithValue("@OrderDetailsId", orderDetailsIntKey);
            sqlCommand.Parameters.AddWithValue("@TnADefaultId", tnADefaultId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            return GetList(sqlCommand);
        }

        internal bool AddResponsibility(int orderDetailsId,int tnaDefaultId,int responsibilityId,DateTime startDate,int productId)
        {


            SqlCommand command = new SqlCommand(@"INSERT INTO TnACalendars
                                                    (StartDate, OrderDetailsId, TnADefaultId, ResponsibilityId, CreatedById, CreatedOn,ProductId)
                                                  VALUES(@StartDate,@OrderDetailsId, @TnADefaultId, @ResponsibilityId, @CreatedById, @CreatedOn,@ProductId)");

            command.Parameters.AddWithValue("@StartDate", startDate);
            command.Parameters.AddWithValue("@OrderDetailsId", orderDetailsId);
            command.Parameters.AddWithValue("@TnADefaultId", tnaDefaultId);
            command.Parameters.AddWithValue("@ResponsibilityId", responsibilityId);
            command.Parameters.AddWithValue("@ProductId", productId);

            this.SetCreatedDetails(command);
            try
            {
                return _dataAccess.ExecuteScalar(command) != null;
                
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal bool UpdateResponsiblity(int id, int responsibilityId)
        {
            SqlCommand command = new SqlCommand(@"UPDATE TnACalendars
                                                  SET ResponsibilityId=@ResponsibilityId, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn 
                                                  WHERE Id=@Id");

            command.Parameters.AddWithValue("@ResponsibilityId", responsibilityId);
            command.Parameters.AddWithValue("@Id", id);

            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal DataRow GetEndDate(DateTime startDate, double duration)
        {
            SqlCommand sqlCommand = new SqlCommand("GetEndDate");
            sqlCommand.Parameters.AddWithValue("@StartDate", startDate);
            sqlCommand.Parameters.AddWithValue("@Duration", duration);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataTable dt = _dataAccess.GetDataTable(sqlCommand);
            return dt?.Rows.Count > 0 ? dt.Rows[0] : null;
        }
        internal DataRow GetStartDate(DateTime endDate, double duration)
        {
            SqlCommand sqlCommand = new SqlCommand("GetStartDate");
            sqlCommand.Parameters.AddWithValue("@EndDate", endDate);
            sqlCommand.Parameters.AddWithValue("@Duration", duration);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataTable dt = _dataAccess.GetDataTable(sqlCommand);
            return dt?.Rows.Count > 0 ? dt.Rows[0] : null;
        }

        internal bool IsValidActualStartDate(int orderId, int productId, DateTime actualStartDate)
        {
            SqlCommand sqlCommand = new SqlCommand("SELECT dbo.IsValidActualStartDate(@OrderId,@ProductId,@StartDate)");
            sqlCommand.Parameters.AddWithValue("@OrderId", orderId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.Parameters.AddWithValue("@StartDate", actualStartDate);
            sqlCommand.CommandType = System.Data.CommandType.Text;
            object ret = _dataAccess.ExecuteScalar(sqlCommand);
            return ret == null ? true : Convert.ToBoolean(ret);
        }

        internal bool CanStartInHalfDay(int orderId,int productId, DateTime startDate)
        {
            SqlCommand sqlCommand = new SqlCommand("SELECT dbo.CanStartInHalfDay(@StartDate,@OrderId,@ProductId)");
            sqlCommand.Parameters.AddWithValue("@OrderId", orderId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            sqlCommand.Parameters.AddWithValue("@StartDate", startDate);
            sqlCommand.CommandType = System.Data.CommandType.Text;
            object ret = _dataAccess.ExecuteScalar(sqlCommand);
            return ret == null ? true : Convert.ToBoolean(ret);
        }

        internal bool UpdatePlanEfficiency(int orderDetailsId, int productId, float planEfficiency)
        {
            SqlCommand command = new SqlCommand(@"UPDATE TnACalendars
                                                  SET PlanEfficiency=@PlanEfficiency, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn 
                                                  WHERE OrderDetailsId=@OrderDetailsId AND ProductId=@ProductId AND TnADefaultId=@TnADefaultId");

            command.Parameters.AddWithValue("@PlanEfficiency", planEfficiency);
            command.Parameters.AddWithValue("@OrderDetailsId", orderDetailsId);
            command.Parameters.AddWithValue("@ProductId", productId);
            command.Parameters.AddWithValue("@TnADefaultId", 16);

            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal DataRow GetDuration(DateTime startDate, DateTime endDate, int lineModuleId)
        {
            SqlCommand sqlCommand = new SqlCommand("GetDuration");
            sqlCommand.Parameters.AddWithValue("@EndDate", endDate);
            sqlCommand.Parameters.AddWithValue("@StartDate", startDate);
            sqlCommand.Parameters.AddWithValue("@LineModuleId", lineModuleId);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataTable dt = _dataAccess.GetDataTable(sqlCommand);
            return dt?.Rows.Count > 0 ? dt.Rows[0] : null;
        }
    }
}
