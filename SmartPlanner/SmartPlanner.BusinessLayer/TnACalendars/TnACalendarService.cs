﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class TnACalendarService: ServiceBase<TnACalendar>
    {
        private TnACalendarDA _dataAccess = new TnACalendarDA();

        private void SendNotification(TnACalendar calendar )
        {
            if (calendar.OrderDetail == null) return;
            OrderDetail order = calendar.OrderDetail;
            EmailNotificationDTO dto = new EmailNotificationDTO()
            {
                Buyer = order.Buyer?.Name,
                OrderNumber = order.SalesOrderNumber,
                OrderQty = order.OrderQty.ToString(),
                PlanEndDate = calendar.EndDate.Value.ToString("dd/MMM/yyyy"),
                PlanStartDate = calendar.StartDate.Value.ToString("dd/MMM/yyyy"),
                ReceivedDate = order.CreatedDate.HasValue ? order.CreatedDate.Value.ToString("dd/MMM/yyyy") : string.Empty ,
                Status = order.OrderInquiryStatus?.StatusName,
                Style = order.Style?.StyleNumber,
                StyleDescription = order.Style?.Description
            };

            int emailId = 0;

            if ((calendar.StartDate.HasValue && calendar.PlannedStartDate.HasValue && calendar.StartDate.Value.Date != calendar.PlannedStartDate.Value.Date) ||
                        calendar.EndDate.HasValue && calendar.PlannedEndDate.HasValue && calendar.EndDate.Value.Date != calendar.PlannedEndDate.Value.Date
                            )
            {
                switch (calendar.TnADefaultId)
                {
                    case 9:
                        emailId = (int)EnumEmailType.FabricInhouseDateChanged;
                        break;
                    case 10:
                        emailId = (int)EnumEmailType.FtrimsInhouseDateChanged;
                        break;
                    case 12:
                        emailId = (int)EnumEmailType.PlannedCutDateChanged;
                        break;
                    case 16:
                        emailId = (int)EnumEmailType.FabricStichingDateChanged;
                        break;
                    case 17:
                    case 18:
                        emailId = (int)EnumEmailType.FabricFinishingDateChanged;
                        break;
                }

                if (emailId > 0)
                    new Email().SendChangedNotification(dto, emailId);
            }
        }

        public bool Save(TnACalendar tnACalendar)
        {
            if (!this.IsValidToSave(tnACalendar)) return false;
            bool isSuccess = false;
            if (tnACalendar.Id > 0)
                isSuccess = _dataAccess.Update(tnACalendar);
            else
            {
                isSuccess = _dataAccess.Add(tnACalendar);
                if (isSuccess)
                    this.SendNotification(tnACalendar);
            }
            return isSuccess;
        }
        public List<TnACalendar> GetCalendarsForOrder(int orderDetailsIntKey,int productId)
        {
            return _dataAccess.GetCalendarsForOrder(orderDetailsIntKey, productId);
        }
        public TnACalendar GetCalendarForOrder(int orderDetailsIntKey,int tnADefaultId, int productId)
        {
            return _dataAccess.GetCalendarsForOrder(orderDetailsIntKey, productId, tnADefaultId).FirstOrDefault();
        }
        public bool SaveResponsibility(int id, int responsibilityId, int tnaDefaultId, int orderDetailsId,int productId)
        {
            bool isSuccess = false;
            if (id > 0)
                isSuccess = _dataAccess.UpdateResponsiblity(id, responsibilityId);
            else
            {
                OrderDetail order = new OrderDetailDA().GetEntityById(orderDetailsId);
                isSuccess = order == null ? false : _dataAccess.AddResponsibility(orderDetailsId, tnaDefaultId, responsibilityId, DateTime.Today, productId);
            }

            if(isSuccess )
            {
                TnADefault tna = new TnADefaultDA().GetEntityById(tnaDefaultId);

                new NotificationDA().Add(new Notification()
                {
                    ToId = responsibilityId,
                    Message = $"{tna?.KeyProcess.Trim() ?? string.Empty } is assigned by {GV.LoggedUser().UserName}",
                });
            }

            return isSuccess;
        }
        public DataRow GetEndDate(DateTime startDate, double duration)
        {
            return _dataAccess.GetEndDate(startDate, duration);
        }
        public DataRow GetStartDate(DateTime endDate, double duration)
        {
            return _dataAccess.GetStartDate(endDate, duration);
        }
        public bool CanStartInHalfDay(int orderId,int productId, DateTime startDate)
        {
            return _dataAccess.CanStartInHalfDay(orderId, productId, startDate);
        }

        public bool UpdatePlanEfficiency(int orderDetailsId, int productId, float planEfficiency)
        {
            return _dataAccess.UpdatePlanEfficiency(orderDetailsId, productId, planEfficiency);
        }

        public DataRow GetDuration(DateTime startDate, DateTime endDate, int lineModuleId)
        {
            return _dataAccess.GetDuration(startDate, endDate, lineModuleId);
        }
    }
}
