﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class EmailReceiverService
    {
        public List<EmailReceiver> ListByEmailId(int id)
        {
            return new EmailReceiverDA().ListByEmailId(id);
        }
    }
}
