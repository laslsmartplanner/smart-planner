﻿using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class EmailReceiverDA
    {
        private DataAccess _dataAccess = new DataAccess();
        internal List<EmailReceiver> ListByEmailId(int id)
        {
            SqlCommand sqlCommand = new SqlCommand("Select * FROM EmailReceivers Where EmailId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", id);
            return EmailReceiver.Load(_dataAccess.GetDataTable(sqlCommand));
        }
    }
}
