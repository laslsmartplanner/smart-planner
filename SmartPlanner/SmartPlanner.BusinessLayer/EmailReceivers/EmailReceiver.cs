﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class EmailReceiver
    {
        private LITUser receieveUser = null;

        public int EmailId { get; set; }
        public EnumEmailSendingOptions ToOrCC { get; set; }
        public int ReceiverId { get; set; }

        public LITUser RecieveUser
        {
            get
            {
                if (this.receieveUser == null)
                    this.receieveUser = new RoleDA().GetEntityById(this.ReceiverId)?.User;
                return this.receieveUser;
            }
        }

        public string ReceiverEmail { get { return this.RecieveUser?.Email ?? string.Empty; } }

        internal static List<EmailReceiver> Load(DataTable detail)
        {
            List<EmailReceiver> receivers = new List<EmailReceiver>();
            foreach (DataRow row in detail.Rows)
            {
                receivers.Add(new EmailReceiver()
                {
                    EmailId = Convert.ToInt32(row["EmailId"]),
                    ReceiverId = Convert.ToInt32(row["ReceiverId"]),
                    ToOrCC = (EnumEmailSendingOptions)Convert.ToInt32(row["ToOrCC"])
                });
            }
            return receivers;
        }
    }
}
