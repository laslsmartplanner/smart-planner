﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class LineModuleService : ServiceBase<LineModule>
    {
        private LineModuleDA _dataAccess = new LineModuleDA();

        public bool Save(LineModule lineModule)
        {
            if (!this.IsValidToSave(lineModule)) return false;
            if (lineModule.Id > 0)
                _dataAccess.Update(lineModule);
            else
                _dataAccess.Add(lineModule);
            return true;
        }

        public List<LineModule> GetLineModules()
        {
            return _dataAccess.GetList("ModuleNumber");
        }

        public List<Factory> ListInvolvedFactoies()
        {
            return new FactoryDA().ListInvolvedFactoiesInLineModules();
        }
        public List<LineModule> ListForFactoryId(int factoryId)
        {
            return _dataAccess.ListForFactoryId(factoryId);
        }
        public List<LineModule> ListForBoard(int factoryId, DateTime fromDate, DateTime toDate)
        {
            return _dataAccess.ListForBoard(factoryId, fromDate, toDate);
        }
    }
}
