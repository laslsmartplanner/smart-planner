﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class LineModule : EntityBase<LineModule>
    {
        private Factory _factoryName = null;

        public override string TableName => "LineModules";
        //public override List<string> IsAlreadyExistsProperties => new List<string>() { "FactoryNameId", "ModuleNumber" };

        [Required]
        [Display(Name ="Factory Name")]
        public int FactoryNameId { get; set; }

        [Required]
        [Display(Name = "Module Number")]
        public string  ModuleNumber { get; set; }

        [Display(Name = "Plan Employees")]
        public int PlanEmployees { get; set; }

        [Display(Name = "Plan Efficiency")]
        public decimal PlanEfficiency { get; set; }

        [Display(Name = "Available Minutes")]
        public decimal AvailableMinutes { get; set; }

        [Display(Name = "Average Production Out")]
        public decimal AverageProductionOut { get; set; }

        [Display(Name = "Production Per Day")]
        public int ProductionPerDay { get; set; }

        public string ModuleNumberBoard
        {
            get
            {
                return $"{this.ModuleNumber} : {this.AvailableMinutes.ToString()}";
            }
        }

        public Factory FactoryName
        {
            get
            {
                if (this._factoryName == null && this.FactoryNameId > 0)
                    this._factoryName = new FactoryService().GetEntityById(this.FactoryNameId);
                return this._factoryName;
            }
        }
        public List<Factory> FactoryNames { get; set; }

        public List<string> GridColumns
        {
            get; set;
        }
        public List<int> HolidayColumns { get; set; }


        internal static List<LineModule> GetList(DataTable lines, DataTable holidays)
        {
            List<LineModule> list = new List<LineModule>();

            DataColumnCollection columns = lines.Columns;
            foreach (DataRow row in lines.Rows)
            {
                LineModule lineModule = new LineModule();
                lineModule.Load(row, columns);

                DataRow[] holidayRows = holidays.Select($"Id={lineModule.Id}");
                List<int> holidayList = new List<int>();

                foreach (DataRow holiday in holidayRows)
                    holidayList.Add(Convert.ToInt32(holiday["Col"]));

                lineModule.HolidayColumns = holidayList;
                list.Add(lineModule);
            }
            
            return list;
        }
    }
}
