﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class LineModuleDA : EntityDataAccessBase<LineModule>
    {
        private DataAccess _dataAccess = new DataAccess();

        private void SetParameters(SqlCommand command,LineModule lineModule)
        {
            command.Parameters.AddWithValue("@FactoryNameId", lineModule.FactoryNameId);
            command.Parameters.AddWithValue("@ModuleNumber", lineModule.ModuleNumber);
            command.Parameters.AddWithValue("@PlanEmployees", lineModule.PlanEmployees);
            command.Parameters.AddWithValue("@PlanEfficiency", lineModule.PlanEfficiency);
            command.Parameters.AddWithValue("@AvailableMinutes", lineModule.AvailableMinutes);
            command.Parameters.AddWithValue("@AverageProductionOut", lineModule.AverageProductionOut);
            command.Parameters.AddWithValue("@ProductionPerDay", lineModule.ProductionPerDay);
        }

        internal bool Add(LineModule lineModule)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO LineModules
                      (FactoryNameId, ModuleNumber, PlanEmployees, PlanEfficiency, AvailableMinutes, AverageProductionOut, ProductionPerDay, CreatedById, CreatedOn)
               VALUES (@FactoryNameId, @ModuleNumber, @PlanEmployees, @PlanEfficiency, @AvailableMinutes, @AverageProductionOut, @ProductionPerDay, @CreatedById, @CreatedOn)");
            this.SetParameters(command, lineModule);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                lineModule.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal bool Update(LineModule lineModule)
        {
            SqlCommand command = new SqlCommand(@"UPDATE LineModules
                                                SET FactoryNameId =@FactoryNameId , ModuleNumber =@ModuleNumber , PlanEmployees =@PlanEmployees , PlanEfficiency=@PlanEfficiency , 
                                                    AvailableMinutes = @AvailableMinutes , AverageProductionOut = @AverageProductionOut , ProductionPerDay = @ProductionPerDay , UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn 
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", lineModule.Id);
            this.SetParameters(command, lineModule);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal List<LineModule> ListForFactoryId(int factoryId)
        {
            SqlCommand sqlCommand = new SqlCommand(@"Select * 
                                                    From LineModules l WITH(NOLOCK)
                                                    Where FactoryNameId = @FactoryNameId
                                                    order by l.ModuleNumber ");
            sqlCommand.Parameters.AddWithValue("@FactoryNameId", factoryId);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal List<LineModule> ListForBoard(int factoryId,DateTime fromDate,DateTime toDate)
        {
            SqlCommand sqlCommand = new SqlCommand("ListLineModulesForBoard");
            sqlCommand.Parameters.AddWithValue("@FactoryNameId", factoryId);
            sqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
            sqlCommand.Parameters.AddWithValue("@ToDate", toDate);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            DataSet ds = new DataAccess().GetDataSet(sqlCommand);

            if (ds.Tables.Count != 2) return null;

            return LineModule.GetList(ds.Tables[0], ds.Tables[1]);

        }
        
    }
}
