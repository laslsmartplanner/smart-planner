﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class PlacedOrder : EntityBase<PlacedOrder>
    {
        public override string TableName => "PlacedOrders";

        private ConfigurationTableValue _product = null;

        public int FactoryId { get; set; }
        public int LineModuleId { get; set; }
        public int OrderDetailId { get; set; }
        public int ProductId { get; set; }
        public double Duration { get; set; }
        public double Holidays { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool StartInHalfDay { get; set; }
        public bool EndInHalfDay { get; set; }
        public int ColumnCount { get; set; }
        public int StartColumn { get; set; }
        public string SalesOrderNumber { get; set; }
        public bool IsTemp { get; set; }
        public DateTime StartedFrom { get; set; }
        public decimal LeftPosition { get; set; }

        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public double ActualDuration { get; set; }
        public double ActualHolidays { get; set; }

        public int ActualStartColumn { get; set; }
        public int ActualEndColumn { get; set; }

        public int StyleId { get; set; }

        public DateTime? PlannedStart { get; set; }
        public DateTime? PlannedEnd { get; set; }
        
        public bool IsAfterPlanned
        {
            get
            {
                if (!this.PlannedStart.HasValue)
                    return false;
                return this.StartDate.Date > this.PlannedStart.Value.Date;
            }
        }

        public bool Locked { get; set; }

        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = new ConfigurationTableValueService().GetEntityById(this.ProductId);
                return this._product;
            }
        }
        //public OrderDetail OrderDetail { get
        //    {
        //        if (this.orderDetail == null)
        //            this.orderDetail = new OrderDetailService().GetEntityById(this.OrderDetailId);
        //        return this.orderDetail;
        //    }
        //}

        public string CominationColor { get; set; }
        public OrderDetail OrderDetail { get; set; }
        public StyleDetail StyleDetail { get; set; }
        public string StyleNumber { get; set; }
        public int OrderQty { get; set; }
        public string CustomerPONumber { get; set; }
        public double ProductionSMV { get; set; }
        public DateTime? ShipmentDate { get; set; }

        //public StyleDetail StyleDetail
        //{
        //    get
        //    {
        //        if (this.styleDetail == null && this.orderDetail != null)
        //            this.styleDetail = new StyleDetailService().GetEntityById(this.orderDetail.StyleId);
        //        return this.styleDetail;
        //    }
        //}

        //public string StyleNumber
        //{
        //    get
        //    {
        //        return this.StyleDetail?.StyleNumber ?? string.Empty;
        //    }
        //}

        public string ProductName { get { return this.Product?.ConfigValue ?? string.Empty; } }

        public List<ProductivityDTO> ProductivityDetails { get; set; }

        public bool HasChanged { get; set; }
        public bool IsOverlap { get; set; }

        public string BuyerName { get; set; }
        public string PlannedColor { get; set; }
        public string ActualColor { get; set; }
        public string ChangedColor { get; set; }

        public DateTime OrderCreatedDate { get; set; }

        public string OrderInquiryStatus { get; set; }
        public string StyleDescription { get; set; }

        public string StartDateString { get { return this.StartDate.ToString(GV.DisplayDateFormat) ; } }

        public string EndDateString { get { return this.EndDate.ToString(GV.DisplayDateFormat); } }

        public double AdditionalDays { get; set; }

        public double AdditionalHolidays { get; set; }
        public int AdditionalEndColumn { get; set; }
        public DateTime? AdditionalEndDate { get; set; }

        public double ActualAdditionalHolidays { get; set; }

        public DateTime? ActualAdditionalEndDate { get; set; }

        public DateTime? ActuallyEnded { get; set; }

        public string ShipmentDateString { get { return this.ShipmentDate.HasValue ? this.ShipmentDate.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public string PlannedStartString { get { return this.PlannedStart.HasValue ? this.PlannedStart.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public string PlannedEndString { get { return this.PlannedEnd.HasValue ? this.PlannedEnd.Value.ToString(GV.DisplayDateFormat) : string.Empty; } }

        public bool IsActualEndInHalfDay { get; set; }
        public bool IsActualStartInHalfDay { get; set; }

        public double PlanEfficiency { get; set; }
        public DateTime? CuttingDate { get; set; }
        public string CuttingDateString
        {
            get
            {
                return this.CuttingDate.HasValue ? this.CuttingDate.Value.ToString(GV.DisplayDateFormat) : string.Empty;
            }
        }
        public decimal ProductCM { get; set; }
        public string ChangeOverCategory { get; set; }
        public double PlanAdjustment { get; set; }
        public double ActAdjustment { get; set; }
        public int IsInquiry { get; set; }

        internal static List<PlacedOrder> GetList(DataTable placedDt, DataTable productivities)
        {
            List<PlacedOrder> list = new List<PlacedOrder>();

            DataColumnCollection columns = placedDt.Columns;
            foreach (DataRow row in placedDt.Rows)
            {
                PlacedOrder placedOrder = new PlacedOrder();
                placedOrder.Load(row, columns);

                placedOrder.ProductivityDetails = ProductivityDTO.Load(productivities.Select($"LineModuleId = {placedOrder.LineModuleId} AND OrderDetailId = {placedOrder.OrderDetailId} AND ProductId = {placedOrder.ProductId}"));

                list.Add(placedOrder);
            }

            return list;
        }

    }
}
