﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class PlacedOrderService: ServiceBase<PlacedOrder>
    {
        private CalendarDayService _calendarDayService = new CalendarDayService();

        private PlacedOrderDA _dataAccess = new PlacedOrderDA();
        public PlacedOrder AddOrderToLine(PlacedOrder placedOrder)
        {
            PlacedOrder savedOrder =  _dataAccess.AddOrderToLine(placedOrder);

            if(savedOrder.HasChanged)
            {
                OrderDetail order = savedOrder.OrderDetail;

                EmailNotificationDTO dto = new EmailNotificationDTO()
                {
                    Buyer = savedOrder.BuyerName,
                    OrderNumber = savedOrder.SalesOrderNumber,
                    OrderQty = savedOrder.OrderQty.ToString(),
                    PlanEndDate = savedOrder.EndDate.ToString("dd/MMM/yyyy"),
                    PlanStartDate = savedOrder.StartDate.ToString("dd/MMM/yyyy"),
                    ReceivedDate = savedOrder.OrderCreatedDate.ToString("dd/MMM/yyyy") ,
                    Status = savedOrder.OrderInquiryStatus,
                    Style = savedOrder.StyleNumber,
                    StyleDescription = savedOrder.StyleDescription

                };

                new Email().SendChangedNotification(dto, (int)EnumEmailType.FabricStichingDateChanged);
            }

            return savedOrder;
        }
        public List<PlacedOrder> ListPlacedOrders(DateTime from, DateTime to, int factoryId)
        {
            return _dataAccess.ListPlacedOrders(from, to, factoryId);

            //foreach(PlacedOrder order in dto.PlacedOrders)
            //{
            //    if (!order.ActualStartDate.HasValue) continue;

                //order.IsOverlap = dto.PlacedOrders.Count(x => x.ActualEndDate > order.StartDate && x.LineModuleId == order.LineModuleId && x.OrderDetailId != order.OrderDetailId) > 0;

                //order.ProductivityDetails = dto.Productivities.Where(x => x.LineModuleId == order.LineModuleId && x.OrderId == order.OrderDetailId && x.ProductId == order.ProductId).ToList();
                //if (productivities == null || productivities.Count == 0) continue;

                //List<ProductivityDTO> productivityDetails = new List<ProductivityDTO>();
                
                //int column = order.ActualStartColumn;

                //DateTime date = order.ActualStartDate.Value < from ? from : order.ActualStartDate.Value;

                //DateTime actEnd = order.ActualEndDate.Value > to ? to : order.ActualEndDate.Value;
                //if (order.ActualAdditionalEndDate.HasValue)
                //    actEnd = order.ActualAdditionalEndDate.Value;

                //while (date <= actEnd)
                //{
                //    Productivity productivity = productivities.FirstOrDefault(x => x.WorkingDate.Value.Date == date.Date);

                //    productivityDetails.Add(new ProductivityDTO() { ColumnNumber = column, WorkingDate = date, ProducedQty = productivity?.ProducedQty.ToString() ?? string.Empty });
                //    date = date.AddDays(1);
                //    column++;
                //}

                //order.ProductivityDetails = productivityDetails;
            //}

            //return dto.PlacedOrders;
        }

        public List<ReportDTOPlacedOrders> ListPlacedOrdersForReport(DateTime from, DateTime to, int factoryId)
        {
            List<ReportDTOPlacedOrders> reportDTOPlacedOrders = new List<ReportDTOPlacedOrders>();

            //PlacedOrderDTO dto = _dataAccess.ListPlacedOrders(from, to, factoryId);

            List<PlacedOrder> placedOrders = _dataAccess.ListPlacedOrders(from, to, factoryId); 

            ProductivityDA productivityDA = new ProductivityDA();
            foreach (PlacedOrder order in placedOrders)
            {
                //double orderQty = order.OrderQty;
                //double qtyPerDay = orderQty / order.Duration;
                
                DataRow qtys = productivityDA.GetPlannedQty(order.OrderDetailId, order.ProductId, order.LineModuleId, order.FactoryId, order.Duration);

                DateTime date = order.StartDate;
                int workedDays = 1;
                int orderQty = order.OrderQty;
                double balanceQty = orderQty;

                LineModule lineModule = new LineModuleService().GetEntityById(order.LineModuleId);
                double lineDet = Convert.ToDouble(Math.Floor(Convert.ToDouble(lineModule.AvailableMinutes * lineModule.PlanEmployees * lineModule.PlanEfficiency) / (100 * order.ProductionSMV)));

                while (date <= order.EndDate)
                {
                    ReportDTOPlacedOrders reportDTOPlacedOrder = new ReportDTOPlacedOrders();

                    reportDTOPlacedOrder.FactoryId = order.FactoryId;
                    reportDTOPlacedOrder.LineModuleId = order.LineModuleId;
                    reportDTOPlacedOrder.OrderId = order.OrderDetailId;
                    reportDTOPlacedOrder.StyleId = order.StyleId;
                    reportDTOPlacedOrder.ProductId = order.ProductId;
                    reportDTOPlacedOrder.ProductionSMV = order.ProductionSMV;
                    reportDTOPlacedOrder.CombinationColor = order.CominationColor;
                    reportDTOPlacedOrder.PlanCuttingDate = order.CuttingDateString;
                    reportDTOPlacedOrder.ProductCM = order.ProductCM;

                    if (!_calendarDayService.IsHolidayWithWeekEnds(date,order.LineModuleId))
                    {
                        
                        //reportDTOPlacedOrder.PlannedQty = Math.Floor(reportDTOPlacedOrder.Order.OrderQty/ order.Duration);

                        if (qtys != null)
                        {
                            switch (workedDays)
                            {
                                case 1:
                                    if (order.PlanEfficiency != 30 && order.PlanEfficiency != 50)
                                    {
                                        double firstLineDet = lineDet;

                                        if(order.StartInHalfDay)
                                            firstLineDet = lineDet/2;

                                        if (balanceQty > firstLineDet)
                                        {
                                            reportDTOPlacedOrder.PlannedQty = firstLineDet;
                                            
                                        }
                                        else
                                        {
                                            reportDTOPlacedOrder.PlannedQty = balanceQty;
                                        }

                                        balanceQty = balanceQty - firstLineDet;

                                    }
                                    else
                                    {
                                        reportDTOPlacedOrder.PlannedQty = Convert.ToDouble(qtys["Qty1"]);
                                        balanceQty = balanceQty - Convert.ToDouble(qtys["Qty1"]);
                                    }

                                    
                                    break;
                                case 2:
                                    if(order.PlanEfficiency !=30 && order.PlanEfficiency != 50)
                                    {
                                        if (balanceQty > lineDet)
                                        {
                                            reportDTOPlacedOrder.PlannedQty = lineDet;
                                            
                                        }
                                        else
                                        {
                                            reportDTOPlacedOrder.PlannedQty = balanceQty;
                                        }

                                        balanceQty = balanceQty - lineDet;
                                    }
                                    else
                                    {
                                        reportDTOPlacedOrder.PlannedQty = Convert.ToDouble(qtys["Qty2"]);
                                        balanceQty = balanceQty - Convert.ToDouble(qtys["Qty2"]);
                                    }
                                    
                                    
                                    break;
                                default:
                                    if(balanceQty> lineDet)
                                    {
                                        reportDTOPlacedOrder.PlannedQty = lineDet;
                                        balanceQty = balanceQty - lineDet;
                                    }
                                    else
                                        reportDTOPlacedOrder.PlannedQty = balanceQty;
                                    break;
                            }
                        }

                        workedDays++;
                    }

                    reportDTOPlacedOrder.WorkingDate = date;

                    if (date >= from && date <= to)
                    {
                        reportDTOPlacedOrders.Add(reportDTOPlacedOrder);
                    }

                    
                    

                    date = date.AddDays(1);
                }

            }

            return reportDTOPlacedOrders;
        }

        public List<SearchPlacedOrdersDTO> PlacedOrderForSearch()
        {
            return _dataAccess.PlacedOrderForSearch();
        }
    }
}
