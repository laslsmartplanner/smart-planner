﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class PlacedOrderDA : EntityDataAccessBase<PlacedOrder>
    {
        private DataAccess _dataAccess = new DataAccess();
        internal PlacedOrder AddOrderToLine(PlacedOrder placedOrder )
        {
            SqlCommand sqlCommand = new SqlCommand("AddOrderToLine");
            sqlCommand.Parameters.AddWithValue("@LineModuleId", placedOrder.LineModuleId);
            sqlCommand.Parameters.AddWithValue("@OrderDetailId", placedOrder.OrderDetailId);
            sqlCommand.Parameters.AddWithValue("@ProductId", placedOrder.ProductId);
            sqlCommand.Parameters.AddWithValue("@FactoryId", placedOrder.FactoryId);
            sqlCommand.Parameters.AddWithValue("@UserId", GV.LoggedUser().Id);
            sqlCommand.Parameters.AddWithValue("@Duration", placedOrder.Duration);
            sqlCommand.Parameters.AddWithValue("@StartFrom", placedOrder.StartedFrom);
            sqlCommand.Parameters.AddWithValue("@EndTo", placedOrder.EndDate);
            sqlCommand.Parameters.AddWithValue("@StartDate", placedOrder.StartDate);
            sqlCommand.Parameters.AddWithValue("@IsInquiry", placedOrder.IsInquiry);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            DataTable result = _dataAccess.GetDataTable(sqlCommand);

            PlacedOrder placed = new PlacedOrder();

            if (result == null || result.Rows.Count == 0)
                return null;
            placed.Load(result.Rows[0], result.Columns);
            return placed;
        }

        internal List<PlacedOrder> ListPlacedOrders(DateTime from , DateTime to,int factoryId)
        {
            SqlCommand sqlCommand = new SqlCommand("ListPlacedOrders");
            sqlCommand.Parameters.AddWithValue("@From", from);
            sqlCommand.Parameters.AddWithValue("@To", to);
            sqlCommand.Parameters.AddWithValue("@FactoryId", factoryId);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            DataSet ds = new DataAccess().GetDataSet(sqlCommand);

            if (ds.Tables.Count != 2) return null;

            return PlacedOrder.GetList(ds.Tables[0], ds.Tables[1]);

            //List<ProductivityDTO> productivities = ProductivityDTO.Load(ds.Tables[1]);

            ////List<Productivity> productivities = new ProductivityDA().GetList(ds.Tables[1]);

            //return new PlacedOrderDTO() { PlacedOrders = placedOrders, Productivities = productivities };
        }

        internal PlacedOrder GetPlacedOrder(int orderId, int productId)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM PlacedOrders WITH(NOLOCK) WHERE OrderDetailId=@OrderId and ProductId=@ProductId and IsInquiry=0");
            command.Parameters.AddWithValue("@OrderId", orderId);
            command.Parameters.AddWithValue("@ProductId", productId);
            return this.GetEntity(command);
        }

        internal List<SearchPlacedOrdersDTO> PlacedOrderForSearch()
        {
            SqlCommand sqlCommand = new SqlCommand(@"SELECT f.FactoryName,l.ModuleNumber,o.SalesOrderNumber,s.StyleNumber,p.ConfigValue ProductName,po.StartDate,po.EndDate, tc.StartDate ShipmentDate
                FROM PlacedOrders po  with(nolock) 
                INNER JOIN Factories f on f.id=po.FactoryId
                INNER JOIN LineModules l on l.id=po.LineModuleId
                INNER JOIN OrderDetails o on o.id= po.OrderDetailId
                INNER JOIN ConfigurationTableValues p on p.Id = po.ProductId 
                INNER JOIN StyleDetails s on s.Id = o.StyleId
				inner join TnACalendars tc on tc.OrderDetailsId = po.OrderDetailId and tc.ProductId = po.ProductId and tc.TnADefaultId = 21");           

            try
            {
                return SearchPlacedOrdersDTO.Load(_dataAccess.GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
