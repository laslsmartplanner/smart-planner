﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.SystemConfigurations
{
    public class ConfigurationTable : EntityBase<ConfigurationTable>
    {
        public override string TableName => "ConfigurationTables";

        public string ConfigName { get; set; }
        public int ConfigId { get; set; }
        public int DisplayOrder { get; set; }
    }
}
