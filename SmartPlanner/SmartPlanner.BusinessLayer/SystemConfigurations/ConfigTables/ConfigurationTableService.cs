﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.SystemConfigurations
{
    public class ConfigurationTableService : ServiceBase<ConfigurationTable>
    {
        private ConfigurationTableDA _dataAccess = new ConfigurationTableDA();
        public List<ConfigurationTable> ListConfigurationTables()
        {
            return _dataAccess.ListConfigurationTables();
        }
        public ConfigurationTable GetByConfigId(int configId)
        {
            return _dataAccess.GetByConfigId(configId);
        }
    }
}
