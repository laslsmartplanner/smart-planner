﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.SystemConfigurations
{
    internal class ConfigurationTableDA : EntityDataAccessBase<ConfigurationTable>
    {
        internal List<ConfigurationTable> ListConfigurationTables()
        {
            return this.GetList("DisplayOrder");
        }

        internal ConfigurationTable GetByConfigId(int configId)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM ConfigurationTables WHERE ConfigId=@Id");
            command.Parameters.AddWithValue("@Id", configId);
            return this.GetEntity(command);
        }
    }

}
