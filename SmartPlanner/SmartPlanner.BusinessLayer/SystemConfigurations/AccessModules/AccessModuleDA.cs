﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.SystemConfigurations
{
    internal class AccessModuleDA : EntityDataAccessBase<AccessModule>
    {
        internal AccessModule GetAccessModuleByModuleNumber(int moduleNumber)
        {
            SqlCommand sqlCommand = new SqlCommand("Select * From SysConfig_AccessModules Where ModuleNumber = @Id");
            sqlCommand.Parameters.AddWithValue("@Id", moduleNumber);
            return this.GetEntity(sqlCommand);
        }
    }
}
