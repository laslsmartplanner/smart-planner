﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.SystemConfigurations.AccessModules
{
    public class AccessModuleService
    {
        private AccessModuleDA _dataAccess = new AccessModuleDA();
        public AccessModule GetAccessModuleById(int id)
        {
            return _dataAccess.GetEntityById(id);
        }
        public AccessModule GetAccessModuleByModuleNumber(int moduleNumber)
        {
            return _dataAccess.GetAccessModuleByModuleNumber(moduleNumber);
        }
    }
}
