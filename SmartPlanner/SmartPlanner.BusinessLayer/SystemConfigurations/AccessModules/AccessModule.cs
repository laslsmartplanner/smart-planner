﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer.SystemConfigurations
{
    public class AccessModule : EntityBase<AccessModule>
    {
        //public int Id { get; set; }
        public string ModuleName { get; set; }
        public int ModuleNumber { get; set; }
        public override string TableName => "SysConfig_AccessModules";
    }
}
