﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class StyleOperationService : ServiceBase<StyleOperation>
    {
        private StyleOperationDA _dataAccess = new StyleOperationDA();

        public bool Save(StyleOperation styleOperation)
        {
            if (styleOperation.Id > 0)
                _dataAccess.Update(styleOperation);
            else
            {
                if (!this.IsValidToSave(styleOperation)) return false;

                _dataAccess.Add(styleOperation);
            }
            //Updating the product SMV style product
            StyleProductService styleProductService = new StyleProductService();

            StyleProduct styleProduct = styleProductService.GetEntityById(styleOperation.StyleProductId);
            styleProduct.ProductionSMV = _dataAccess.GetTotalSMVForStyleProductId(styleOperation.StyleProductId);
            styleProductService.Save(styleProduct);

            return true;
        }

        public List<StyleOperation> ListForStyleProductId(int styleProductId)
        {
            return _dataAccess.ListForStyleProductId(styleProductId);
        }

        public decimal GetTotalSMVForStyleProductId(int styleProductId)
        {
            return _dataAccess.GetTotalSMVForStyleProductId(styleProductId);
        }

        public void DeleteOperation(int styleProductId, int styleOperationId)
        {
            _dataAccess.DeleteById(styleOperationId);

            //Updating the product SMV style product
            StyleProductService styleProductService = new StyleProductService();

            StyleProduct styleProduct = styleProductService.GetEntityById(styleProductId);
            styleProduct.ProductionSMV = _dataAccess.GetTotalSMVForStyleProductId(styleProductId);
            styleProductService.Save(styleProduct);
        }

    }
}
