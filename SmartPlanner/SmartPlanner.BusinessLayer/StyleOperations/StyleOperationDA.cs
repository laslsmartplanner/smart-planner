﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class StyleOperationDA : EntityDataAccessBase<StyleOperation>
    {
        private DataAccess _dataAccess = new DataAccess();


        private void SetParameters(SqlCommand command, StyleOperation styleOperation)
        {
            command.Parameters.AddWithValue("@StyleProductId", styleOperation.StyleProductId);
            command.Parameters.AddWithValue("@OperationId", styleOperation.OperationId);
            command.Parameters.AddWithValue("@SMV", styleOperation.SMV);
        }

        internal bool Add(StyleOperation styleOperation)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO StyleProducts_Operations (StyleProductId,OperationId,SMV,CreatedById, CreatedOn)
                                                                                                VALUES(@StyleProductId,@OperationId,@SMV, @CreatedById, @CreatedOn)");

            this.SetParameters(command, styleOperation);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                styleOperation.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(StyleOperation styleOperation)
        {
            SqlCommand command = new SqlCommand(@"UPDATE StyleProducts_Operations
                                                SET StyleProductId =@StyleProductId , OperationId =@OperationId, SMV=@SMV , UpdatedById = @UpdatedById , UpdatedOn = @UpdatedOn 
                                                WHERE     (Id = @Id)");
            command.Parameters.AddWithValue("@Id", styleOperation.Id);
            this.SetParameters(command, styleOperation);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }


        internal List<StyleOperation> ListForStyleProductId(int styleProductId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select spo.* FROM StyleProducts_Operations spo INNER JOIN Operations o on spo.OperationId = o.Id WHERE StyleProductId=@Id ORDER BY o.OperationName");
            sqlCommand.Parameters.AddWithValue("@Id", styleProductId);

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal bool IsOperationExists(int styleProductId, int operationId)
        {
            SqlCommand command = new SqlCommand(@"SELECT Id FROM StyleProducts_Operations WITH(NOLOCK) WHERE StyleProductId=@StyleProductId AND OperationId=@OperationId");
            command.Parameters.AddWithValue("@StyleProductId", styleProductId);
            command.Parameters.AddWithValue("@OperationId", operationId);

            return _dataAccess.ExecuteScalar(command) != null;
        }

        internal decimal GetTotalSMVForStyleProductId(int styleProductId)
        {
            decimal smvTotalVal = 0;

            SqlCommand command = new SqlCommand(@"SELECT isnull(SUM(SMV),0) FROM StyleProducts_Operations WITH(NOLOCK) WHERE StyleProductId=@StyleProductId");
            command.Parameters.AddWithValue("@StyleProductId", styleProductId);

            try
            {
                object smvTotal = _dataAccess.ExecuteScalar(command);

                if (smvTotal == null)
                    smvTotalVal = 0;
                else
                    smvTotalVal = Convert.ToDecimal(smvTotal);
            }
            catch (Exception ex)
            {
                throw;
            }

            return smvTotalVal;
        }

    }
}
