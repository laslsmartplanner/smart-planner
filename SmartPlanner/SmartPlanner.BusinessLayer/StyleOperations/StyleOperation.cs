﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class StyleOperation : EntityBase<StyleOperation>
    {
        private Operation _operation = null;

        public override string TableName => "StyleProducts_Operations";

        [Required]
        [Display(Name ="Operation")]
        public int OperationId { get; set; }

        [Required]
        [Display(Name = "Style Product")]
        public int StyleProductId { get; set; }

        [Display(Name = "SMV")]
        public decimal SMV { get; set; }

        public Operation Operation
        {
            get
            {
                if (this._operation == null && this.OperationId > 0)
                    this._operation = new OperationService().GetEntityById(this.OperationId);
                return this._operation;
            }
        }

        internal override void ValidateOnSave()
        {
            if (new StyleOperationDA().IsOperationExists(this.StyleProductId, this.OperationId))
                this.BrokenBusinessRules.Add("Operation already added to this product. Please select another.");
        }

    }
}
