﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class CalendarDay 
    {
        public int Id { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End{ get; set; }
        public string Title{ get; set; }
        public double? StartValue { get; set; }
        public double? EndValue { get; set; }
        public bool HalfDay { get; set; }
        public string BackColor { get; set; }
        public decimal OtMinutes { get; set; }

        internal static List<CalendarDay> Load(DataTable data)
        {
            List<CalendarDay> list = new List<CalendarDay>();
            foreach (DataRow row in data.Rows)
            {
                list.Add(new CalendarDay()
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Title = Convert.ToString(row["Title"]),
                    Start = Convert.ToDateTime(row["Start"]),
                    End = Convert.ToDateTime(row["End"]),
                    HalfDay = Convert.ToBoolean(row["HalfDay"]),
                    BackColor = Convert.ToString(row["BackColor"])
                });
            }
            return list;
        }

    }
}
