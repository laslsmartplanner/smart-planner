﻿using SmartPlanner.BusinessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SmartPlanner.BusinessLayer.Common.Enums;

namespace SmartPlanner.BusinessLayer
{
    public class CalendarDayService
    {
        private CalendarDayDA _dataAccess = new CalendarDayDA();
        public bool Save(CalendarDay calendarDay)
        {
            if (calendarDay.Start.HasValue && (calendarDay.Start.Value.DayOfWeek == DayOfWeek.Saturday || calendarDay.Start.Value.DayOfWeek == DayOfWeek.Sunday))
                _dataAccess.DeleteWorkingWeekEnd(calendarDay.Start.Value);
            if (calendarDay.Id > 0)
                return _dataAccess.Update(calendarDay);
            if( _dataAccess.Add(calendarDay))
            {
                if (calendarDay.Start.HasValue)
                {
                    List<Role> roles = new RoleService().GetList();

                    List<Role> receivers = roles.Where(x => x.Id == (int)EnumRoles.PlaningMgr || x.Id == (int)EnumRoles.FM || x.Id == (int)EnumRoles.CEO).ToList();
                    
                    foreach(Role role in receivers)
                        new Email().SendEmailNotification(new EmailNotificationDTO() { Holiday = calendarDay.Start.Value.ToString(GV.DisplayDateFormat), User = GV.LoggedUser().UserName },
                                                        calendarDay.HalfDay ? (int)EnumEmailType.CompanyHolidayHDAdded : (int)EnumEmailType.CompanyHolidayFDAdded, role.User.Email);
                }
                return true;
            }
            return false;
        }
        
        public bool Delete(int id, DateTime holiday)
        {
            if (holiday.Date.DayOfWeek == DayOfWeek.Sunday || holiday.Date.DayOfWeek == DayOfWeek.Saturday)
                _dataAccess.AddWorkingWeekEnd(holiday);

            return id > 0 ? _dataAccess.Delete(id) : true;
        }
        public List<CalendarDay> GetCalendarDays(DateTime start, DateTime end)
        {
            return _dataAccess.GetCalendarDays(start, end);
        }
        public bool IsWorkingWeekEnd(DateTime date)
        {
            return _dataAccess.IsWorkingWeekEnd(date);
        }
        public bool IsHoliday(DateTime date)
        {
            return _dataAccess.IsHoliday(date);
        }
        public bool IsHolidayWithWeekEnds(DateTime date,int lineModuleId)
        {
            bool isHoliday = false;
            if (date.Date.DayOfWeek == DayOfWeek.Sunday || date.Date.DayOfWeek == DayOfWeek.Saturday)
                isHoliday = !_dataAccess.IsWorkingWeekEnd(date);
            else
                isHoliday = _dataAccess.IsHoliday(date);

            if (isHoliday)
                isHoliday = !_dataAccess.IsLineWorkingHoliday(date, lineModuleId);
            else
                isHoliday = _dataAccess.IsLineHoliday(date, lineModuleId);

            return isHoliday;
        }
        public CalendarDay GetCalendarDay(DateTime date)
        {
            return _dataAccess.GetCalendarDay(date);
        }
        public List<CalendarDay> GetLineCalendarDays(DateTime start, DateTime end, int lineModuleId)
        {
            return _dataAccess.GetLineCalendarDays(start, end, lineModuleId);
        }

        public bool SaveLineHoliday(CalendarDay calendarDay, string lineModuleId)
        {
            _dataAccess.DeleteLineWorkingHoliday(calendarDay.Start.Value, lineModuleId);
            if (calendarDay.Id > 0)
                return _dataAccess.UpdateLineModuleHoliday(calendarDay, lineModuleId);
            
            if( _dataAccess.AddLineModuleHoliday(calendarDay, lineModuleId))
            {
                if (calendarDay.Start.HasValue)
                {
                    List<Role> roles = new RoleService().GetList();
                    LineModule line = new LineModuleService().GetEntityById(Convert.ToInt32(lineModuleId));

                    List<Role> receivers = roles.Where(x => x.Id == (int)EnumRoles.PlaningMgr || x.Id == (int)EnumRoles.HeadPattern || x.Id == (int)EnumRoles.CEO).ToList();

                    foreach (Role role in receivers)
                        new Email().SendEmailNotification(new EmailNotificationDTO() { Holiday = calendarDay.Start.Value.ToString(GV.DisplayDateFormat), LineModule = line.ModuleNumber, User = GV.LoggedUser().UserName },
                                                        calendarDay.HalfDay ? (int)EnumEmailType.LineHolidayHDAdded : (int)EnumEmailType.LineHolidayFDAdded, role.User.Email);
                }
                return true;
            }
            return false;
        }
        public bool SaveOtMinutes(CalendarDay calendarDay , string lineModuleId)
        {
            return _dataAccess.AddLineModuleOtMinutes(calendarDay, lineModuleId);
        }

        public bool DeleteLineHoliday(int id, DateTime holiday, string lineModuleId)
        {
            if (_dataAccess.IsHoliday(holiday) || (holiday.Date.DayOfWeek == DayOfWeek.Sunday || holiday.Date.DayOfWeek == DayOfWeek.Saturday))
                _dataAccess.AddLineWorkingHoliday(holiday, lineModuleId);

            return id > 0 ? _dataAccess.DeleteLineModuleHoliday(id) : true;
        }
        public double GetCompanyHolidaysCount(DateTime start, DateTime end)
        {
            return _dataAccess.GetCompanyHolidaysCount(start, end);
        }
    }
}
