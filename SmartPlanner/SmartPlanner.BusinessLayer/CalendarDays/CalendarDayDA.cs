﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class CalendarDayDA
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Add(CalendarDay calendarDay)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE CalendarDays WHERE CAST(Start AS DATE) = CAST( @Start AS DATE) AND CAST([End] AS DATE) = CAST(@End AS DATE)");
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@End ", calendarDay.End);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.Parameters.Clear();

                cmd.CommandText  = @"INSERT  INTO CalendarDays(Title, Start, [End],HalfDay, CreatedById, CreatedOn)
                                   VALUES(@Title, @Start, @End,@HalfDay, @CreatedById, @CreatedOn) SELECT SCOPE_IDENTITY()";
                cmd.Parameters.AddWithValue("@Title", calendarDay.Title);
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@End ", calendarDay.End);
                cmd.Parameters.AddWithValue("@HalfDay", calendarDay.HalfDay);
                cmd.Parameters.AddWithValue("@CreatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@CreatedOn ", DateTime.Now);

                object objRet = _dataAccess.ExecuteScalar(cmd);
                if (objRet != null)
                {
                    calendarDay.Id = Convert.ToInt32(objRet);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }

        }
        internal bool Update(CalendarDay calendarDay)
        {
            try
            {    
                SqlCommand cmd = new SqlCommand(@"UPDATE CalendarDays
                                                SET Title =@Title, HalfDay=@HalfDay, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn  WHERE Id=@Id");
                cmd.Parameters.AddWithValue("@Title", calendarDay.Title);
                cmd.Parameters.AddWithValue("@Id", calendarDay.Id);
                cmd.Parameters.AddWithValue("@HalfDay", calendarDay.HalfDay);

                cmd.Parameters.AddWithValue("@UpdatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@UpdatedOn ", DateTime.Now);

                _dataAccess.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

        internal bool DeleteWorkingWeekEnd(DateTime weekEnd)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE FROM  WorkingWeekEnds WHERE(CAST(WeekEnd AS DATE) = CAST(@WeekEnd as DATE))");

                cmd.Parameters.AddWithValue("@WeekEnd", weekEnd);
                _dataAccess.ExecuteNonQuery(cmd);
                cmd.Parameters.Clear();
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

        internal bool Delete(int id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE CalendarDays WHERE Id=@Id");
                cmd.Parameters.AddWithValue("@Id", id);

                _dataAccess.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal List<CalendarDay> GetCalendarDays(DateTime start, DateTime end)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetHolidays");
                cmd.Parameters.AddWithValue("@StartDate", start);
                cmd.Parameters.AddWithValue("@EndDate", end);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return CalendarDay.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal bool AddWorkingWeekEnd(DateTime weekEnd)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE FROM  WorkingWeekEnds WHERE(CAST(WeekEnd AS DATE) = CAST(@WeekEnd as DATE))");
                cmd.Parameters.AddWithValue("@WeekEnd", weekEnd);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.CommandText = @"INSERT INTO WorkingWeekEnds
                                        (WeekEnd, CreatedById, CreatedOn)
                                    VALUES (@WeekEnd, @CreatedById, @CreatedOn)";

                cmd.Parameters.AddWithValue("@CreatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@CreatedOn ", DateTime.Now);

                _dataAccess.ExecuteNonQuery(cmd);

                return true;
            }
            catch (Exception)
            {

                throw;
            }

            
        }
        internal bool IsWorkingWeekEnd(DateTime date)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT Id FROM WorkingWeekEnds WHERE(CAST(WeekEnd AS DATE) = CAST(@WeekEnd as DATE)) SELECT SCOPE_IDENTITY()");
                cmd.Parameters.AddWithValue("@WeekEnd", date);
                object objRet = _dataAccess.ExecuteScalar(cmd);
                return objRet != null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal bool IsHoliday(DateTime date)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT Id FROM CalendarDays WHERE(CAST(Start AS DATE) = CAST(@WeekEnd as DATE)) SELECT SCOPE_IDENTITY()");
                cmd.Parameters.AddWithValue("@WeekEnd", date);
                object objRet = _dataAccess.ExecuteScalar(cmd);
                return objRet != null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal CalendarDay GetCalendarDay(DateTime date)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT *,'' BackColor FROM CalendarDays WHERE(CAST(Start AS DATE) = CAST(@WeekEnd as DATE))");
                cmd.Parameters.AddWithValue("@WeekEnd", date);
                return CalendarDay.Load(_dataAccess.GetDataTable(cmd)).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal List<CalendarDay> GetLineCalendarDays(DateTime start, DateTime end, int lineModuleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetLineHolidays");
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                cmd.Parameters.AddWithValue("@StartDate", start);
                cmd.Parameters.AddWithValue("@EndDate", end);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return CalendarDay.Load(_dataAccess.GetDataTable(cmd));
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal bool DeleteLineWorkingHoliday(DateTime holiday,string lineModuleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE FROM  LineWorkingHolidays WHERE(CAST(WorkingHoliday AS DATE) = CAST(@WeekEnd as DATE)) and LineModuleId=@LineModuleId");

                cmd.Parameters.AddWithValue("@WeekEnd", holiday);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                _dataAccess.ExecuteNonQuery(cmd);
                cmd.Parameters.Clear();
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }
        internal bool UpdateLineModuleHoliday(CalendarDay calendarDay,string lineModuleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"UPDATE LineHolidays
                                                SET Title =@Title, HalfDay=@HalfDay, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn  WHERE Id=@Id and LineModuleId=@LineModuleId");
                cmd.Parameters.AddWithValue("@Title", calendarDay.Title);
                cmd.Parameters.AddWithValue("@Id", calendarDay.Id);
                cmd.Parameters.AddWithValue("@HalfDay", calendarDay.HalfDay);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);

                cmd.Parameters.AddWithValue("@UpdatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@UpdatedOn ", DateTime.Now);

                _dataAccess.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal bool AddLineModuleHoliday(CalendarDay calendarDay,string lineModuleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE LineHolidays WHERE CAST(Start AS DATE) = CAST( @Start AS DATE) AND CAST([End] AS DATE) = CAST(@End AS DATE) and LineModuleId=@LineModuleId");
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@End ", calendarDay.End);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.Parameters.Clear();

                cmd = new SqlCommand(@"DELETE LineOtMinutes WHERE CAST(WorkingDate AS DATE) = CAST( @Start AS DATE) AND LineModuleId=@LineModuleId");
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.Parameters.Clear();

                cmd.CommandText = @"INSERT  INTO LineHolidays(Title, Start, [End],HalfDay,LineModuleId, CreatedById, CreatedOn)
                                   VALUES(@Title, @Start, @End,@HalfDay,@LineModuleId, @CreatedById, @CreatedOn) SELECT SCOPE_IDENTITY()";
                cmd.Parameters.AddWithValue("@Title", calendarDay.Title);
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@End ", calendarDay.End);
                cmd.Parameters.AddWithValue("@HalfDay", calendarDay.HalfDay);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                cmd.Parameters.AddWithValue("@CreatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@CreatedOn ", DateTime.Now);

                object objRet = _dataAccess.ExecuteScalar(cmd);
                if (objRet != null)
                {
                    calendarDay.Id = Convert.ToInt32(objRet);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }

        }

        internal bool AddLineWorkingHoliday(DateTime holiday,string lineModuleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE FROM  LineWorkingHolidays WHERE(CAST(WorkingHoliday AS DATE) = CAST(@WeekEnd as DATE)) and LineModuleId=@LineModuleId");
                cmd.Parameters.AddWithValue("@WeekEnd", holiday);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.CommandText = @"INSERT INTO LineWorkingHolidays
                                        (WorkingHoliday,LineModuleId, CreatedById, CreatedOn)
                                    VALUES (@WeekEnd,@LineModuleId, @CreatedById, @CreatedOn)";

                cmd.Parameters.AddWithValue("@CreatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@CreatedOn ", DateTime.Now);

                _dataAccess.ExecuteNonQuery(cmd);

                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal bool DeleteLineModuleHoliday(int id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE LineHolidays WHERE Id=@Id");
                cmd.Parameters.AddWithValue("@Id", id);

                _dataAccess.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal double GetCompanyHolidaysCount(DateTime start, DateTime end)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetCompanyHolidaysCount");
                cmd.Parameters.AddWithValue("@Start", start);
                cmd.Parameters.AddWithValue("@End", end);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                object ret = _dataAccess.ExecuteScalar(cmd);
                return ret == null ? 0 : Convert.ToDouble(ret);
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal bool IsLineWorkingHoliday(DateTime date,int lineId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT Id FROM LineWorkingHolidays WITH(NOLOCK) WHERE(CAST(WorkingHoliday AS DATE) = CAST(@WeekEnd as DATE)) and LineModuleId=@LineModuleId SELECT SCOPE_IDENTITY()");
                cmd.Parameters.AddWithValue("@WeekEnd", date);
                cmd.Parameters.AddWithValue("@LineModuleId", lineId);
                object objRet = _dataAccess.ExecuteScalar(cmd);
                return objRet != null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        internal bool IsLineHoliday(DateTime date, int lineId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT Id FROM LineHolidays WITH(NOLOCK) WHERE(CAST(Start AS DATE) = CAST(@WeekEnd as DATE)) and LineModuleId=@LineModuleId SELECT SCOPE_IDENTITY()");
                cmd.Parameters.AddWithValue("@WeekEnd", date);
                cmd.Parameters.AddWithValue("@LineModuleId", lineId);
                object objRet = _dataAccess.ExecuteScalar(cmd);
                return objRet != null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal bool AddLineModuleOtMinutes(CalendarDay calendarDay, string lineModuleId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE CalendarDays WHERE CAST(Start AS DATE) = CAST( @Start AS DATE) AND CAST([End] AS DATE) = CAST(@End AS DATE)");
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@End ", calendarDay.End);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.Parameters.Clear();

                cmd = new SqlCommand(@"DELETE LineOtMinutes WHERE CAST(WorkingDate AS DATE) = CAST( @Start AS DATE) AND LineModuleId=@LineModuleId");
                cmd.Parameters.AddWithValue("@Start ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                _dataAccess.ExecuteNonQuery(cmd);

                cmd.Parameters.Clear();

                cmd.CommandText = @"INSERT  INTO LineOtMinutes(OtMinutes, WorkingDate,LineModuleId, CreatedById, CreatedOn)
                                   VALUES(@OtMinutes, @WorkingDate,@LineModuleId, @CreatedById, @CreatedOn) SELECT SCOPE_IDENTITY()";
                cmd.Parameters.AddWithValue("@OtMinutes", calendarDay.OtMinutes);
                cmd.Parameters.AddWithValue("@WorkingDate ", calendarDay.Start);
                cmd.Parameters.AddWithValue("@LineModuleId", lineModuleId);
                cmd.Parameters.AddWithValue("@CreatedById ", GV.LoggedUser().Id);
                cmd.Parameters.AddWithValue("@CreatedOn ", DateTime.Now);

                object objRet = _dataAccess.ExecuteScalar(cmd);
                if (objRet != null)
                {
                    calendarDay.Id = Convert.ToInt32(objRet);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
