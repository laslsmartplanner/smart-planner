﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class SubCategoryDA: EntityDataAccessBase<SubCategory>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Add(SubCategory subCategory)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO SubCategories(CategoryId, SubCategoryName)
                                                  VALUES (@CategoryId, @SubCategoryName)");
            this.SetParameters(command, subCategory);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                subCategory.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal bool Update(SubCategory subCategory)
        {
            SqlCommand command = new SqlCommand(@"UPDATE    SubCategories
                                            SET  CategoryId = @CategoryId , SubCategoryName = @SubCategoryName 
                                            WHERE     (Id = @Id )");
            command.Parameters.AddWithValue("@Id", subCategory.Id);
            this.SetParameters(command, subCategory);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private void SetParameters(SqlCommand command,SubCategory subCategory)
        {
            command.Parameters.AddWithValue("@CategoryId", subCategory.CategoryId);
            command.Parameters.AddWithValue("@SubCategoryName", subCategory.SubCategoryName);
        }

        internal List<SubCategory> ListForCategoryId(int categoryId)
        {
            SqlCommand sqlCommand = new SqlCommand(@"Select * 
                                                    From SubCategories l WITH(NOLOCK)
                                                    Where CategoryId = @CategoryId
                                                    order by l.SubCategoryName ");
            sqlCommand.Parameters.AddWithValue("@CategoryId", categoryId);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
