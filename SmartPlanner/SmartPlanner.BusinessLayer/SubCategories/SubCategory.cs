﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class SubCategory : EntityBase<SubCategory>
    {
        private ConfigurationTableValue _category = null;
        public override string TableName => "SubCategories";
        public override string NameField => "SubCategoryName";

        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Sub Category")]
        public string SubCategoryName { get; set; }

        public ConfigurationTableValue Category
        {
            get
            {
                if (this._category == null && this.CategoryId > 0)
                    this._category = new ConfigurationTableValueService().GetEntityById(this.CategoryId);
                return this._category;
            }
        }

        public List<ConfigurationTableValue> Categories { get; set; }
    }
}
