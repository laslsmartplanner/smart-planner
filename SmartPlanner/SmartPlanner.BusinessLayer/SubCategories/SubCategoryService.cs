﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class SubCategoryService : ServiceBase<SubCategory>
    {
        private SubCategoryDA _dataAccess = new SubCategoryDA();
        public bool Save(SubCategory subCategory)
        {
            if (!this.IsValidToSave(subCategory)) return false;
            if (subCategory.Id > 0)
                _dataAccess.Update(subCategory);
            else
                _dataAccess.Add(subCategory);
            return true;
        }
        public List<SubCategory> GetList(string orderBy = "")
        {
            return _dataAccess.GetList(orderBy);
        }

        public List<SubCategory> ListSubCategories(string orderBy, bool addEmpty)
        {
            return _dataAccess.GetList(orderBy, addEmpty);
        }

        public List<SubCategory> ListForCategoryId(int categoryId)
        {
            return _dataAccess.ListForCategoryId(categoryId);
        }
    }
}
