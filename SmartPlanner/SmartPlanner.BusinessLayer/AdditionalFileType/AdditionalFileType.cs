﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class AdditionalFileType : EntityBase<AdditionalFileType>
    {
        public override string TableName => "AdditionalFileType";

        public string TypeName { get; set; }

    }
}
