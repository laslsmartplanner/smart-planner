﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class AdditionalFileTypeService : ServiceBase<AdditionalFileType>
    {
        private AdditionalFileTypeDA _dataAccess = new AdditionalFileTypeDA();

        public List<AdditionalFileType> ListAdditionalFileType()
        {
            return _dataAccess.GetList();
        }
    }
}
