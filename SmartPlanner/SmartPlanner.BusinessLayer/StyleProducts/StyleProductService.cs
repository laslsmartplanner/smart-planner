﻿using SmartPlanner.BusinessLayer.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartPlanner.BusinessLayer
{
    public class StyleProductService : ServiceBase<StyleProduct>
    {
        private StyleProductDA _dataAccess = new StyleProductDA();

        public bool Save(StyleProduct styleProduct)
        {
            if (!this.IsValidToSave(styleProduct)) return false;

            if (styleProduct.Id > 0)
            {
                if (styleProduct.ThreadSheetFile != null && !styleProduct.ThreadSheetFileName.Equals(styleProduct.OldThreadSheetFileName))
                    styleProduct.ThreadSheetFileName = this.SaveFile(styleProduct.ThreadSheetFile, styleProduct.Id, "TnA");

                if (styleProduct.TrimConsumptionFile != null && !styleProduct.TrimConsumptionFileName.Equals(styleProduct.OldTrimConsumptionFileName))
                    styleProduct.TrimConsumptionFileName = this.SaveFile(styleProduct.TrimConsumptionFile, styleProduct.Id, "TnA");

                if (styleProduct.PPRAReportFile != null && !styleProduct.PPRAReportFileName.Equals(styleProduct.OldPPRAReportFileName))
                    styleProduct.PPRAReportFileName = this.SaveFile(styleProduct.PPRAReportFile, styleProduct.Id, "TnA");

            }
                

            if (styleProduct.Id > 0)
                _dataAccess.Update(styleProduct);
            else
                _dataAccess.Add(styleProduct);

            return true;
        }

        private string SaveFile(HttpPostedFileBase file, int styleNo, string folderName)
        {

            string uploadPath = Path.Combine(HttpContext.Current.Server.MapPath("~"), ConfigurationManager.AppSettings["StyleSkethDirectory"].ToString());
            uploadPath = Path.Combine(uploadPath, styleNo.ToString());

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            uploadPath = Path.Combine(uploadPath, folderName);

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            string fileName = file.FileName;

            int count = 1;
            string fileNameOnly = Path.GetFileNameWithoutExtension(fileName);
            string extension = Path.GetExtension(fileName);

            while (File.Exists(Path.Combine(uploadPath, fileName)))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                fileName = Path.Combine(uploadPath, tempFileName + extension);
            }
            fileName = Path.Combine(uploadPath, fileName);

            file.SaveAs(fileName);
            return Path.GetFileName(fileName);
        }

        public bool SaveAdditionalInformations(HttpPostedFileBase file, int styleNo, int fileTypeId)
        {
            string fileName = this.SaveFile(file, styleNo, "Additional");

            StyleAdditionalFile additionalFile = new StyleAdditionalFile();
            additionalFile.FileName = fileName;
            additionalFile.StyleProductId = styleNo;
            additionalFile.FileTypeId = fileTypeId;

            return new StyleAdditionalFileService().Save(additionalFile);
        }

        public List<StyleProduct> GetList(string orderBy = "")
        {
            return _dataAccess.GetList(orderBy);
        }

        public List<StyleProduct> ListForBuyerId(int buyerId)
        {
            return _dataAccess.ListForBuyerId(buyerId);
        }

        public List<StyleProduct> ListForStyleId(int styleId)
        {
            return _dataAccess.ListForStyleId(styleId);
        }

        public List<IdNameDTO> ListIdNameForStyleId(int styleId)
        {
            return _dataAccess.ListIdNameForStyleId(styleId);
        }

        public List<IdNameDTO> ListProductIdNameForStyleId(int styleId)
        {
            return _dataAccess.ListProductIdNameForStyleId(styleId);
        }

        public List<ReportDTOStyleSMV> ListStyleSMV(int buyerId, int customerId, int styleDetailId)
        {
            return _dataAccess.ListStyleSMV(buyerId, customerId, styleDetailId);
        }

        public StyleProduct GetByStyleAndProductId(int styleId, int productId)
        {
            return _dataAccess.GetByStyleAndProductId(styleId, productId);
        }

        public ProductionCostSMVDTO GetSMVSumForStyleId(int styleId)
        {
            return _dataAccess.GetSMVSumForStyleId(styleId);
        }

        public List<IdNameDTO> GetChangeOverForBoard(int factoryId, int styleProductId)
        {
            return _dataAccess.GetChangeOverForBoard(factoryId, styleProductId);
        }
    }
}
