﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.BusinessLayer.DTOs;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class StyleProductDA : EntityDataAccessBase<StyleProduct>
    {
        private DataAccess _dataAccess = new DataAccess();

        internal bool Add(StyleProduct styleDetail)
        {
            SqlCommand command = new SqlCommand(@"INSERT INTO StyleProducts(StyleId,ProductTypeId, ProductGroupId, ProductCategoryId, CostSMV,ProductionSMV,ProductHierarchy ,
                                                    ProductId,SpecialMachine,SpecialProcess,ThreadSheet,TrimConsumption,PPRAReport,SubCategoryId,CategoryId,CM, CreatedById, CreatedOn)
                                                    VALUES (@StyleId,@ProductTypeId, @ProductGroupId, @ProductCategoryId, @CostSMV,@ProductionSMV,@ProductHierarchy ,
                                                    @ProductId,@SpecialMachine,@SpecialProcess,@ThreadSheet,@TrimConsumption,@PPRAReport,@SubCategoryId,@CategoryId,@CM, @CreatedById, @CreatedOn)");
            this.SetParameters(command, styleDetail);
            this.SetCreatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;

                //this.SaveAdditionalInformations(Convert.ToInt32(ret), styleDetail.AdditionalInformationFileNames);

                styleDetail.SetId(ret);
                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        private void SetParameters(SqlCommand command, StyleProduct styleProduct)
        {
            command.Parameters.AddWithValue("@StyleId", styleProduct.StyleId);
            command.Parameters.AddWithValue("@ProductTypeId", GetNullIfLessOne(styleProduct.ProductTypeId));
            command.Parameters.AddWithValue("@ProductGroupId", GetNullIfLessOne(styleProduct.ProductGroupId));
            command.Parameters.AddWithValue("@ProductCategoryId", GetNullIfLessOne(styleProduct.ProductCategoryId));

            command.Parameters.AddWithValue("@CostSMV", styleProduct.CostSMV);
            command.Parameters.AddWithValue("@ProductionSMV", styleProduct.ProductionSMV);
            command.Parameters.AddWithValue("@ProductHierarchy", styleProduct.ProductHierarchy ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ProductId", GetNullIfLessOne(styleProduct.ProductId));
            
            command.Parameters.AddWithValue("@SpecialMachine", styleProduct.SpecialMachine ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SpecialProcess", styleProduct.SpecialProcess ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@ThreadSheet", styleProduct.ThreadSheetFileName ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@TrimConsumption", styleProduct.TrimConsumptionFileName ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@PPRAReport", styleProduct.PPRAReportFileName ?? (object)DBNull.Value);
            
            command.Parameters.AddWithValue("@SubCategoryId", GetNullIfLessOne(styleProduct.SubCategoryId));
            command.Parameters.AddWithValue("@CategoryId", GetNullIfLessOne(styleProduct.CategoryId));

            command.Parameters.AddWithValue("@CM", styleProduct.CM);
        }

        internal bool Update(StyleProduct styleDetail)
        {
            SqlCommand command = new SqlCommand(@"UPDATE StyleProducts
                                                SET StyleId=@StyleId,ProductTypeId=@ProductTypeId, ProductGroupId=@ProductGroupId, ProductCategoryId=@ProductCategoryId, CostSMV=@CostSMV,ProductionSMV=@ProductionSMV,ProductHierarchy =@ProductHierarchy ,
                                                    ProductId=@ProductId,SpecialMachine=@SpecialMachine,SpecialProcess=@SpecialProcess,ThreadSheet=@ThreadSheet,TrimConsumption=@TrimConsumption,PPRAReport=@PPRAReport,SubCategoryId=@SubCategoryId ,CategoryId=@CategoryId, CM=@CM, UpdatedById =@UpdatedById , UpdatedOn =@UpdatedOn WHERE (Id = @Id)");
            command.Parameters.AddWithValue("@Id", styleDetail.Id);
            this.SetParameters(command, styleDetail);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.BeginTransaction();
                _dataAccess.ExecuteNonQuery(command);
                //this.SaveAdditionalInformations(styleDetail.Id, styleDetail.AdditionalInformationFileNames);
                _dataAccess.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                _dataAccess.RollbackTransaction();
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }

        internal DataTable GetAdditionalInformationFileNames(int styleProductId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * From StyleProducts_AdditionalInformations Where StyleProductId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", styleProductId);
            try
            {
                return new DataAccess().GetDataTable(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<StyleProduct> ListForBuyerId(int buyerId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * From StyleProducts WITH(NOLOCK) Where BuyerId=@BuyerId ORDER BY StyleNumber");
            sqlCommand.Parameters.AddWithValue("@BuyerId", buyerId);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal DataTable GetFeaturesList(int styleProductId)
        {
            SqlCommand sqlCommand = new SqlCommand($"Select * FROM StyleProducts_FeaturesList WHERE StyleProductId=@Id");
            sqlCommand.Parameters.AddWithValue("@Id", styleProductId);
            try
            {
                return new DataAccess().GetDataTable(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<StyleProduct> ListForSMV(int buyerId, int customerId)
        {
            string whereCondition = buyerId>0 ? " BuyerId = @BuyerId" : string.Empty;

            if(customerId>0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " CustomerId = @CustomerId" : " AND CustomerId = @CustomerId";
            }

            //if (categoryId > 0)
            //{
            //    whereCondition += string.IsNullOrEmpty(whereCondition) ? " CategoryId = @CategoryId" : " AND CategoryId = @CategoryId";
            //}

            //if (subCategoryId > 0)
            //{
            //    whereCondition += string.IsNullOrEmpty(whereCondition) ? " SubCategoryId = @SubCategoryId" : " AND SubCategoryId = @SubCategoryId";
            //}

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " WHERE " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"Select * From StyleProducts {whereCondition} ORDER BY StyleNumber");

            if (buyerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@BuyerId", buyerId);
            }

            if (customerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);
            }

            //if (categoryId > 0)
            //{
            //    sqlCommand.Parameters.AddWithValue("@CategoryId", categoryId);
            //}

            //if (subCategoryId > 0)
            //{
            //    sqlCommand.Parameters.AddWithValue("@SubCategoryId", subCategoryId);
            //}

            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<StyleProduct> ListForStyleId(int styleId)
        {
            SqlCommand sqlCommand = new SqlCommand($"SELECT * FROM StyleProducts WITH(NOLOCK) WHERE StyleId=@StyleId");
            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            try
            {
                return this.GetList(sqlCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<IdNameDTO> ListIdNameForStyleId(int styleId)
        {
            SqlCommand sqlCommand = new SqlCommand($"SELECT sp.Id, p.ConfigValue as Name FROM StyleProducts sp WITH(NOLOCK) " +
                $"INNER JOIN ConfigurationTableValues p WITH(NOLOCK) on p.id = sp.ProductId WHERE StyleId=@StyleId");

            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            try
            {
                return IdNameDTO.Load(_dataAccess.GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }


        internal List<IdNameDTO> ListProductIdNameForStyleId(int styleId)
        {
            SqlCommand sqlCommand = new SqlCommand(@"SELECT p.Id, p.ConfigValue as Name FROM StyleProducts sp WITH(NOLOCK) 
                                                     INNER JOIN ConfigurationTableValues p WITH(NOLOCK) on p.id = sp.ProductId
                                                     WHERE StyleId=@StyleId");

            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            try
            {
                return IdNameDTO.Load(_dataAccess.GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<ReportDTOStyleSMV> ListStyleSMV(int buyerId, int customerId, int styleDetailId)
        {
            string whereCondition = buyerId > 0 ? " BuyerId = @BuyerId" : string.Empty;

            if (customerId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " CustomerId = @CustomerId" : " AND CustomerId = @CustomerId";
            }

            if (styleDetailId > 0)
            {
                whereCondition += string.IsNullOrEmpty(whereCondition) ? " StyleId = @StyleId" : " AND StyleId = @StyleId";
            }

            whereCondition = string.IsNullOrEmpty(whereCondition) ? string.Empty : " WHERE " + whereCondition;

            SqlCommand sqlCommand = new SqlCommand($"SELECT StyleId,ProductId,ProductionSMV FROM StyleProducts sp WITH(NOLOCK) INNER JOIN StyleDetails sd WITH(NOLOCK) ON sd.Id = sp.StyleId {whereCondition} ORDER BY sd.StyleNumber");

            if (buyerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@BuyerId", buyerId);
            }

            if (customerId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);
            }

            if (styleDetailId > 0)
            {
                sqlCommand.Parameters.AddWithValue("@StyleId", styleDetailId);
            }

            try
            {
                return ReportDTOStyleSMV.Load(_dataAccess.GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal StyleProduct GetByStyleAndProductId(int styleId,int productId)
        {
            SqlCommand sqlCommand = new SqlCommand($"SELECT * FROM StyleProducts WITH(NOLOCK) WHERE StyleId=@StyleId AND ProductId=@ProductId");
            sqlCommand.Parameters.AddWithValue("@StyleId", styleId);
            sqlCommand.Parameters.AddWithValue("@ProductId", productId);
            try
            {
                return this.GetList(sqlCommand).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal ProductionCostSMVDTO GetSMVSumForStyleId(int styleId)
        {
            SqlCommand sqlCommand = new SqlCommand(@"SELECT ISNULL(SUM(ProductionSMV),0) as ProductionSMV,ISNULL(SUM(CostSMV),0) as CostSMV,ISNULL(SUM(CM),0) as CM from StyleProducts WITH(NOLOCK)
                WHERE StyleId = @styleId");

            sqlCommand.Parameters.AddWithValue("@styleId", styleId);

            try
            {
                return ProductionCostSMVDTO.Load(_dataAccess.GetDataTable(sqlCommand)).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<IdNameDTO> GetChangeOverForBoard(int factoryId, int styleProductId)
        {
            SqlCommand sqlCommand = new SqlCommand("GetChangeOverForBoard");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.AddWithValue("@FactoryNameId", factoryId);
            sqlCommand.Parameters.AddWithValue("@SelectedStyleProductId", styleProductId);
            sqlCommand.Parameters.AddWithValue("@UserId", GV.LoggedUser().Id);

            try
            {
                return IdNameDTO.Load(_dataAccess.GetDataTable(sqlCommand));
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
