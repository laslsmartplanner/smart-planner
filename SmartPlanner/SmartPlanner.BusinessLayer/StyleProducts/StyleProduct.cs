﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartPlanner.BusinessLayer
{
    public class StyleProduct : EntityBase<StyleProduct>
    {
        public override string TableName => "StyleProducts";

        private ConfigurationTableValueService _configValueService = new ConfigurationTableValueService();

        private ConfigurationTableValue _productType;
        private ConfigurationTableValue _productGroup;
        private ConfigurationTableValue _productCategory;
        private ConfigurationTableValue _category;
        private ConfigurationTableValue _product;
        private SubCategory _subCategory;
        private StyleDetail _styleDetail;
        private StyleOperation _operation;

        [Display(Name = "Style")]
        public int StyleId { get; set; }

        [Display(Name = "Product")]
        public int ProductId { get; set; }

        [Display(Name = "Product Type")]
        public int ProductTypeId { get; set; }

        [Display(Name = "Product Group")]
        public int ProductGroupId { get; set; }

        [Display(Name = "Product Hierarchy")]
        public string ProductHierarchy { get; set; }

        [Display(Name = "Product Category")]
        public int ProductCategoryId { get; set; }

        [Display(Name = "Product Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Product Sub Category")]
        public int SubCategoryId { get; set; }

        [Display(Name = "Feature")]
        public int FeatureId { get; set; }

        [Display(Name = "Features")]
        public List<StyleFeature> FeatureList { get; set; }

        [Display(Name = "Additional Files")]
        public IEnumerable<HttpPostedFileBase> AdditionalInformationFiles { get; set; }

        [Display(Name = "Additional File Names")]
        public List<StyleAdditionalFile> AdditionalInformationFileNames { get; set; }

        [Display(Name = "File Names")]
        public StyleAdditionalFile AdditionalInformationFileName { get; set; }

        [Display(Name = "File Type")]
        public int FileTypeId { get; set; }

        [Display(Name = "File Type")]
        public List<AdditionalFileType> FileTypeList { get; set; }

        [Display(Name = "Operation")]
        public int OperationId { get; set; }

        [Display(Name = "Operation")]
        public StyleOperation Operation
        {
            get
            {
                if (this._operation == null && this.OperationId > 0)
                    this._operation = new StyleOperationService().GetEntityById(this.OperationId);
                return this._operation;
            }
        }

        [Display(Name = "Operations")]
        public List<Operation> OperationsList { get; set; }

        public List<StyleOperation> OperationBreakdowns { get; set; }

        [Display(Name = "Production SMV")]
        public decimal ProductionSMV { get; set; }

        [Display(Name = "Cost SMV")]
        public decimal CostSMV { get; set; }

        [Display(Name = "CM")]
        public decimal CM { get; set; }

        [Display(Name = "Special Process or Skill Requirement")]
        public string SpecialProcess { get; set; }

        [Display(Name = "Special Machine")]
        public string SpecialMachine { get; set; }

        [Display(Name = "Thread Sheet File")]
        public HttpPostedFileBase ThreadSheetFile { get; set; }

        [Display(Name = "Thread Sheet File Name")]
        public string ThreadSheetFileName { get; set; }

        [Display(Name = "Thread Sheet File Name")]
        public string ThreadSheetFilePath { get; set; }

        [Display(Name = "Thread Sheet File Name")]
        public string OldThreadSheetFileName { get; set; }

        [Display(Name = "Trim Consumption File")]
        public HttpPostedFileBase TrimConsumptionFile { get; set; }

        [Display(Name = "Trim Consumption File Name")]
        public string TrimConsumptionFileName { get; set; }

        [Display(Name = "Trim Consumption File Name")]
        public string TrimConsumptionFilePath { get; set; }

        [Display(Name = "Trim Consumption File Name")]
        public string OldTrimConsumptionFileName { get; set; }

        [Display(Name = "PPRA Report File")]
        public HttpPostedFileBase PPRAReportFile { get; set; }

        [Display(Name = "PPRA Report File Name")]
        public string PPRAReportFileName { get; set; }

        [Display(Name = "PPRA Report File Name")]
        public string PPRAReportFilePath { get; set; }

        [Display(Name = "PPRA Report File Name")]
        public string OldPPRAReportFileName { get; set; }

        
        public ConfigurationTableValue Category
        {
            get
            {
                if (this._category == null && this.CategoryId > 0)
                    this._category = _configValueService.GetEntityById(this.CategoryId);
                return this._category;
            }
        }
        public ConfigurationTableValue ProductType
        {
            get
            {
                if (this._productType == null && this.ProductTypeId > 0)
                    this._productType = _configValueService.GetEntityById(this.ProductTypeId);
                return this._productType;
            }
        }
        public ConfigurationTableValue ProductGroup
        {
            get
            {
                if (this._productGroup == null && this.ProductGroupId > 0)
                    this._productGroup = _configValueService.GetEntityById(this.ProductGroupId);
                return this._productGroup;
            }
        }
        public ConfigurationTableValue ProductCategory
        {
            get
            {
                if (this._productCategory == null && this.ProductCategoryId > 0)
                    this._productCategory = _configValueService.GetEntityById(this.ProductCategoryId);
                return this._productCategory;
            }
        }

        public ConfigurationTableValue Product
        {
            get
            {
                if (this._product == null && this.ProductId > 0)
                    this._product = _configValueService.GetEntityById(this.ProductId);
                return this._product;
            }
        }

        public SubCategory SubCategory
        {
            get
            {
                if (this._subCategory == null && this.SubCategoryId > 0)
                    this._subCategory = new SubCategoryService().GetEntityById(this.SubCategoryId);
                return this._subCategory;
            }
        }

        [Display(Name = "Product")]
        public string ProductName
        {
            get
            {
                return this.Product?.ConfigValue ?? string.Empty;
            }
        }

        public StyleDetail StyleDetail
        {
            get
            {
                if (this._styleDetail == null && this.StyleId > 0)
                    this._styleDetail = new StyleDetailService().GetEntityById(this.StyleId);
                return this._styleDetail;
            }
        }

        public List<ConfigurationTableValue> ProductTypes { get; set; }
        public List<ConfigurationTableValue> ProductGroups { get; set; }
        public List<ConfigurationTableValue> ProductCategories { get; set; }
        public List<ConfigurationTableValue> Categories { get; set; }
        public List<ConfigurationTableValue> Products { get; set; }
        public List<SubCategory> SubCategories { get; set; }
        public List<ConfigurationTableValue> FeaturesList { get; set; }

        internal override void LoadAdditionalProperties(DataRow row)
        {
            string uploadPath = Path.Combine(ConfigurationManager.AppSettings["StyleSkethDirectory"].ToString());
            uploadPath = Path.Combine(uploadPath, Convert.ToInt32(row["Id"]).ToString());

            string additionalPath = Path.Combine(uploadPath, "Additional");

            this.AdditionalInformationFileNames = new StyleAdditionalFileService().ListForStyleProductId(Convert.ToInt32(row["Id"]));

            foreach (StyleAdditionalFile additionalfile in AdditionalInformationFileNames)
            {
                string fileName = additionalfile.FileName;
                additionalfile.FilePath = Path.Combine(additionalPath, fileName);
            }

            this.FeatureList = new StyleFeatureService().ListForStyleProductId(Convert.ToInt32(row["Id"]));         

            this.OperationBreakdowns = new StyleOperationService().ListForStyleProductId(Convert.ToInt32(row["Id"]));

            //Get the image upload path
            string sketchPath = Path.Combine(uploadPath, "Sketch");
            string tnaPath = Path.Combine(uploadPath, "TnA");
            

            this.ThreadSheetFileName = Convert.ToString(row["ThreadSheet"]);
            this.OldThreadSheetFileName = Convert.ToString(row["ThreadSheet"]);
            this.ThreadSheetFilePath = Path.Combine(tnaPath, this.ThreadSheetFileName);

            this.TrimConsumptionFileName = Convert.ToString(row["TrimConsumption"]);
            this.OldTrimConsumptionFileName = Convert.ToString(row["TrimConsumption"]);
            this.TrimConsumptionFilePath = Path.Combine(tnaPath, this.TrimConsumptionFileName);

            this.PPRAReportFileName = Convert.ToString(row["PPRAReport"]);
            this.OldPPRAReportFileName = Convert.ToString(row["PPRAReport"]);
            this.PPRAReportFilePath = Path.Combine(tnaPath, this.PPRAReportFileName);


        }
    }
}
