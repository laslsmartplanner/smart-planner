﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class ConfigurationTableValueService : ServiceBase<ConfigurationTableValue>
    {
        private ConfigurationTableValueDA _dataAccess = new ConfigurationTableValueDA();
        public List<ConfigurationTableValue> ListByConfigId(int configId, bool addEmpty = false)
        {
            return _dataAccess.ListByConfigId(configId, addEmpty);
        }
        public bool Save(ConfigurationTableValue configValue)
        {
            if (!this.IsValidToSave(configValue)) return false;
            if (configValue.Id > 0)
                _dataAccess.Update(configValue);
            else
                _dataAccess.Add(configValue);
            return true;
        }

    }
}
