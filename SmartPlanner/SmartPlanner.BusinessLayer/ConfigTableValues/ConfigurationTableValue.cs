﻿using SmartPlanner.BusinessLayer.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    public class ConfigurationTableValue : EntityBase<ConfigurationTableValue>
    {
        private ConfigurationTable _configTable = null;

        public override string TableName => "ConfigurationTableValues";
        public int ConfigId { get; set; }

        [Required(ErrorMessage = "Value required")]
        public string ConfigValue { get; set; }
        public ConfigurationTable ConfigTable
        {
            get
            {
                if (this._configTable == null && this.ConfigId > 0)
                    this._configTable = new ConfigurationTableService().GetByConfigId(this.ConfigId);
                return this._configTable;
            }
        }

        public List<TnADefault> TnADefaults { get; set; }

        internal override void ValidateOnSave()
        {
            ConfigurationTableValue value = new ConfigurationTableValueDA().GetByValueAndConfigId(this.ConfigValue, this.ConfigId);
            if (value == null)
                return;
            if ((this.Id > 0 && value.Id > 0 && value.Id != this.Id) || (this.Id < 1 && value.Id > 0))
                this.BrokenBusinessRules.Add($"{this.ConfigValue} already exists");
        }

    }
     
}
