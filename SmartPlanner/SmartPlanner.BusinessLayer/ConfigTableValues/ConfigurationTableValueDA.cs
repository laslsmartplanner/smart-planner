﻿using SmartPlanner.BusinessLayer.Common;
using SmartPlanner.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPlanner.BusinessLayer
{
    internal class ConfigurationTableValueDA : EntityDataAccessBase<ConfigurationTableValue>
    {
        private DataAccess _dataAccess = new DataAccess();
        internal List<ConfigurationTableValue> ListByConfigId(int configId, bool addEmpty)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM ConfigurationTableValues WHERE ConfigId=@ConfigId ORDER BY ConfigValue");
            command.Parameters.AddWithValue("@ConfigId", configId);
            List<ConfigurationTableValue> configs = this.GetList(command,addEmpty);
            if (addEmpty && configs.Count > 0)
                configs[0].ConfigValue = "Select One...";
            return configs;
        }
        internal bool Add(ConfigurationTableValue configValue)
        {
            SqlCommand command = new SqlCommand(@"Insert into ConfigurationTableValues(ConfigId,ConfigValue,CreatedById,CreatedOn) 
                                                Values(@ConfigId,@ConfigValue,@CreatedById,@CreatedOn)");
            command.Parameters.AddWithValue("@ConfigId", configValue.ConfigId);
            command.Parameters.AddWithValue("@ConfigValue", configValue.ConfigValue);
            this.SetCreatedDetails(command);
            try
            {
                object ret = _dataAccess.ExecuteScalar(command);
                if (ret == null) return false;
                configValue.SetId(ret);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal bool Update(ConfigurationTableValue configValue)
        {
            SqlCommand command = new SqlCommand(@"Update ConfigurationTableValues SET ConfigValue=@ConfigValue Where Id=@Id");
            command.Parameters.AddWithValue("@Id", configValue.Id);
            command.Parameters.AddWithValue("@ConfigValue", configValue.ConfigValue);
            this.SetUpdatedDetails(command);
            try
            {
                _dataAccess.ExecuteNonQuery(command);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _dataAccess.DBDisconnect();
            }
        }
        internal ConfigurationTableValue GetByValueAndConfigId(string value, int configId)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM ConfigurationTableValues WHERE ConfigId=@ConfigId AND ConfigValue=@Value");
            command.Parameters.AddWithValue("@ConfigId", configId);
            command.Parameters.AddWithValue("@Value", value);
            return this.GetEntity(command);
        }
        
    }
}
