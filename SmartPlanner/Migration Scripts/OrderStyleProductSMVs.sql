INSERT INTO OrderStyleProductSMVs( OrderInquiryId, SplitKey, OrderId, StyleId, ProductId, ProductionSMV)
select 0,o.SplitKey, o.Id,o.StyleId, sp.ProductId,sp.ProductionSMV 
from OrderDetails o with(nolock)
inner join StyleProducts sp WITH(NOLOCK) on o.StyleId = sp.StyleId 
left join OrderStyleProductSMVs sm WITH(NOLOCK) ON o.Id = sm.OrderId and sp.ProductId = sm.ProductId and o.StyleId = sm.StyleId
where sm.id is null

INSERT INTO OrderStyleProductSMVs( OrderInquiryId, SplitKey, OrderId, StyleId, ProductId, ProductionSMV)
select i.id,os.Id,0,sp.StyleId , sp.ProductId,sp.ProductionSMV
from OrderInquiries i WITH(NOLOCK)
inner join orderinquirysplits os WITH(NOLOCK) ON i.Id = os.OrderInquiryId
INNER JOIN styleProducts sp WITH(NOLOCK) ON i.StyleId = sp.StyleId
left join OrderStyleProductSMVs sm WITH(NOLOCK) ON i.Id = sm.OrderInquiryId and sp.ProductId = sm.ProductId and sp.StyleId = sm.StyleId
where orderInquiryStatusId<>1 and sm.id is null

