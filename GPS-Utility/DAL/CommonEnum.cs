﻿public enum enSqlExceptions
{
    PrimaryKeyCons = 2627,
    CannotOpenDb = 4060,
    CannotConnectToServer = -1
}

public enum enFormMode
{
    New = 1,
    Edit = 2,
    Duplicate = 3,
    Freezed = 4,
}
