﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Gps
{
    public class SQLDataAccess
    {
        private SqlConnection _sqlConnection;
        private SqlConnectionStringBuilder _sqlConnectionStrBuilder;

        #region Constructors

        public SQLDataAccess()
        {
            try
            {
                _sqlConnection = new SqlConnection();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(sqlEx.Message);
            }
        }

        #endregion

        private void CreateConnectionString()
        {
            //building the connection string
            SqlConnectionStringBuilder _sqlConnectionStrBuilder = new SqlConnectionStringBuilder();

            _sqlConnectionStrBuilder.DataSource = ConfigurationManager.AppSettings["ServerName"];

            _sqlConnectionStrBuilder.IntegratedSecurity = (ConfigurationManager.AppSettings["integratedsecurity"].Trim() == "false") ? false : true;
            _sqlConnectionStrBuilder.UserID = ConfigurationManager.AppSettings["UserId"];
            _sqlConnectionStrBuilder.Password = ConfigurationManager.AppSettings["Password"];
            _sqlConnectionStrBuilder.InitialCatalog = ConfigurationManager.AppSettings["ProjectDatabase"];

            _sqlConnection.ConnectionString = _sqlConnectionStrBuilder.ConnectionString;
        }

        public void Connect()
        {
            CreateConnectionString();
            try
            {
                _sqlConnection.Open();
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
        }

        public void Disconnect()
        {
            if (_sqlConnection.State == ConnectionState.Open)
                _sqlConnection.Close();

            _sqlConnection.Dispose();
        }


        public SqlDataReader GetDataReader(SqlCommand sqlCommand)
        {
            //Input validation
            if (sqlCommand == null)
                throw new ArgumentNullException("SqlCommand", "SqlCommand reference cannot be null.");


            if (_sqlConnection.State == ConnectionState.Closed)
            {
                Connect();
            }

            sqlCommand.Connection = _sqlConnection;

            try
            {

                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader == null)
                    throw new ArgumentNullException("DataReader", "Sql Data Reader object cannot be null.");

                return reader;
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            


        }


        public bool SqlDelete(SqlCommand sqlCommand)
        {
            //Input validation
            if (sqlCommand == null)
                throw new ArgumentNullException("SqlCommand", "SqlCommand reference cannot be null.");

            bool isConnectionOpened = false;
            bool isSuccess = false;

            if (_sqlConnection.State == ConnectionState.Closed)
            {
                Connect();
                isConnectionOpened = true;
            }

            sqlCommand.Connection = _sqlConnection;

            try
            {
                // Get data reader
                int nRec = sqlCommand.ExecuteNonQuery();
                isSuccess = true;
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                //Close connection
                if (isConnectionOpened)
                    Disconnect();
            }
            return isSuccess;
        }

        public object ExecuteScalar(SqlCommand sqlCommand)
        {
            object idObject;

            //Input validation
            if (sqlCommand == null)
                throw new ArgumentNullException("SqlCommand", "SqlCommand reference cannot be null.");

            bool isConnectionOpened = false;

            if (_sqlConnection.State == ConnectionState.Closed)
            {
                Connect();
                isConnectionOpened = true;
            }

            sqlCommand.Connection = _sqlConnection;

            try
            {
                // Get data reader
                idObject = sqlCommand.ExecuteScalar();
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                //Close connection
                if (isConnectionOpened)
                    Disconnect();
            }
            return idObject;
        }
    }
}
