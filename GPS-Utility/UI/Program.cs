﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;
using System.IO;

namespace Gps
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Set the product defaults before run the system
            Utility.SetProductDefaults();

            Application.Run(new gpsClientMain());


        }
    }
}
