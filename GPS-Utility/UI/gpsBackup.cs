﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gps
{
    public partial class gpsBackup : Form
    {
        FolderBrowserDialog _fldBackupPath;

        BackupServices _backupServices;
        DefaultServices _defaultService;

        string _msgCaption = "System Backup";

        public gpsBackup()
        {
            InitializeComponent();
        }

        #region Form Events
        
        private void SmBackup_Load(object sender, EventArgs e)
        {
            _fldBackupPath = new FolderBrowserDialog();
            _backupServices = new BackupServices();
            _defaultService = new DefaultServices();

            txtBackupPath.Text = _defaultService.GetDefault().BackupPath;
            _fldBackupPath.SelectedPath = txtBackupPath.Text;
        }

        #endregion

        #region Button Events

        private void btnBackupPath_Click(object sender, EventArgs e)
        {
            
            if (_fldBackupPath.ShowDialog() == DialogResult.OK)
                txtBackupPath.Text = _fldBackupPath.SelectedPath;
            else
                txtBackupPath.Text = string.Empty;
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_fldBackupPath.SelectedPath == string.Empty)
            {
                Utility.ShowInformationMsg("Please select the location", _msgCaption);
                return;
            }

            bool isSuccess = false;

            this.Cursor = Cursors.WaitCursor;

            try
            {
                isSuccess = _backupServices.BackupData(_fldBackupPath.SelectedPath);
            }
            catch
            {
                isSuccess = false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


            if (!isSuccess)
            {
                Utility.ShowInformationMsg("Unable to backup. Please try again.", _msgCaption);
                return;
            }

            Utility.ShowInformationMsg("Backup process completed successfully.", _msgCaption);
            this.Close();

        }


        #endregion

        private void SmBackup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
                return;
            }
        }


    }
}
