﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gps
{
    
    public partial class gpsClientMain : Form
    {
        
        public gpsClientMain()
        {
            InitializeComponent();
        }

        private void smLogin_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
           
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void smLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
                this.Close();

            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
                return;
            }
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            gpsActivate frmActivate = new gpsActivate();
            frmActivate.ShowDialog();

            //GlobalVariables.IsActivate = Utility.IsSystemActivate();

            //if (GlobalVariables.IsActivate.Equals(0))
            //{
            //    btnActivate.Visible = true;
            //}
            //else
            //{
            //    btnActivate.Visible = false;
            //}

        }
    }
}
