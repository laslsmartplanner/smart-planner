﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Gps
{
    public partial class gpsActivate : Form
    {

        public string CancelReason = string.Empty;
        public bool YesSelected = false;

        private string _moduleName = string.Empty;
        private string _form = string.Empty;
        DefaultServices _defaultService = new DefaultServices();

        public gpsActivate()
        {
            InitializeComponent();
        }

        private void smActivate_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;

            txtSystemKey.Text = Utility.GetSystemKey();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            CancelReason = string.Empty;
            YesSelected = false;
            this.Close();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if(!Utility.IsActivationKeyValid(txtActivationKey.Text))
            {
                Utility.ShowInformationMsg("ඔබ විසින් ඇතුලත් කල සක්‍රීය කිරීමේ යතුර වලංගු නොවේ. කරුණාකර නිවැරදි යතුර ඇතුලත් කරන්න.", this.Text);
                return;
            }

            _defaultService.UpdateActivatoinCode(txtActivationKey.Text);

            Utility.ShowInformationMsg("ඔබ විසින් සාර්ථකව මෘදුකාංගය සක්‍රීය කර ඇත.", this.Text);

            YesSelected = true;

            this.Close();
        }

        private void smCancelReason_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
                return;
            }
        }

    }
}
