﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Gps
{
    class DefaultDA
    {
        private static SQLDataAccess _dataAccess = new SQLDataAccess();

        #region Create Customer
        internal static List<Defaults> CreateDefault(SqlCommand sqlCommand)
        {
            //Validating parameters
            if (sqlCommand == null)
                return null;

            List<Defaults> defaultsList = null;
            SqlDataReader reader = null;

            try
            {
                reader = _dataAccess.GetDataReader(sqlCommand);

                defaultsList = Defaults.CreateDefaultList(reader);
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                //if reader created
                if (reader != null)
                    reader.Close();

                _dataAccess.Disconnect();
            }

            return defaultsList;
        }

        #endregion

        #region Get Defaults
        internal static Defaults GetDefaults()
        {
            Defaults _defaults = new Defaults();

            string commandString = "SELECT * FROM fDefault";

            SqlCommand sqlCommand = new SqlCommand();

            sqlCommand.CommandText = commandString;

           

            SqlDataReader reader = null;

            try
            {
                reader = _dataAccess.GetDataReader(sqlCommand);
                // 
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }


            while (reader.Read())
            {
                _defaults.CompName = Convert.ToString(reader["CompName"]);
                _defaults.Address1 = Convert.ToString(reader["Address1"]);
                _defaults.Address2 = Convert.ToString(reader["Address2"]);
                _defaults.Tel1 = Convert.ToString(reader["Tel1"]);
                _defaults.Tel2 = Convert.ToString(reader["Tel2"]);
                _defaults.Hunt1 = Convert.ToString(reader["Hunt1"]);
                _defaults.Hunt2 = Convert.ToString(reader["Hunt2"]);
                _defaults.Email1 = Convert.ToString(reader["Email1"]);
                _defaults.Email2 = Convert.ToString(reader["Email2"]);
                _defaults.Web = Convert.ToString(reader["Web"]);
                _defaults.LogoPath = Convert.ToString(reader["LogoPath"]);
                _defaults.Remarks = Convert.ToString(reader["Remarks"]);

                _defaults.ApprovedBy = Convert.ToString(reader["ApprovedBy"]);
                _defaults.PoPrefix = Convert.ToString(reader["PoPrefix"]);
                _defaults.InvPrefix = Convert.ToString(reader["InvPrefix"]);
                _defaults.QuotPrefix = Convert.ToString(reader["QuotPrefix"]);
                _defaults.DelNotePrefix = Convert.ToString(reader["DelNotePrefix"]);
                _defaults.PayVouPrefix = Convert.ToString(reader["PayVouPrefix"]);
                _defaults.QRPrefix = Convert.ToString(reader["QRPrefix"]);

                _defaults.ExRate = Convert.ToDecimal(reader["ExRate"]);
                _defaults.CreditPeriod = Convert.ToDecimal(reader["CreditPeriod"]);
                _defaults.GrnPrefix = Convert.ToString(reader["GrnPrefix"]);
                _defaults.SrnPrefix = Convert.ToString(reader["SrnPrefix"]);
                _defaults.CNPrefix = Convert.ToString(reader["CNPrefix"]);
                _defaults.DBNPrefix = Convert.ToString(reader["DBNPrefix"]);
                _defaults.BackupPath = Convert.ToString(reader["BackupPath"]);
                _defaults.PrintHeader = Convert.ToInt32(reader["PrintHeader"]);

                _defaults.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                _defaults.EditedBy = Convert.ToString(reader["EditedBy"]);
                _defaults.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
                _defaults.EditedOn = Convert.ToDateTime(reader["EditedOn"]);
                _defaults.StartDate = Convert.ToDateTime(reader["StartDate"]);
                _defaults.VATLKR = Convert.ToDecimal(reader["VATLKR"]);
               
                
                
            }

            _dataAccess.Disconnect();

            return _defaults;
        }

        #endregion

        #region List Defaults
        internal static List<Defaults> ListDefaults()
        {
            List<Defaults> defaultsList = new List<Defaults>();

            string commandString = "SELECT * FROM fDefault";
            SqlCommand sqlCommand = new SqlCommand();

            sqlCommand.CommandText = commandString;

            try
            {
                defaultsList = CreateDefault(sqlCommand);
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return defaultsList;
        }

        #endregion

        #region Delete defaults
        internal static bool DeleteDefaults(string compName)
        {
            if (compName == null)
                return false;

            string commandString = "Delete From fDefault WHERE CompName=@CompName";

            bool isSuccess = false;
            SqlCommand sqlCommand = new SqlCommand(commandString);

            sqlCommand.Parameters.AddWithValue("@CompName", compName);

            try
            {
                _dataAccess.Connect();

                isSuccess = _dataAccess.SqlDelete(sqlCommand);

            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _dataAccess.Disconnect();
            }

            return isSuccess;
        }

        #endregion

        #region Update Default
        internal static bool UpdateDefault(Defaults defaults, enFormMode formMode)
        {
            bool isSuccess = false;
            string commandString;
            Object idObject = null;

            DataRow defaultDetails = defaults.ExtractDefaultComponent();

            if (formMode == enFormMode.New)

                commandString = "INSERT INTO fDefault " +
                      "(CompName,Address1,Address2,Tel1,Tel2,Hunt1,Hunt2,Email1,Email2,Web,LogoPath,Remarks,ApprovedBy,PoPrefix,InvPrefix,QuotPrefix,DelNotePrefix,PayVouPrefix,CreditPeriod,ExRate, GrnPrefix,QRPrefix,SrnPrefix,CNPrefix,DBNPrefix,BackupPath, PrintHeader,CreatedBy,EditedBy,CreatedOn,EditedOn,StartDate,VATLKR) Values " +
                      "(@CompName,@Address1,@Address2,@Tel1,@Tel2,@Hunt1,@Hunt2,@Email1,@Email2,@Web,@LogoPath,@Remarks,@ApprovedBy,@PoPrefix,@InvPrefix,@QuotPrefix,@DelNotePrefix,@PayVouPrefix,@CreditPeriod,@ExRate,@GrnPrefix,@QRPrefix, @SrnPrefix,@CNPrefix,@DBNPrefix, @BackupPath, @PrintHeader,@CreatedBy,@EditedBy,@CreatedOn,@EditedOn,@StartDate,@VATLKR) ; select SCOPE_IDENTITY()";

            else
                commandString = "UPDATE fDefault " +
                            "SET CompName = @CompName, Address1=@Address1,Address2=@Address2, Tel1=@Tel1,Tel2=@Tel2,Hunt1=@Hunt1,Hunt2=@Hunt2,Email1=@Email1,Email2=@Email2,Web=@Web,LogoPath=@LogoPath,Remarks=@Remarks,ApprovedBy=@ApprovedBy,PoPrefix=@PoPrefix,InvPrefix=@InvPrefix,QuotPrefix=@QuotPrefix,DelNotePrefix=@DelNotePrefix,PayVouPrefix=@PayVouPrefix,CreditPeriod = @CreditPeriod, ExRate=@ExRate, GrnPrefix =@GrnPrefix,QRPrefix=@QRPrefix, SrnPrefix =@SrnPrefix,CNPrefix=@CNPrefix,DBNPrefix=@DBNPrefix, BackupPath=@BackupPath, PrintHeader=@PrintHeader,EditedBy=@EditedBy,EditedOn=@EditedOn,StartDate=@StartDate,VATLKR=@VATLKR WHERE CompName = @CompName";

            SqlCommand sqlCommand = new SqlCommand(commandString);

            if (formMode == enFormMode.New)
            {
	            sqlCommand.Parameters.AddWithValue("@CreatedBy", 1);
	            sqlCommand.Parameters.AddWithValue("@EditedBy", 1);

	            sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
	            sqlCommand.Parameters.AddWithValue("@EditedOn", DateTime.Now);
            }
            else
            {
	            sqlCommand.Parameters.AddWithValue("@EditedBy", 1);
	            sqlCommand.Parameters.AddWithValue("@EditedOn", DateTime.Now);
            }

            sqlCommand.Parameters.AddWithValue("@CompName", defaultDetails["CompName"]);
            sqlCommand.Parameters.AddWithValue("@Address1", defaultDetails["Address1"]);
            sqlCommand.Parameters.AddWithValue("@Address2", defaultDetails["Address2"]);
            sqlCommand.Parameters.AddWithValue("@Tel1", defaultDetails["Tel1"]);
            sqlCommand.Parameters.AddWithValue("@Tel2", defaultDetails["Tel2"]);
            sqlCommand.Parameters.AddWithValue("@Hunt1", defaultDetails["Hunt1"]);
            sqlCommand.Parameters.AddWithValue("@Hunt2", defaultDetails["Hunt2"]);
            sqlCommand.Parameters.AddWithValue("@Email1", defaultDetails["Email1"]);
            sqlCommand.Parameters.AddWithValue("@Email2", defaultDetails["Email2"]);
            sqlCommand.Parameters.AddWithValue("@Web", defaultDetails["Web"]);
            sqlCommand.Parameters.AddWithValue("@LogoPath", defaultDetails["LogoPath"]);
            sqlCommand.Parameters.AddWithValue("@Remarks", defaultDetails["Remarks"]);

            sqlCommand.Parameters.AddWithValue("@ApprovedBy", defaultDetails["ApprovedBy"]);
            sqlCommand.Parameters.AddWithValue("@PoPrefix", defaultDetails["PoPrefix"]);
            sqlCommand.Parameters.AddWithValue("@InvPrefix", defaultDetails["InvPrefix"]);
            sqlCommand.Parameters.AddWithValue("@QuotPrefix", defaultDetails["QuotPrefix"]);
            sqlCommand.Parameters.AddWithValue("@DelNotePrefix", defaultDetails["DelNotePrefix"]);
            sqlCommand.Parameters.AddWithValue("@PayVouPrefix", defaultDetails["PayVouPrefix"]);

            sqlCommand.Parameters.AddWithValue("@CreditPeriod", defaultDetails["CreditPeriod"]);
            sqlCommand.Parameters.AddWithValue("@ExRate", defaultDetails["ExRate"]);
            sqlCommand.Parameters.AddWithValue("@GrnPrefix", defaultDetails["GrnPrefix"]);
            sqlCommand.Parameters.AddWithValue("@QRPrefix", defaultDetails["QRPrefix"]);
            sqlCommand.Parameters.AddWithValue("@SrnPrefix", defaultDetails["SrnPrefix"]);
            sqlCommand.Parameters.AddWithValue("@CNPrefix", defaultDetails["CNPrefix"]);
            sqlCommand.Parameters.AddWithValue("@DBNPrefix", defaultDetails["DBNPrefix"]);

            sqlCommand.Parameters.AddWithValue("@BackupPath", defaultDetails["BackupPath"]);
            sqlCommand.Parameters.AddWithValue("@PrintHeader", defaultDetails["PrintHeader"]);
            sqlCommand.Parameters.AddWithValue("@StartDate", defaultDetails["StartDate"]);
            sqlCommand.Parameters.AddWithValue("@VATLKR", defaultDetails["VATLKR"]);

            try
            {
                idObject = _dataAccess.ExecuteScalar(sqlCommand);
                isSuccess = true;
            }
            catch (SqlException exception)
            {

                if (exception.Number == (int)enSqlExceptions.PrimaryKeyCons)
                    throw new ApplicationException("Company Name : '" + defaultDetails["CompName"] + "' already exists.");
                else if (exception.Number == (int)enSqlExceptions.CannotOpenDb)
                    throw new ApplicationException("Failed to connect with the database.");
                else
                    throw new ApplicationException(exception.Message);
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }

            return isSuccess;
        }

        #endregion

        #region Is Default Exists

        internal static bool IsDefaultExists(string compName)
        {
            bool isSuccess = false;

            string commandString = "SELECT count(CompName) FROM fCustomer where CompName = @CompName";

            SqlCommand sqlCommand = new SqlCommand();

            sqlCommand.CommandText = commandString;

            sqlCommand.Parameters.AddWithValue("@CompName", compName);

            object defaultCount = null;

            try
            {
                defaultCount = _dataAccess.ExecuteScalar(sqlCommand);
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

            if ((int)defaultCount > 0)
                isSuccess = true;

            _dataAccess.Disconnect();
            return isSuccess;
        }

        #endregion

        internal static string GetActivationCode()
        {
            string _defaultsProductKey = string.Empty;

            string commandString = "SELECT ProductKey FROM fDefault";

            SqlCommand sqlCommand = new SqlCommand();

            sqlCommand.CommandText = commandString;
                       
            SqlDataReader reader = null;

            try
            {
                reader = _dataAccess.GetDataReader(sqlCommand);
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }


            while (reader.Read())
            {
                _defaultsProductKey = Convert.ToString(reader["ProductKey"]);

            }

            _dataAccess.Disconnect();

            return _defaultsProductKey;
        }

        internal static bool UpdateActivatoinCode(string activationCode)
        {
            bool isSuccess = false;
            string commandString;
            Object idObject = null;

            commandString = "UPDATE fDefault " +
                            "SET ProductKey = @ProductKey,EditedOn=@EditedOn";

            SqlCommand sqlCommand = new SqlCommand(commandString);

            sqlCommand.Parameters.AddWithValue("@EditedOn", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@ProductKey", activationCode);

            try
            {
                idObject = _dataAccess.ExecuteScalar(sqlCommand);
                isSuccess = true;
            }
            catch (SqlException exception)
            {
                if (exception.Number == (int)enSqlExceptions.CannotOpenDb)
                    throw new ApplicationException("Failed to connect with the database.");
                else
                    throw new ApplicationException(exception.Message);
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }

            return isSuccess;
        }
    }
}
