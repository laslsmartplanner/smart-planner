﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Gps
{
    public class Defaults
    {
        #region Private Variables

        private uint _intKey = 0;
        private string _compName = string.Empty;

        private string _address1 = string.Empty;
        private string _address2 = string.Empty;

        private string _tel1 = string.Empty;
        private string _tel2 = string.Empty;
        private string _hunt1 = string.Empty;
        private string _hunt2 = string.Empty;
        private string _email1 = string.Empty;
        private string _email2 = string.Empty;

        private string _web = string.Empty;
        private string _logoPath = string.Empty;
        
        private string _remarks = string.Empty;

        private string _approvedBy = string.Empty;

        private string _poPrefix = string.Empty;
        private string _invPrefix = string.Empty;
        private string _quotPrefix = string.Empty;
        private string _delNotePrefix = string.Empty;
        private string _payVouPrefix = string.Empty;
        private string _QRPrefix = string.Empty;

        private decimal _creditPeriod = 0;
        private decimal _exRate = 0;

        private string _grnPrefix = string.Empty;
        private string _srnPrefix = string.Empty;
        private string _cnPrefix = string.Empty;
        private string _dbnPrefix = string.Empty;

        private string _backupPath = string.Empty;
        private int _printHeader = 0;

        private string _CreatedBy = string.Empty;
        private string _EditedBy = string.Empty;
        private DateTime _CreatedOn = DateTime.Now;
        private DateTime _EditedOn = DateTime.Now;
        private DateTime _StartDate = DateTime.Now;

        private string _productKey = string.Empty;

        private decimal _VATLKR = 0;


        public decimal VATLKR
        {
            get { return _VATLKR; }
            set
            {
                _VATLKR = (value == null) ? 0 : value;
            }
        }

        public DateTime? StartDate
        {
            get { return _StartDate; }
            set
            {
                _StartDate = (DateTime)value;
            }
        }

        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                _CreatedBy = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string EditedBy
        {
            get { return _EditedBy; }
            set
            {
                _EditedBy = (value == null) ? string.Empty : value.Trim();
            }
        }

        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set
            {
                _CreatedOn = (DateTime)value;
            }
        }

        public DateTime? EditedOn
        {
            get { return _EditedOn; }
            set
            {
                _EditedOn = (DateTime)value;
            }
        }

        #endregion

        #region Default Properties

        public uint IntKey
        {
            get { return _intKey; }
        }

        public Decimal CreditPeriod
        {
            get { return _creditPeriod; }
            set
            {
                _creditPeriod = (value == null) ? 0 : value;
            }
        }

        public Decimal ExRate
        {
            get { return _exRate; }
            set
            {
                _exRate = (value == null) ? 0 : value;
            }
        }
        
        public string ApprovedBy
        {
            get { return _approvedBy; }
            set
            {
                _approvedBy = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string CNPrefix
        {
            get { return _cnPrefix; }
            set
            {
                _cnPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string DBNPrefix
        {
            get { return _dbnPrefix; }
            set
            {
                _dbnPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }
        public string QRPrefix
        {
            get { return _QRPrefix; }
            set
            {
                _QRPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string PoPrefix
        {
            get { return _poPrefix; }
            set
            {
                _poPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string InvPrefix
        {
            get { return _invPrefix; }
            set
            {
                _invPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string QuotPrefix
        {
            get { return _quotPrefix; }
            set
            {
                _quotPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string DelNotePrefix
        {
            get { return _delNotePrefix; }
            set
            {
                _delNotePrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string PayVouPrefix
        {
            get { return _payVouPrefix; }
            set
            {
                _payVouPrefix = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string CompName
        {
            get { return _compName; }
            set
            {
                _compName = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Address1
        {
            get { return _address1; }
            set
            {
                _address1 = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Address2
        {
            get { return _address2; }
            set
            {
                _address2 = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Tel1
        {
            get { return _tel1; }
            set
            {
                _tel1 = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Tel2
        {
            get { return _tel2; }
            set
            {
                _tel2 = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Hunt1
        {
            get { return _hunt1; }
            set
            {
                _hunt1 = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Hunt2
        {
            get { return _hunt2; }
            set
            {
                _hunt2 = (value == null) ? string.Empty : value.Trim();
            }
        }
        public string Email1
        {
            get { return _email1; }
            set
            {
                _email1 = (value == null) ? string.Empty : value.Trim();
            }
        }

        public string Email2
        {
            get { return _email2; }
            set
            {
                _email2 = (value == null) ? string.Empty : value.Trim();
            }
        }
        public string Remarks
        {
            get { return _remarks; }
            set
            {
                _remarks = (value == null) ? string.Empty : value.Trim();
            }
        }
        public string Web
        {
            get { return _web; }
            set
            {
                _web = (value == null) ? string.Empty : value.Trim();
            }
        }

         public string LogoPath
        {
            get { return _logoPath; }
            set
            {
                _logoPath = (value == null) ? string.Empty : value.Trim();
            }
        }

         public string GrnPrefix
         {
             get { return _grnPrefix; }
             set
             {
                 _grnPrefix = (value == null) ? string.Empty : value.Trim();
             }
         }

         public string SrnPrefix
         {
             get { return _srnPrefix; }
             set
             {
                 _srnPrefix = (value == null) ? string.Empty : value.Trim();
             }
         }

         public string BackupPath
         {
             get { return _backupPath; }
             set
             {
                 _backupPath = (value == null) ? string.Empty : value.Trim();
             }
         }

         public int PrintHeader
         {
             get { return _printHeader; }
             set
             {
                 _printHeader = (value == null) ? 0 : value;
             }
         }

        public string ProductKey
        {
            get { return _productKey; }
            set
            {
                _productKey = (value == null) ? string.Empty : value.Trim();
            }
        }
        #endregion

        #region Constructors

        public Defaults()
        { }

        public Defaults(string compName)
        {
            CompName = compName;
        }

        #endregion

        #region Create Default List

        internal static List<Defaults> CreateDefaultList(SqlDataReader dataReader)
        {
            if (dataReader == null)
                throw new ArgumentNullException("DataReader", "Sql Data Reader object cannot be null.");

            List<Defaults> defaultsList = new List<Defaults>();
            Defaults defaults = null;

            while (dataReader.Read())
            {
                defaults = new Defaults();

                defaults._intKey = Convert.ToUInt32(dataReader["Int_Key"]);
                defaults.CompName = Convert.ToString(dataReader["CompName"]);
                defaults.Address1 = Convert.ToString(dataReader["Address1"]);
                defaults.Address2 = Convert.ToString(dataReader["Address2"]);
                defaults.Tel1 = Convert.ToString(dataReader["Tel1"]);
                defaults.Tel2 = Convert.ToString(dataReader["Tel2"]);
                defaults.Hunt1 = Convert.ToString(dataReader["Hunt1"]);
                defaults.Hunt2 = Convert.ToString(dataReader["Hunt2"]);
                defaults.Email1 = Convert.ToString(dataReader["Email1"]);
                defaults.Email2 = Convert.ToString(dataReader["Email2"]);
                defaults.Web = Convert.ToString(dataReader["Web"]);
                defaults.LogoPath= Convert.ToString(dataReader["LogoPath"]);
                defaults.Remarks = Convert.ToString(dataReader["Remarks"]);
                defaults.ApprovedBy = Convert.ToString(dataReader["ApprovedBy"]);
                defaults.PoPrefix = Convert.ToString(dataReader["PoPrefix"]);
                defaults.InvPrefix = Convert.ToString(dataReader["InvPrefix"]);
                defaults.QuotPrefix = Convert.ToString(dataReader["QuotPrefix"]);
                defaults.DelNotePrefix = Convert.ToString(dataReader["DelNotePrefix"]);
                defaults.PayVouPrefix = Convert.ToString(dataReader["PayVouPrefix"]);
                defaults.QRPrefix = Convert.ToString(dataReader["QRPrefix"]);

                defaults.CreditPeriod = Convert.ToDecimal(dataReader["CreditPeriod"]);
                defaults.ExRate = Convert.ToDecimal(dataReader["ExRate"]);

                defaults.GrnPrefix = Convert.ToString(dataReader["GrnPrefix"]);
                defaults.SrnPrefix = Convert.ToString(dataReader["SrnPrefix"]);

                defaults.CNPrefix = Convert.ToString(dataReader["CNPrefix"]);
                defaults.DBNPrefix = Convert.ToString(dataReader["DBNPrefix"]);

                defaults.BackupPath = Convert.ToString(dataReader["BackupPath"]);
                defaults.PrintHeader = Convert.ToInt32(dataReader["PrintHeader"]);

                defaults.CreatedBy = Convert.ToString(dataReader["CreatedBy"]);
                defaults.EditedBy = Convert.ToString(dataReader["EditedBy"]);
                defaults.CreatedOn = Convert.ToDateTime(dataReader["CreatedOn"]);
                defaults.EditedOn = Convert.ToDateTime(dataReader["EditedOn"]);
                defaults.StartDate = Convert.ToDateTime(dataReader["StartDate"]);
                defaults.VATLKR = Convert.ToDecimal(dataReader["VATLKR"]);

                defaults.ProductKey = Convert.ToString(dataReader["ProductKey"]);



                defaultsList.Add(defaults);

            }

            return defaultsList;
        }

        #endregion

        #region Extract Default Component
        internal DataRow ExtractDefaultComponent()
        {
            DataTable defaultTable = new DataTable();

            defaultTable.Columns.Add("IntKey", typeof(uint));
            defaultTable.Columns.Add("CompName", typeof(string));
            defaultTable.Columns.Add("Address1", typeof(string));
            defaultTable.Columns.Add("Address2", typeof(string));
            defaultTable.Columns.Add("Tel1", typeof(string));
            defaultTable.Columns.Add("Tel2", typeof(string));
            defaultTable.Columns.Add("Hunt1", typeof(string));
            defaultTable.Columns.Add("Hunt2", typeof(string));
            defaultTable.Columns.Add("Email1", typeof(string));
            defaultTable.Columns.Add("Email2", typeof(string));
            defaultTable.Columns.Add("Web", typeof(string));
            defaultTable.Columns.Add("LogoPath", typeof(string));
            defaultTable.Columns.Add("Remarks", typeof(string));
            defaultTable.Columns.Add("ApprovedBy", typeof(string));
            defaultTable.Columns.Add("PoPrefix", typeof(string));
            defaultTable.Columns.Add("InvPrefix", typeof(string));
            defaultTable.Columns.Add("QuotPrefix", typeof(string));
            defaultTable.Columns.Add("DelNotePrefix", typeof(string));
            defaultTable.Columns.Add("PayVouPrefix", typeof(string));
            defaultTable.Columns.Add("CreditPeriod", typeof(decimal));
            defaultTable.Columns.Add("ExRate", typeof(decimal));
            defaultTable.Columns.Add("GrnPrefix", typeof(string));
            defaultTable.Columns.Add("QRPrefix", typeof(string));
            defaultTable.Columns.Add("SrnPrefix", typeof(string));
            defaultTable.Columns.Add("CNPrefix", typeof(string));
            defaultTable.Columns.Add("DBNPrefix", typeof(string));

            defaultTable.Columns.Add("BackupPath", typeof(string));
            defaultTable.Columns.Add("PrintHeader", typeof(int));

            defaultTable.Columns.Add("CreatedBy", typeof(string));
            defaultTable.Columns.Add("EditedBy", typeof(string));
            defaultTable.Columns.Add("CreatedOn", typeof(DateTime));
            defaultTable.Columns.Add("EditedOn", typeof(DateTime));
            defaultTable.Columns.Add("StartDate", typeof(DateTime));
            defaultTable.Columns.Add("VATLKR", typeof(decimal));
            defaultTable.Columns.Add("ProductKey", typeof(string));

            DataRow dataRow = defaultTable.NewRow();

            dataRow.SetField<uint>("IntKey", _intKey);
            dataRow.SetField("CompName", _compName);
            dataRow.SetField("Address1", _address1);
            dataRow.SetField("Address2", _address2);
            dataRow.SetField("Tel1", _tel1);
            dataRow.SetField("Tel2", _tel2);
            dataRow.SetField("Hunt1", _hunt1);
            dataRow.SetField("Hunt2", _hunt2);
            dataRow.SetField("Email1", _email1);
            dataRow.SetField("Email2", _email2);
            dataRow.SetField("Web", _web);
            dataRow.SetField("LogoPath", _logoPath);
            dataRow.SetField("Remarks", _remarks);
            dataRow.SetField("ApprovedBy", _approvedBy);
            dataRow.SetField("PoPrefix", _poPrefix);
            dataRow.SetField("InvPrefix", _invPrefix);
            dataRow.SetField("QuotPrefix", _quotPrefix);
            dataRow.SetField("DelNotePrefix", _delNotePrefix);
            dataRow.SetField("PayVouPrefix", _payVouPrefix);
            dataRow.SetField("CreditPeriod", _creditPeriod);
            dataRow.SetField("ExRate", _exRate);
            dataRow.SetField("GrnPrefix", _grnPrefix);
            dataRow.SetField("QRPrefix", _QRPrefix);
            dataRow.SetField("SrnPrefix", _srnPrefix);
            dataRow.SetField("CNPrefix", _cnPrefix);
            dataRow.SetField("DBNPrefix", _dbnPrefix);

            dataRow.SetField("BackupPath", _backupPath);
            dataRow.SetField("PrintHeader", _printHeader);

            dataRow.SetField("CreatedBy", _CreatedBy);
            dataRow.SetField("EditedBy", _EditedBy);
            dataRow.SetField<DateTime>("CreatedOn", _CreatedOn);
            dataRow.SetField<DateTime>("EditedOn", _EditedOn);
            dataRow.SetField<DateTime>("StartDate", _StartDate);
            dataRow.SetField("VATLKR", _VATLKR);
            dataRow.SetField("ProductKey", _productKey);



            return dataRow;
        }

        #endregion
    }
}
