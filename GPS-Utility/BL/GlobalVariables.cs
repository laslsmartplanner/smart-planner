﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gps
{
    public static class GlobalVariables
    {
        public static string RootDirectory { get; set; }
        public static string ProductId { get; set; }
        public static string ErrorLogFile { get; set; }
        public static string ReportDirectory { get; set; }
        public static string HelpDirectory { get; set; }
        

        public static string TempDirectory { get; set; }
        public static int IsActivate { get; set; }

        //public static Company LoggedInCompany { get; set; }
    }
}
