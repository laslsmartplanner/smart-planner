﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gps
{
    public class DefaultServices
    {
        public List<Defaults> ListDefault()
        {
            return DefaultDA.ListDefaults();
        }

        public Defaults GetDefault()
        {
            return DefaultDA.GetDefaults();
        }

        public bool DeleteDefault(string compName)
        {
            bool isSuccess = false;

            isSuccess = DefaultDA.DeleteDefaults(compName);

            return isSuccess;
        }

        public bool UpdateDefaults(Defaults defaults, enFormMode formMode)
        {
            bool isSuccess = false;
            isSuccess = DefaultDA.UpdateDefault(defaults, formMode);
            return isSuccess;
        }

        public bool IsDefaultExists(string compName)
        {
            return DefaultDA.IsDefaultExists(compName);
        }

        public string GetActivationCode()
        {
            return DefaultDA.GetActivationCode();
        }

        public bool UpdateActivatoinCode(string activationCode)
        {
            bool isSuccess = false;
            isSuccess = DefaultDA.UpdateActivatoinCode(activationCode);
            return isSuccess;
        }


    }
}
