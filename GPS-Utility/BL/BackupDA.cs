﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Gps
{
    class BackupDA
    {
        private static SQLDataAccess _dataAccess = new SQLDataAccess();

        internal static bool BackupData(string folderPath)
        {
            bool isSuccess = false;
            Object idObject = null;

            if (folderPath == string.Empty)
                return isSuccess;

            string commandString = "BACKUP DATABASE gps_app TO DISK= @Disk";

            SqlCommand sqlCommand = new SqlCommand(commandString);

            string backupDate = string.Format("{0}{1}{2}", DateTime.Today.Year.ToString(), 
                DateTime.Today.Month.ToString(), DateTime.Today.Day.ToString());
            
            folderPath = Path.Combine(folderPath + @"\" + backupDate + ".BAK");

            sqlCommand.Parameters.AddWithValue("@Disk", folderPath);

            try
            {
                idObject = _dataAccess.ExecuteScalar(sqlCommand);
                isSuccess = true;
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return isSuccess;
        }
    }
}
