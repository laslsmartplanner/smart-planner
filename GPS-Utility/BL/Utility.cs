﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Security.Cryptography;

namespace Gps
{
    public class Utility
    {
        #region Set Product Defaults

        public static void SetProductDefaults()
        {
            GlobalVariables.RootDirectory = Path.Combine(Directory.GetParent(Application.ExecutablePath) + @"\");
            GlobalVariables.ProductId = "GPS Utility";
            GlobalVariables.ErrorLogFile = Path.Combine(GlobalVariables.RootDirectory + @"\ERRORLOG\GpErrorLog.Log");
            GlobalVariables.ReportDirectory = Path.Combine(GlobalVariables.RootDirectory + @"\REPORTS\");
            GlobalVariables.HelpDirectory = Path.Combine(GlobalVariables.RootDirectory + @"HELP\");
            GlobalVariables.TempDirectory = Path.Combine(GlobalVariables.RootDirectory + @"\TEMP\");
            //GlobalVariables.IsActivate = IsSystemActivate();
        }

        #endregion

        #region File writer

        public static bool FileWriter(string fileName, string content)
        {
            bool isSuccess = false;

            if (content == null || content == string.Empty)
                return false;

            fileName = fileName.Trim();

            // Checking the existence file            
            if (File.Exists(fileName))
            {
                try
                {
                    //Open file and read the content
                    using (StreamWriter streamWriter = new StreamWriter(fileName,true))
                    {
                        streamWriter.WriteLine(content);
                        streamWriter.Close();
                        isSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    // Show an error message
                    Utility.ShowErrorMsg("Error occured while writing to the file: " + fileName,"Error");
                }

            }

            return isSuccess;
        }

        #endregion

        #region Display messages


        public static void ShowCriticalErrorMsg(string message, string caption)
        {
            MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowErrorMsg(string message, string caption)
        {
            MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static void ShowWarningMsg(string message, string caption)
        {
            MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void ShowInformationMsg(string message, string caption)
        {
            MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult ShowOKCancelMsg(string message, string caption)
        {
            return MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        }

        public static DialogResult ShowYesNoMsg(string message, string caption)
        {
            return MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        }

        public static DialogResult ShowYesNoCancelMsg(string message, string caption)
        {
            return MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3);
        }

        public static DialogResult ShowOKCancelWarningMsg(string message, string caption)
        {
            return MessageBox.Show(message, GlobalVariables.ProductId + " - " + caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
        }


        #endregion

        #region Create File

        public static bool CreateFile(string fullFilePath,uint deleteIfExists)
        { 

            bool isSuccess = false;

            try
            {
                //Check whether the file exists.
                if (File.Exists(fullFilePath))
                {
                    if (deleteIfExists > 0)
                        DeleteFile(fullFilePath);
                }

                FileStream fileStream = File.Create(fullFilePath);
                fileStream.Close();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                // Log the error
                Utility.LogError("Error occured while creating the file: " + fullFilePath + ex.Message,"CreateFile:Utility.cs");
            }
            return isSuccess;
        }

        #endregion

        #region Delete File

        public static bool DeleteFile(string fullPath)
        {
            try
            {
                //Check file exits
                if (File.Exists(fullPath))
                {
                    //Delete the file
                    File.Delete(fullPath);
                }
            }
            catch (IOException ioEx)
            {
                //Log error
                Utility.LogError("Error while deleting the file '" + fullPath + "'. :\n" + ioEx.Message,"DeleteFile:Utility.cs");
                return false;
            }

            return true;
         }

        #endregion

        #region Log Error

        public static bool LogError(string message, string program)
        {
            bool isSuccess = true;
            string textToLog;

            if (!File.Exists(GlobalVariables.ErrorLogFile))
                isSuccess =  CreateFile(GlobalVariables.ErrorLogFile,1);

            textToLog = "-------- Begin Error ----- \r\n\r\n";
            textToLog = string.Format("{0}{1}{2}{3}", textToLog, "Date Time: ", DateTime.Now.ToString(), "\r\n");
            textToLog = string.Format("{0}{1}{2}{3}", textToLog, "Details: ", message, "\r\n");
            textToLog = string.Format("{0}{1}{2}{3}", textToLog, "Program: ", program, "\r\n\r\n");
            textToLog = string.Format("{0}{1}", textToLog, "--------- End Error ------ \r\n\r\n");

            if (isSuccess)
                isSuccess = FileWriter(GlobalVariables.ErrorLogFile, textToLog);

            return isSuccess;
        }

        #endregion

        public static int GetCurrentYear()
        {
            int curentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;
            
            if(currentMonth< 4)
            { 
                curentYear = curentYear - 1;
            }

            return curentYear;
        }

        public static int IsSystemActivate()
        {
            int isActive = 0;

            string defaultsProductKey = DefaultDA.GetActivationCode();

            if(string.IsNullOrEmpty(defaultsProductKey))
            {
                return isActive;
            }

            string systemProductKey = string.Empty;

            string machineKey = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId());

            systemProductKey = GetHash(machineKey + "\nPROD >> " + "LASL_GPS_USABA_SMART");

            if (defaultsProductKey.Trim().Equals(systemProductKey))
                isActive = 1;

            return isActive;            
        }

        public static bool IsActivationKeyValid(string activationKey)
        {
            bool isActivationValid = false;

            if (string.IsNullOrEmpty(activationKey))
            {
                return isActivationValid;
            }

            string systemProductKey = string.Empty;

            string machineKey = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId());

            systemProductKey = GetHash(machineKey + "\nPROD >> " + "LASL_GPS_USABA_SMART");

            if (activationKey.Equals(systemProductKey))
                isActivationValid = true;

            return isActivationValid;
        }

        private static string GetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            return GetHexString(sec.ComputeHash(bt));
        }

        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte b = bt[i];
                int n, n1, n2;
                n = (int)b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char)(n2 - 10 + (int)'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char)(n1 - 10 + (int)'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
            }
            return s;
        }

        private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();

            foreach (System.Management.ManagementObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() == "True")
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            result = mo[wmiProperty].ToString();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return result;
        }

        private static  string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        private static string cpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = identifier("Win32_Processor", "UniqueId");

            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = identifier("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }
        
        //BIOS Identifier
        private static string biosId()
        {
            return identifier("Win32_BIOS", "Manufacturer")
            + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
            + identifier("Win32_BIOS", "IdentificationCode")
            + identifier("Win32_BIOS", "SerialNumber")
            + identifier("Win32_BIOS", "ReleaseDate")
            + identifier("Win32_BIOS", "Version");
        }

        //Motherboard ID
        private static string baseId()
        {
            return identifier("Win32_BaseBoard", "Model")
            + identifier("Win32_BaseBoard", "Manufacturer")
            + identifier("Win32_BaseBoard", "Name")
            + identifier("Win32_BaseBoard", "SerialNumber");
        }

        public static string GetSystemKey()
        {
            string systemProductKey = string.Empty;

            systemProductKey = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId());

            return systemProductKey;
        }

    }
}
